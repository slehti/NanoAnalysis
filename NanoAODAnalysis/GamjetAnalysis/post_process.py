#!/usr/bin/env python

import os,sys,re
import datetime
import ROOT

ROOT.gROOT.SetBatch(True)

basepath_re = re.compile("(?P<basepath>\S+/NanoAnalysis)/")
match = basepath_re.search(os.getcwd())
if match:
    sys.path.append(os.path.join(match.group("basepath"),"NanoAODAnalysis/Framework/python"))

from nanoanalysis.aux import execute
from nanoanalysis.tools.plotting import *

OUTPUTDIR = "figures"

root_re = re.compile("(?P<rootfile>([^/]*histograms.root))")

def usage():
    print()
    print("### Usage:  ",os.path.basename(sys.argv[0]),"<multicrab histograms>")
    print()

def writeMetadata(rootfile,path,dataset):
    if path != "":
        tdir = rootfile.Get(path)
        if not tdir:
            tdir = rootfile.mkdir(path)
        rootfile.cd(path)

    days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    now = datetime.datetime.now()
    m = "produced: %s %s-%s-%s %02d:%02d:%02d"%(days[now.weekday()],now.year,now.month,now.day,now.hour,now.minute,now.second)

    h_timestamp = ROOT.TNamed(m,"")
    h_timestamp.Write()

    if dataset.isData():
        h_lumi = ROOT.TH1D("lumi","",1,0,1)
        h_lumi.SetBinContent(1,dataset.getLumi())
        h_lumi.Write()

        h_energy = ROOT.TH1D("energy","",1,0,1)
        h_energy.SetBinContent(1,dataset.getEnergy())
        h_energy.Write()

        h_run = ROOT.TNamed("run",dataset.getRun())
        h_run.Write()

    h_year = ROOT.TNamed("year",dataset.getYear())
    h_year.Write()

    jec = dataset.getJEC()
    s_jec = ""
    for jec in dataset.getJEC()[1:]:
        if len(s_jec) > 0:
            s_jec += ';'
        s_jec += jec
    if len(s_jec) == 0:
        s_jec = "None"
    h_jec = ROOT.TNamed("JEC",s_jec)
    h_jec.Write()

    rootfile.cd()

def writeResponse(rootfile,histogram,newname):
    if not isinstance(histogram, ROOT.TH2D):
        return

    nbinsx = histogram.GetNbinsX()
    x     = []
    x_err = []
    y     = []
    y_err = []
    firstBin = 1
    for ibin in range(1,nbinsx):
        bincenter = histogram.GetXaxis().GetBinCenter(ibin)
        x.append(bincenter)
        x_err.append(0.0)
        firstBin = ibin
        yslice = histogram.ProjectionY("",firstBin,ibin)
        y.append(yslice.GetMean())
        y_err.append(yslice.GetMeanError())
    rootfile.cd()
    graph = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',y),array.array('d',x_err),array.array('d',y_err))
    if '/' in newname:
        path = os.path.dirname(newname)
        name = os.path.basename(newname)
        tdir = rootfile.Get(path)
        if not tdir:
            rootfile.mkdir(path)
        tdir = rootfile.Get(path)
        rootfile.cd(path)
        graph.Write(name)
    else:
        graph.Write(newname)
    rootfile.cd()

def writeHistogram(rootfile,histogram,newname):
    rootfile.cd()
    if '/' in newname:
        path = os.path.dirname(newname)
        name = os.path.basename(newname)
        tdir = rootfile.Get(path)
        if not tdir:
            tdir = rootfile.mkdir(path)
        rootfile.cd(path)
        histogram.Write(name)
    else:
        histogram.Write(newname)
    rootfile.cd()

def main():

    if len(sys.argv) == 1:
        usage()
        sys.exit()

    multicrabdirs = sys.argv[1:]
    for multicrabdir in multicrabdirs:
        if not os.path.exists(multicrabdir) or not os.path.isdir(multicrabdir):
            usage()
            sys.exit()
        else:
            print(multicrabdir)

    whitelist = []
    blacklist = []

    print("Getting datasets")
    datasets = getDatasetsMany(multicrabdirs,whitelist=whitelist,blacklist=blacklist)
    print("Datasets received")

    datasets = read(datasets)
    datasets = mergeExtDatasets(datasets)
    datasets = normalizeToLumi(datasets)

    runs = getRuns(datasets)
    #for run in runs:
    #    datasets = mergeDatasets("Data_%s"%run,run,datasets,keep=True)
    datasets = mergeDatasets("Data_MERGED",'_Run20\d\d\S_',datasets)
    datasets = mergeDatasets("MC","^(?!Data).*",datasets)
    Data_MERGED = getDatasetByName("Data_MERGED",datasets)
    Data_MERGED_name = "Run%s"%Data_MERGED.getRun()
    if Data_MERGED != None:
        datasets = renameDataset("Data_MERGED",Data_MERGED_name,datasets)
#    datasets = mergeDatasets("MC","^(?!Data).*",datasets)
    #printd(datasets)

    respselection = []
    respselection.append(re.compile("\S+?/eta_\d\d_\d\d/(?P<label>\S+)_db_gamjet_a100$"))
    respselection.append(re.compile("\S+?/eta_\d\d_\d\d/(?P<label>\S+)_mpf_gamjet_a100$"))
    respselection.append(re.compile("\S+?/eta_\d\d_\d\d/(?P<label>\S+)_mpf1_gamjet_a10"))
    respselection.append(re.compile("\S+?/eta_\d\d_\d\d/(?P<label>\S+)_mpfn_gamjet_a10"))
    respselection.append(re.compile("\S+?/eta_\d\d_\d\d/(?P<label>\S+)_mpfu_gamjet_a10"))

    fNAME = "jme_GammaplusJet_vX.root"
    fOUT = ROOT.TFile.Open(fNAME,"RECREATE")
    for dataset in datasets:
        prefix = "mc"
        if dataset.isData():
            prefix = "data"

        jec = dataset.getJEC()
        jecname = jec[0]
        rootdir = Data_MERGED_name+'/'+jecname+'/'+prefix

        writeMetadata(fOUT,rootdir,dataset)

        #Howto make one response histogram
        #plotResponse(fOUT,dataset.getHistogram('analysis/%s_Mu_a10_eta_00_03'%prefix),'%s/DATA_Mu_a10_eta_00_03'%rootdir)

        #Howto plot response for matching histograms
        for histoname in dataset.getHistogramNames():
            isResponseHistogram = False
            for reg in respselection:
                match = reg.search(histoname)
                if match:
                    isResponseHistogram = True
                    # response histograms
                    #newname = os.path.join(rootdir,os.path.basename(histoname))
                    newname = os.path.join(rootdir,*(histoname.split('/')[1:]))
                    writeResponse(fOUT,dataset.getHistogram(histoname),newname)
                    break
            if not isResponseHistogram:
                # rest of the histograms
                newname = os.path.join(rootdir,*(histoname.split('/')[1:]))
                writeHistogram(fOUT,dataset.getHistogram(histoname),newname)

    fOUT.Close()
    print("Wrote output in %s"%fNAME)

if __name__ == "__main__":
    main()
