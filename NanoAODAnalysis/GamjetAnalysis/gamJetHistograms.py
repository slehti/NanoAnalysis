import itertools
import sys
import re

import awkward as ak
import numpy as np

import hist

HISTOGRAMS = {}

class AnalysisHistograms():
    def __init__(self, isData):
        #self.histograms = {}
        self.isData = isData
        self.name = "gamjet"

        # self.histodir = "mc/"
        # if self.isData:
        #     self.histodir = "data/"
        self.histodir = "analysis/"

    def book(self):
        bins_y = np.arange(-3.99,6.01,0.02)
        bins_photonpt = np.array([12, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 85, 105, 130, 175, 230, 300, 400, 500, 700, 1000, 1500])
        bins_mu = np.arange(1,100,1)
        bins_01 = np.arange(0,1.01,0.01)
        bins_mass = np.arange(60,121,1)
        self.addHistogram('mass_'+self.name, 60, 60, 120)
#        self.add2DHistogram('h_Photonpt_RpT',bins_photonpt,bins_y)
#        self.add2DHistogram('h_Photonpt_RMPF',bins_photonpt,bins_y)

        variation_alpha = {'a030': '(alpha < 0.3)','a100': '(alpha < 1.0)'}
        variation_eta = {'eta_00_13': '(eta < 1.3) & (eta > -1.3)','eta_00_25': '(eta < 2.5) & (eta > -2.5)'}
        variation_btag = {'incl': '', 'btagDeepBtight': '(btag == True)', 'btagDeepCtight': '(ctag == True)', 'gluontag': '(gluontag == True)', 'quarktag': '(quarktag == True)', 'notag': '(notag == True)'}
        variation_gen = {'incl': ''}
        variation_psweight= {'PSWeight':''}

        variation_etaNarrow = {'eta_00_03': '(abseta > 0.000) & (abseta < 0.261)',
                               'eta_03_05': '(abseta > 0.261) & (abseta < 0.522)',
                               'eta_05_08': '(abseta > 0.522) & (abseta < 0.783)',
                               'eta_08_10': '(abseta > 0.783) & (abseta < 1.044)',
                               'eta_10_13': '(abseta > 1.044) & (abseta < 1.305)',
                               'eta_13_16': '(abseta > 1.305) & (abseta < 1.566)',
                               'eta_16_17': '(abseta > 1.566) & (abseta < 1.740)',
                               'eta_17_19': '(abseta > 1.740) & (abseta < 1.930)',
                               'eta_19_20': '(abseta > 1.930) & (abseta < 2.043)',
                               'eta_20_22': '(abseta > 2.043) & (abseta < 2.172)',
                               'eta_22_23': '(abseta > 2.172) & (abseta < 2.322)',
                               'eta_23_25': '(abseta > 2.322) & (abseta < 2.500)',
                               'eta_25_27': '(abseta > 2.500) & (abseta < 2.650)',
                               'eta_27_29': '(abseta > 2.650) & (abseta < 2.853)',
                               'eta_29_30': '(abseta > 2.853) & (abseta < 2.964)',
                               'eta_30_31': '(abseta > 2.964) & (abseta < 3.139)',
                               'eta_31_35': '(abseta > 3.139) & (abseta < 3.489)',
                               'eta_35_38': '(abseta > 3.489) & (abseta < 3.839)',
                               'eta_38_52': '(abseta > 3.839) & (abseta < 5.191)'
        }
        variation_etaWide = {'eta_13_19': '(abseta > 1.305) & (abseta < 1.93)',
                             'eta_19_25': '(abseta > 1.93) & (abseta < 2.5)',
                             'eta_25_30': '(abseta > 2.5) & (abseta < 2.964)',
                             'eta_30_32': '(abseta > 2.964) & (abseta < 3.2)',
                             'eta_32_52': '(abseta > 3.2) & (abseta < 5.191)'
        }

        variation_alpha_etaNarrowWide = {'a100': '(alpha < 1.0)'}
  
        if not self.isData:
            variation_psweight = {'PSWeight':'', 'PSWeight0': '', 'PSWeight1': '', 'PSWeight2': '', 'PSWeight3': ''}
            variation_gen = {
                'incl': '',
                'genb': '(ljetGenFlavor == 5)',
                'genc': '(ljetGenFlavor == 4)',
                'genuds': '(ljetGenFlavor > 0) & (ljetGenFlavor < 4)',
                'geng': '(ljetGenFlavor == 21)',
                'unclassified': '(ljetGenFlavor == 0)'
            }

        variationSet = {
            'alpha': variation_alpha,
#            'eta': variation_eta,
#            'eta': {**variation_etaNarrow, **variation_etaWide, **variation_eta},
            'btag': variation_btag,
            'gen': variation_gen
#            'psweight': variation_psweight
        }

        variationSet_etaNarrowWide = {
            'alpha': variation_alpha_etaNarrowWide
#            'eta': {**variation_etaNarrow, **variation_etaWide, **variation_eta}
        }

        variationSet_dir_etaNarrowWide = {
            'eta': {**variation_etaNarrow, **variation_etaWide, **variation_eta}
        }
        variationSet_dir = {
            'eta': variation_eta
        }

        #prefix = 'MC'
        #if self.isData:
        #    prefix = 'DATA'
        prefix = ''
        postfix = self.name
        # Narrow and Wide eta bins
        self.PhotonJeta_photonpt_rawNEvents = VariationHistograms('RawNEvents',"",{"photonpt":bins_photonpt},variationSet_dir_etaNarrowWide,variationSet_etaNarrowWide, self.isData)
        self.PhotonJeta_photonpt_photonmass = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"mass":bins_mass},variationSet_dir_etaNarrowWide,variationSet_etaNarrowWide, self.isData)

        self.PhotonJeta_photonpt_rmpf = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"mpf":bins_y},variationSet_dir_etaNarrowWide,variationSet_etaNarrowWide, self.isData)
        self.PhotonJeta_photonpt_ptbal = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"db":bins_y},variationSet_dir_etaNarrowWide,variationSet_etaNarrowWide, self.isData)
#        self.PhotonJeta_photonpt_rmpfnotypeI = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"MPFnotypeI":bins_y},variationSet_dir,variationSet_etaNarrowWide, self.isData)

        self.PhotonJeta_photonpt_npv = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"npv":bins_mu},variationSet_dir_etaNarrowWide,variationSet_etaNarrowWide, self.isData)
        self.PhotonJeta_photonpt_rho = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"rho":bins_mu},variationSet_dir_etaNarrowWide,variationSet_etaNarrowWide, self.isData)
        self.PhotonJeta_photonpt_mu = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"mu":bins_mu},variationSet_dir_etaNarrowWide,variationSet_etaNarrowWide, self.isData)

        self.PhotonJeta_photonpt_jetchf = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"chf":bins_01},variationSet_dir_etaNarrowWide,variationSet_etaNarrowWide, self.isData)
        self.PhotonJeta_photonpt_jetnhf = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"nhf":bins_01},variationSet_dir_etaNarrowWide,variationSet_etaNarrowWide, self.isData)
        self.PhotonJeta_photonpt_jetcef = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"cef":bins_01},variationSet_dir_etaNarrowWide,variationSet_etaNarrowWide, self.isData)
        self.PhotonJeta_photonpt_jetnef = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"nef":bins_01},variationSet_dir_etaNarrowWide,variationSet_etaNarrowWide, self.isData)
#        self.PhotonJeta_photonpt_jethfhf = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"JethfHF":bins_01},variationSet_dir,variationSet_etaNarrowWide, self.isData)
#        self.histograms.update(self.PhotonJeta_photonpt_jethfhf.histograms)
#        self.PhotonJeta_photonpt_jethfemf = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"JethfEMF":bins_01},variationSet_dir,variationSet_etaNarrowWide, self.isData)
#        self.histograms.update(self.PhotonJeta_photonpt_jethfemf.histograms)

#        self.PhotonJeta_photonpt_rho_npumean = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"Rho":bins_mu,"npumean":bins_mu},variationSet_dir,variationSet_etaNarrowWide, self.isData)
#        self.histograms.update(self.PhotonJeta_photonpt_rho_npumean.histograms)
#        self.PhotonJeta_photonpt_npv_npumean = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"NPV":bins_mu,"npumean":bins_mu},variationSet_dir,variationSet_etaNarrowWide, self.isData)
#        self.histograms.update(self.PhotonJeta_photonpt_npv_npumean.histograms)
        """
        PhotonJetPtDiff

        JetPF
        Rho_vs_npumean
        NPV_vs_npumean
        PhotonJetPtDiff_vs_npumean
        """
        self.h_photonpt_rpt = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"db":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_photonpt_rmpf = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"mpf":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_photonpt_rmpfjet1 = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"mpf1":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_photonpt_rmpfjetn = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"mpfn":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_photonpt_rmpfuncl = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"mpfu":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_photonpt_rmpfx = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"mpfx":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_photonpt_rmpfjet1x = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"mpf1x":bins_y},variationSet_dir,variationSet, self.isData)

        self.h_jpt_rpt = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"db":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_jpt_rmpf = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"mpf":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_jpt_rmpfjet1 = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"mpf1":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_jpt_rmpfjetn = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"mpfn":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_jpt_rmpfuncl = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"mpfu":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_jpt_rmpfx = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"mpfx":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_jpt_rmpfjet1x = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"mpf1x":bins_y},variationSet_dir,variationSet, self.isData)

        self.h_ptave_rpt = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"db":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_ptave_rmpf = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"mpf":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_ptave_rmpfjet1 = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"mpf1":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_ptave_rmpfjetn = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"mpfn":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_ptave_rmpfuncl = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"mpfu":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_ptave_rmpfx = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"mpfx":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_ptave_rmpfjet1x = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"mpf1x":bins_y},variationSet_dir,variationSet, self.isData)

        self.h_mu_rpt = VariationHistograms(prefix,postfix,{"mu":bins_mu,"db":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_mu_rmpf = VariationHistograms(prefix,postfix,{"mu":bins_mu,"mpf":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_mu_rmpfjet1 = VariationHistograms(prefix,postfix,{"mu":bins_mu,"mpf1":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_mu_rmpfjetn = VariationHistograms(prefix,postfix,{"mu":bins_mu,"mpfn":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_mu_rmpfuncl = VariationHistograms(prefix,postfix,{"mu":bins_mu,"mpfu":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_mu_rmpfx = VariationHistograms(prefix,postfix,{"mu":bins_mu,"mpfx":bins_y},variationSet_dir,variationSet, self.isData)
        self.h_mu_rmpfjet1x = VariationHistograms(prefix,postfix,{"mu":bins_mu,"mpf1x":bins_y},variationSet_dir,variationSet, self.isData)

        #
        self.h_photonpt_QGL = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"qgl":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_photonpt_muEF = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"muf":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_photonpt_chEmEF = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"cef":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_photonpt_chHEF = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"chf":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_photonpt_neEmEF = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"nef":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_photonpt_neHEF = VariationHistograms(prefix,postfix,{"photonpt":bins_photonpt,"nhf":bins_01},variationSet_dir,variationSet, self.isData)

        self.h_jpt_QGL = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"qgl":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_jpt_muEF = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"muf":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_jpt_chEmEF = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"cef":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_jpt_chHEF = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"chf":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_jpt_neEmEF = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"nef":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_jpt_neHEF = VariationHistograms(prefix,postfix,{"jetpt":bins_photonpt,"nhf":bins_01},variationSet_dir,variationSet, self.isData)

        self.h_ptave_QGL = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"qgl":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_ptave_muEF = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"muf":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_ptave_chEmEF = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"cef":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_ptave_chHEF = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"chf":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_ptave_neEmEF = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"nef":bins_01},variationSet_dir,variationSet, self.isData)
        self.h_ptave_neHEF = VariationHistograms(prefix,postfix,{"ptave":bins_photonpt,"nhf":bins_01},variationSet_dir,variationSet, self.isData)

        return HISTOGRAMS

    def addHistogram(self, name, nbins, binmin, binmax):
        name = self.histodir+name
        HISTOGRAMS[name] = hist.Hist(
            hist.axis.StrCategory([], growth=True, name=name, label="label"),
            hist.axis.Regular(nbins, binmin, binmax, name="value", label="x value"),
            storage="weight",
            name=name
        )

    def add2DHistogram(self, name, xbins, ybins):
        name = self.histodir+name
        HISTOGRAMS[name] = hist.Hist(
            hist.axis.StrCategory([], growth=True, name=name, label="label"),
            hist.axis.Variable(xbins, name="x", label="x label"),
            hist.axis.Variable(ybins, name="y", label="y label"),
            storage="weight",
            name=name
        )

    def cloneHistogram(self, nameOrig, nameClone):
        nameOrig = self.histodir+nameOrig
        nameClone = self.histodir+nameClone
        HISTOGRAMS[nameClone] = HISTOGRAMS[nameOrig].copy()

    def fillHistogram(self, name, value, weight):
        name = self.histodir+name
        HISTOGRAMS[name].fill(name,value=value,weight=weight)

    def fill(self,events,out):

        if len(events) > 0:
            # variables
            weight     = events.weight
            mu         = events.mu
            rho        = np.ones(len(events)) #FIXME events.fixedGridRhoFastjetAll
            npv        = events.PV.npvsGood
            MET        = events.METtype1
            leadingJet = events.leadingJet
            photon     = events.photon


            # Adding a pt-0 jet in cases there are no subleading jets
            events1j = events[(ak.num(events.Jet) == 1)]
            zerojets = events1j.Jet
            zerojets['pt'] = 0
            zerojets['px'] = 0
            zerojets['py'] = 0
            zerojets['phi'] = 0
            jets = ak.concatenate([events1j.Jet,zerojets], axis=1)
            events1j['Jet'] = jets
            eventsNj = events[(ak.num(events.Jet) > 1)]
            events = ak.concatenate([eventsNj,events1j])

            subLeadingJet = events.Jet[:, 1]

            x = ak.sum(events.Jet.pt*np.cos(events.Jet.phi), axis=1)
            y = ak.sum(events.Jet.pt*np.sin(events.Jet.phi), axis=1)
            jets_all = ak.zip({"pt": np.hypot(x, y), "phi": np.arctan2(y, x), "px": x, "py": y})

            x2 = x - leadingJet.px
            y2 = y - leadingJet.py
            jets_notLeadingJet = ak.zip({"pt": np.hypot(x2, y2), "phi": np.arctan2(y2, x2), "px": x2, "py": y2})


            mx = -MET.px -jets_all.px -photon.px
            my = -MET.py -jets_all.py -photon.py
            METuncl = ak.zip({"pt": np.hypot(mx, my), "phi": np.arctan2(my, mx), "px": mx, "py": my})

            # alpha = 0 if subLeadingJet.pt < 15
            subLeadingJetpt = np.array(subLeadingJet.pt)
            subLeadingJetpt[(subLeadingJetpt < 15)] = 0
            events["alpha"] = ak.flatten(subLeadingJetpt/photon.pt)
            events["eta"] = events.leadingJet.eta
            events["abseta"] = np.abs(events.leadingJet.eta)

            R_pT = ak.flatten(leadingJet.pt/photon.pt)
            R_MPF = ak.flatten(1 + MET.pt*(np.cos(MET.phi)*photon.px + np.sin(MET.phi)*photon.py)/(photon.pt*photon.pt))
            R_MPFjet1 = ak.flatten(-leadingJet.pt*(np.cos(leadingJet.phi)*photon.px + np.sin(leadingJet.phi)*photon.py)/(photon.pt*photon.pt))
            R_MPFjetn = ak.flatten(-jets_notLeadingJet.pt*(np.cos(jets_notLeadingJet.phi)*photon.px + np.sin(jets_notLeadingJet.phi)*photon.py)/(photon.pt*photon.pt))
            R_MPFuncl = ak.flatten(-METuncl.pt*(np.cos(METuncl.phi)*photon.px + np.sin(METuncl.phi)*photon.py)/(photon.pt*photon.pt))

            pi2 = np.pi/2
            R_MPFx = ak.flatten(1 + MET.pt*(np.cos(MET.phi+pi2)*photon.px + np.sin(MET.phi+pi2)*photon.py)/(photon.pt*photon.pt))
            R_MPFjet1x = ak.flatten(-leadingJet.pt*(np.cos(leadingJet.phi+pi2)*photon.px + np.sin(leadingJet.phi+pi2)*photon.py)/(photon.pt*photon.pt))

            R_MPFnotypeI = R_MPF #FIXME

            jpt = np.array(leadingJet.pt)
            photonpt = np.array(ak.flatten(photon.pt))
            jpt[(jpt < 12) | (photonpt < 12)] = 0
            photonpt[(jpt < 12) | (photonpt < 12)] = 0
            ptave = 0.5*(jpt + photonpt)

            #filling
            
            self.PhotonJeta_photonpt_rawNEvents.fill(events,photonpt,weight=weight)
            self.PhotonJeta_photonpt_photonmass.fill(events,photonpt,ak.flatten(events.photon.mass,axis=1),weight=weight)
            self.PhotonJeta_photonpt_rmpf.fill(events,photonpt,R_MPF,weight=weight)
            self.PhotonJeta_photonpt_ptbal.fill(events,photonpt,R_pT,weight=weight)
            #self.PhotonJeta_photonpt_rmpfnotypeI.fill(events,photonpt,R_MPFnotypeI,weight=weight)
            self.PhotonJeta_photonpt_npv.fill(events,photonpt,npv,weight=weight)
            self.PhotonJeta_photonpt_rho.fill(events,photonpt,rho,weight=weight)
            self.PhotonJeta_photonpt_mu.fill(events,photonpt,mu,weight=weight)
            self.PhotonJeta_photonpt_jetchf.fill(events,photonpt,leadingJet.chHEF,weight=weight)
            self.PhotonJeta_photonpt_jetnhf.fill(events,photonpt,leadingJet.neHEF,weight=weight)
            self.PhotonJeta_photonpt_jetcef.fill(events,photonpt,leadingJet.chEmEF,weight=weight)
            self.PhotonJeta_photonpt_jetnef.fill(events,photonpt,leadingJet.neEmEF,weight=weight)
            #self.PhotonJeta_photonpt_jethfhf.fill(events,photonpt,leadingJet.,weight=weight)
            #self.PhotonJeta_photonpt_jethfemf.fill(events,photonpt,leadingJet.,weight=weight)

            self.h_photonpt_rpt.fill(events,photonpt,R_pT,weight=weight)
            self.h_photonpt_rmpf.fill(events,photonpt,R_MPF,weight=weight)
            self.h_photonpt_rmpfjet1.fill(events,photonpt,R_MPFjet1,weight=weight)
            self.h_photonpt_rmpfjetn.fill(events,photonpt,R_MPFjetn,weight=weight)
            self.h_photonpt_rmpfuncl.fill(events,photonpt,R_MPFuncl,weight=weight)
            self.h_photonpt_rmpfx.fill(events,photonpt,R_MPFx,weight=weight)
            self.h_photonpt_rmpfjet1x.fill(events,photonpt,R_MPFjet1x,weight=weight)

            self.h_jpt_rpt.fill(events,jpt,R_pT,weight=weight)
            self.h_jpt_rmpf.fill(events,jpt,R_MPF,weight=weight)
            self.h_jpt_rmpfjet1.fill(events,jpt,R_MPFjet1,weight=weight)
            self.h_jpt_rmpfjetn.fill(events,jpt,R_MPFjetn,weight=weight)
            self.h_jpt_rmpfuncl.fill(events,jpt,R_MPFuncl,weight=weight)
            self.h_jpt_rmpfx.fill(events,jpt,R_MPFx,weight=weight)
            self.h_jpt_rmpfjet1x.fill(events,jpt,R_MPFjet1x,weight=weight)

            self.h_ptave_rpt.fill(events,ptave,R_pT,weight=weight)
            self.h_ptave_rmpf.fill(events,ptave,R_MPF,weight=weight)
            self.h_ptave_rmpfjet1.fill(events,ptave,R_MPFjet1,weight=weight)
            self.h_ptave_rmpfjetn.fill(events,ptave,R_MPFjetn,weight=weight)
            self.h_ptave_rmpfuncl.fill(events,ptave,R_MPFuncl,weight=weight)
            self.h_ptave_rmpfx.fill(events,ptave,R_MPFx,weight=weight)
            self.h_ptave_rmpfjet1x.fill(events,ptave,R_MPFjet1x,weight=weight)

            self.h_mu_rpt.fill(events,mu,R_pT,weight=weight)
            self.h_mu_rmpf.fill(events,mu,R_MPF,weight=weight)
            self.h_mu_rmpfjet1.fill(events,mu,R_MPFjet1,weight=weight)
            self.h_mu_rmpfjetn.fill(events,mu,R_MPFjetn,weight=weight)
            self.h_mu_rmpfuncl.fill(events,mu,R_MPFuncl,weight=weight)
            self.h_mu_rmpfx.fill(events,mu,R_MPFx,weight=weight)
            self.h_mu_rmpfjet1x.fill(events,mu,R_MPFjet1x,weight=weight)

            self.h_photonpt_QGL.fill(events,photonpt,leadingJet.qgl,weight=weight)
            self.h_photonpt_muEF.fill(events,photonpt,leadingJet.muEF,weight=weight)
            self.h_photonpt_chEmEF.fill(events,photonpt,leadingJet.chEmEF,weight=weight)
            self.h_photonpt_chHEF.fill(events,photonpt,leadingJet.chHEF,weight=weight)
            self.h_photonpt_neEmEF.fill(events,photonpt,leadingJet.neEmEF,weight=weight)
            self.h_photonpt_neHEF.fill(events,photonpt,leadingJet.neHEF,weight=weight)

            self.h_jpt_QGL.fill(events,jpt,leadingJet.qgl,weight=weight)
            self.h_jpt_muEF.fill(events,jpt,leadingJet.muEF,weight=weight)
            self.h_jpt_chEmEF.fill(events,jpt,leadingJet.chEmEF,weight=weight)
            self.h_jpt_chHEF.fill(events,jpt,leadingJet.chHEF,weight=weight)
            self.h_jpt_neEmEF.fill(events,jpt,leadingJet.neEmEF,weight=weight)
            self.h_jpt_neHEF.fill(events,jpt,leadingJet.neHEF,weight=weight)

            self.h_ptave_QGL.fill(events,ptave,leadingJet.qgl,weight=weight)
            self.h_ptave_muEF.fill(events,ptave,leadingJet.muEF,weight=weight)
            self.h_ptave_chEmEF.fill(events,ptave,leadingJet.chEmEF,weight=weight)
            self.h_ptave_chHEF.fill(events,ptave,leadingJet.chHEF,weight=weight)
            self.h_ptave_neEmEF.fill(events,ptave,leadingJet.neEmEF,weight=weight)
            self.h_ptave_neHEF.fill(events,ptave,leadingJet.neHEF,weight=weight)

            self.fillHistogram('mass_'+self.name,value=ak.flatten(photon.mass),weight=weight)
        return HISTOGRAMS

class VariationHistograms():
    def __init__(self,prefix,postfix,xybins,dirvar,var,isData):
        self.histograms = {}
        self.isData = isData
        self.selection = {}
        self.weights = {}

        dirvariations = []
        dirvarSelections = {}

        for key in dirvar.keys():
            dirvariations.append(list(dirvar[key].keys()))
            for k2 in dirvar[key].keys():
                dirvarSelections[k2] = dirvar[key][k2]

        variations = []
        varSelections = {}

        for key in var.keys():
            variations.append(list(var[key].keys()))
            for k2 in var[key].keys():
                varSelections[k2] = var[key][k2]

        # nameBase = name
        # nameBase = "mc/"
        # if isData:
        #     nameBase = "data/"
        nameBase = "analysis/"

        histoname = ""
        if len(prefix) > 0:
            histoname += prefix
        xKey = ''
        yKey = ''
        zKey = ''
        for key in xybins.keys():
#            nameBase = nameBase + '_' + key
            if len(histoname) == 0:
                histoname = histoname + key
            else:
                histoname = histoname + '_' + key
            if xKey == '':
                xKey = key
            elif yKey == '':
                yKey = key
            else:
                zKey = key
        if len(postfix) > 0:
            histoname = histoname + '_' + postfix

        if len(dirvariations) > 0:
            for dirvariation in list(itertools.product(*dirvariations)):
                dname = nameBase
                dsele = None

                for dcomb in dirvariation:
                    dname = dname + '%s/'%dcomb
                    if dsele == None:
                        dsele = dirvarSelections[dcomb]
                    else:
                        dsele = dsele + " & " + dirvarSelections[dcomb]

                    for combination in list(itertools.product(*variations)):
                        hname = dname+histoname
                        sele = dsele

                        for comb in combination:
                            if not (comb == '' or 'incl' in comb or comb == 'PSWeight'):
                                hname = hname + '_%s'%comb

                                if 'weight' in comb or 'Weight' in comb:
                                    self.weights[hname] = comb
                                    continue
                                if sele == None:
                                    sele = varSelections[comb]
                                else:
                                    sele = sele + " & " + varSelections[comb]

                        self.selection[hname] = sele
                        if len(xybins.keys()) == 1:
                            self.book1d(hname,xKey,xybins[xKey])
                        if len(xybins.keys()) == 2:
                            self.book2d(hname,xKey,xybins[xKey],yKey, xybins[yKey])
                        if len(xybins.keys()) == 3:
                            self.book3d(hname,xKey,xybins[xKey],yKey,xybins[yKey],zKey,xybins[zKey])
        else:
            for combination in list(itertools.product(*variations)):
                hname = nameBase+histoname
                sele = None

                for comb in combination:
                    if not (comb == '' or 'incl' in comb or comb == 'PSWeight'):
                        hname = hname + '_%s'%comb

                        if 'weight' in comb or 'Weight' in comb:
                            self.weights[hname] = comb
                            continue
                        if sele == None:
                            sele = varSelections[comb]
                        else:
                            sele = sele + " & " + varSelections[comb]

                self.selection[hname] = sele
                if len(xybins.keys()) == 1:
                    self.book1d(hname,xKey,xybins[xKey])
                if len(xybins.keys()) == 2:
                    self.book2d(hname,xKey,xybins[xKey],yKey, xybins[yKey])
                if len(xybins.keys()) == 3:
                    self.book3d(hname,xKey,xybins[xKey],yKey,xybins[yKey],zKey,xybins[zKey])

        self.selection_re = re.compile("^\((?P<variable>\S+)\s*(?P<operator>\S+)\s*(?P<value>\S+)\)")
        HISTOGRAMS.update(self.histograms)

    def book1d(self,name,xname,xbins):
        self.histograms[name] = hist.Hist(
            hist.axis.StrCategory([], growth=True, name=name, label="label"),
            hist.axis.Variable(xbins, name="x", label=xname)
        )

    def book2d(self,name,xname,xbins,yname,ybins):
        self.histograms[name] = hist.Hist(
            hist.axis.StrCategory([], growth=True, name=name, label="label"),
            hist.axis.Variable(xbins, name="x", label=xname),
            hist.axis.Variable(ybins, name="y", label=yname),
            storage="weight",
            name="Counts"
        )

    def book3d(self,name,xname,xbins,yname,ybins,zname,zbins):
        self.histograms[name] = hist.Hist(
            hist.axis.StrCategory([], growth=True, name=name, label="label"),
            hist.axis.Variable(xbins, name="x", label=xname),
            hist.axis.Variable(ybins, name="y", label=yname),
            hist.axis.Variable(zbins, name="z", label=zname),
            storage="weight",
            name="Counts"
        )

    def fill(self,events,x,y=[],z=[],weight=[]):
        for key in self.histograms:
            sele = self.select(events,key)
            selected_x = x[sele]
            selected_w = np.ones(len(selected_x))
            if len(weight) > 0:
                w = self.getweight(events,key)
                selected_w = weight[sele]*w[sele]
            if len(y) == 0:
                self.histograms[key].fill(key,selected_x,weight=selected_w)
            elif len(z) == 0:
                selected_y = y[sele]
                self.histograms[key].fill(key,selected_x,
                                          selected_y,
                                          weight=selected_w)
            else:
                selected_y = y[sele]
                selected_z = z[sele]
                self.histograms[key].fill(key,selected_x,
                                          selected_y,
                                          selected_z,
                                          weight=selected_w)
        HISTOGRAMS.update(self.histograms)

    def getweight(self,events,variable):
        w = np.array([1.0]*len(events))
        if variable in self.weights.keys():
            if 'PSWeight' in self.weights[variable] and len(self.weights[variable]) > 8:
                weightnumber = int(self.weights[variable].replace('PSWeight',''))
                w = events['PSWeight'][:,weightnumber]
        return w

    def select(self,events,variable):
        return_sele = (events["event"] > 0)
        if self.selection[variable] == None:
            return return_sele
        selections = self.selection[variable].split('&')

        for s in selections:
            s = s.strip()
            match = self.selection_re.search(s)
            if match:
                variable = match.group('variable')
                operator = match.group('operator')
                value    = eval(match.group('value'))

                if operator == '<':
                    return_sele = return_sele & (events[variable] < value)
                if operator == '>':
                    return_sele = return_sele & (events[variable] > value)
                if operator == '==':
                    return_sele = return_sele & (events[variable] == value)
            else:
                print("Problem with VariationHistograms selection",s)
                sys.exit()
        return return_sele

