from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os
from tempfile import TemporaryDirectory as td
from string import Template
from pathlib import Path

import numpy as np
from numpy.random import SeedSequence

from nanoanalysis import aux
from nanoanalysis import condor
from nanoanalysis.limits.dcard_dir import for_each_mass

def parse_args():
    parser = ArgumentParser(
        prog="Interface to Combine for signal injection testing",
        formatter_class = ArgumentDefaultsHelpFormatter,
        description="See: https://twiki.cern.ch/twiki/bin/view/CMS/B2GStatisticsRecommendations#B2G_Requested_Statistical_Tests"
    )

    parser.add_argument(
        "inputs", help="input datacard directory, the output of pseudos2dcards.py"
    )
    parser.add_argument(
        "--job-flavour", help="Condor job maximum runtime setting.",
        default="espresso"
    )
    parser.add_argument(
        "--stat", help="Test statistic (one of \"saturated\", \"KS\", \"AD\")",
        default="saturated"
    )
    parser.add_argument(
        "--unblinded", action="store_true", help = (
            "ONLY WITH APPROVAL For final analysis, "\
            "calculate goodness of fit for the observed data."
        )
    )
    parser.add_argument(
        "-j", "--jobs", type=int, default=5, help="Number of parallel jobs"
    )
    parser.add_argument(
        "-t", "--toys", type=int, default=500, help="Number of generated toys"
    )
    parser.add_argument(
        "-s", "--seed", type=int, default=123456, help="Seed for toy generation"
    )
    parser.add_argument(
        "--title", default="Goodness of fit", help="Title for created plot"
    )
    parser.add_argument(
        "--b-only", action="store_true", help= (
            "Fit the background only model instead of a floating signal strenght"
        )
    )
    parser.add_argument(
        "--condor-job", action="store_true", help="Submit fit jobs to htcondor"
    )
    parser.add_argument(
        "-f", "--skip-fits", action="store_true",
        help="Specify this flag if the gof values have already been calculated"\
            " to avoid recomputing them."
    )
    parser.add_argument(
        "--skip-workspace", action="store_true",
        help="Specify this flag if the impacts workspaces have already been created"\
            " to avoid recomputing them."
    )
    return parser.parse_known_args()

def main(args, unknowns):

    # file name configuration
    toysname = f".toys_GoF_{args.stat}"
    blindtext = "" if args.unblinded else "_Blind"
    bonlytext = "_Bonly" if args.b_only else ""
    toysname += blindtext + bonlytext
    dataname = ".data_GoF" if args.unblinded else ".asimov_GoF"
    wsname = "ws_GoF.root"

    # plot configuration
    title = args.title.replace("{","{{").replace("}","}}")

    outputname = f"gof_plot{blindtext}{bonlytext}_{args.stat}"

    # Combine command line, mass placeholder gets formatted in for_each_mass
    base_command = (
        f"combineTool.py -M GoodnessOfFit -d {wsname} -m {{mass}} --algo={args.stat}"
    )

    # optionally use the background only model
    if args.b_only:
        base_command += " --fixedSignalStrength 0"

    # arguments for throwing toys
    ntoys = args.toys // args.jobs
    toy_cmd = base_command + f" -t {ntoys} -n {toysname}_$jobid -s $seed"
    if args.stat == "saturated":
        toy_cmd += " --toysFreq"
        if not args.unblinded:
            toy_cmd += " --bypassFrequentistFit"

    # Only toy calculations need to be distributed, data is fast
    if args.condor_job:
        toy_cmd += (
            " --job-mode condor --task-name GoF{mass}_$jobid"
            f" --sub-opts='+JobFlavour=\"{args.job_flavour}\"'"
        )

    # arguments for observed (or asimov when blind) gof
    asimov_opt = "" if args.unblinded else " -t -1"
    data_cmd = base_command + f"{asimov_opt} -n {dataname}"

    os.chdir(args.inputs)

    # This can throw a bunch of segfaults is the datacard directory has
    # Additional files or directories. This doesn't affect functionality
    if not args.skip_workspace:
        aux.execute(f"combineTool.py -M T2W -i ./* -o {wsname} --parallel 4")

    # For throwing toys in parallel, create a sequence of combineTool calls
    # with different seeds
    toy_cmd = Template(toy_cmd)

    # positive, signed 32 bit ints initialized by the global seed
    seeds = SeedSequence(args.seed).generate_state(args.jobs) >> 1
    toys_split = [toy_cmd.substitute(seed=s, jobid=i) for i,s in enumerate(seeds)]
    toys_parallel = " & ".join(toys_split) + " & wait"

    # Run the calculations
    if not args.skip_fits:
        for_each_mass(toys_parallel)
        for_each_mass(data_cmd)
        if args.condor_job:
            condor.wait()

    # Merge toy results from each job
    toy_resultfiles = f"higgsCombine{toysname}_*.GoodnessOfFit.*.root"
    hadd_toyjobs = f"hadd -f {toysname}.root {toy_resultfiles}"
    for_each_mass(hadd_toyjobs)

    script_path = Path(__file__).parent

    # Write .json and plot
    data_resfile = f"higgsCombine{dataname}.GoodnessOfFit.mH{{mass}}.root"
    cmd = (
        f"combineTool.py -M CollectGoodnessOfFit --input {data_resfile} {toysname}.root"
        f" -m {{mass}} -o {dataname}.json; "
        f"{script_path}/plot_gof.py {dataname}.json --statistic {args.stat} --mass {{mass}}.0"
        f" -o {outputname} --title-right \"{title}\" {' '.join(unknowns)}"
    )
    for_each_mass(cmd)

    print("run_gof.py done, check above for errors.")
    return


if __name__ == "__main__":
    args, unknowns = parse_args()
    main(args, unknowns)

