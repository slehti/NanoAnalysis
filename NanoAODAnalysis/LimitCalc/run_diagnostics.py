from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os
from tempfile import TemporaryDirectory as td

from nanoanalysis import aux
from nanoanalysis import condor
from nanoanalysis.limits.dcard_dir import for_each_mass

def parse_args():
    parser = ArgumentParser(
        prog="Interface to Combine FitDiagnostics for closure checks",
        formatter_class = ArgumentDefaultsHelpFormatter,
        description="See: https://twiki.cern.ch/twiki/bin/viewauth/CMS/HiggsWG/HiggsPAGPreapprovalChecks"
    )

    parser.add_argument(
        "inputs", help="input datacard directory, the output of pseudos2dcards.py"
    )
    parser.add_argument(
        "--unblinded", action="store_true", help="ONLY WITH APPROVAL For final analysis, "\
            "calculate observed best fit and pulls in addition to B/S+B toy datasets."
    )
    parser.add_argument(
        "--minos", help=(
            "Which parameter uncertainties to estimate with MINOS"
            " (one of \"all\", \"poi\", \"none\")"
        )
        , default="all"
    )
    parser.add_argument(
        "--condor-job", action="store_true", help="Submit fit jobs to htcondor"
    )
    return parser.parse_args()

def load_diffNuisances(directory):
    aux.execute(
        "wget https://raw.githubusercontent.com/cms-analysis/"
        "HiggsAnalysis-CombinedLimit/main/test/diffNuisances.py"
        f" -P {directory}; "
        f"chmod +x {directory}/diffNuisances.py"
    )
    return f"{directory}/diffNuisances.py"


def main(args):
    bname = "BAsimov"
    sbname = "SBAsimov"
    wsname = "ws_diagnostics.root"

    base_command = (
        f"combineTool.py -M FitDiagnostics {wsname}"
        f" --rMin -5 --rMax 5 --minos {args.minos} -m {{mass}}"
    )
    if args.condor_job:
        base_command += (
            " --job-mode condor --task-name fitDiagnostics{mass}"
            " --sub-opts='+JobFlavour=\"longlunch\"'"
        )
    bonly_cmd = base_command + f" -t -1 --expectSignal 0 -n {bname}"
    sb_cmd = base_command + f" -t -1 --expectSignal 1 -n {sbname}"

    with td() as diffdir:

        diffNuisances = load_diffNuisances(diffdir)

        os.chdir(args.inputs)
        aux.execute(f"combineTool.py -M T2W -i ./* -o {wsname} --parallel 4")
        for_each_mass(bonly_cmd)
        for_each_mass(sb_cmd)
        if args.condor_job:
            condor.wait()
        for_each_mass(
            f"{diffNuisances} -a fitDiagnostics{bname}.root"
            f" >> diagnostics.{bname}.txt"
        )

        for_each_mass(
            f"{diffNuisances} -a fitDiagnostics{sbname}.root"
            f">> diagnostics.{sbname}.txt"
        )

        if args.unblinded:
            cmd = (
                base_command +
                " --robustFit=1 --saveShapes --saveWithUncertainties"
                " --saveOverallShapes --saveNormalizations --saveWorkspace --plots"
            )
            for_each_mass(cmd)

    print("run_diagnostics.py done, check above for errors.")
    if args.condor_job:
        print("Please wait for condor tasks to finish")
    return


if __name__ == "__main__":
    args = parse_args()
    main(args)

