from pathlib import Path
import json
from itertools import product
import re
import os
import argparse
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from datetime import datetime
from array import array

import numpy as np
import ROOT
import CombineHarvester.CombineTools.ch as ch

import nanoanalysis.tools.tdrstyle as tdrstyle
import nanoanalysis.tools.plotting as plotting
import nanoanalysis.limits.dcard_description as dcard_description
from nanoanalysis.limits.systematics import systematics

style = tdrstyle.setTDRStyle()
ROOT.gStyle.SetPalette(ROOT.kAlpine)
# ROOT.gROOT.SetStyle("self.tdrStyle")
# ROOT.gROOT.ForceStyle()
ROOT.gStyle.SetHatchesLineWidth(1)
ROOT.gStyle.SetHatchesSpacing(1)

def parse_args():
    parser = ArgumentParser(
        description = "Create datacards from pseudomulticrab histograms.",
        formatter_class = ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "pseudos", nargs="*", help="paths to the pseudomulticrabs "\
        "containing the histograms from event selections for each channel and category"
    )

    parser.add_argument(
        "-o", "--output_dir", help="path where to save the generated shape "\
        "histograms", default = "datacards"
    )

    parser.add_argument(
        "-c", "--config", help="path to the analysis configuration json file",
        default = None
    )
    parser.add_argument(
        "-s", "--separate", action="store_true",
        help="Specify this to generate individual datacards for each bin"\
             "in addition to the combined datacards"
    )
    parser.add_argument(
        "--bb-lite", action=argparse.BooleanOptionalAction, default=True,
        help= (
            "Control whether statistical uncertainty is handled via the background"
            "merging Barlow-Beeston lite approach, or by separate statistical"
            "uncertainties for each process."
        )

    )
    return parser.parse_args()

def get_histo(dataset, hname):

    histos = dataset.histo
    histo_re = re.compile(hname)

    found = []
    keys = histos.keys()
    for k in keys:
        if histo_re.search(k):
            found.append(histos[k])

    if not found:
        msg = f"no shape histogram matching {hname} found for dataset {dataset.name}!"
        print(f"WARNING: {msg}")
        return None
    if len(found) > 1:
        msg = f"Multiple shape histograms matching {hname} found for dataset {dataset.name}, cannot continue!"
        raise ValueError(msg)
    h = found[0]
    return h

def get_shapehisto(dataset, config):
    histoname = config["shapehisto_name"]
    h = get_histo(dataset, histoname)
    if config["rebin_factor"] != 1:
        h.Rebin(config["rebin_factor"])
    return h

def get_harvester(config, eras, masses, backgrounds, signals):
    cb = ch.CombineHarvester()

    # data definition
    cb.AddObservations(["*"], [config["name"]], eras, config["channels"], config["categories"])

    for channel in config["channels"]:
        for category in config["categories"]:
            key = (channel, category[1])
            # background process definition
            cb.AddProcesses(
                ["*"], [config["name"]], eras,
                [channel],
                backgrounds[key],
                [category],
                False
            )

            # signal process definition
            for m in masses[key]:
                cb.AddProcesses(
                    [m], [config["name"]], eras,
                    [channel],
                    [signals[key][m]],
                    [category],
                    True
                )

    return cb

def name_datacard(analysis_name, mass, bin=None):
    binname = "combined" if bin is None else bin
    binname = f"{analysis_name}_{binname}"
    name = Path(binname) / mass / "datacard"
    return name

def write_dcards(analysis_name, output_dir, cb, separate, years, lumis, **kwargs):
    #  This method will produce a set of unique bin names by considering all
    #  Observation, Process and Systematic entries in the CombineHarvester
    #  instance.

    bins = cb.bin_set()
    masses = cb.mass_set()

    if separate:
        #  Finally we iterate through each bin,mass combination and write a
        #  datacard.
        for b in bins:
            for m in masses:
                if m == "*": continue
                name = name_datacard(analysis_name, m, bin=b)
                path = output_dir / name
                path.parent.mkdir(parents=True, exist_ok=True)
                write_dcard(path, cb.cp().bin([b]).mass([m, "*"]), m, years, lumis)

    for m in masses:
        if m == "*": continue
        name = name_datacard(analysis_name, m)
        path = output_dir / name
        path.parent.mkdir(parents=True, exist_ok=True)
        plotting.plot_shapes(
            cb.cp().mass([m, "*"]),
            output_dir / f"plots/combine_shapes_M{m}",
            **kwargs
        )
        write_dcard(path, cb.cp().mass([m, "*"]), m, years, lumis)

    return

def write_dcard(path, cb, mass, years, lumis):
    with ROOT.TFile.Open(str(path.with_suffix(".root")), "RECREATE") as out_shapes:
        #  We need to filter on both the mass and the mass hypothesis,
        #  where we must remember to include the "*" mass entry to get
        #  all the data and backgrounds.
        dcard_name = str(path.with_suffix(".txt"))
        cb.WriteDatacard(dcard_name, out_shapes)
        dcard_description.add_dcard_description(dcard_name, mass, years, lumis)
    return

def add_shapes(cb, histos, skip_missing=False):
    if skip_missing:
        def add_shape(proc):
            hist = histos[(proc.mass(), proc.channel(), proc.bin(), proc.process())]
            if hist is not None:
                proc.set_shape(hist, True)
            else:
                print(f"WARNING: no histogram for bin {proc.bin()}, process {proc.process()}")
    else:
        def add_shape(proc):
            proc.set_shape(histos[(proc.mass(), proc.channel(), proc.bin(), proc.process())], True)
    cb.ForEachObs(add_shape)
    cb.ForEachProc(add_shape)
    return cb

def add_systematics(cb, config):
    all_procs = cb.process_set()
    for systgroup in config["systematics"]:
        systematic = systematics[systgroup]
        cb.cp().AddSyst(
            cb,
            *systematic.to_AddSyst()
        )
    return cb

def main(args):

    config = limits.config.load_config(args.config)
    output_dir = Path(args.output_dir)

    channels_categories = product(config["channels"], config["categories"])
    shape_histos = {}
    years = {}
    lumis = {}
    masses = {}
    backgrounds = {}
    signals = {}

    for (channel, category), pseudodir in zip(channels_categories, args.pseudos):
        datasets = plotting.getDatasets(pseudodir)
        key = (channel, category[1])

        # remove exclude datasets
        if isinstance(config["exclude"], dict):
            for re in config["exclude"][channel]:
                datasets = plotting.removeDatasets(re, datasets)
        else:
            for re in config["exclude"]:
                datasets = plotting.removeDatasets(re, datasets)

        # read remaining datasets
        datasets = plotting.read(datasets)

        # normalize before merging
        datasets = plotting.normalizeToLumi(datasets, xsec_signal=config["signal_xsect"])

        # Merging datasets
        if channel in config["merge"].keys():
            for name, re in config["merge"][channel].items():
                datasets = plotting.mergeDatasets(name, re, datasets)
        else:
            for name, re in config["merge"].items():
                datasets = plotting.mergeDatasets(name, re, datasets)
        sig_datasets = [d for d in datasets if d.is_signal(config)]
        years[key] = plotting.getRuns(datasets)
        lumis[key] = plotting.getLumi(datasets)
        sig_datasets= [d for d in datasets if d.is_signal(config)]
        bkg_datasets = [d for d in datasets if d.is_background(config)]
        bin_masses = [d.get_mass(config) for d in sig_datasets]
        masses[key] = bin_masses
        sig = {m: d.name for m, d in zip(bin_masses, sig_datasets)}
        signals[key] = sig
        bkg = [d.name for d in datasets if d.is_background(config)]
        backgrounds[key] = bkg

        # store shape histograms for CombineHarvester
        for d in datasets:
            mass = d.get_mass(config)
            shape_histos[(mass, channel, category[1], d.name)] = (
                get_shapehisto(d, config)
            )

        # Plot pdfs specified in config
        for hname in config["plotted_histos"]:
            plot_opts = dict(config["default_plot_options"])
            plot_opts["process_labels"] = config["process_labels"]
            plot_opts["signal_mass_re"] = config["signal_mass_re"]
            plot_opts["lumi_by_era"] = dcard_description.lumis_by_era(
                {"bins": {key: {"years": years[key][0], "lumi": lumis[key]}}}
            )

            plot_config = config.copy()
            plot_config["channels"] = [channel]
            plot_config["categories"] = [category]
            plot_cb = get_harvester(
                plot_config,
                [era for era in plot_opts["lumi_by_era"]],
                {key: bin_masses},
                {key: bkg},
                {key: sig}
            )

            if hname in config["plot_options"]:
                plot_opts.update(config["plot_options"][hname])
            if "histoname" in plot_opts:
                histo_path = plot_opts["histoname"]
            else:
                histo_path = hname
            plot_histos = {}
            for d in datasets:
                mass = d.get_mass(config)
                plot_histos[(mass, channel, category[1], d.name)] = (
                    get_histo(d, histo_path)
            )
            plot_cb = add_shapes(plot_cb, plot_histos, skip_missing=True)
            plot_cb = add_systematics(plot_cb, plot_config)
            plot_bbb = ch.BinByBinFactory().SetAddThreshold(0)
            plot_bbb.AddBinByBin(plot_cb.cp().backgrounds(), plot_cb)

            for m in bin_masses:
                if m == "*": continue
                plotting.plot_shapes(
                    plot_cb.cp().mass([m, "*"]),
                    output_dir / f"plots/{hname}/{m}",
                    **plot_opts
                )

    era_lumis = dcard_description.lumis_by_era(
        {"bins": {b: {"years": years[b][0], "lumi": lumis[b]} for b in lumis}}
    )
    eras = [era for era in plot_opts["lumi_by_era"]]
    cb = get_harvester(config, eras, masses, backgrounds, signals)
    cb = add_shapes(cb, shape_histos)
    cb = add_systematics(cb, config)
    rebinner = (
        ch.AutoRebin().SetBinThreshold(config["rebinThreshold"])
        .SetBinUncertFraction(1/np.sqrt(config["rebinNeff"]))
        .SetRebinMode(2)
    )
    for cat in config["categories"]:
        b = cat[1]
        rebinner.Rebin(cb.cp().bin([b]).backgrounds(), cb)

    if not args.bb_lite:
        bbb = ch.BinByBinFactory().SetAddThreshold(0)
        bbb.AddBinByBin(cb.cp().backgrounds(), cb)
        bbb.AddBinByBin(cb.cp().signals(), cb)
    else:
        cb.SetAutoMCStats(cb, config["autoMCStatsThresh"], False, 1)

    plot_opts = config["default_plot_options"]
    plot_opts["signal_mass_re"] = config["signal_mass_re"]
    if "combine_shapes" in config["plot_options"]:
        plot_opts.update(config["plot_options"]["combine_shapes"])


    # ch.SetStandardBinNames(cb)
    write_dcards(
        config["name"],
        output_dir,
        cb,
        args.separate,
        years,
        lumis,
        lumi_by_era = era_lumis,
        hasBBlite = args.bb_lite,
        **plot_opts
    )
    return

if __name__ == "__main__":
    args = parse_args()
    main(args)
