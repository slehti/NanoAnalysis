#!/usr/bin/env python3

############################################################
############################################################
## Script modified from CombinedLimit/scripts/plotGof.py ##
############################################################
############################################################

from array import array
import ROOT
import nanoanalysis.tools.plotting as plot
import argparse
import json
from six.moves import range
from nanoanalysis.tools.tdrstyle import setTDRStyle

parser = argparse.ArgumentParser()
parser.add_argument("input", help="""Input json file""")
parser.add_argument(
    "--output",
    "-o",
    default="",
    help="""Name of the output
    plot without file extension""",
)
parser.add_argument("--mass", default="160.0", help="""Higgs Boson mass to be used""")
parser.add_argument(
    "--warn", default=True, action=argparse.BooleanOptionalAction,
    help="""Warn if observed test statistic is out of bounds"""
)
parser.add_argument("--statistic", default="saturated", help="""Used Test Statistic""")
parser.add_argument("-i", "--info", default=None, help="""Custom text under the legend""")
parser.add_argument("--x-title", default="Goodness of Fit", help="""Title for the x-axis""")
parser.add_argument("--y-title", default="Number of Toys", help="""Title for the y-axis""")
parser.add_argument("--cms-sub", default="Internal", help="""Text below the CMS logo""")
parser.add_argument("--title-right", default="", help="""Right header text above the frame""")
parser.add_argument("--title-left", default="", help="""Left header text above the frame""")
parser.add_argument("--pad-style", default=None, help="""Extra style options for the pad, e.g. Grid=(1,1)""")
parser.add_argument("--auto-style", nargs="?", const="", default=None, help="""Take line colors and styles from a pre-defined list""")
parser.add_argument("--table_vals", help="Amount of values to be written in a table for different masses", default=10)
parser.add_argument("--bins", default=100, type=int, help="Number of bins in histogram")
parser.add_argument("--lineheight", default=0.04, type=int, help="Size of legend texts")
parser.add_argument("--range", nargs=2, type=float, help="Range of histograms. Requires two arguments in the form of <min> <max>")
parser.add_argument(
    "--percentile",
    nargs=2,
    type=float,
    help="Range of percentile from the distribution to be included. Requires two arguments in the form of <min> <max>. Overrides range option.",
)
args = parser.parse_args()
lineheight = args.lineheight

def DrawTitle(pad, text, align, textOffset=0.2, textSize=0.6):
    pad_backup = ROOT.gPad
    pad.cd()
    t = pad.GetTopMargin()
    l = pad.GetLeftMargin()
    r = pad.GetRightMargin()

    pad_ratio = (float(pad.GetWh()) * pad.GetAbsHNDC()) / (float(pad.GetWw()) * pad.GetAbsWNDC())
    if pad_ratio < 1.0:
        pad_ratio = 1.0

    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextAngle(0)
    latex.SetTextColor(ROOT.kBlack)
    latex.SetTextFont(42)
    latex.SetTextSize(textSize * t * pad_ratio)

    y_off = 1 - t + textOffset * t
    if align == 1:
        latex.SetTextAlign(11)
    if align == 1:
        latex.DrawLatex(l, y_off, text)
    if align == 2:
        latex.SetTextAlign(21)
    if align == 2:
        latex.DrawLatex(l + (1 - l - r) * 0.5, y_off, text)
    if align == 3:
        latex.SetTextAlign(31)
    if align == 3:
        latex.DrawLatex(1 - r, y_off, text)
    pad_backup.cd()

def DrawAxisHists(pads, axis_hists, def_pad=None):
    for i, pad in enumerate(pads):
        pad.cd()
        axis_hists[i].Draw("AXIS")
        axis_hists[i].Draw("AXIGSAME")
    if def_pad is not None:
        def_pad.cd()

def ToyTGraphFromJSON(js, label):
    xvals = []
    yvals = []
    if isinstance(label, (str,)):
        for entry in js[label]:
            xvals.append(float(entry))
            yvals.append(1.0)
    else:
        if len(label) == 1:
            return ToyTGraphFromJSON(js, label[0])
        else:
            return ToyTGraphFromJSON(js[label[0]], label[1:])
    graph = ROOT.TGraph(len(xvals), array("d", xvals), array("d", yvals))
    graph.Sort()
    return graph

def makeHist1D(name, xbins, graph, scaleXrange=1.0, absoluteXrange=None):
    len_x = graph.GetX()[graph.GetN() - 1] - graph.GetX()[0]
    binw_x = (len_x * 0.5 / (float(xbins) - 1.0)) - 1e-5
    if absoluteXrange:
        hist = ROOT.TH1F(name, "", xbins, absoluteXrange[0], absoluteXrange[1])
    else:
        hist = ROOT.TH1F(name, "", xbins, graph.GetX()[0], scaleXrange * (graph.GetX()[graph.GetN() - 1] + binw_x))
    return hist

## Boilerplate
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(ROOT.kTRUE)
setTDRStyle()
ROOT.gStyle.SetNdivisions(510, "XYZ")  # probably looks better

canv = ROOT.TCanvas(args.output, args.output)
canv.cd()

pad = ROOT.TPad("toy pad", "toy pad", 0,0,1,1)
pad.Draw()
pad.cd()

# Set the style options of the pads
# Use tick marks on oppsite axis edges
plot.Set(pad, Tickx=1, Ticky=1)
if args.pad_style is not None:
    settings = {x.split("=")[0]: eval(x.split("=")[1]) for x in args.pad_style.split(",")}
    print("Applying style options to the TPad(s):")
    print(settings)
    plot.Set(pad, **settings)

graphs = []
graph_sets = []

if args.info is not None:
    info_lines = args.info.split("\\n")
    info_height = (lineheight+0.01) * len(info_lines)
else:
    info_height = 0.0

pave = ROOT.TPaveText(0.68, 0.82-info_height, 0.90, 0.82, "NDC")
pave.SetBorderSize(0)
pave.SetFillStyle(0)
pave.SetTextSize(lineheight)

#plot.Set(legend, NColumns=2)
if args.info is not None:
    plot.fill_pave(pave, info_lines, alignment=32)

axis = None

defcols = [
    ROOT.kGreen + 3,
    ROOT.kRed,
    ROOT.kBlue,
    ROOT.kBlack,
    ROOT.kYellow + 2,
    ROOT.kOrange + 10,
    ROOT.kCyan + 3,
    ROOT.kMagenta + 2,
    ROOT.kViolet - 5,
    ROOT.kGray,
]

deflines = [1, 2, 3]

if args.auto_style is not None:
    icol = {x: 0 for x in args.auto_style.split(",")}
    icol["default"] = 0
    iline = {}
    iline["default"] = 1
    for i, x in enumerate(args.auto_style.split(",")):
        iline[x] = i + 1

pValue = 0
if args.statistic in ["AD", "KS"]:
    titles = {
        "htt_em_8_13TeV": "e#mu, nobtag",
        "htt_em_9_13TeV": "e#mu, btag",
        "htt_et_8_13TeV": "e#tau_{h}, nobtag",
        "htt_et_9_13TeV": "e#tau_{h}, btag",
        "htt_mt_8_13TeV": "#mu#tau_{h}, nobtag",
        "htt_mt_9_13TeV": "#mu#tau_{h}, btag",
        "htt_tt_8_13TeV": "#tau_{h}#tau_{h}, nobtag",
        "htt_tt_9_13TeV": "#tau_{h}#tau_{h}, btag",
    }
    with open(args.input) as jsfile:
        js = json.load(jsfile)
    for key in js[args.mass]:  ## these are the channels
        # title = key if key not in titles else titles[key]
        title = titles.get(key, key)
        # if key not in titles:
        #     continue
        toy_graph = ToyTGraphFromJSON(js, [args.mass, key, "toy"])
        if args.percentile:
            min_range = toy_graph.GetX()[int(toy_graph.GetN() * args.percentile[0])]
            max_range = toy_graph.GetX()[int(toy_graph.GetN() * args.percentile[1])]
            toy_hist = makeHist1D("toys", args.bins, toy_graph, absoluteXrange=(min_range, max_range))
        elif args.range:
            toy_hist = makeHist1D("toys", args.bins, toy_graph, absoluteXrange=args.range)
        else:
            toy_hist = makeHist1D("toys", args.bins, toy_graph, 1.15)
        for i in range(toy_graph.GetN()):
            toy_hist.Fill(toy_graph.GetX()[i])
        toy_hist.GetYaxis().SetTitle("Entries")
        pValue = js[args.mass][key]["p"]
        obs = ToyTGraphFromJSON(js, [args.mass, key, "obs"])
        arr = ROOT.TArrow(obs.GetX()[0], 0.001, obs.GetX()[0], toy_hist.GetMaximum() / 8, 0.02, "<|")
        arr.SetLineColor(ROOT.kBlue)
        arr.SetFillColor(ROOT.kBlue)
        arr.SetFillStyle(1001)
        arr.SetLineWidth(6)
        arr.SetLineStyle(1)
        arr.SetAngle(60)
        toy_hist.Draw()
        arr.Draw("<|same")
        pad.RedrawAxis()
        pad.RedrawAxis("g")
        pad.GetFrame().Draw()

        # axis[0].GetYaxis().SetTitle(args.y_title)
        # axis[0].GetXaxis().SetTitle(args.x_title)
        # axis[0].GetXaxis().SetLabelOffset(axis[0].GetXaxis().GetLabelOffset()*2)

        y_min, y_max = plot.get_pad_ylim(pad)
        plot.FixBothRanges(pad, 0, 0, y_max, 0.25)

        ratio_graph_sets = []
        ratio_graphs = []

        pad.cd()

        #box = ROOT.TPave(pad.GetLeftMargin(), 0.81, 1 - pad.GetRightMargin(), 1 - pad.GetTopMargin(), 1, "NDC")
        #box.Draw()

        pave.Draw()
        logotext, ipos = ("", 0) if args.cms_sub.lower() == "private work" else ("CMS", 10)

        plot.DrawCMSLogo(pad, logotext, args.cms_sub, 0, 0.045, 0.035, 1.2, "", 0.8)
        DrawTitle(pad, args.title_right, 3)
        DrawTitle(pad, title, 1)

        textlabel = ROOT.TPaveText(0.68, 0.88, 0.90, 0.92, "NDC")
        textlabel.SetBorderSize(0)
        textlabel.SetFillStyle(0)
        textlabel.SetTextAlign(32)
        textlabel.SetTextSize(lineheight)
        textlabel.SetTextColor(1)
        textlabel.SetTextFont(42)
        textlabel.AddText(args.statistic + ", %s Toys" % (toy_graph.GetN()))
        textlabel.Draw()

        pvalue = ROOT.TPaveText(0.68, 0.83, 0.90, 0.87, "NDC")
        pvalue.SetBorderSize(0)
        pvalue.SetFillStyle(0)
        pvalue.SetTextAlign(32)
        pvalue.SetTextSize(lineheight)
        pvalue.SetTextColor(1)
        pvalue.SetTextFont(42)
        pvalue.AddText("p-value = %0.3f" % pValue)
        pvalue.Draw()

        canv.Print(key + args.output + ".pdf")
        canv.Print(key + args.output + ".png")

        # some cleaning up
        del toy_hist

else:
    with open(args.input) as jsfile:
        js = json.load(jsfile)
    # graph_sets.append(plot.StandardLimitsFromJSONFile(file, args.show.split(',')))
    toy_graph = ToyTGraphFromJSON(js, [args.mass, "toy"])
    if args.percentile:
        min_range = toy_graph.GetX()[int(toy_graph.GetN() * args.percentile[0])]
        max_range = toy_graph.GetX()[int(toy_graph.GetN() * args.percentile[1])]
        toy_hist = makeHist1D("toys", args.bins, toy_graph, absoluteXrange=(min_range, max_range))
    elif args.range:
        toy_hist = makeHist1D("toys", args.bins, toy_graph, absoluteXrange=args.range)
    else:
        toy_hist = makeHist1D("toys", args.bins, toy_graph, 1.15)
    for i in range(toy_graph.GetN()):
        toy_hist.Fill(toy_graph.GetX()[i])
    toy_hist.GetYaxis().SetTitle("Entries")
    pValue = js[args.mass]["p"]
    underflow_count = toy_hist.GetBinContent(0)
    overflow_count = toy_hist.GetBinContent(args.bins + 1)
    obs = ToyTGraphFromJSON(js, [args.mass, "obs"])
    arr = ROOT.TArrow(obs.GetX()[0], 0.001, obs.GetX()[0], toy_hist.GetMaximum() / 8, 0.02, "<|")
    # if axis is None:
    # axis = plot.CreateAxisHists(1, graph_sets[-1].values()[0], True)
    # DrawAxisHists(pads, axis, pad)
    arr.SetLineColor(ROOT.kBlue)
    arr.SetFillColor(ROOT.kBlue)
    arr.SetFillStyle(1001)
    arr.SetLineWidth(6)
    arr.SetLineStyle(1)
    arr.SetAngle(60)
    toy_hist.Draw()
    arr.Draw("<|same")
    pad.RedrawAxis()
    pad.RedrawAxis("g")
    pad.GetFrame().Draw()
    # axis[0].GetYaxis().SetTitle(args.y_title)
    # axis[0].GetXaxis().SetTitle(args.x_title)
    # axis[0].GetXaxis().SetLabelOffset(axis[0].GetXaxis().GetLabelOffset()*2)

    y_min, y_max = plot.get_pad_ylim(pad)
    plot.FixBothRanges(pad, 0, 0, y_max, 0.25)

    ratio_graph_sets = []
    ratio_graphs = []

    pad.cd()

    #box = ROOT.TPave(pad.GetLeftMargin(), 0.81, 1 - pad.GetRightMargin(), 1 - pad.GetTopMargin(), 1, "NDC")
    #box.Draw()

    pave.Draw()
    logotext, ipos = ("", 0) if args.cms_sub.lower() == "private work" else ("CMS", 10)


    plot.DrawCMSLogo(pad, logotext, args.cms_sub, ipos, 0.045, 0.035, 1.2, "", 0.8)
    DrawTitle(pad, args.title_right, 3)
    DrawTitle(pad, args.title_left, 1)

    textlabel = ROOT.TPaveText(0.68, 0.88, 0.90, 0.92, "NDC")
    textlabel.SetBorderSize(0)
    textlabel.SetFillStyle(0)
    textlabel.SetTextAlign(32)
    textlabel.SetTextSize(lineheight)
    textlabel.SetTextColor(1)
    textlabel.SetTextFont(42)
    textlabel.AddText(args.statistic + ", %s Toys" % (toy_graph.GetN()))
    textlabel.Draw()

    pvalue = ROOT.TPaveText(0.68, 0.83, 0.90, 0.87, "NDC")
    pvalue.SetBorderSize(0)
    pvalue.SetFillStyle(0)
    pvalue.SetTextAlign(32)
    pvalue.SetTextSize(lineheight)
    pvalue.SetTextColor(1)
    pvalue.SetTextFont(42)
    pvalue.AddText("p-value = %0.3f" % pValue)
    pvalue.Draw()

    if args.warn:
        arrow_not_in_range = (obs.GetX()[0] > toy_hist.GetBinLowEdge(args.bins + 1)) or (obs.GetX()[0] < toy_hist.GetBinLowEdge(0))

        warningtext1 = ROOT.TPaveText(0.68, 0.78, 0.90, 0.82, "NDC")
        warningtext1.SetBorderSize(0)
        warningtext1.SetFillStyle(0)
        warningtext1.SetTextAlign(32)
        warningtext1.SetTextSize(lineheight)
        warningtext1.SetTextColor(2)
        warningtext1.SetTextFont(62)

        if arrow_not_in_range and ((underflow_count != 0) or (overflow_count != 0)):
            warningstrings = []
            if underflow_count != 0:
                warningstrings.append("%d underflow" % underflow_count)
            if overflow_count != 0:
                warningstrings.append("%d overflow" % overflow_count)
            warningtext1.AddText(", ".join(warningstrings))
            warningtext1.Draw()

            warningtext2 = ROOT.TPaveText(0.68, 0.73, 0.90, 0.77, "NDC")
            warningtext2.SetBorderSize(0)
            warningtext2.SetFillStyle(0)
            warningtext2.SetTextAlign(32)
            warningtext2.SetTextSize(lineheight)
            warningtext2.SetTextColor(2)
            warningtext2.SetTextFont(62)
            warningtext2.AddText("observed value not in range")
            warningtext2.Draw()
        else:
            if (underflow_count != 0) or (overflow_count != 0):
                warningstrings = []
                if underflow_count != 0:
                    warningstrings.append("%d underflow" % underflow_count)
                if overflow_count != 0:
                    warningstrings.append("%d overflow" % overflow_count)
                warningtext1.AddText(", ".join(warningstrings))
            elif arrow_not_in_range:
                warningtext1.AddText("observed value not in range")
            warningtext1.Draw()

    canv.Print(".pdf")
    canv.Print(".png")

ROOT.gROOT.SetBatch(ROOT.kTRUE)

