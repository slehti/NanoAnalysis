from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os

from nanoanalysis import aux
from nanoanalysis import condor

def parse_args():
    parser = ArgumentParser(
        "interface to Combine -M AsymptoticLimits",
        description = (
            "Interface to run AsymptotocLimits on a set of datacards "
            "generated with pseudo2dcards. Unknown arguments will be passed "
            "through to Combine. This can be used for example to freeze "
            "systematics."
        ),
        formatter_class = ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "inputs", help="input datacard directory, the output of pseudos2dcards.py"
    )
    parser.add_argument(
        "-n", "--n-processes", type=int, help="Number of parallel Combine instances", default=4
    )
    parser.add_argument(
        "-r", "--run-type", help=(
            "ONLY MODIFY WHEN YOU HAVE APPROVAL FOR UNBLINDING!"
            "Combine --run argument. One of both, observed, expected or blind"
        ), default="blind"
    )
    parser.add_argument(
        "--condor-job", action="store_true", help="Submit fit jobs to htcondor"
    )
    parser.add_argument(
        "--crab-job", action="store_true", help="Submit fit jobs to grid"
    )
    return parser.parse_known_args()

def main(args, unknowns):
    if args.condor_job:
        #TODO: configurable job lifetime requirement
        submit_args = (
            "--job-mode condor --sub-opts='+JobFlavour=\"workday\"'"
            " --task-name .limits."
        )
    elif args.crab_job:
        raise NotImplementedError #FIXME
        submit_args = ""
    else:
        submit_args = f"--parallel {args.n_processes}"

    os.chdir(args.inputs)
    aux.execute("combineTool.py -M T2W -i ./* -o workspace_limits.root")
    aux.execute(
        f"combineTool.py -M AsymptoticLimits -d */workspace_limits.root --there -n .limit "
        f"{submit_args} --run {args.run_type} {' '.join(unknowns)}")
    if args.condor_job:
        condor.wait()
    print(f"Calculated limits stored in their respective subdirectories.")
    return


if __name__ == "__main__":
    args, unknowns = parse_args()
    main(args, unknowns)
