from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, BooleanOptionalAction
import os
from tempfile import NamedTemporaryFile
import json

from nanoanalysis import aux
from nanoanalysis import condor
import nanoanalysis.limits.config as limit_config
from nanoanalysis.limits.systematics import get_translations
from nanoanalysis.limits.dcard_dir import for_each_mass

def parse_args():
    parser = ArgumentParser(
        prog="Interface to Combine for impact plot generation",
        formatter_class = ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "inputs", help="input datacard directory, the output of pseudos2dcards.py"
    )
    parser.add_argument(
        "-c", "--config", help="Datacard configuration file for creating readable process labels",
        default=None,
    )
    parser.add_argument(
        "-o", "--output", help="name of the generated impacts json and pdf files", default="impacts"
    )
    parser.add_argument(
        "--exclude", help="Exclusion patterns to combineTool.py Impacts", default=""
    )
    parser.add_argument(
        "-p", "--plot-opts", help="Arguments for plotImpacts.py", default="--summary"
    )
    parser.add_argument(
        "--rmin", type=float, help="POI lower bound", default=-1
    )
    parser.add_argument(
        "--rmax", type=float, help="POI upper bound", default=1
    )
    parser.add_argument(
        "-n", "--n-processes", type=int, help="Number of parallel Combine instances", default=4
    )
    parser.add_argument(
        "--unblinded", action="store_true", help="ONLY WITH APPROVAL For final analysis, "\
            "calculate observed pulls instead of a background only toy dataset."
    )
    parser.add_argument(
        "--condor-job", action="store_true", help="Submit fit jobs to htcondor"
    )
    parser.add_argument(
        "--crab-job", action="store_true", help="Submit fit jobs to grid"
    )
    parser.add_argument(
        "-i", "--skip-initial-fit", action="store_true",
        help="Specify this flag if the initial fits have already been calculated"\
            " to avoid recomputing them."
    )
    parser.add_argument(
        "-f", "--skip-fits", action="store_true",
        help="Specify this flag if the parameter fits have already been calculated"\
            " to avoid recomputing them."
    )
    parser.add_argument(
        "-w", "--skip-workspace-creation", action="store_true",
        help="Specify this flag if the impacts workspaces have already been created"\
            " to avoid recomputing them."
    )
    parser.add_argument(
        "--stats", action=BooleanOptionalAction, default=True,
        help="Specify whether bin statistical uncertainty impacts should be included"
    )
    return parser.parse_args()

def main(args):
    config = limit_config.load_config(args.config)
    proc_labels = config["process_labels"]

    job_opts = ""
    if args.condor_job:
        submit_args = "--job-mode condor --sub-opts='+JobFlavour=\"espresso\"'"
    elif args.crab_job:
        raise NotImplementedError #FIXME
        submit_args = ""
    else:
        submit_args = f"--parallel {args.n_processes}"

    # Do not consider statistical uncertainty in the NP impacts
    # first pattern excludes full Template based bin-by-bin uncertainties,
    # second matches BB-lite type uncertainties
    base_command = (
        "combineTool.py -M Impacts -d workspace_impacts.root -n .impact "
        f"--rMin {args.rmin} --rMax {args.rmax} --robustFit 1"
    )
    exclusions = args.exclude
    if not args.stats:
        exclusions = "rgx{{_bin_}},rgx{{_bin\\d+(_|$)}}" + args.exclude
    base_command += f" --exclude \"{exclusions}\""
    if not args.unblinded:
        base_command += " -t -1 --expectSignal 0"

    os.chdir(args.inputs)
    if not args.skip_workspace_creation:
        aux.execute(
            f"combineTool.py -M T2W -i ./* -o workspace_impacts.root --parallel {args.n_processes}",
            echo=True
        )

    # Run the initial best fit and uncertainty estimation for the POI
    if not args.skip_initial_fit:
        cmd = f"{base_command} -m {{mass}} --doInitialFit {submit_args}"
        for_each_mass(cmd)
        if args.condor_job:
            condor.wait()

    # Run the parameter impact fits
    if not args.skip_fits:
        cmd = f"{base_command} -m {{mass}} --doFits {submit_args}"
        for_each_mass(cmd)
        if args.condor_job:
            condor.wait()

    # Collect results into .json format and plot
    translations = get_translations(proc_labels)
    with NamedTemporaryFile(mode="w+") as f:
        json.dump(translations, f, indent=2)
        f.flush()
        translation_file = f.name

        cmd = (
            f"{base_command} -m {{mass}} --output {args.output}.json; "
            f"plotImpacts.py -i {args.output}.json -o {args.output} "
            f"{args.plot_opts} -t {translation_file}"
        )
        for_each_mass(cmd)

    print("run_impacts.py done, check above for errors.")
    return


if __name__ == "__main__":
    args = parse_args()
    main(args)

