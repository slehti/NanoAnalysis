from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import ctypes
import os
import json
from array import array
import math

import ROOT

from nanoanalysis import aux
import nanoanalysis.tools.tdrstyle as tdrstyle
import nanoanalysis.tools.plotting as plotting
import nanoanalysis.limits.dcard_description as dcard_description

ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(ROOT.kTRUE)
tdrstyle.setTDRStyle()

def parse_args():
    parser = ArgumentParser(
        description="Create limit plots from json limit files",
        formatter_class=ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "input", help="Directory containing calculated limits"\
                      "from Combine, divided into subdirectories for each mass point",
    )
    parser.add_argument(
        "-s", "--subtext", help="Text under the CMS logo in the plot", default="Internal"
    )
    parser.add_argument(
        "-d", "--description", help="Text under the plot legend",
        default=None
    )
    parser.add_argument(
        "-o", "--output", help="name to use for the generated plots", default="limit"
    )
    parser.add_argument(
        "-x", help="X axis variable name", default="m_{H}"
    )
    parser.add_argument(
        "-y", help="Y axis name, what limit is plotted", default="#sigma"
    )
    parser.add_argument(
        "--logy", help="Use log scaling for the Y axis", action="store_true"
    )
    parser.add_argument(
        "--logx", help="Use log scaling for the X axis", action="store_true"
    )
    parser.add_argument(
        "--unblind", help="Draw the observed limit", action="store_true"
    )
    return parser.parse_args()

def StyleLimitBand(graph_dict, overwrite_style_dict=None):
    style_dict = {
            'obs' : { 'LineWidth' : 2},
            'exp0' : {
                'LineWidth' : 2,
                'LineColor' : ROOT.kBlack,
                'LineStyle': 2,
            },
            'exp1' : { 'FillColor' : ROOT.TColor.GetColor("#607641")},
            'exp2' : { 'FillColor' : ROOT.TColor.GetColor("#F5BB54")}
            }
    if overwrite_style_dict is not None:
        for key in overwrite_style_dict:
            if key in style_dict:
                style_dict[key].update(overwrite_style_dict[key])
            else:
                style_dict[key] = overwrite_style_dict[key]
    for key in graph_dict:
        plotting.Set(graph_dict[key],**style_dict[key])

def DrawLimitBand(pad, graph_dict, draw=['exp2', 'exp1', 'exp0', 'obs'], draw_legend=None,
                  legend=None, legend_overwrite=None):
    legend_dict = {
        'obs' : { 'Label' : 'Observed', 'LegendStyle' : 'LP', 'DrawStyle' : 'PLSAME'},
        'exp0' : { 'Label' : 'Median expected', 'LegendStyle' : 'L', 'DrawStyle' : 'LSAME'},
        'exp1' : { 'Label' : '68% expected', 'LegendStyle' : 'F', 'DrawStyle' : '3SAME'},
        'exp2' : { 'Label' : '95% expected', 'LegendStyle' : 'F', 'DrawStyle' : '3SAME'}
    }
    if legend_overwrite is not None:
        for key in legend_overwrite:
            if key in legend_dict:
                legend_dict[key].update(legend_overwrite[key])
            else:
                legend_dict[key] = legend_overwrite[key]
    pad.cd()
    for key in draw:
        if key in graph_dict:
            graph_dict[key].Draw(legend_dict[key]['DrawStyle'])
    if legend is not None:
        if draw_legend is None:
            draw_legend = reversed(draw)
        for key in draw_legend:
            if key in graph_dict:
                legend.AddEntry(graph_dict[key],legend_dict[key]['Label'],legend_dict[key]['LegendStyle'])

def LimitTGraphFromJSON(js, label):
    xvals = []
    yvals = []
    for key in js:
        xvals.append(float(key))
        yvals.append(js[key][label])
    graph = ROOT.TGraph(len(xvals), array('d', xvals), array('d', yvals))
    graph.Sort()
    return graph

def LimitBandTGraphFromJSON(js, central, lo, hi):
    xvals = []
    yvals = []
    yvals_lo = []
    yvals_hi = []
    for key in js:
        xvals.append(float(key))
        yvals.append(js[key][central])
        yvals_lo.append(js[key][central] - js[key][lo])
        yvals_hi.append(js[key][hi] - js[key][central])
    graph = ROOT.TGraphAsymmErrors(len(xvals), array('d', xvals), array('d', yvals), array(
        'd', [0]), array('d', [0]), array('d', yvals_lo), array('d', yvals_hi))
    graph.Sort()
    return graph

def StandardLimitsFromJSONFile(json_file, draw=['obs', 'exp0', 'exp1', 'exp2']):
    graphs = {}
    data = {}
    with open(json_file) as jsonfile:
        data = json.load(jsonfile)
    if 'obs' in draw:
        graphs['obs'] = LimitTGraphFromJSON(data, 'obs')
    if 'exp0' in draw or 'exp' in draw:
        graphs['exp0'] = LimitTGraphFromJSON(data, 'exp0')
    if 'exp1' in draw or 'exp' in draw:
        graphs['exp1'] = LimitBandTGraphFromJSON(data, 'exp0', 'exp-1', 'exp+1')
    if 'exp2' in draw or 'exp' in draw:
        graphs['exp2'] = LimitBandTGraphFromJSON(data, 'exp0', 'exp-2', 'exp+2')
    return graphs


def main(args):

    # Get limits as JSON file
    os.chdir(args.input)
    aux.execute("combineTool.py -M CollectLimits */*.limit.* -o limits.json")

    # get metadata from datacards
    metadata = dcard_description.parse_dcard_descriptions(".")
    lumis_by_era = dcard_description.lumis_by_era(metadata)

    # Style and pads
    canv = ROOT.TCanvas(args.output, args.output)
    pad = ROOT.TPad('pad', 'pad', 0., 0., 1., 1.)
    pad.Draw()
    pad.cd()

    draw = ["exp0", "exp1", "exp2"]
    if args.unblind:
        draw.append("obs")
    # Get limit TGraphs as a dictionary
    graphs = StandardLimitsFromJSONFile("limits.json", draw=draw)

    # Create an empty TH1 from the first TGraph to serve as the pad axis and frame
    axis = plotting.create_axis_hist(list(graphs.values())[0])
    axis.GetXaxis().SetTitle(args.x)
    axis.GetYaxis().SetTitle(args.y)
    pad.cd()
    axis.Draw('axis')

    # Create a legend in the top right
    description_lines = args.description.split("\\n")
    description_height = 0.04 * len(description_lines)
    legend, pave = plotting.positioned_legend(0.3, 0.2, 3, 0.015, description_height=description_height)
    legend.SetHeader("95% CL upper limits")
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)

    # Set the standard green and yellow colors and draw
    StyleLimitBand(graphs)
    DrawLimitBand(pad, graphs, legend=legend)

    # finalize legend with description of analysis
    plotting.fill_pave(pave, description_lines)
    legend.Draw()

    # Re-draw the frame and tick marks
    pad.RedrawAxis()

    # Adjust the y-axis range such that the maximum graph value sits 25% below
    # the top of the frame. Fix the minimum to zero or 0.9*pad_minimum for logy.
    ymin, ymax = plotting.get_pad_ylim(pad)
    if args.logx:
        pad.SetLogx()
    if args.logy:
        pad.SetLogy()
        plotting.FixBothRanges(pad, ymin, 0.1, ymax, 0.25)
    else:
        plotting.FixBothRanges(pad, 0,0, ymax, 0.25)
    pad.GetFrame().Draw()

    # Logo text
    if args.subtext.lower() == "private work":
        logotext = ""
        ipos = 0
    else:
        logotext = "CMS"
        ipos = 10

    plotting.DrawCMSLogo(pad, logotext, args.subtext, ipos, 0.045, 0.035, 1.2, '', 0.8)

    # Lumi text
    plotting.CMS_lumi(pad, lumis_by_era)

    canv.Print('.pdf')
    canv.Print('.png')


if __name__ == "__main__":
    args = parse_args()
    main(args)

