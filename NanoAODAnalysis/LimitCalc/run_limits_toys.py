from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os

from nanoanalysis import aux
from nanoanalysis import condor

def parse_args():
    parser = ArgumentParser(
        "interface to Combine",
        formatter_class = ArgumentDefaultsHelpFormatter,
        description = (
            "Run toy-based LHC style limit calculations."
            "Custom arguments to Combine are passed through this"
            "interface even if they are not explicitly listed here."
            "Example: \"python run_limits.py datacards_Hplus2018/combined/2000/ --condor-job \\"
            "--freezeParameters CMS_topPtReweight --setParameters CMS_topPtReweight=-1\""
            "In the example, the top pt reweighting systematic is set and frozen to the -1sigma"
            "value, effectively disabling the reweighting if the -1sigma variation corresponds to "
            "the histogram where no reweighting has been applied, and limits are calculated "
            "for the mH+=2000 GeV sample. This script is intended to be used as validation for "
            "the results of run_limits.py"
        )
    )

    parser.add_argument(
        "input", help="input datacard directory containing a datacard for a single mass point, from the output of pseudos2dcards.py"
    )
    parser.add_argument(
        "-n", "--n-processes", type=int, help="Number of parallel Combine instances", default=4
    )
    parser.add_argument(
        "-t", "--ntoys", type=int, help="Number of generated toys", default=500
    )
    parser.add_argument(
        "-r", "--run-type", help=(
            "ONLY MODIFY WHEN YOU HAVE APPROVAL FOR UNBLINDING!"
            "Combine --run argument. One of both, observed, expected or blind"
        ), default="blind"
    )
    parser.add_argument(
        "--condor-job", action="store_true", help="Submit fit jobs to htcondor"
    )
    parser.add_argument(
        "--crab-job", action="store_true", help="Submit fit jobs to grid"
    )
    parser.add_argument(
        "--mu-range", help="range of signal hypotheses to test in format <lo>:<hi>:<step>", default="0.0:1.0:0.01"
    )
    return parser.parse_known_args()

def main(args, unknown):
    input(
        "WARNING: this script is untested, this is dangerous especially for a "
        "blind analysis. Only continue if you are sure of what you are doing "
        "AND you understand perfectly each command and argument this script will execute!"
    )
    if args.condor_job:
        #TODO: configurable job lifetime requirement
        submit_args = (
            "--job-mode condor --sub-opts='+JobFlavour=\"workday\"'"
            " --task-name .limits_toys."
        )
    elif args.crab_job:
        raise NotImplementedError #FIXME
        submit_args = ""
    else:
        submit_args = f"--parallel {args.n_processes}"

    bilnd_string=""
    collect_args = [f"--expectedFromGrid {p}" for p in [0.025 0.16 0.84 0.975]]
    if args.run_type == "blind": # run blinded expected limits
        blind_string = " -t -1"
    elif args.run_type == "exp": # run only expected limits, NOT BLIND TO DATA
        pass
    elif args.run_type == "obs": # run only observed limit
        collect_args = [""]
    elif args.run_type == "both": # first run expected limits, finally observed
        collect_args.append("")

    os.chdir(args.input)
    aux.execute("combineTool.py -M T2W -i datacard.txt -o workspace.root")
    if not args.condor_job:
        print("warning: Limit calculation with toys is computationally intensive, this will take a while...")
    cmd = (
        f"combineTool.py -M HybridNew -d workspace.root -n .limitFull {submit_args} "
        "--LHCmode LHC-limits --singlePoint {args.mu_range} --clsAcc 0 -s -1 "
        "--saveHybridResult -T {args.ntoys}{blind_string}"
    )
    aux.execute(cmd)
    if args.condor_job:
        condor.wait()
    # collect results
    for collect in collect_args:
        aux.execute("combine datacard.txt -M HybridNew --LHCmode LHC-limits --readHybridResults --grid=merged.root {collect}")

    print("run_limits_toys.py done.")
    return


if __name__ == "__main__":
    args, unknown = parse_args()
    main(args, unknown)
