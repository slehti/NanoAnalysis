"""
Work-in-progress: directly plot shapes from a datacard directory

Not currently the focus, since we would also like to be able to
plot shapes of other variables than only the one used in the limit
calculation.

"""
import argparse
import os

import ROOT

import nanoanalysis.tools.tdrstyle as tdrstyle

def parse_args():
    parser = argparse.ArgumentParser(
        description='Plot prefit histograms from a Combine workspace'
    )
    parser.add_argument(
        'workspace', help='Path to the Combine workspace file'
    )
    parser.add_argument(
        '-d', '--datacard', default='datacard.txt', help='Path to the Combine datacard'
    )
    parser.add_argument(
        '-o', '--output', default='prefit_histograms', help='Output directory for the histograms'
    )
    return parser.parse_args()

def plot_prefit_histograms(workspace_file, datacard_file, output_dir):
    os.system(f"PostFitShapesFromWorkspace -w {workspace_file} -d {datacard_file} --output {output_dir}/shapes.root -m 1500")
    # Open the workspace file
    style = tdrstyle.TDRStyle()
    with ROOT.TFile(f"{output_dir}/shapes.root") as f:


        # Iterate over all the bins in the prefit_shapes directory
        for bin_key in f.GetListOfKeys():
            bin_name = bin_key.GetName()
            bin_dir = f.Get(bin_name)

            # Create a canvas for each bin
            canvas = ROOT.TCanvas(f'canvas_{bin_name}', bin_name, 800, 600)
            stack = ROOT.THStack(f'stack_{bin_name}', bin_name)

            # Iterate over all the histograms (backgrounds) in the bin directory
            for hist_key in bin_dir.GetListOfKeys():
                hist_name = hist_key.GetName()
                if 'data' in hist_name or 'total' in hist_name or "_M" in hist_name:
                    continue  # Skip data, sig and total background histograms

                hist = bin_dir.Get(hist_name)
                hist.SetTitle(hist_name)
                stack.Add(hist)

            # Draw the stack
            stack.Draw('hist')
            stack.GetXaxis().SetTitle('Observable')
            stack.GetYaxis().SetTitle('Events')

            # Draw the legend
            legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
            for hist in stack.GetHists():
                legend.AddEntry(hist, hist.GetName(), 'f')
            legend.Draw()

            # Save the canvas to a file
            canvas.SaveAs(f'{output_dir}/{bin_name}_prefit.pdf')
            canvas.SaveAs(f'{output_dir}/{bin_name}_prefit.png')

def main(args):
    plot_prefit_histograms(args.workspace, args.datacard, args.output)

if __name__ == "__main__":
    main(parse_args())
