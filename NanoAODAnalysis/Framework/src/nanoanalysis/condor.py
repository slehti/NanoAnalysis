import subprocess
import os
import time

USER = os.getlogin()
CONDOR_KEYS = (
    "owner", "_",      "batch",  "date",    "time",    "n_done",
    "n_run", "n_idle", "n_hold", "n_total", "job_ids"
)

def is_dataline(condor_q_line):
    return USER in condor_q_line and ("Submitter" not in condor_q_line) and ("Total" not in condor_q_line)

def get_jobs():
    info = []
    result = subprocess.run(
        [f"condor_q"],
        stdout=subprocess.PIPE
    )
    out = result.stdout.decode("utf-8")
    for line in out.splitlines():
        if is_dataline(line):
           items = line.split() 
           info.append({k: v for k, v in zip(CONDOR_KEYS, items)})

    info_dict = {i["batch"]: i for i in info}
    return info_dict

def to_int(field):
    if field == "_":
        return 0
    try:
        return int(field)
    except:
        raise ValueError(f"Could not convert {field} from condor_q response to an integer")

def wait(job_names=[]):
    while True:
        job_info = get_jobs()
        if not job_info:
            break
        if len(job_names):
            names = job_names
        else:
            names = job_info.keys()
        any_waiting = False
        for name in names:
            if name in job_info:
                waiting = to_int(job_info[name]["n_run"])
                waiting += to_int(job_info[name]["n_idle"])
                if waiting:
                    any_waiting = True
        if not any_waiting:
            break
        time.sleep(15)
    print("all condor jobs ready, continuing")
    return

