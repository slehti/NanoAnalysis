from coffea import processor
import awkward as ak

class Counters:

    def __init__(self):
        self.counters = {}
        self.counters["unweighted_counter"] = {}
        self.counters["weighted_counter"]   = {}
        self.first = True
        self.counters["unweighted_counter"]["Control"] = 1
        self.counters["weighted_counter"]["Control"]   = 1

    def __add__(self, x):
        for key in self.counters["unweighted_counter"].keys():
            self.counters['unweighted_counter'][key] += x.counters['unweighted_counter'][key]
            self.counters['weighted_counter'][key] += x.counters['weighted_counter'][key]
        return self

    def book(self,counterhisto):
        self.counterhisto = counterhisto
        self.setSkimCounter()

    def setAccumulatorIdentity(self,identity):
        self.identity = identity

    def setSkimCounter(self):
        if not self.first or not hasattr(self,'counterhisto'): return
        values = self.counterhisto.values()
        for i in range(len(values)):
            label = self.counterhisto._axis().value(i)
            self.counters["unweighted_counter"][label] = int(values[i])
            self.counters["weighted_counter"][label] = values[i]
        self.first = False

    def increment(self,label,events):
        if not label in list(self.counters["unweighted_counter"].keys()):
            self.counters["unweighted_counter"][label] = 0
            self.counters["weighted_counter"][label]   = 0
        self.counters["unweighted_counter"][label] += ak.num(events.event, axis=0)
        if hasattr(events,"weight"):
            self.counters["weighted_counter"][label]   += ak.sum(events.weight)
        else:
            self.counters["weighted_counter"][label]   += ak.num(events.event, axis=0)

    def get(self):
        return self.counters

    def print(self):
        print("    Counters                  unw.           weighted")
        control = self.counters['unweighted_counter']["Control"]
        for k in self.counters['unweighted_counter'].keys():
            counter = "     "+k
            while len(counter) < 30:
                counter+=" "
            count = self.counters["unweighted_counter"][k]
            wcount = self.counters['weighted_counter'][k]
            if "Skim" in k:
                count = int(count/control)
                wcount = wcount/control
            counter +="%s"%(count)
            while len(counter) < 45:
                counter+=" "
            counter +="%s"%round(wcount,1)
            print(counter)
    """
    def histo(self):
        import ROOT
        n = len(self.counters['unweighted_counter'].keys())
        h_uCounter = ROOT.TH1F("unweighted_counter","",n,1,n)
        h_wCounter = ROOT.TH1F("weighted_counter","",n,1,n)
        for i,k in enumerate(self.counters['unweighted_counter'].keys()):
            h_uCounter.GetXaxis().SetBinLabel(i+1,k)
            h_uCounter.SetBinContent(i+1,self.counters['unweighted_counter'][k])
            h_wCounter.GetXaxis().SetBinLabel(i+1,k)
            h_wCounter.SetBinContent(i+1,self.counters['weighted_counter'][k])
        return h_uCounter,h_wCounter
    """
    def histo(self):
        import hist
        n = len(self.counters['unweighted_counter'].keys())

        uCounter_axis = hist.axis.StrCategory([], growth=True, name="unweighted_counter", label="")
        h_uCounter = hist.Hist(uCounter_axis, storage="weight", name="ucounter")
        control = int(self.counters["unweighted_counter"]["Control"])
        for k in self.counters["unweighted_counter"].keys():
            count = self.counters["unweighted_counter"][k]
            if "Skim" in k:
                count = int(count/control)
            h_uCounter.fill(
                unweighted_counter=k, weight=count
            )
        wCounter_axis = hist.axis.StrCategory([], growth=True, name="weighted_counter", label="")
        h_wCounter = hist.Hist(wCounter_axis, storage="weight", name="wcounter")
        for k in self.counters["weighted_counter"].keys():
            count = self.counters["weighted_counter"][k]
            if "Skim" in k:
                count = count/control
            h_wCounter.fill(
                weighted_counter=k, weight=count
            )
        return h_uCounter,h_wCounter

# work in progress, counters not priority
class VariationCounters(Counters):

    def __init__(self, variations):
        self.variations = variations
        self.counters = {}
        self.counters["unweighted_counter"] = processor.defaultdict_accumulator(processor.defaultdict_accumulator)
        self.counters["weighted_counter"]   = processor.defaultdict_accumulator(processor.defaultdict_accumulator)
        self.first = True

        self.selection = {}
        self.weights = {}

        variations = []
        varSelections = {}

        for key in var.keys():
            variations.append(list(var[key].keys()))
            for k2 in var[key].keys():
                varSelections[k2] = var[key][k2]

        nameBase = name
        for combination in list(itertools.product(*variations)):
            hname = nameBase
            sele = None

            for comb in combination:
                if not (comb == '' or 'incl' in comb or comb == 'PSWeight'):
                    hname = hname + '_%s'%comb

                    if 'weight' in comb or 'Weight' in comb:
                        self.weights[hname] = comb
                        continue
                    if sele == None:
                        sele = varSelections[comb]
                    else:
                        sele = sele + " & " + varSelections[comb]
            self.selection[hname] = sele

        self.selection_re = re.compile("(?P<variable>[\w\.]+)\s*(?P<operator>\S+)\s*(?P<value>\S+)\)")

    def increment(self, label, events):
        for name in self.selection:
            sele = self.select(events, name)
            self.identity["unweighted_counter"][name][label] += len(events.event)
            self.identity["weighted_counter"][name][label]   += ak.sum(events.weight)

    def select(events, key):
        return_sele = (events["event"] > 0)
        if not self.is_unblinded:
            # NOTE: this currently automatically unblinds all events that passed the INVERTED tau isolation check
            return_sele = return_sele & (events["TauIsolation"] <= 0)
        if self.selection[variable] == None:
            return return_sele

        selections = self.selection[variable].split('&')

        for s in selections:
            match = self.selection_re.search(s)
            if match:
                rec_variables = match.group('variable').split('.')
                operator = match.group('operator')
                value    = eval(match.group('value'))

                variable = events
                for field in rec_variables:
                    variable = variable[field]

                if operator == '<':
                    return_sele = return_sele & (variable < value)
                if operator == '>':
                    return_sele = return_sele & (variable > value)
                if operator == '==':
                    return_sele = return_sele & (variable == value)
        return return_sele
