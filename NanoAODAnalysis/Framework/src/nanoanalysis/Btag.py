import sys

class Btag():
    def __init__(self,algorithm,year):
        self.algo = algorithm
        self.year = year
        self.tagger = 'deepCSV'
        if 'DeepFlav' in algorithm:
            self.tagger = 'deepJet'
        if ":" in algorithm:
            self.algo = algorithm[:algorithm.rfind(':')]

        # https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation
        # https://btv-wiki.docs.cern.ch/ScaleFactors
        self.workingPoints = {}
        self.workingPoints['btagDeepB'] = {
            "2016APV": {'loose': 0.2027, 'medium': 0.6001, 'tight': 0.8819},
            "2016": {'loose': 0.1918, 'medium': 0.5847, 'tight': 0.8767},
            "2017": {'loose': 0.1355, 'medium': 0.4506, 'tight': 0.7738},
            "2018": {'loose': 0.1208, 'medium': 0.4168, 'tight': 0.7665},
            "2022": {'loose': 0.1208, 'medium': 0.4168, 'tight': 0.7665}, # FIXME: using 2018 values for 2022
            "2022E": {'loose': 0.1208, 'medium': 0.4168, 'tight': 0.7665}, # FIXME: using 2018 values for 2022
        }
        self.workingPoints["btagDeepFlavB"] = {
            "2016APV": {'loose': 0.0508, 'medium': 0.2598, 'tight': 0.6502},
            "2016": {'loose': 0.0480, 'medium': 0.2489, 'tight': 0.6377},
            "2017": {'loose': 0.0532, 'medium': 0.3040, 'tight': 0.7476},
            "2018": {'loose': 0.0490, 'medium': 0.2783, 'tight': 0.7100},
            "2022": {'loose': 0.0490, 'medium': 0.2783, 'tight': 0.7100}, # FIXME: using 2018 values for 2022
            "2022E": {'loose': 0.0490, 'medium': 0.2783, 'tight': 0.7100}, # FIXME: using 2018 values for 2022
            "2023": {'loose': 0.0479, 'medium': 0.2431, 'tight': 0.6553}, # FIXME: values subject to change
        }
        self.workingPoints['btagPNetB'] = {
            "2023": {'loose': 0.0358, 'medium': 0.1917, 'tight': 0.6172}, # FIXME: values subject to change
        }

        self.workingPoints['btagDeepCvL'] = {
            "2016APV": {'loose': 0.088, 'medium': 0.181, 'tight': 0.417},
            "2016": {'loose': 0.088, 'medium': 0.180, 'tight': 0.407},
            "2017": {'loose': 0.040, 'medium': 0.144, 'tight': 0.730},
            "2018": {'loose': 0.064, 'medium': 0.153, 'tight': 0.405},
            "2022": {'loose': 0.064, 'medium': 0.153, 'tight': 0.405}, # FIXME: using 2018 values for 2022
            "2022E": {'loose': 0.064, 'medium': 0.153, 'tight': 0.405}, # FIXME: using 2018 values for 2022
        }
        #frac = 0.5
        self.workingPoints['btagDeepFlavCvL'] = {
            #"2016": {'loose': -0.48, 'medium': -0.1, 'tight': 0.69},
            #"2017": {'loose':  0.05, 'medium': 0.15, 'tight': 0.8},
            #"2018": {'loose':  0.04, 'medium': 0.137,'tight': 0.66}
            # loosening the tight WP to get about same nember of c jets as b jets
            #"2016": {'loose': -0.48, 'medium': -0.1, 'tight': -0.1+frac*(0.69+0.1)},
            #"2017": {'loose':  0.05, 'medium': 0.15, 'tight': 0.15+frac*(0.8-0.15)},
            #"2018": {'loose':  0.04, 'medium': 0.137,'tight': 0.137+frac*(0.66-0.137)},
            #"2022": {'loose':  0.04, 'medium': 0.137,'tight': 0.137+frac*(0.66-0.137)} # FIXME: using 2018 values for 2022
            "2016APV": {'loose': 0.039, 'medium': 0.098, 'tight': 0.270},
            "2016": {'loose': 0.039, 'medium': 0.099, 'tight': 0.269},
            "2017": {'loose': 0.030, 'medium': 0.085, 'tight': 0.520},
            "2018": {'loose': 0.038, 'medium': 0.099, 'tight': 0.282},
            "2022": {'loose': 0.038, 'medium': 0.099, 'tight': 0.282}, # FIXME: using 2018 values for 2022
            "2022E": {'loose': 0.038, 'medium': 0.099, 'tight': 0.282}, # FIXME: using 2018 values for 2022
        }
        self.workingPoints['quarktag:particleNetAK4_QvsG'] = {
            "2016APV": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2016": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2017": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2018": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2022": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2022E": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
        }
        self.workingPoints['gluontag:particleNetAK4_QvsG'] = {
            "2016APV": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2016": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2017": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2018": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2022": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2022E": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
        }
        self.workingPoints['quarktag:btagDeepFlavQG'] = {
            "2016APV": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2016": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2017": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2018": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2022": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2022E": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
        }
        self.workingPoints['gluontag:btagDeepFlavQG'] = {
            "2016APV": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2016": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2017": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2018": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2022": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
            "2022E": {'loose': 0.32, 'medium': 0.32, 'tight': 0.32},
        }

    def exists(self):
        if not self.algo in self.workingPoints.keys():
            print("B tagging algo",self.algo,"not found. Available algorithms:",self.workingPoints.keys())
            sys.exit()
        if not self.year in self.workingPoints[self.algo].keys():
            print("B tagging year",self.year,"not found. Available years:",self.algo,":",self.workingPoints[self.algo].keys())
            sys.exit()
        return True

    def loose(self):
        if self.exists():
            return self.workingPoints[self.algo][self.year]['loose']
        return None

    def medium(self):
        if self.exists():
            return self.workingPoints[self.algo][self.year]['medium']
        return None

    def tight(self):
        if self.exists():
            return self.workingPoints[self.algo][self.year]['tight']
        return None

    def tag(self,wp):
        if self.exists():
            return self.workingPoints[self.algo][self.year][wp]
        return None

    def btag(self,wp):
        # find b tag corresponding to c tag algo
        b_algo = self.algo.replace("CvL","B")
        if self.exists():
            return self.workingPoints[b_algo][self.year][wp]
        return None

    def ctag(self,wp):
        # find c tag corresponding to b tag algo
        c_algo = 'btagDeepCvL'
        if "deepJet" in self.tagger:
            c_algo = 'btagDeepFlavCvL'
        if self.exists():
            return self.workingPoints[c_algo][self.year][wp]
        return None

    def selection(self,jet,wp):
        if self.algo == "btagDeepB":
            return (jet.btagDeepB > self.tag(wp))

        if self.algo == "btagDeepFlavB":
            return (jet.btagDeepFlavB > self.tag(wp))

        if self.algo == "btagDeepCvL":
            return ((jet.btagDeepB <= self.btag(wp)) & (jet.btagDeepCvL > self.tag(wp)))

        if self.algo == "btagDeepFlavCvL":
            return ((jet.btagDeepFlavB <= self.btag(wp)) & (jet.btagDeepFlavCvL > self.tag(wp)))

        if "quarktag" in self.algo and "btagDeepFlavQG" in self.algo:
            return (
                    (jet.btagDeepFlavB <= self.btag(wp)) &
                    (jet.btagDeepFlavCvL <= self.ctag(wp)) &
                    (jet.btagDeepFlavQG > self.tag(wp))
                )
        if "quarktag" in self.algo and "particleNetAK4_QvsG" in self.algo:
            if "deepJet" in self.tagger:
                return (
                    (jet.btagDeepFlavB <= self.btag(wp)) &
                    (jet.btagDeepFlavCvL <= self.ctag(wp)) &
                    (jet.particleNetAK4_QvsG > self.tag(wp))
                )
            else:
                return (
                    (jet.btagDeepB <= self.btag(wp)) &
                    (jet.btagDeepCvL <= self.ctag(wp)) &
                    (jet.particleNetAK4_QvsG > self.tag(wp))
                )

        if "gluontag" in self.algo and "btagDeepFlavQG" in self.algo:
            return (
                    (jet.btagDeepFlavB <= self.btag(wp)) &
                    (jet.btagDeepFlavCvL <= self.ctag(wp)) &
                    (jet.btagDeepFlavQG >= 0) &
                    (jet.btagDeepFlavQG <= self.tag(wp))
                )
        if "gluontag" in self.algo and "particleNetAK4_QvsG" in self.algo:
            if "deepJet" in self.tagger:
                return (
                    (jet.btagDeepFlavB <= self.btag(wp)) &
                    (jet.btagDeepFlavCvL <= self.ctag(wp)) &
                    (jet.particleNetAK4_QvsG >= 0) &
                    (jet.particleNetAK4_QvsG <= self.tag(wp))
                )
            else:
                return (
                    (jet.btagDeepB <= self.btag(wp)) &
                    (jet.btagDeepCvL <= self.ctag(wp)) &
                    (jet.particleNetAK4_QvsG >= 0) &
                    (jet.particleNetAK4_QvsG <= self.tag(wp))
                )

        print("b/c/q/g tagger selection algo not implemented", self.algo)
        print("File Framework/python/Btag.py")
        sys.exit()

