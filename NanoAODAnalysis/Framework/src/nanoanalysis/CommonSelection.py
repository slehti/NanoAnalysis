import awkward as ak
import numpy as np
import sys
import os
import re
import math

from  nanoanalysis.METCleaning import METCleaning
from nanoanalysis.JetCorrections import JEC

import nanoanalysis.tools.Print

def lumimask(events,lmask):
    return lmask(events.run, events.luminosityBlock)

def PtCut(candidate,cut):
    return (
        (candidate.pt > cut)
    )

def JetSelection(events):
    jet_cands = events.Jet
    if hasattr(events,'Muon'):
        jet_cands = drClean(jet_cands,events.Muon)
    #if hasattr(events,'Electron'):
    #    jet_cands = drClean(jet_cands,events.Electron)

    #jet_cands = jet_cands[(jet_cands.jetId >= 4)] # Int_t Jet ID flags bit1 is loose, bit2 is tight, bit3 is tightLepVeto

    retEvents = events
    retEvents["Jet"] = jet_cands

    retEvents = retEvents[(ak.num(retEvents.Jet.pt > 0) > 0)]
    return retEvents

def JetCorrections(events,jetCorrections):
    events['Jet'] = jetCorrections.apply(events)
    if hasattr(jetCorrections,'corrected_jets_l1'):
        events['Jet_L1RC'] = jetCorrections.corrected_jets_l1
    return events

def Jetvetomap(events,jetvetomap):
    return events[(jetvetomap.passmap(events.leadingJet))]

def drClean(coll1,coll2,cone=0.3):
    from coffea.nanoevents.methods import vector
    j_eta = coll1.eta
    j_phi = coll1.phi
    l_eta = coll2.eta
    l_phi = coll2.phi

    j_eta, l_eta = ak.unzip(ak.cartesian([j_eta, l_eta], nested=True))
    j_phi, l_phi = ak.unzip(ak.cartesian([j_phi, l_phi], nested=True))
    delta_eta = j_eta - l_eta
    delta_phi = vector._deltaphi_kernel(j_phi,l_phi)
    dr = np.hypot(delta_eta, delta_phi)
    jets_noleptons = coll1[~ak.any(dr < cone, axis=2)]
    return jets_noleptons

def pileup_reweight(events,year):
    weights = coffea.analysis_tools.Weights(len(events),storeIndividual=True)

