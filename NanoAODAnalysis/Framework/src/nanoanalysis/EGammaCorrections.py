#from coffea.lookup_tools import extractor
#import cachetools
import os
import numpy as np
import awkward as ak

#from DataPath import getDataPath

#WIP

class TEST():
    def __init__(self,run,isData):
        self.isData = isData
        print("Hi?")

    def apply(self,events):
        print("APPLY")
        photons = events.Photon
        photons['pt_orig'] = photons['pt']

        SF = np.array(ak.flatten(ak.ones_like(events.Photon.pt) * 2))
        SF = ak.unflatten(SF,ak.num(events.Photon.pt))

        corrected_photons = photons

        corrected_photons["pt"] = corrected_photons.pt * SF


        return corrected_photons
