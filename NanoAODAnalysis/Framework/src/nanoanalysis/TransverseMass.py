
def reconstruct_transverse_mass(tau, met):
    import numpy as np
    import awkward as ak
    tau_mt = np.sqrt(tau.mass**2 + tau.pt**2)  # get tau transverse mass
    myCosPhi = (tau.x*met.x + tau.y*met.y) / (tau.pt*met.pt) # get cosine of angle between tau and met
    myCosPhi = ak.fill_none(myCosPhi, np.nan)
    myCosPhi = ak.where(np.abs(myCosPhi)> 1., np.sign(myCosPhi), myCosPhi)
    #myCosPhi = myCosPhi.clip(-1.0, 1.0) # get rid of fp inaccuracy causing unphysical cosine
    mt_precursor = np.sqrt(2*tau.pt*(1.0-myCosPhi)) # get mt between tau and met, not yet multiplied by maybe-defined missing pt
    mt = ak.where(np.isnan(mt_precursor), tau_mt, mt_precursor*np.sqrt(met.pt))
    # if ak.any(np.isnan(mt)):
    #     msg = f"Encountered nan when calculating event transverse mass with input:\ntaus:\n{tau}\nMET\n{met}"
    #     raise Exception(msg)
    return mt

def rdf_declare_mT():
    from nanoanalysis import cpptools
    from nanoanalysis.rdf_backend import TransverseMass as backend
    cpptools.initialize(backend)

def rdf_reconstruct_transverse_mass(df):
    return df.Define("mT", "nanoanalysis::mT_tau_met(leadTau_mass, leadTau_pt, leadTau_phi, MET_pt, MET_phi)")


