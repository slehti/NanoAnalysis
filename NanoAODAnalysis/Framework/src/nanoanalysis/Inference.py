from io import StringIO
import sys
import re

import ROOT as R

from nanoanalysis import cpptools
from nanoanalysis.ML.input_features import load_input_vars


COLUMN_DEFINITIONS = {
	"btagDeepB": "Jet_btagDeepB[Jet_passed]",
	"dPhiTauNu": "ROOT::VecOps::DeltaPhi(leadTau_phi, MET_phi)",
    "dPhiTauJets": "nanoanalysis::dPhi(leadTau_phi, HJet_phi)",
    "dPhiNuJets": "nanoanalysis::dPhi(MET_phi, HJet_phi)",
    "dPhiAllJets": "nanoanalysis::dPhi_combinations(nanoanalysis::fix_size(HJet_phi, {s}))",
}

class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self
    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout

class InferenceRunner:
    def __init__(self, trained_path, cutoff):
        if not self._module_loaded():
            self._initialize()
        self.create_classifier(trained_path)
        self.cutoff = cutoff

    @staticmethod
    def _module_loaded():

        # first check if header is included
        try:
            inference_func = R.run_inference
        except AttributeError:
            return False

        # also check for presence of the shared object
        with Capturing() as libs:
            R.gSystem.ListLibraries()
        return any("Inference." in lib for lib in libs)

    @staticmethod
    def _initialize():
        if not InferenceRunner._module_loaded():
            from nanoanalysis.rdf_backend import Inference as backend
            cpptools.initialize(backend)

    def _load_model(self, path):
        model_initialized = R.nanoanalysis.load_model(path)
        return model_initialized

    def create_classifier(self, trained_path):
        folds = trained_path.glob("fold_*/")
        self.input_features = load_input_vars(str(trained_path))
        self.loaded_models = []
        for fold_path in folds:
            tflite_path = str(fold_path / "model_trained.tflite")
            success = self._load_model(tflite_path)
            if not success:
                raise RuntimeError(f"Failed loading model at {fold_path}!")
            else:
                self.loaded_models.append(tflite_path)
        self.n_folds = len(self.loaded_models)

    def forward(self, df, column_name="classifier_score"):

        input_string = ",".join(self.input_features)
        paths_string = '"' + "\",\"".join(self.loaded_models) + '"'

        df = define_input_columns(df, self.input_features)
        df = df.Define("fold", f"static_cast<unsigned int>(event % {self.n_folds})")
        df = df.Define("classifier_input", f"std::vector<float>{{{input_string}}}")

        df = R.nanoanalysis.apply_classifier(R.RDF.AsRNode(df), self.loaded_models)
        # df = df.Define(
        #     "classifier_output_raw",
        #     expression
        # )
        df = df.Define(column_name, "classifier_output_raw[0][0]")
        if self.cutoff:
            df = df.Filter(f"{column_name} >= {self.cutoff}", "ML classifier selection")
        return df

def vec_to_floats(df, column, length):
    df = df.Redefine(column, f"nanoanalysis::fix_size({column}, {length})")
    for i in range(length):
        df = df.Define(f"{column}_{i}", f"{column}[{i}]")
    return df

def define_input_columns(df, input_features):

    def split_input_name(input_name):
        match = re.search(r'(.+)_(\d+)$', input_name)
        if match:
            return match.group(1), int(match.group(2)) + 1
        return None, None

    input_bases = []
    input_lengths = {}
    for x in input_features:
        base, l = split_input_name(x)
        if base is None:
            base = x
        if base not in input_bases:
            input_bases.append(base)
        if base not in input_lengths:
            input_lengths[base] = l
        elif l is not None:
            input_lengths[base] = max(input_lengths[base], l)

    existing_cols = df.GetColumnNames()
    for base in input_bases:
        if (base not in existing_cols) and (base in COLUMN_DEFINITIONS):
            df = df.Define(base, COLUMN_DEFINITIONS[base])
        if input_lengths[base]:
            df = vec_to_floats(df, base, input_lengths[base])

    return df

