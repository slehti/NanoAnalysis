from coffea.lookup_tools import extractor
from coffea.jetmet_tools import FactorizedJetCorrector, JetCorrectionUncertainty
from coffea.jetmet_tools import JECStack, CorrectedJetsFactory, CorrectedMETFactory
from coffea.jetmet_tools import JetResolution, JetResolutionScaleFactor
from coffea.jetmet_tools.CorrectedJetsFactory import awkward_rewrap, rewrap_recordarray
import cachetools
import awkward as ak
import numpy as np
import os,sys
from functools import partial
import operator
import copy

from nanoanalysis.DataPath import getDataPath

JEC_CORRECTION_SETS = {
    "NoCorrection": {},
    "2016APV": {
        '2016B': ['Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L2L3Residual_AK4PFchs.txt'],
        '2016C': ['Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L2L3Residual_AK4PFchs.txt'],
        '2016D': ['Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L2L3Residual_AK4PFchs.txt'],
        '2016E': ['Summer19UL16APV_RunEF_V7_DATA/Summer19UL16APV_RunEF_V7_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL16APV_RunEF_V7_DATA/Summer19UL16APV_RunEF_V7_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL16APV_RunEF_V7_DATA/Summer19UL16APV_RunEF_V7_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL16APV_RunEF_V7_DATA/Summer19UL16APV_RunEF_V7_DATA_L2L3Residual_AK4PFchs.txt'],
        '2016F': ['Summer19UL16APV_RunEF_V7_DATA/Summer19UL16APV_RunEF_V7_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL16APV_RunEF_V7_DATA/Summer19UL16APV_RunEF_V7_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL16APV_RunEF_V7_DATA/Summer19UL16APV_RunEF_V7_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL16APV_RunEF_V7_DATA/Summer19UL16APV_RunEF_V7_DATA_L2L3Residual_AK4PFchs.txt'],
        'MC':    ['Summer19UL16APV_V7_MC/Summer19UL16APV_V7_MC_L1FastJet_AK4PFchs.txt',
                  'Summer19UL16APV_V7_MC/Summer19UL16APV_V7_MC_L2Relative_AK4PFchs.txt',
                  'Summer19UL16APV_V7_MC/Summer19UL16APV_V7_MC_L3Absolute_AK4PFchs.txt',
                  'Summer19UL16APV_V7_MC/Summer19UL16APV_V7_MC_L2L3Residual_AK4PFchs.txt']
    },
    # JEC L1RC only
    "2016APVL1RC": {
        '2016B': ['Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L1RC_AK4PFchs.txt'],
        '2016C': ['Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L1RC_AK4PFchs.txt'],
        '2016D': ['Summer19UL16APV_RunBCD_V7_DATA/Summer19UL16APV_RunBCD_V7_DATA_L1RC_AK4PFchs.txt'],
        '2016E': ['Summer19UL16APV_RunEF_V7_DATA/Summer19UL16APV_RunEF_V7_DATA_L1RC_AK4PFchs.txt'],
        '2016F': ['Summer19UL16APV_RunEF_V7_DATA/Summer19UL16APV_RunEF_V7_DATA_L1RC_AK4PFchs.txt'],
        'MC':    ['Summer19UL16APV_V7_MC/Summer19UL16APV_V7_MC_L1RC_AK4PFchs.txt']
    },
    "2016": {
        '2016F': ['Summer19UL16_RunFGH_V7_DATA/Summer19UL16_RunFGH_V7_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL16_RunFGH_V7_DATA/Summer19UL16_RunFGH_V7_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL16_RunFGH_V7_DATA/Summer19UL16_RunFGH_V7_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL16_RunFGH_V7_DATA/Summer19UL16_RunFGH_V7_DATA_L2L3Residual_AK4PFchs.txt'],
        '2016G': ['Summer20UL16_RunGH_V1_DATA/Summer20UL16_RunGH_V1_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer20UL16_RunGH_V1_DATA/Summer20UL16_RunGH_V1_DATA_L2Relative_AK4PFchs.txt',
                  'Summer20UL16_RunGH_V1_DATA/Summer20UL16_RunGH_V1_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer20UL16_RunGH_V1_DATA/Summer20UL16_RunGH_V1_DATA_L2L3Residual_AK4PFchs.txt'],
        '2016H': ['Summer20UL16_RunGH_V1_DATA/Summer20UL16_RunGH_V1_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer20UL16_RunGH_V1_DATA/Summer20UL16_RunGH_V1_DATA_L2Relative_AK4PFchs.txt',
                  'Summer20UL16_RunGH_V1_DATA/Summer20UL16_RunGH_V1_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer20UL16_RunGH_V1_DATA/Summer20UL16_RunGH_V1_DATA_L2L3Residual_AK4PFchs.txt'],
        'MC':    ['Summer20UL16_V1_MC/Summer20UL16_V1_MC_L1FastJet_AK4PFchs.txt',
                  'Summer20UL16_V1_MC/Summer20UL16_V1_MC_L2Relative_AK4PFchs.txt',
                  'Summer20UL16_V1_MC/Summer20UL16_V1_MC_L3Absolute_AK4PFchs.txt',
                  'Summer20UL16_V1_MC/Summer20UL16_V1_MC_L2L3Residual_AK4PFchs.txt']
    },
    # JEC L1RC only
    "2016L1RC": {
        '2016F': ['Summer19UL16_RunFGH_V7_DATA/Summer19UL16_RunFGH_V7_DATA_L1RC_AK4PFchs.txt'],
        '2016G': ['Summer19UL16_RunFGH_V7_DATA/Summer19UL16_RunFGH_V7_DATA_L1RC_AK4PFchs.txt'],
        '2016H': ['Summer19UL16_RunFGH_V7_DATA/Summer19UL16_RunFGH_V7_DATA_L1RC_AK4PFchs.txt'],
        'MC':    ['Summer19UL16_V7_MC/Summer19UL16_V7_MC_L1RC_AK4PFchs.txt']
    },
    "2017": {
        '2017B': ['Summer19UL17_RunB_V6_DATA/Summer19UL17_RunB_V6_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL17_RunB_V6_DATA/Summer19UL17_RunB_V6_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL17_RunB_V6_DATA/Summer19UL17_RunB_V6_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL17_RunB_V6_DATA/Summer19UL17_RunB_V6_DATA_L2L3Residual_AK4PFchs.txt'],
        '2017C': ['Summer19UL17_RunC_V6_DATA/Summer19UL17_RunC_V6_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL17_RunC_V6_DATA/Summer19UL17_RunC_V6_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL17_RunC_V6_DATA/Summer19UL17_RunC_V6_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL17_RunC_V6_DATA/Summer19UL17_RunC_V6_DATA_L2L3Residual_AK4PFchs.txt'],
        '2017D': ['Summer19UL17_RunD_V6_DATA/Summer19UL17_RunD_V6_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL17_RunD_V6_DATA/Summer19UL17_RunD_V6_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL17_RunD_V6_DATA/Summer19UL17_RunD_V6_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL17_RunD_V6_DATA/Summer19UL17_RunD_V6_DATA_L2L3Residual_AK4PFchs.txt'],
        '2017E': ['Summer19UL17_RunE_V6_DATA/Summer19UL17_RunE_V6_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL17_RunE_V6_DATA/Summer19UL17_RunE_V6_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL17_RunE_V6_DATA/Summer19UL17_RunE_V6_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL17_RunE_V6_DATA/Summer19UL17_RunE_V6_DATA_L2L3Residual_AK4PFchs.txt'],
        '2017F': ['Summer19UL17_RunF_V6_DATA/Summer19UL17_RunF_V6_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL17_RunF_V6_DATA/Summer19UL17_RunF_V6_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL17_RunF_V6_DATA/Summer19UL17_RunF_V6_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL17_RunF_V6_DATA/Summer19UL17_RunF_V6_DATA_L2L3Residual_AK4PFchs.txt'],
        'MC':    ['Summer19UL17_V6_MC/Summer19UL17_V6_MC_L1FastJet_AK4PFchs.txt',
                  'Summer19UL17_V6_MC/Summer19UL17_V6_MC_L2Relative_AK4PFchs.txt',
                  'Summer19UL17_V6_MC/Summer19UL17_V6_MC_L3Absolute_AK4PFchs.txt',
                  'Summer19UL17_V6_MC/Summer19UL17_V6_MC_L2L3Residual_AK4PFchs.txt']
    },
    # JEC L1RC only
    "2017L1RC": {
        '2017B': ['Summer19UL17_RunB_V6_DATA/Summer19UL17_RunB_V6_DATA_L1RC_AK4PFchs.txt'],
        '2017C': ['Summer19UL17_RunC_V6_DATA/Summer19UL17_RunC_V6_DATA_L1RC_AK4PFchs.txt'],
        '2017D': ['Summer19UL17_RunD_V6_DATA/Summer19UL17_RunD_V6_DATA_L1RC_AK4PFchs.txt'],
        '2017E': ['Summer19UL17_RunE_V6_DATA/Summer19UL17_RunE_V6_DATA_L1RC_AK4PFchs.txt'],
        '2017F': ['Summer19UL17_RunF_V6_DATA/Summer19UL17_RunF_V6_DATA_L1RC_AK4PFchs.txt'],
        'MC':    ['Summer19UL17_V6_MC/Summer19UL17_V6_MC_L1RC_AK4PFchs.txt']
    },
    "2018": {
        '2018A': ['Summer19UL18_RunA_V5_DATA/Summer19UL18_RunA_V5_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL18_RunA_V5_DATA/Summer19UL18_RunA_V5_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL18_RunA_V5_DATA/Summer19UL18_RunA_V5_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL18_RunA_V5_DATA/Summer19UL18_RunA_V5_DATA_L2L3Residual_AK4PFchs.txt'],
        '2018B': ['Summer19UL18_RunB_V5_DATA/Summer19UL18_RunB_V5_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL18_RunB_V5_DATA/Summer19UL18_RunB_V5_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL18_RunB_V5_DATA/Summer19UL18_RunB_V5_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL18_RunB_V5_DATA/Summer19UL18_RunB_V5_DATA_L2L3Residual_AK4PFchs.txt'],
        '2018C': ['Summer19UL18_RunC_V5_DATA/Summer19UL18_RunC_V5_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL18_RunC_V5_DATA/Summer19UL18_RunC_V5_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL18_RunC_V5_DATA/Summer19UL18_RunC_V5_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL18_RunC_V5_DATA/Summer19UL18_RunC_V5_DATA_L2L3Residual_AK4PFchs.txt'],
        '2018D': ['Summer19UL18_RunD_V5_DATA/Summer19UL18_RunD_V5_DATA_L1FastJet_AK4PFchs.txt',
                  'Summer19UL18_RunD_V5_DATA/Summer19UL18_RunD_V5_DATA_L2Relative_AK4PFchs.txt',
                  'Summer19UL18_RunD_V5_DATA/Summer19UL18_RunD_V5_DATA_L3Absolute_AK4PFchs.txt',
                  'Summer19UL18_RunD_V5_DATA/Summer19UL18_RunD_V5_DATA_L2L3Residual_AK4PFchs.txt'],
        'MC':    ['Summer19UL18_V5_MC/Summer19UL18_V5_MC_L1FastJet_AK4PFchs.txt',
                  'Summer19UL18_V5_MC/Summer19UL18_V5_MC_L2Relative_AK4PFchs.txt',
                  'Summer19UL18_V5_MC/Summer19UL18_V5_MC_L3Absolute_AK4PFchs.txt',
                  'Summer19UL18_V5_MC/Summer19UL18_V5_MC_L2L3Residual_AK4PFchs.txt']
    },
    # JEC L1RC only
    "2018L1RC": {
        '2018A': ['Summer19UL18_RunA_V5_DATA/Summer19UL18_RunA_V5_DATA_L1RC_AK4PFchs.txt'],
        '2018B': ['Summer19UL18_RunB_V5_DATA/Summer19UL18_RunB_V5_DATA_L1RC_AK4PFchs.txt'],
        '2018C': ['Summer19UL18_RunC_V5_DATA/Summer19UL18_RunC_V5_DATA_L1RC_AK4PFchs.txt'],
        '2018D': ['Summer19UL18_RunD_V5_DATA/Summer19UL18_RunD_V5_DATA_L1RC_AK4PFchs.txt'],
        'MC':    ['Summer19UL18_V5_MC/Summer19UL18_V5_MC_L1RC_AK4PFchs.txt']
    },
    "2022": {
        '2022C': ['Winter22Run3_RunC_V2_DATA/Winter22Run3_RunC_V2_DATA_L2Relative_AK4PFPuppi.txt',
                  'Winter22Run3_RunC_V2_DATA/Winter22Run3_RunC_V2_DATA_L3Absolute_AK4PFPuppi.txt',
                  'Winter22Run3_RunC_V2_DATA/Winter22Run3_RunC_V2_DATA_L2L3Residual_AK4PFPuppi.txt'],
        '2022D': ['Winter22Run3_RunD_V2_DATA/Winter22Run3_RunD_V2_DATA_L2Relative_AK4PFPuppi.txt',
                  'Winter22Run3_RunD_V2_DATA/Winter22Run3_RunD_V2_DATA_L3Absolute_AK4PFPuppi.txt',
                  'Winter22Run3_RunD_V2_DATA/Winter22Run3_RunD_V2_DATA_L2L3Residual_AK4PFPuppi.txt'],
        'MC': ['Winter22Run3_V2_MC/Winter22Run3_V2_MC_L2Relative_AK4PFPuppi.txt',
               'Winter22Run3_V2_MC/Winter22Run3_V2_MC_L3Absolute_AK4PFPuppi.txt',
               'Winter22Run3_V2_MC/Winter22Run3_V2_MC_L2L3Residual_AK4PFPuppi.txt']
    },
    "2022E": {
        '2022E': ['Winter23Prompt23_RunA_V1_DATA/Winter23Prompt23_RunA_V1_DATA_L2Relative_AK4PFPuppi.txt',
                  'Winter23Prompt23_RunA_V1_DATA/Winter23Prompt23_RunA_V1_DATA_L3Absolute_AK4PFPuppi.txt',
                  'Winter23Prompt23_RunA_V1_DATA/Winter23Prompt23_RunA_V1_DATA_L2L3Residual_AK4PFPuppi.txt'],
        '2022F': ['Winter23Prompt23_RunA_V1_DATA/Winter23Prompt23_RunA_V1_DATA_L2Relative_AK4PFPuppi.txt',
                  'Winter23Prompt23_RunA_V1_DATA/Winter23Prompt23_RunA_V1_DATA_L3Absolute_AK4PFPuppi.txt',
                  'Winter23Prompt23_RunA_V1_DATA/Winter23Prompt23_RunA_V1_DATA_L2L3Residual_AK4PFPuppi.txt'],
        '2022G': ['Winter23Prompt23_RunA_V1_DATA/Winter23Prompt23_RunA_V1_DATA_L2Relative_AK4PFPuppi.txt',
                  'Winter23Prompt23_RunA_V1_DATA/Winter23Prompt23_RunA_V1_DATA_L3Absolute_AK4PFPuppi.txt',
                  'Winter23Prompt23_RunA_V1_DATA/Winter23Prompt23_RunA_V1_DATA_L2L3Residual_AK4PFPuppi.txt'],
        'MC': ['Winter23Prompt23_V1_MC/Winter23Prompt23_V1_MC_L2Relative_AK4PFPuppi.txt',
               'Winter23Prompt23_V1_MC/Winter23Prompt23_V1_MC_L3Absolute_AK4PFPuppi.txt',
               'Winter23Prompt23_V1_MC/Winter23Prompt23_V1_MC_L2L3Residual_AK4PFPuppi.txt']
    },
    "2022L2Rel": {
        '2022C': ['Winter22Run3_RunC_V2_DATA/Winter22Run3_RunC_V2_DATA_L2Relative_AK4PFPuppi.txt'],
        '2022D': ['Winter22Run3_RunD_V2_DATA/Winter22Run3_RunD_V2_DATA_L2Relative_AK4PFPuppi.txt'],
        '2022E': ['Winter22Run3_RunC_V2_DATA/Winter22Run3_RunC_V2_DATA_L2Relative_AK4PFPuppi.txt'],
        '2022F': ['Winter22Run3_RunC_V2_DATA/Winter22Run3_RunC_V2_DATA_L2Relative_AK4PFPuppi.txt'],
        'MC': ['Winter22Run3_V1_MC/Winter22Run3_V1_MC_L2Relative_AK4PFPuppi.txt']
    }
}

JER_CORRECTION_SETS = {
    "2016APV": {
        '2016B': ['Summer20UL16APV_JRV3_DATA/Summer20UL16APV_JRV3_DATA_PtResolution_AK4PFchs.txt',
                  'Summer20UL16APV_JRV3_DATA/Summer20UL16APV_JRV3_DATA_SF_AK4PFchs.txt'],
        '2016C': ['Summer20UL16APV_JRV3_DATA/Summer20UL16APV_JRV3_DATA_PtResolution_AK4PFchs.txt',
                  'Summer20UL16APV_JRV3_DATA/Summer20UL16APV_JRV3_DATA_SF_AK4PFchs.txt'],
        '2016D': ['Summer20UL16APV_JRV3_DATA/Summer20UL16APV_JRV3_DATA_PtResolution_AK4PFchs.txt',
                  'Summer20UL16APV_JRV3_DATA/Summer20UL16APV_JRV3_DATA_SF_AK4PFchs.txt'],
        '2016E': ['Summer20UL16APV_JRV3_DATA/Summer20UL16APV_JRV3_DATA_PtResolution_AK4PFchs.txt',
                  'Summer20UL16APV_JRV3_DATA/Summer20UL16APV_JRV3_DATA_SF_AK4PFchs.txt'],
        '2016F': ['Summer20UL16APV_JRV3_DATA/Summer20UL16APV_JRV3_DATA_PtResolution_AK4PFchs.txt',
                  'Summer20UL16APV_JRV3_DATA/Summer20UL16APV_JRV3_DATA_SF_AK4PFchs.txt'],
        'MC': ['Summer20UL16APV_JRV3_MC/Summer20UL16APV_JRV3_MC_PtResolution_AK4PFchs.txt',
               'Summer20UL16APV_JRV3_MC/Summer20UL16APV_JRV3_MC_SF_AK4PFchs.txt'],
    },
    "2016": {
        '2016F': ['Summer20UL16_JRV3_DATA/Summer20UL16_JRV3_DATA_PtResolution_AK4PFchs.txt',
                  'Summer20UL16_JRV3_DATA/Summer20UL16_JRV3_DATA_SF_AK4PFchs.txt'],
        '2016G': ['Summer20UL16_JRV3_DATA/Summer20UL16_JRV3_DATA_PtResolution_AK4PFchs.txt',
                  'Summer20UL16_JRV3_DATA/Summer20UL16_JRV3_DATA_SF_AK4PFchs.txt'],
        '2016H': ['Summer20UL16_JRV3_DATA/Summer20UL16_JRV3_DATA_PtResolution_AK4PFchs.txt',
                  'Summer20UL16_JRV3_DATA/Summer20UL16_JRV3_DATA_SF_AK4PFchs.txt'],
        'MC': ['Summer20UL16_JRV3_MC/Summer20UL16_JRV3_MC_PtResolution_AK4PFchs.txt',
               'Summer20UL16_JRV3_MC/Summer20UL16_JRV3_MC_SF_AK4PFchs.txt'],
    },
    "2017": {
        '2017B': ['Summer19UL17_JRV2_DATA/Summer19UL17_JRV2_DATA_PtResolution_AK4PFchs.txt',
                  'Summer19UL17_JRV2_DATA/Summer19UL17_JRV2_DATA_SF_AK4PFchs.txt'],
        '2017C': ['Summer19UL17_JRV2_DATA/Summer19UL17_JRV2_DATA_PtResolution_AK4PFchs.txt',
                  'Summer19UL17_JRV2_DATA/Summer19UL17_JRV2_DATA_SF_AK4PFchs.txt'],
        '2017D': ['Summer19UL17_JRV2_DATA/Summer19UL17_JRV2_DATA_PtResolution_AK4PFchs.txt',
                  'Summer19UL17_JRV2_DATA/Summer19UL17_JRV2_DATA_SF_AK4PFchs.txt'],
        '2017E': ['Summer19UL17_JRV2_DATA/Summer19UL17_JRV2_DATA_PtResolution_AK4PFchs.txt',
                  'Summer19UL17_JRV2_DATA/Summer19UL17_JRV2_DATA_SF_AK4PFchs.txt'],
        '2017F': ['Summer19UL17_JRV2_DATA/Summer19UL17_JRV2_DATA_PtResolution_AK4PFchs.txt',
                  'Summer19UL17_JRV2_DATA/Summer19UL17_JRV2_DATA_SF_AK4PFchs.txt'],
        'MC': ['Summer19UL17_JRV2_MC/Summer19UL17_JRV2_MC_PtResolution_AK4PFchs.txt',
               'Summer19UL17_JRV2_MC/Summer19UL17_JRV2_MC_SF_AK4PFchs.txt'],
    },
    "2018": {
        '2018A': ['Summer19UL18_JRV2_DATA/Summer19UL18_JRV2_DATA_PtResolution_AK4PFchs.txt',
                  'Summer19UL18_JRV2_DATA/Summer19UL18_JRV2_DATA_SF_AK4PFchs.txt'],
        '2018B': ['Summer19UL18_JRV2_DATA/Summer19UL18_JRV2_DATA_PtResolution_AK4PFchs.txt',
                  'Summer19UL18_JRV2_DATA/Summer19UL18_JRV2_DATA_SF_AK4PFchs.txt'],
        '2018C': ['Summer19UL18_JRV2_DATA/Summer19UL18_JRV2_DATA_PtResolution_AK4PFchs.txt',
                  'Summer19UL18_JRV2_DATA/Summer19UL18_JRV2_DATA_SF_AK4PFchs.txt'],
        '2018D': ['Summer19UL18_JRV2_DATA/Summer19UL18_JRV2_DATA_PtResolution_AK4PFchs.txt',
                  'Summer19UL18_JRV2_DATA/Summer19UL18_JRV2_DATA_SF_AK4PFchs.txt'],
        'MC': ['Summer19UL18_JRV2_MC/Summer19UL18_JRV2_MC_PtResolution_AK4PFchs.txt',
               'Summer19UL18_JRV2_MC/Summer19UL18_JRV2_MC_SF_AK4PFchs.txt'],
    },
    "2022": {
        '2022C': [],
        '2022D': [],
        'MC': []
    },
    "2022E": {
        '2022E': [],
        '2022F': [],
        '2022G': [],
        'MC': []
    },
    "2022L2Rel": {
        '2022C': [],
        '2022D': [],
        '2022E': [],
        '2022F': [],
        'MC': []
    }
}
"""
    "2022": {
        '2022C': ['JR_Winter22Run3_V1_DATA/JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt',
                  'JR_Winter22Run3_V1_DATA/JR_Winter22Run3_V1_DATA_SF_AK4PFPuppi.txt'],
        '2022D': ['JR_Winter22Run3_V1_DATA/JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt',
                  'JR_Winter22Run3_V1_DATA/JR_Winter22Run3_V1_DATA_SF_AK4PFPuppi.txt'],
        '2022E': ['JR_Winter22Run3_V1_DATA/JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt',
                  'JR_Winter22Run3_V1_DATA/JR_Winter22Run3_V1_DATA_SF_AK4PFPuppi.txt'],
        '2022F': ['JR_Winter22Run3_V1_DATA/JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt',
                  'JR_Winter22Run3_V1_DATA/JR_Winter22Run3_V1_DATA_SF_AK4PFPuppi.txt'],
        'MC': ['JR_Winter22Run3_V1_MC/JR_Winter22Run3_V1_MC_PtResolution_AK4PFPuppi.txt',
               'JR_Winter22Run3_V1_MC/JR_Winter22Run3_V1_MC_SF_AK4PFPuppi.txt']
    }
}
"""
class JEC():
    def __init__(self,JECName,year,run,isData):
        # JEC
        if not JECName in JEC_CORRECTION_SETS.keys():
            JECName = year
        if not JECName in JEC_CORRECTION_SETS.keys():
            print("JEC",JECName,"not found in JEC_CORRECTION_SETS. Please add the missing set in ../Framework/python/JetCorrections.py")
            sys.exit()

        self.name = JECName

        self.isData = isData
        if len(JEC_CORRECTION_SETS[JECName]) == 0:
            print("Not using JEC")
            self.noCorrections = True
        else:
            self.noCorrections = False

            CORRECTION_SET = JEC_CORRECTION_SETS[JECName]
            CSET = "MC"
            if isData:
                CSET = run

            for corr in CORRECTION_SET[CSET]:
                print(" Using JEC",os.path.basename(corr).split('.')[0])

            datapath = getDataPath()
            weight_sets = ['* * %s'%(os.path.join(datapath,'JECDatabase/textFiles',correctionset)) for correctionset in CORRECTION_SET[CSET]]
            self.jec_stack_names = [os.path.basename(correctionset).split('.')[0] for correctionset in CORRECTION_SET[CSET]]

            ext = extractor()
            ext.add_weight_sets(weight_sets)
            ext.finalize()

            evaluator = ext.make_evaluator()
            jec_inputs = {name: evaluator[name] for name in self.jec_stack_names}
            jec_stack = JECStack(jec_inputs)

            name_map = jec_stack.blank_name_map
            name_map['JetPt'] = 'pt'
            name_map['JetMass'] = 'mass'
            name_map['JetEta'] = 'eta'
            name_map['JetA'] = 'area'
            name_map['ptRaw'] = 'pt_raw'
            name_map['massRaw'] = 'mass_raw'
            name_map['Rho'] = 'rho'
            name_map['ptGenJet'] = 'pt_genjet'

            self.jet_factory = CorrectedJetsFactory(name_map, jec_stack)

            # JEC L1RC only
            self.l1rc_found = False
            L1RCName = JECName+'L1RC'
            if L1RCName in JEC_CORRECTION_SETS.keys():
                self.l1rc_found = True
                CORRECTION_SET_L1 = JEC_CORRECTION_SETS[L1RCName]
                for corr in CORRECTION_SET_L1[CSET]:
                    print(" Using L1RC",os.path.basename(corr).split('.')[0])
                weight_sets_l1 = ['* * %s'%(os.path.join(datapath,'JECDatabase/textFiles',correctionset)) for correctionset in CORRECTION_SET_L1[CSET]]
                jec_stack_names_l1 = [os.path.basename(correctionset).split('.')[0] for correctionset in CORRECTION_SET_L1[CSET]]

                ext_l1 = extractor()
                ext_l1.add_weight_sets(weight_sets_l1)
                ext_l1.finalize()

                evaluator_l1 = ext_l1.make_evaluator()

                jec_inputs_l1 = {name: evaluator_l1[name] for name in jec_stack_names_l1}
                jec_stack_l1 = JECStack(jec_inputs_l1)

                name_map_l1 = jec_stack_l1.blank_name_map
                name_map_l1['JetPt'] = 'pt'
                name_map_l1['JetMass'] = 'mass'
                name_map_l1['JetEta'] = 'eta'
                name_map_l1['JetA'] = 'area'
                name_map_l1['ptRaw'] = 'pt_raw'
                name_map_l1['massRaw'] = 'mass_raw'
                name_map_l1['Rho'] = 'rho'
                name_map_l1['ptGenJet'] = 'pt_genjet'

                self.jet_factory_l1 = CorrectedJetsFactory(name_map_l1, jec_stack_l1)

            # MET for new JEC
            name_map["METpt"] = "pt"
            name_map["METphi"] = "phi"
            name_map["JetPhi"] = "phi"
            name_map["UnClusteredEnergyDeltaX"] = "MetUnclustEnUpDeltaX"
            name_map["UnClusteredEnergyDeltaY"] = "MetUnclustEnUpDeltaY"
            self.met_factory = CorrectedMETFactory(name_map)

            # JER, JER Scale Factor
            CORRECTION_SET_JER = JER_CORRECTION_SETS[JECName]
            for corr in CORRECTION_SET_JER[CSET]:
                print(" Using JR",os.path.basename(corr).split('.')[0])

            JERweight_sets = ['* * %s'%(os.path.join(datapath,'JRDatabase/textFiles',correctionset)) for correctionset in CORRECTION_SET_JER[CSET]]
            jer_stack_names = [os.path.basename(correctionset).split('.')[0] for correctionset in CORRECTION_SET_JER[CSET]]
            if len(jer_stack_names) > 0:
                jerext = extractor()
                jerext.add_weight_sets(JERweight_sets)
                jerext.finalize()
                jerevaluator = jerext.make_evaluator()
                self.reso = JetResolution(**{name: jerevaluator[name] for name in jer_stack_names[0:1]})
                self.resosf = JetResolutionScaleFactor(**{name: jerevaluator[name] for name in jer_stack_names[1:2]})


    def getCorrections(self):
        corrections = []
        corrections.append(self.name)
        if hasattr(self,"jec_stack_names"):
            corrections.extend(self.jec_stack_names)
        return corrections

    def apply(self,events):
        #if self.noCorrections:
        #    return events.Jet

        jets = events.Jet

        jets['pt_orig'] = jets['pt']
        jets['pt_raw'] = (1 - jets['rawFactor']) * jets['pt']
        jets['mass_raw'] = (1 - jets['rawFactor']) * jets['mass']
        jets['rho'] = ak.broadcast_arrays(events.fixedGridRhoFastjetAll, jets.pt)[0]

        corrected_jets = copy.deepcopy(jets)
        jets_l1 = copy.deepcopy(jets)

        if self.noCorrections:
            corrected_jets['pt'] = jets['pt_raw']
            corrected_jets['mass'] = jets['mass_raw']
            return corrected_jets

        if not self.isData:
            jets['pt_genjet'] = events.GenJet[jets.genJetIdx].pt

        events_cache = events.caches[0]
        ####corrected_jets_array = self.jet_factory.build(jets, lazy_cache=events_cache)
        corrected_jets_array = self.CorrectedJetsFactorybuild(jets, lazy_cache=events_cache)
        corrected_jets['pt'] = corrected_jets_array['pt']
        corrected_jets['mass'] = corrected_jets_array['mass']
        corrected_jets['corr'] = corrected_jets_array['jet_energy_correction']

        if hasattr(corrected_jets_array,'jet_energy_subcorrections'):
            counts = ak.num(corrected_jets_array['jet_energy_subcorrections'])
            subcorr = ak.flatten(corrected_jets_array['jet_energy_subcorrections'])
            res = ak.unflatten(subcorr[:,-1]/subcorr[:,-2],counts)
            corrected_jets['res'] = ak.Array(res)

        if self.l1rc_found:
            events_cache_l1 = events.caches[0]
            corrected_jets_l1_array = self.jet_factory_l1.build(jets_l1, lazy_cache=events_cache_l1)
            corrected_jets["pt_l1rc"] = corrected_jets_l1_array['pt']
            corrected_jets['mass_l1rc'] = corrected_jets_l1_array['mass']
        if not self.isData:
            smear = self.smearing(events)
            corrected_jets["pt"] = corrected_jets.pt*smear
            corrected_jets["mass"] = corrected_jets.mass*smear
            if self.l1rc_found:
                corrected_jets["pt_l1rc"] = corrected_jets_l1_array['pt']*smear
                corrected_jets['mass_l1rc'] = corrected_jets_l1_array['mass']*smear
        # pt-order the corrected jets
        corrected_jets = corrected_jets[ak.argsort(corrected_jets.pt, axis=1, ascending=False)]
        return corrected_jets

    def recalculateMET(self,events):
        if self.noCorrections:
            return events.MET
        met = events.MET
        corrected_jets = events.Jet
        jec_cache = cachetools.Cache(np.inf)
        corrected_met = self.met_factory.build(met, corrected_jets, lazy_cache=jec_cache)
        return corrected_met

    def recalculateMET2(self,events,puppi=False):
        met = events.ChsMET
        if puppi:
            met = events.PuppiMET

        if self.noCorrections:
            met = events.RawMET
            if puppi:
                met = events.RawPuppiMET
            return met

        jets = events.Jet[(events.Jet.pt>15)]
        jet_pt_corr     = jets.pt
        jet_phi         = jets.phi
        jet_pt_orig     = jets.pt_orig
        jet_pt_raw      = jets.pt_raw
        sj, cj = np.sin(jet_phi), np.cos(jet_phi)
        if hasattr(jets,"pt_l1rc"):
            jet_pt_corrL1rc = jets.pt_l1rc
            x = met.pt * np.cos(met.phi) + ak.sum(jet_pt_corrL1rc * cj - jet_pt_corr * cj, axis=1)
            y = met.pt * np.sin(met.phi) + ak.sum(jet_pt_corrL1rc * sj - jet_pt_corr * sj, axis=1)
            #met += jet_orig - jet_corr - (jet_raw - jet_corrL1rc)
        else:
            x = met.pt * np.cos(met.phi) + ak.sum(jet_pt_orig * cj - jet_pt_corr * cj - jet_pt_raw * cj, axis=1)
            y = met.pt * np.sin(met.phi) + ak.sum(jet_pt_orig * sj - jet_pt_corr * sj - jet_pt_raw * sj, axis=1)

        return ak.zip({"pt": np.hypot(x, y), "phi": np.arctan2(y, x), "px": x, "py": y})

    # MET = −sum(jets) -sum(uncl)
    # MET_type1 = −sum(jets_corrected) -sum(uncl) = −sum(jets_corrected) + MET +sum(jets)
    # met += jet_orig - jet_corr - (jet_raw - jet_corrL1rc);

    def smearing(self,events): # not needed, smearing done already by the POG code
        jets = events.Jet
        jets['pt_raw'] = (1 - jets['rawFactor']) * jets['pt']
        jets['rho'] = ak.broadcast_arrays(events.fixedGridRhoFastjetAll, jets.pt)[0]

        scalar_form = ak.without_parameters(jets["pt_raw"]).layout.form
        jec_cache = cachetools.Cache(np.inf)
        jetResolution = ak.flatten(self.reso.getResolution(
            JetEta=jets["eta"],
            Rho=jets["rho"],
            JetPt=jets["pt_raw"],
            form=scalar_form,
            lazy_cache=jec_cache,
        ))
        eta = ak.flatten(jets["eta"])

        jetResolutionSF = self.resosf.getScaleFactor(JetEta=eta)
        # getScaleFactor output format central-up-down, taking the central value
        jetResolutionSF = jetResolutionSF[:, 0]

        #KIT: jecSmearFactor = 1 + std::normal_distribution<>(0, jetResolution)(m_randomNumberGenerator) * std::sqrt(std::max(jetResolutionSF * jetResolutionSF - 1, 0.0));
        #KIT: // apply factor (prevent negative values)
        #KIT: recoJets[iJet]->p4 *= (jecSmearFactor < 0) ? 0.0 : jecSmearFactor;

        jecSmearFactor = np.random.normal(1.0,jetResolution)*np.sqrt(np.maximum(jetResolutionSF*jetResolutionSF-1,0))
        jecSmearFactor = ak.unflatten(jecSmearFactor,ak.num(events.Jet.eta))
        return jecSmearFactor

    def CorrectedJetsFactorybuild(self, jets, lazy_cache):
        if lazy_cache is None:
            raise Exception(
                "CorrectedJetsFactory requires a awkward-array cache to function correctly."
            )
        lazy_cache = ak._util.MappingProxy.maybe_wrap(lazy_cache)
        if not isinstance(jets, ak.highlevel.Array):
            raise Exception("'jets' must be an awkward > 1.0.0 array of some kind!")
        fields = ak.fields(jets)
        if len(fields) == 0:
            raise Exception(
                "Empty record, please pass a jet object with at least {self.real_sig} defined!"
            )
        out = ak.flatten(jets)
        wrap = partial(awkward_rewrap, like_what=jets, gfunc=rewrap_recordarray)
        scalar_form = ak.without_parameters(
            out[self.jet_factory.name_map["ptRaw"]]
        ).layout.form

        in_dict = {field: out[field] for field in fields}
        out_dict = dict(in_dict)

        # take care of nominal JEC (no JER if available)
        out_dict[self.jet_factory.name_map["JetPt"] + "_orig"] = out_dict[self.jet_factory.name_map["JetPt"]]
        out_dict[self.jet_factory.name_map["JetMass"] + "_orig"] = out_dict[
            self.jet_factory.name_map["JetMass"]
        ]
        if self.jet_factory.treat_pt_as_raw:
            out_dict[self.jet_factory.name_map["ptRaw"]] = out_dict[self.jet_factory.name_map["JetPt"]]
            out_dict[self.jet_factory.name_map["massRaw"]] = out_dict[self.jet_factory.name_map["JetMass"]]

        jec_name_map = dict(self.jet_factory.name_map)
        jec_name_map["JetPt"] = jec_name_map["ptRaw"]
        jec_name_map["JetMass"] = jec_name_map["massRaw"]
        if self.jet_factory.jec_stack.jec is not None:
            jec_args = {
                k: out_dict[jec_name_map[k]] for k in self.jet_factory.jec_stack.jec.signature
            }
            out_dict["jet_energy_correction"] = self.jet_factory.jec_stack.jec.getCorrection(
                **jec_args, form=scalar_form, lazy_cache=lazy_cache
            )
            jet_energy_subcorrections = self.jet_factory.jec_stack.jec.getSubCorrections(
                **jec_args, form=scalar_form, lazy_cache=lazy_cache
            )
            out_dict["jet_energy_subcorrections"] = np.transpose(jet_energy_subcorrections)
        else:
            out_dict["jet_energy_correction"] = ak.without_parameters(
                ak.ones_like(out_dict[self.jet_factory.name_map["JetPt"]])
            )

        # finally the lazy binding to the JEC
        init_pt = partial(
            ak.virtual,
            operator.mul,
            args=(out_dict["jet_energy_correction"], out_dict[self.jet_factory.name_map["ptRaw"]]),
            cache=lazy_cache,
        )
        init_mass = partial(
            ak.virtual,
            operator.mul,
            args=(
                out_dict["jet_energy_correction"],
                out_dict[self.jet_factory.name_map["massRaw"]],
            ),
            cache=lazy_cache,
        )

        out_dict[self.jet_factory.name_map["JetPt"]] = init_pt(length=len(out), form=scalar_form)
        out_dict[self.jet_factory.name_map["JetMass"]] = init_mass(
            length=len(out), form=scalar_form
        )

        out_dict[self.jet_factory.name_map["JetPt"] + "_jec"] = out_dict[self.jet_factory.name_map["JetPt"]]
        out_dict[self.jet_factory.name_map["JetMass"] + "_jec"] = out_dict[self.jet_factory.name_map["JetMass"]]

        # in jer we need to have a stash for the intermediate JEC products
        has_jer = False
        if self.jet_factory.jec_stack.jer is not None and self.jet_factory.jec_stack.jersf is not None:
            has_jer = True
            jer_name_map = dict(self.jet_factory.name_map)
            jer_name_map["JetPt"] = jer_name_map["JetPt"] + "_jec"
            jer_name_map["JetMass"] = jer_name_map["JetMass"] + "_jec"

            jerargs = {
                k: out_dict[jer_name_map[k]] for k in self.jet_factory.jec_stack.jer.signature
            }
            out_dict["jet_energy_resolution"] = self.jet_factory.jec_stack.jer.getResolution(
                **jerargs, form=scalar_form, lazy_cache=lazy_cache
            )

            jersfargs = {
                k: out_dict[jer_name_map[k]] for k in self.jet_factory.jec_stack.jersf.signature
            }
            out_dict[
                "jet_energy_resolution_scale_factor"
            ] = self.jet_factory.jec_stack.jersf.getScaleFactor(
                **jersfargs, form=_JERSF_FORM, lazy_cache=lazy_cache
            )

            seeds = numpy.array(out_dict[self.jet_factory.name_map["JetPt"] + "_orig"])[
                [0, -1]
            ].view("i4")
            out_dict["jet_resolution_rand_gauss"] = ak.virtual(
                rand_gauss,
                args=(
                    out_dict[self.jet_factory.name_map["JetPt"] + "_orig"],
                    numpy.random.Generator(numpy.random.PCG64(seeds)),
                ),
                cache=lazy_cache,
                length=len(out),
                form=scalar_form,
            )

            init_jerc = partial(
                ak.virtual,
                jer_smear,
                args=(
                    0,
                    self.jet_factory.forceStochastic,
                    out_dict[jer_name_map["ptGenJet"]],
                    out_dict[jer_name_map["JetPt"]],
                    out_dict[jer_name_map["JetEta"]],
                    out_dict["jet_energy_resolution"],
                    out_dict["jet_resolution_rand_gauss"],
                    out_dict["jet_energy_resolution_scale_factor"],
                ),
                cache=lazy_cache,
            )
            out_dict["jet_energy_resolution_correction"] = init_jerc(
                length=len(out), form=scalar_form
            )

            init_pt_jer = partial(
                ak.virtual,
                operator.mul,
                args=(
                    out_dict["jet_energy_resolution_correction"],
                    out_dict[jer_name_map["JetPt"]],
                ),
                cache=lazy_cache,
            )
            init_mass_jer = partial(
                ak.virtual,
                operator.mul,
                args=(
                    out_dict["jet_energy_resolution_correction"],
                    out_dict[jer_name_map["JetMass"]],
                ),
                cache=lazy_cache,
            )
            out_dict[self.jet_factory.name_map["JetPt"]] = init_pt_jer(
                length=len(out), form=scalar_form
            )
            out_dict[self.jet_factory.name_map["JetMass"]] = init_mass_jer(
                length=len(out), form=scalar_form
            )
            out_dict[self.jet_factory.name_map["JetPt"] + "_jer"] = out_dict[self.jet_factory.name_map["JetPt"]]
            out_dict[self.jet_factory.name_map["JetMass"] + "_jer"] = out_dict[
                self.jet_factory.name_map["JetMass"]
            ]

            # JER systematics
            jerc_up = partial(
                ak.virtual,
                jer_smear,
                args=(
                    1,
                    self.jet_factory.forceStochastic,
                    out_dict[jer_name_map["ptGenJet"]],
                    out_dict[jer_name_map["JetPt"]],
                    out_dict[jer_name_map["JetEta"]],
                    out_dict["jet_energy_resolution"],
                    out_dict["jet_resolution_rand_gauss"],
                    out_dict["jet_energy_resolution_scale_factor"],
                ),
                cache=lazy_cache,
            )
            up = ak.flatten(jets)
            up["jet_energy_resolution_correction"] = jerc_up(
                length=len(out), form=scalar_form
            )
            init_pt_jer = partial(
                ak.virtual,
                operator.mul,
                args=(
                    up["jet_energy_resolution_correction"],
                    out_dict[jer_name_map["JetPt"]],
                ),
                cache=lazy_cache,
            )
            init_mass_jer = partial(
                ak.virtual,
                operator.mul,
                args=(
                    up["jet_energy_resolution_correction"],
                    out_dict[jer_name_map["JetMass"]],
                ),
                cache=lazy_cache,
            )
            up[self.jet_factory.name_map["JetPt"]] = init_pt_jer(length=len(out), form=scalar_form)
            up[self.jet_factory.name_map["JetMass"]] = init_mass_jer(
                length=len(out), form=scalar_form
            )

            jerc_down = partial(
                ak.virtual,
                jer_smear,
                args=(
                    2,
                    self.jet_factory.forceStochastic,
                    out_dict[jer_name_map["ptGenJet"]],
                    out_dict[jer_name_map["JetPt"]],
                    out_dict[jer_name_map["JetEta"]],
                    out_dict["jet_energy_resolution"],
                    out_dict["jet_resolution_rand_gauss"],
                    out_dict["jet_energy_resolution_scale_factor"],
                ),
                cache=lazy_cache,
            )
            down = ak.flatten(jets)
            down["jet_energy_resolution_correction"] = jerc_down(
                length=len(out), form=scalar_form
            )
            init_pt_jer = partial(
                ak.virtual,
                operator.mul,
                args=(
                    down["jet_energy_resolution_correction"],
                    out_dict[jer_name_map["JetPt"]],
                ),
                cache=lazy_cache,
            )
            init_mass_jer = partial(
                ak.virtual,
                operator.mul,
                args=(
                    down["jet_energy_resolution_correction"],
                    out_dict[jer_name_map["JetMass"]],
                ),
                cache=lazy_cache,
            )
            down[self.jet_factory.name_map["JetPt"]] = init_pt_jer(
                length=len(out), form=scalar_form
            )
            down[self.jet_factory.name_map["JetMass"]] = init_mass_jer(
                length=len(out), form=scalar_form
            )
            out_dict["JER"] = ak.zip(
                {"up": up, "down": down}, depth_limit=1, with_name="JetSystematic"
            )

        if self.jet_factory.jec_stack.junc is not None:
            juncnames = {}
            juncnames.update(self.jet_factory.name_map)
            if has_jer:
                juncnames["JetPt"] = juncnames["JetPt"] + "_jer"
                juncnames["JetMass"] = juncnames["JetMass"] + "_jer"
            else:
                juncnames["JetPt"] = juncnames["JetPt"] + "_jec"
                juncnames["JetMass"] = juncnames["JetMass"] + "_jec"
            juncargs = {
                k: out_dict[juncnames[k]] for k in self.jet_factory.jec_stack.junc.signature
            }
            juncs = self.jet_factory.jec_stack.junc.getUncertainty(**juncargs)

            def junc_smeared_val(uncvals, up_down, variable):
                return ak.materialized(uncvals[:, up_down] * variable)

            def build_variation(unc, jetpt, jetpt_orig, jetmass, jetmass_orig, updown):
                var_dict = dict(in_dict)
                var_dict[jetpt] = ak.virtual(
                    junc_smeared_val,
                    args=(
                        unc,
                        updown,
                        jetpt_orig,
                    ),
                    length=len(out),
                    form=scalar_form,
                    cache=lazy_cache,
                )
                var_dict[jetmass] = ak.virtual(
                    junc_smeared_val,
                    args=(
                        unc,
                        updown,
                        jetmass_orig,
                    ),
                    length=len(out),
                    form=scalar_form,
                    cache=lazy_cache,
                )
                return ak.zip(
                    var_dict,
                    depth_limit=1,
                    parameters=out.layout.parameters,
                    behavior=out.behavior,
                )

            def build_variant(unc, jetpt, jetpt_orig, jetmass, jetmass_orig):
                up = build_variation(unc, jetpt, jetpt_orig, jetmass, jetmass_orig, 0)
                down = build_variation(unc, jetpt, jetpt_orig, jetmass, jetmass_orig, 1)
                return ak.zip(
                    {"up": up, "down": down}, depth_limit=1, with_name="JetSystematic"
                )

            for name, func in juncs:
                out_dict[f"jet_energy_uncertainty_{name}"] = func
                out_dict[f"JES_{name}"] = build_variant(
                    func,
                    self.jet_factory.name_map["JetPt"],
                    out_dict[juncnames["JetPt"]],
                    self.jet_factory.name_map["JetMass"],
                    out_dict[juncnames["JetMass"]],
                )

        out_parms = out.layout.parameters
        out_parms["corrected"] = True
        out = ak.zip(
            out_dict, depth_limit=1, parameters=out_parms, behavior=out.behavior
        )

        return wrap(out)
