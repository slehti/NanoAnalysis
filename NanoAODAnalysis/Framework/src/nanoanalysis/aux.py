import subprocess
import sys

def execute(cmd, echo=False):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    f = p.stdout
    ret=[]
    for line in f:
        line = line.decode('utf-8')
        if echo:
            sys.stdout.write(line)
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def listDirectoryContent(tdirectory): #, predicate=None):
    if not hasattr(tdirectory, "GetListOfKeys"):
        return None

    dirlist = tdirectory.GetListOfKeys()

    # Suppress the warning message of missing dictionary for some iterator
    backup = ROOT.gErrorIgnoreLevel
    ROOT.gErrorIgnoreLevel = ROOT.kError
    diriter = dirlist.MakeIterator()
    ROOT.gErrorIgnoreLevel = backup

    key = diriter.Next()

    ret = []
    while key:
        #if predicate is not None and predicate(key):
        #    ret.append(key.GetName())
        #elif predicate == None:
        ret.append(key.GetName())
        key = diriter.Next()
    print("check keys",ret)
    return ret
