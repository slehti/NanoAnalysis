from functools import cache
from importlib.resources import files

import ROOT as R

from nanoanalysis.aux import execute

def initialize(cpp_module):
    src = cpp_module.__file__
    name = cpp_module.__name__.split(".")[-1]
    header = source_path().joinpath(f"{name}.h")
    include(src, header)
    return

def jit_include(source_file, header_file=None):
    if header_file is None:
        header_file = source_file.replace(".cpp",".h")
    obj_file = header_file.replace(".h",".o")
    shared_file = obj_file.replace(".o",".so")
    execute(
        f"g++ -c -o {obj_file} {source_file} -O3 -march=native "
        "$(root-config --glibs --cflags --libs) -fPIC; "
        f"gcc -shared -o {shared_file} {obj_file} "
        "$(root-config --glibs --cflags --libs)"
    )
    return include(shared_file, header_file)

def include(shared_object, header_file):
    R.gSystem.Load(shared_object)
    R.gInterpreter.Declare(f"#include \"{header_file}\"")
    return

def DeclareToCpp(**kwargs):
    for k, v in kwargs.items():
        R.gInterpreter.Declare(
            f"namespace PyVars {{ const auto &{k} ="
            f"*reinterpret_cast<{type(v).__cpp_name__}*>({R.addressof(v)}); }}"
        )
    return

@cache
def source_path():
    headers = files('nanoanalysis.include')
    R.gInterpreter.AddIncludePath(str(headers))
    return headers
