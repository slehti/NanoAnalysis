#!/usr/bin/env python

import os
import sys
import re
import numpy as np
# import numba as nb
# import dask_awkward as dak
# import awkward as ak

from coffea.lookup_tools import extractor

from nanoanalysis.DataPath import getDataPath

# Get pileup for data as a function of run and lumiSection.
# It needs the pileup json file for lookup, value from the
# file multiplied with the minbias cross section
#
# Usage: call parsePileUpJSON2 in the Analysis.__init__, which
# makes the lookup table, and call getAvgPU in the Analysis.process
#
# pileup jsons
# scp lxplus.cern.ch:/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/PileUp/UltraLegacy/pileup_latest.txt pileup_2016.txt
# scp lxplus.cern.ch:/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/PileUp/UltraLegacy/pileup_latest.txt pileup_2017.txt
# scp lxplus.cern.ch:/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PileUp/UltraLegacy/pileup_latest.txt pileup_2018.txt


#MINBIAS_XS = 69200
ext = None

def getAvgPU(run,luminosityBlock):
    global ext
    evaluator = ext.make_evaluator()
    return evaluator["pileup"](run,luminosityBlock)

def getAvgPUNoGlobal(ext,run,luminosityBlock):
    evaluator = ext.make_evaluator()
    return evaluator["pileup"](run,luminosityBlock)

# NOTE: The commented-out functions are an attempt at a coffea version 202X
#       compatible pileup parsing method. They are left commented as a
#       reference/backup in case efforts to migrate to correctionlib or lumitools
#       fail when this functionality is required in a migrated analysis.
#       See https://github.com/CoffeaTeam/coffea/issues/1108

# def getAvgPUDask(pu_stored, runs, lumiBlocks):
#     def apply_PU_numpy(runs, lumis):
#         backend = ak.backend(runs)
#         runs = ak.to_numpy(
#             ak.typetracer.length_zero_if_typetracer(runs), allow_missing=False
#         )
#         lumis = ak.to_numpy(
#             ak.typetracer.length_zero_if_typetracer(lumis), allow_missing=False
#         )
#         pileups = np.empty(dtype=np.float64, shape=runs.shape)
#         _apply_avgPUkernel(pu_stored, runs,lumis,pileups)
#         pileups = ak.Array(pileups)
#         if backend == "typetracer":
#             pileups = ak.Array(
#                 pileups.layout.to_typetracer(forget_length=True)
#             )
#         return pileups
#     return dak.map_partitions(apply_PU_numpy, runs, lumiBlocks)
#
# @nb.njit(parallel=True,fastmath=True)
# def _apply_avgPUkernel(pu_stored, runs, lumiBlocks, pileups):
#     pu_values, nshift, rmin, lmin = pu_stored
#     for iev in nb.prange(len(runs)):
#         run = np.int64(runs[iev]) - rmin
#         lumi= np.int64(lumiBlocks[iev]) - lmin
#         idx = (run << nshift) | lumi
#         pileups[iev] = pu_values[idx]

def parsePileUpJSON2(year):
    datapath = getDataPath()
    filename = ""
    MINBIAS_XS = 69200
    if "2016" in year:
        filename = "pileup_2016.txt"
    if "2017" in year:
        filename = "pileup_2017.txt"
    if "2018" in year:
        filename = "pileup_2018.txt"
    if "2022" in year:
        filename = "pileup_latest_2022.txt"
        MINBIAS_XS = 80000

    filename = os.path.join(datapath,'pileup',filename)
    print("Data pileup from",os.path.basename(filename),", Minimum Bias Cross Section: ",MINBIAS_XS)

    global ext
    ext = extractor()
    ext.add_weight_sets(["pileup pileup %s"%filename])
    multiplyWeight(ext._weights,MINBIAS_XS)
    ext.finalize()

def parsePileUpJSONNoGlobal(year):
    datapath = getDataPath()
    filename = ""
    MINBIAS_XS = 69200
    if "2016" in year:
        filename = "pileup_2016.txt"
    if "2017" in year:
        filename = "pileup_2017.txt"
    if "2018" in year:
        filename = "pileup_2018.txt"
    if "2022" in year:
        filename = "pileup_latest_2022.txt"
        MINBIAS_XS = 80000

    filename = os.path.join(datapath,'pileup',filename)
    print("Data pileup from",os.path.basename(filename),", Minimum Bias Cross Section: ",MINBIAS_XS)

    ext = extractor()
    ext.add_weight_sets(["pileup pileup %s"%filename])
    multiplyWeight(ext._weights,MINBIAS_XS)
    ext.finalize()
    return ext

# def parsePileUpJSONasArray(year):
#     ext = parsePileUpJSONNoGlobal(year)
#     pu_table = ext._weights[0][0]
#     runkeys = []
#     runmax = 0
#     runmin = np.inf
#     lumikeys= []
#     lumimax = 0
#     lumimin = np.inf
#     for runkey in pu_table:
#         runkey_i = np.int64(runkey)
#         runmax = max(runmax, runkey_i)
#         runmin = min(runmin, runkey_i)
#         for lumikey in pu_table[runkey]:
#             lumikey_i = np.int64(lumikey)
#             lumimax= max(lumimax, lumikey_i)
#             lumimin= min(lumimin, lumikey_i)
#             runkeys.append(runkey)
#             lumikeys.append(lumikey)
#     nshift = np.uint32(np.ceil(np.log2(lumimax-lumimin)))
#     nrunbits= np.uint32(np.ceil(np.log2(runmax-runmin)))
#     out_values = np.empty(2**(nshift+nrunbits),dtype=np.float64)
#     lmin = np.int64(lumimin)
#     rmin = np.int64(runmin)
#     for rkey, lkey in zip(runkeys, lumikeys):
#         lkey_i = np.int64(lkey) - lmin
#         rkey_i = np.int64(rkey) - rmin
#         idx = (rkey_i << nshift) | lkey_i
#         out_values[idx] = pu_table[rkey][lkey]
#     return out_values, nshift, rmin, lmin
#
# def multiplyWeight(weightset,value):
#     for i in range(len(weightset)):
#         w2 = weightset[i]
#         for j in range(len(w2)):
#             w3 = w2[j]
#             for k in w3.keys():
#                 w4 = w3[k]
#                 for l in w4.keys():
#                     w4[l] = w4[l]*value


