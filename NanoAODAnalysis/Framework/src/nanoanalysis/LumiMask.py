import os
import json

from nanoanalysis.aux import execute

# SKIMPYTHONPATH = getSkimPythonPath()
# if not SKIMPYTHONPATH in sys.path:
#     sys.path.append(SKIMPYTHONPATH)
# DATAPATH = getSkimDataPath()

def FindLumiJSON(dataset):
    if not dataset.isData:
        return None

    runrange = dataset.runRange.replace('_','-')
    jsonpath = os.path.join(dataset.path,'inputs')
    jsonfile = execute("ls %s/Cert*.*"%(jsonpath))[0]
    if os.path.exists(jsonfile):
        print("Using lumimask",os.path.basename(jsonfile))
        return jsonfile
    print("No json file for lumimask found for run range %s. File Framework/python/LumiMask.py"%runrange)
    return None

class LumiMask():
    def __init__(self,dataset,lumijson = ""):
        self.isdata = dataset.isData
        if self.isdata:
            if lumijson == "":
                lumijson = FindLumiJSON(dataset)
            self.LoadJSON(lumijson)

    def passed(self,event):
        if not self.isdata:
            return [True]*len(event)

        run = event.run
        lumi = event.luminosityBlock

        value = [str(i) in self.lumidata.keys() and (j in itertools.chain.from_iterable(range(self.lumidata[str(i)][k][0],self.lumidata[str(i)][k][1]+1) for k in range(len(self.lumidata[str(i)])))) for i,j in zip(run,lumi)]

        return value

    def LoadJSON(self,fname):
        f = open(fname)
        self.lumidata = json.load(f)
        f.close()

class RDF_LumiMask:
    def __init__(self,dataset,lumijson = ""):
        from nanoanalysis import cpptools
        self.jsonname = f"goldenjson_{dataset.name}"
        self.isdata = dataset.isData
        if self.isdata:
            if lumijson == "":
                lumijson = FindLumiJSON(dataset)
            self.LoadJSON(lumijson)

    @staticmethod
    def declare():
        from nanoanalysis import cpptools
        from nanoanalysis.rdf_backend import LumiMask
        cpptools.initialize(LumiMask)

    def __call__(self,df,is_data):
        if not is_data: return df

        return df.Filter(
            f"nanoanalysis::is_good_lumi(PyVars::{self.jsonname}, run, luminosityBlock)",
            "luminosity mask"
        )

    def LoadJSON(self,fname):
        import ROOT as R
        from nanoanalysis import cpptools
        self.json = R.nanoanalysis.init_json(fname)
        cpptools.DeclareToCpp(**{self.jsonname: self.json})
        return

