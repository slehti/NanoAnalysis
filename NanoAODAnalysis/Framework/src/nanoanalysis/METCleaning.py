def METCleaning(events,year):
    if '2016' in year:
        return (events.Flag.goodVertices &
                events.Flag.globalSuperTightHalo2016Filter &
                events.Flag.HBHENoiseFilter &
                events.Flag.HBHENoiseIsoFilter &
                events.Flag.BadPFMuonFilter &
                events.Flag.eeBadScFilter
        )

    return (events.Flag.goodVertices &
            events.Flag.globalSuperTightHalo2016Filter &
            events.Flag.HBHENoiseFilter &
            events.Flag.HBHENoiseIsoFilter &
            events.Flag.EcalDeadCellTriggerPrimitiveFilter &
            events.Flag.BadPFMuonFilter &
            events.Flag.BadPFMuonDzFilter &
            events.Flag.eeBadScFilter &
            events.Flag.ecalBadCalibFilter
    )

def rdf_METCleaning(df,year=None):
    if year is None:
        dynamic_flags = """
        (year == \"2016\" && (
            Flag_goodVertices &&
            Flag_globalSuperTightHalo2016Filter &&
            Flag_HBHENoiseFilter &&
            Flag_HBHENoiseIsoFilter &&
            Flag_BadPFMuonFilter &&
            Flag_eeBadScFilter
        )) |
        ( year != \"2016\" && (
            Flag_goodVertices &&
            Flag_globalSuperTightHalo2016Filter &&
            Flag_HBHENoiseFilter &&
            Flag_HBHENoiseIsoFilter &&
            Flag_EcalDeadCellTriggerPrimitiveFilter &&
            Flag_BadPFMuonFilter &&
            Flag_BadPFMuonDzFilter &&
            Flag_eeBadScFilter &&
            Flag_ecalBadCalibFilter
        ))
        """
        return df.Filter(dynamic_flags)

    if '2016' in year:
        return df.Filter(
            """
            (Flag_goodVertices &&
            Flag_globalSuperTightHalo2016Filter &&
            Flag_HBHENoiseFilter &&
            Flag_HBHENoiseIsoFilter &&
            Flag_BadPFMuonFilter &&
            Flag_eeBadScFilter
            )
            """,
            "MET cleaning"
        )

    return df.Filter(
        """
        (Flag_goodVertices &&
        Flag_globalSuperTightHalo2016Filter &&
        Flag_HBHENoiseFilter &&
        Flag_HBHENoiseIsoFilter &&
        Flag_EcalDeadCellTriggerPrimitiveFilter &&
        Flag_BadPFMuonFilter &&
        Flag_BadPFMuonDzFilter &&
        Flag_eeBadScFilter &&
        Flag_ecalBadCalibFilter
        )
        """,
        "MET cleaning"
    )

