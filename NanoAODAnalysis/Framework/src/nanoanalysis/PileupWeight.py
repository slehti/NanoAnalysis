#!/usr/bin/env python

import numpy as np
from nanoanalysis import multicrabdatasets

class PileupWeight:
    def __init__(self, hDataPU, hMCPU):
        from coffea import lookup_tools
        puD = hDataPU.values()
        puD = np.divide(puD,puD.sum()) # normalize area=1

        puMC = hMCPU.values()
        puMC = np.divide(puMC,puMC.sum()) # normalize area=1

        puD = np.where(puMC == 0, 0, puD) # remove elements where MC gives zero, in order to have weight zero
        puMC = np.add(puMC,1.e-20) # add epsilon to prevent division by zero

        self.PUfuncD = lookup_tools.dense_lookup.dense_lookup(puD, hDataPU.axes[0].edges)
        self.PUfuncMC= lookup_tools.dense_lookup.dense_lookup(puMC, hDataPU.axes[0].edges)

        """
        pu = hDataPU.axis(0).edges()
        print("check puD",puD)
        print("check puMC",puMC)
        print("check puWeight",puWeight)
        print("check axis",pu)
        print("check puWeight",self.PUfunc(20))
        """

    def getWeight(self, nInteractions, var=0):
        nData=self.PUfuncD(nInteractions)
        nMC  =self.PUfuncMC(nInteractions)
        weights = np.divide(nData,nMC)
        return weights

class RDF_PileupWeight:
    def __init__(self, multicrab, datasets):
        import ROOT as R
        from nanoanalysis import cpptools

        pileup_data = multicrabdatasets.getDataPileup(multicrab,datasets)
        pileup_data = pileup_data / pileup_data.sum().value
        edges = pileup_data.axes[0].edges
        pu_data = pileup_data.values()

        self.pileup_histos = {}
        for dataset in datasets:
            histo_name = self.get_weight_name(dataset.name)
            weight_histo = R.TH1D(histo_name, "pu_weights", len(pu_data), edges)
            if dataset.isData:
                pileup_mc = pileup_data.values()
            else:
                pileup_mc = dataset.getPileup().to_hist()
                pileup_mc = (pileup_mc / pileup_mc.sum()).values()


            try:
                pu_data / pileup_mc
            except:
                breakpoint()
            pu_weight = np.where(pileup_mc==0, 0, pu_data/pileup_mc)

            for i, w in enumerate(pu_weight):
                weight_histo.SetBinContent(i+1,w)
            self.pileup_histos[histo_name] = weight_histo

        cpptools.DeclareToCpp(**self.pileup_histos)

    def get_weight_name(self, dataset_name):
        return f"pu_weights_{dataset_name}"

    # Only call this for MC datasets!
    def apply_weights(self, df, name):
        weight_histo = f"PyVars::{self.get_weight_name(name)}"
        return df.Define("pileup_w", f"{weight_histo}.Interpolate(Pileup_nTrueInt)")

