class PickEvents:
    def __init__(self,filename):
        self.filename = filename
        self.run = []
        self.lumi = []
        self.event = []

    def pick(self,events):
        self.run.extend(events.run)
        self.lumi.extend(events.luminosityBlock)
        self.event.extend(events.event)
        self.write()

    def write(self):
        if len(self.event) > 0:
            fOUT = open(self.filename,"w")
            for i in range(len(self.event)):
                fOUT.write("%s:%s:%s\n"%(self.run[i],self.lumi[i],self.event[i]))
            fOUT.close()

    def print(self,events):
        #[print("%s:%s:%s"%(events.run[i],events.luminosityBlock[i],events.event[i])) for i in range(len(events))]
        with open("picked.txt", "a") as fOUT:
            [fOUT.write("%s:%s:%s\n"%(events.run[i],events.luminosityBlock[i],events.event[i])) for i in range(len(events))]

