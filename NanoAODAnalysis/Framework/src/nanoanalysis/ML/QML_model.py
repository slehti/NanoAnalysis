import inspect

import numpy as np
import tensorflow as tf
import keras
import tensorcircuit as tc

from nanoanalysis.ML.layers import (
	Sanitizer,
	MinMaxScaler,
	accumulate_mass_max_min,
)
from nanoanalysis.ML.func import (
    Print,
    ss, ns, ts, hs, ls, es, cs
)

K = tc.set_backend("tensorflow")

@keras.saving.register_keras_serializable(name="SerializableQuantumLayer")
class SerializableQuantumLayer(tc.keras.QuantumLayer):
    def __init__(self, w_shape, *args, **kwargs) -> None:
        f = get_circuit(w_shape, *args)
        self.visualize_circuit = get_circuit(w_shape, *args, visualize=True)
        super().__init__(f, w_shape)

    def get_config(self):
        return super().get_config()
        # config["f"] = cloudpickle.dumps(self.f).hex()
        # return config

    @classmethod
    def from_config(cls, config):
        # f = cloudpickle.loads(bytes.fromhex(config.pop("f")))
        return cls(**config)


def dense_head(qlayer_out):
    return tf.keras.layers.Dense(1, activation="sigmoid", name="class_out")(qlayer_out)

def softmax_head(qlayer_out):
    Ez = tf.stack((qlayer_out[:,::2], qlayer_out[:,1::2]), axis=1)
    exp_z = tf.math.exp(Ez)
    p_sig = tf.math.reduce_mean(exp_z[:,0] / tf.math.reduce_sum(exp_z, axis=1), axis=1, keepdims=True)
    return p_sig

def get_circuit(w_shape, features, single_gate_name, entangling_gate_name, visualize=False):
    single_gate = getattr(tc.gates, single_gate_name)
    entangling_gate = getattr(tc.gates, entangling_gate_name)
    nq = len(features)
    nl = w_shape[0][0]

    def run_circuit(x, lambdas, w):
        # lambdas = params[0]
        # w = params[1]
        c = tc.Circuit(nq)
        for i in range(nl):
            c = embedding(c, x, lambdas[i])
            c = entangler(
                c,
                single_gate,
                entangling_gate,
                w[i],
                i+1,
                single_gate_name,
                entangling_gate_name
            )

        outputs = K.stack([K.real(c.expectation([tc.gates.z(), [i]])) for i in range(nq)])

        if visualize:
            return tc.vis.qir2tex(c.to_qir(),nq)
        return K.reshape(outputs, [-1])

    return run_circuit

def embedding(c, x, lambdas):
    n = c.circuit_param["nqubits"]
    for i in range(n):
        c.rx(i, theta=x[i]*lambdas[i])

    return c

def entangler(c, single_gate, entangler_gate, params, d, single_name, entangler_name):
    n = c.circuit_param["nqubits"]
    ii = list(range(n))

    # parameterize
    for i in ii:
        c.apply(single_gate(*tf.unstack(params[i])), i, name=single_name)

    # entangle
    shifted = ii[d:] + ii[:d]
    for i, j in zip(ii, shifted):
        c.apply(entangler_gate(), i, j, name=entangler_name)
    return c

def initialize(fold_ds, args):

    # define input layer
    inputs = tf.keras.Input(shape=fold_ds.element_spec[0].shape[1:])

    # layer to transform each variable with their own specific functions
    feature_types = [name.strip("_0123456789") for name in args.input_features]
    sanitizer = Sanitizer(feature_types)
    x = sanitizer(inputs)

    # input feature standardization
    if args.standardize:
        stdzr = MinMaxScaler(scaled_max = np.pi, scaled_min = 0)
        Print("Adding input standardizer layer")
        stdzr.adapt(
            fold_ds.map(lambda *x: sanitizer(x[0]))
        )  # adapt the preprocessing layer to the inputs
        x = stdzr(x)

    # calculate the scale and location of the targets so that the nn outputs can be
    # scaled up s.t. an activation of 1 maps to the largest mass in the dataset.
    if args.regress_mass:
        raise NotImplementedError

    n_qubits = len(args.input_features)
    entangling_gate = getattr(tc.gates, args.entangling_gate)
    n_params = len(inspect.signature(entangling_gate).parameters)

    # weights include data re-uploading coefficients and circuit parameters
    w_shape = ((args.n_qlayers, n_qubits), (args.n_qlayers, n_qubits, n_params))
    circuit_layer = SerializableQuantumLayer(
        w_shape, args.input_features, args.single_gate, args.entangling_gate
    )
    Ez = circuit_layer(x)

    # compute classifier score
    if args.qml_head == "dense":
        y = dense_head(Ez)
    elif args.qml_head == "softmax":
        y = softmax_head(Ez)
    else:
        raise ValueError(
            f"Unknown PQC prediction head {args.qml_head}! "
            "Should be one of \"dense\", \"softmax\""
        )

    model = tf.keras.Model(inputs=inputs, outputs={"class_out": y})
    return model
