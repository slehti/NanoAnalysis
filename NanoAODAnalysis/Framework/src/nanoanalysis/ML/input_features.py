import os

def load_input_vars(savedir):
    name_opts = [
        "/input_vars.txt",
        "/input_variables.txt",
        "/input_columns.txt",
    ]
    found = False
    input_vars = []
    for name in name_opts:
        if os.path.exists(savedir + name):
            with open(savedir + name, "r") as f:
                input_vars = [l.rstrip() for l in f]
                found = True
    if not found:
        raise Exception(
            f"no input variable file found at {savedir}/input_vars.txt or input_variables.txt"
        )
    return input_vars

