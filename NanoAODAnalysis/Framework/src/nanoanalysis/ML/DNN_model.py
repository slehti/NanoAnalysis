import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import (
    Dense,
    Activation,
    BatchNormalization,
    Concatenate,
    Dropout,
)  # , Flatten, Input

from nanoanalysis.ML.func import (
    Print,
    ss, ns, ts, hs, ls, es, cs
)

from nanoanalysis.ML.layers import (
	Sanitizer,
	MinMaxScaler,
	accumulate_mass_max_min,
)

def initialize(
    fold_ds,
    args,

):
    # define input layer
    inputs = tf.keras.Input(shape=fold_ds.element_spec[0].shape[1:])
    # layer to drop undesired input variables (NOTE: dropped in favor of preprocessing)
    # in_sele = InputSelector([var != "_" for var in args.input_features])
    # x = in_sele(inputs)

    # layer to transform each variable with their own specific functions
    feature_types = [name.strip("_0123456789") for name in args.input_features]
    sanitizer = Sanitizer(feature_types)
    x = sanitizer(inputs)

    # optional input feature standardization
    if args.standardize:
        stdzr = MinMaxScaler()
        Print("Adding input standardizer layer")
        stdzr.adapt(
            fold_ds.map(lambda *x: sanitizer(x[0]))
        )  # adapt the preprocessing layer to the inputs
        x = stdzr(x)

    # calculate the scale and location of the targets so that the nn outputs can be
    # scaled up s.t. an activation of 1 maps to the largest mass in the dataset.
    if args.regress_mass:
        mass_max, mass_min = accumulate_mass_max_min(fold_ds, args)
        pred_max = np.log(np.log(mass_max + 1) + 1).astype(np.float32)

    # loop over hidden layers
    final_layer = len(args.neurons) - 1
    for iLayer, n in enumerate(args.neurons, 0):
        layer = "layer#%d" % (int(iLayer) + 1)
        if iLayer == len(args.neurons) - 1:
            layer += " (output Layer)"  # Layers of nodes between the input and output layers. There may be one or more of these layers.
        else:
            layer += " (hidden layer)"  # A layer of nodes that produce the output variables.

        Print(
            "Adding %s, with %s%d neurons%s and activation function %s"
            % (hs + layer + ns, ls, n, ns, ls + args.activation[iLayer] + ns),
            iLayer == 0,
        )

        # if regressing mass, add second output to final layer
        if iLayer == final_layer and args.regress_mass:
            class_pred = Dense(1)(x)
            class_pred = Activation(args.activation[iLayer], name="class_out")(
                class_pred
            )
            mass_pred = Dense(1)(x)
            mass_pred = Activation(args.mass_act)(mass_pred)
            mass_pred = tf.keras.layers.Lambda(
                lambda x: x * pred_max, name="mass_out"
            )(mass_pred)
            combined_pred = Concatenate(axis=-1, name="combined_out")(
                [class_pred, mass_pred]
            )
            outputs = {
                "class_out": class_pred,
                "mass_out": mass_pred,
                "combined": combined_pred,
            }
        # define model output layer
        elif iLayer == final_layer:
            x = Dense(args.neurons[iLayer])(x)
            outputs = {
                "class_out": Activation(args.activation[iLayer], name="class_out")(
                    x
                )
            }
        # define hidden layer
        else:
            x = Dense(args.neurons[iLayer])(x)
            x = Activation(args.activation[iLayer])(x)
            if args.batchnorm[iLayer]:
                x = BatchNormalization()(x)
            if args.dropout > 0.0:
                x = Dropout(args.dropout, seed=args.rndSeed)(x)

    # define model by the transformation from inputs to outputs
    myModel = tf.keras.Model(inputs=inputs, outputs=outputs)
    return myModel
