import sys
import subprocess as sp
from multiprocessing import Process
import http.server
import glob
import numpy as np
import requests
import json

CONTAINERNAME = "HplusModelServer"



def kill_server(name=CONTAINERNAME, docker=True):
    if not docker:
        resp = requests.post("http:localhost/8501", data=json.dumps({}), headers=json.dumps({}))
        return
    p = sp.Popen(f"docker stop {name}", shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = p.communicate()
    return out, err

def server_predict(x, n_model):
    headers = {"content-type": "application/json"}
    data = json.dumps({"signature_name": "serving_default", "instances": x.tolist()})
    resp = requests.post(f"http://localhost:8501/v1/models/HplusModel{n_model}:predict", data=data, headers=headers, timeout=2)
    predslist = json.loads(resp.text)['predictions']
    if isinstance(predslist[0], dict):
        return {key: np.reshape([d[key] for d in predslist], (len(predslist),-1)) for key in predslist[0]}
    return {'class_out': predslist}

# function to initialize worker process that waits for requests
def init_nodocker_worker(models):
    def run():
        pass
    raise NotImplementedError()

def dummy_model():
    m = tf.keras.Sequential(layers=[InputSelector([True])] + [tf.keras.layers.Dense(10, activation="relu") for _ in range(1)] + [tf.keras.layers.Dense(1)])
    m.compile()
    m.build((None,1))
    return m

# function to initialize neural networks for prediction during analysis
def init_server(model_dir, server_name=CONTAINERNAME, use_docker=True, gpus=""):
    import tensorflow as tf
    from  tensorflow import keras as K
    from nanoanalysis.ML.disCo import InputSelector, Sanitizer, MinMaxScaler

    folds = glob.glob(model_dir + "/fold_*/")
    k = len(folds)

    # write models in tf SavedModel format to /tmp
    #NOTE: debugging
    #models = [dummy_model() for _ in folds]
    #[m.save(f"~/tmp/{i}/model_trained.h5") for i, m in enumerate(models)]
    #folds = [f"~/tmp/{i}/" for i in range(k)]

    custom_objects = {"InputSelector": InputSelector, "Sanitizer": Sanitizer, "MinMaxScaler": MinMaxScaler}
    models = [K.models.load_model(d + "model_trained.h5", custom_objects = custom_objects, compile=False) for d in folds]
    [m.compile() for m in models]

    if not use_docker: return init_nodocker_worker(models)
    [tf.saved_model.save(m, f"/tmp/tfserving/HplusModel{i}/1/", signatures=None, options=None) for i, m in enumerate(models)]

    # define model serving config protobuf message to look for all models
    cfg='model_config_list: {\n'\
        '\n'
    for i, d in enumerate(folds):
        m_cfg = '  config: {\n'\
               f'    name: "HplusModel{i}",\n'\
               f'    base_path: "/models/HplusModel{i}",\n'\
                '    model_platform: "tensorflow"\n'\
                '  },\n'
        cfg += m_cfg
    cfg += '}'

    # write model config
    with open("/tmp/tfserving/Hplus_models.config" ,"w") as f:
        f.write(cfg)

    # define the shell command to run the server
    cmd = 'docker run --rm --cpus="1" -m 15g '\
          f'--name {server_name} '\
          '-p 8501:8501 '
    if gpus:
        cmd += f'--gpus \'\"device={gpus}\"\' '
    for i in range(k):
        cmd += f'--mount type=bind,source=/tmp/tfserving/HplusModel{i},target=/models/HplusModel{i} '
    cmd+= '--mount type=bind,source=/tmp/tfserving/Hplus_models.config,target=/models/models.config '
    cmd+= '-v /tmp/tensorboard:/tmp/tensorboard '
    cmd+= f'-t tensorflow/serving:latest{"-gpu" if gpus else ""} '\
          '--model_config_file=/models/models.config'
    print(cmd)
    # start the server
    p = sp.Popen(cmd, stdout = sp.PIPE, stderr = sp.PIPE, shell=True)

    while True:
        line = p.stdout.readline()
        print(line)
        if not line:
            print("Inference server shutdown before successfully initializing")
            return ""
        elif b"event loop" in line:
            print("Inference server ready!")
            break

    return server_name

def main(model_dir):
    server_name = init_server(model_dir)
    if not server_name: raise Exception("inference server initialization failed")
    print(f"initialized model server with docker process name {server_name}")
    return

if __name__ == "__main__":
    model_dir = sys.argv[1]
    main(model_dir)
