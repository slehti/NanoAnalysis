from collections import defaultdict

import tensorflow as tf
import keras

from nanoanalysis.ML.func import (
    ss, ns, ts, hs, ls, es, cs
)

# custom keras layer to scale inputs to the range [-1,1]
@keras.saving.register_keras_serializable(package="NanoAnalysisLayers")
class MinMaxScaler(tf.keras.layers.Layer):
    def __init__(self, name="MinMaxScaler", maxes=None, mins=None, scaled_max=1., scaled_min=-1., **kwargs):
        super().__init__(name=name, **kwargs)
        if (maxes is None and mins is not None) or (maxes is not None and mins is None):
            msg = "Must specify either both maxes and mins or leave both to be adapted"
            raise Exception(es + msg + ns)
        self.input_maxes = None
        self.scaled_max = scaled_max
        self.scaled_min = scaled_min
        if maxes is not None and mins is not None:
            self.input_maxes = tf.constant(maxes)
            self.input_mins = tf.constant(mins)

    def build(self, shape):
        super().build(shape)
        if self.input_maxes is not None:
            self.maxes = self.input_maxes * tf.ones(
                shape[1:], dtype=self.input_maxes.dtype
            )
            self.mins = self.input_mins * tf.ones(
                shape[1:], dtype=self.input_mins.dtype
            )

    def adapt(self, data):  # compute minimum and maximum of features and set as weights
        spec = data.element_spec
        dim = len(spec.shape)
        to_reduce = list(range(dim - 1))
        first = True
        for elem in data:
            maxes = tf.math.reduce_max(elem, axis=to_reduce)
            mins = tf.math.reduce_min(elem, axis=to_reduce)
            if not first:
                maxes = tf.where(maxes > prev_maxes, maxes, prev_maxes)
                mins = tf.where(mins < prev_mins, mins, prev_mins)
            else:
                first = False
            prev_maxes = maxes
            prev_mins = mins

        # add length 1 dimensions to min and max until they are broadcastable to the input shape
        while len(maxes.shape) != dim:
            maxes = tf.expand_dims(maxes, 0)
            mins = tf.expand_dims(mins, 0)

        self.maxes = maxes
        self.mins = mins

    def get_config(self):
        config = super().get_config()
        try:  # if the layer has been adapted, return the final state
            config.update(
                {
                    "maxes": self.maxes.numpy().tolist(),
                    "mins": self.mins.numpy().tolist(),
                }
            )
        except:  # otherwise just return the values used to initialize the layer
            config.update({"maxes": self.input_maxes, "mins": self.input_mins})
        return config

    @classmethod
    def from_config(cls, config):
        print(config)
        return cls(**config)

    @tf.function
    def call(self, x):
        return (
            (self.scaled_max-self.scaled_min)
            * (x - self.mins) / (self.maxes - self.mins)
            - self.scaled_min
        )


# custom keras layer to handle variable specific preprocessing transformations
@keras.saving.register_keras_serializable(package="NanoAnalysisLayers")
class Sanitizer(tf.keras.layers.Layer):
    input_handling = defaultdict(
        lambda: lambda x: x,
        {
            "tau_pt": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
            "leadTau_pt": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
            "pt_miss": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
            "MET_pt": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
            "R_tau": lambda x: tf.math.log(x + 1),
            "rtau": lambda x: tf.math.log(x + 1),
            "bjet_pt": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
            "tau_mt": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
            "mt": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
            "mT": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
            #"dPhiTauNu": tf.math.abs,
            #"dPhitaub": tf.math.abs,
            #"dPhibNu": tf.math.abs,
            "mtrue": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
            "m_true": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
            "btag": lambda x: x,
            "btagDeepB": lambda x: x,
            #"dPhi": tf.math.abs,
            #"dPhiTauJet": lambda x: x,
            #"dPhiNuJet": tf.math.abs,
            #"dPhiJetJet": tf.math.abs,
            "jet_pt": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
            "HJet_pt": lambda x: tf.math.log(tf.math.log(x + 1) + 1),
        }
    )

    def __init__(self,
		in_var_names,
        symmetric_phi=True,
		name="input_sanitizer",
		trainable=False,
		**kwargs
    ):
        super().__init__(trainable=trainable, name=name, **kwargs)
        self.inputvars = in_var_names
        self.symmetric_phi = symmetric_phi
        self.funcs = [self.input_handling[name] for name in self.inputvars]
        if self.symmetric_phi:
            self.mirror_src = self.inputvars.index("dPhiTauNu")
            self.mirrored = [self.inputvars.index(x) for x in self.inputvars if "dPhi" in x]

    def get_config(self):
        config = super().get_config()
        config.update({"in_var_names": self.inputvars})
        config.update({"symmetric_phi": self.symmetric_phi})
        return config

    @classmethod
    def from_config(cls, config):
        print(config)
        return cls(**config)

    def scale(self, x):
        out = []
        if self.symmetric_phi:
            phi_sign = tf.sign(x[...,self.mirror_src])
        x = tf.unstack(x, axis=-1)
        for i, (x_, func) in enumerate(zip(x, self.funcs)):
            y = func(x_)
            if self.symmetric_phi and i in self.mirrored:
                y = y*phi_sign

            out.append(y)
        # x = tf.stack([func(x_) for x_, func in zip(x, self.funcs)], axis=-1)
        out = tf.stack(out, axis=-1)
        out = tf.where(tf.math.is_nan(out), tf.zeros_like(out), out)
        return out

    def adapt(self, data):  # deprecated, use MinMax.adapt() instead
        scaled_data = data.map(lambda *x: self.scale(x[0]))
        self.minMax.adapt(scaled_data)

    @tf.function
    def call(self, x):
        return self.scale(x)

# function to reduce max and min of variables from a dataset
def accumulate_mass_max_min(dataset, args):
    curr_min = tf.float32.max
    curr_max = tf.float32.min
    for elem in dataset:
        new_min = tf.math.reduce_min(elem[1]["mass_out"][..., 0])
        new_max = tf.math.reduce_max(elem[1]["mass_out"][..., 0])
        curr_min = tf.math.minimum(curr_min, new_min)
        curr_max = tf.math.maximum(curr_max, new_max)
    return curr_max.numpy(), curr_min.numpy()


