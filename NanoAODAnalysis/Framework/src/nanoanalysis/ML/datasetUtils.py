import os
import glob
import pickle
from pathlib import Path

import ROOT as R
import numpy as np
import tensorflow as tf

from nanoanalysis.ML.input_features import load_input_vars

TF_OP_SEED = 123456
EPS = 1e-6


def load_true_mass_samples(datadir, mass_idx, k):
    datasets = []
    for fold in range(k):
        dir = os.path.join(datadir, f"/*ChargedHiggs*/data/{fold}/final_*/")
        sig_dirs = glob.glob(dir)

        spec_dir = glob.glob(datadir + f"/*ChargedHiggs*/data/{fold}/elementSpec")[0]
        with open(spec_dir, "rb") as in_:
            element_spec = pickle.load(in_)

        def map_fn(x, y):
            return x[..., mass_idx]

        choice = tf.data.Dataset.range(len(sig_dirs)).repeat()
        data = [
            tf.data.Dataset.load(sig, element_spec=element_spec) for sig in sig_dirs
        ]
        datasets.append(
            tf.data.Dataset.choose_from_datasets(
                data, choice, stop_on_empty_dataset=False
            ).map(map_fn)
        )

    return datasets

def get_ds_sumw(dataset):
    def reduce_size_weight(old_state, elem):
        w = elem[2]
        sumw = tf.math.reduce_sum(tf.math.abs(w))
        n = tf.cast(tf.shape(w)[0], tf.float32)
        return old_state + tf.stack([n, sumw])

    res = dataset.batch(2**16).reduce(tf.constant([0.,0.]), reduce_size_weight)
    n = res[0]
    w = res[1]
    return n, w

def get_ds_len(
    dataset,
):  # TODO: recursive tuples and dictionaries as elements are currently unsupported
    if type(dataset.element_spec) is tuple:

        def get_batch_len(elem):
            return elem[0].shape[0]

    elif type(dataset.element_spec) is dict:
        key = dataset.element_spec.keys()[0]

        def get_batch_len(elem):
            return elem[key].shape[0]

    else:

        def get_batch_len(elem):
            return elem.shape[0]

    n = 0
    dataset = dataset.batch(2**15)
    for elem in dataset:
        n += get_batch_len(elem)
    return n


def interleave_signals(sig_datasets):
    n = len(sig_datasets)
    choice_ds = tf.data.Dataset.range(n).repeat()
    return tf.data.Dataset.choose_from_datasets(
        sig_datasets,
        choice_ds,
        stop_on_empty_dataset=False
    )

def shuffle_datasets(datasets, lens):
    n = len(datasets)
    sampling_arr = tf.random.shuffle(
        tf.repeat(tf.range(n, dtype=tf.int64), lens), seed=TF_OP_SEED
    )
    choice_ds = tf.data.Dataset.from_tensor_slices(sampling_arr)
    return tf.data.Dataset.choose_from_datasets(datasets, choice_ds, stop_on_empty_dataset=False)


def match_df_with_weights(
    name,
    planing,
    bkg_hists,
    sig_hists,
    marginalized,
    energy,
    edges,
):
    if planing:
        total_sig_count = [sum(hist["values"]) for hist in sig_hists.values()]
        total_sig_dist = np.sum(
            np.stack([hist["values"] for hist in sig_hists.values()], axis=0), axis=0
        )

        bg_dist = np.array(bkg_hists["values"])
        bg_count = np.sum(bg_dist)
        normalized_bg_dist = bg_dist / bg_count

        dataset_name = name

        if dataset_name not in sig_hists.keys():
            # sigma = crossection.backgroundCrossSections.crossSection(
            #     dataset_name, energy
            # )
            # if sigma == None:
            #     msg = f"No cross-section data found for dataset named {dataset_name}!"
            #     raise Exception(msg)
            # return tf.zeros_like(edges, dtype=tf.float32) + sigma / N0
            return tf.ones_like(edges, dtype=tf.float32)

        if not marginalized:  # planing for each individual signal dataset
            dataset_sig_count = sum(sig_hists[dataset_name]["values"])
            sig_dist = np.array(sig_hists[dataset_name]["values"]) + EPS
        else:  # planing for the combined signal dataset of samples of different masses
            dataset_sig_count = total_sig_count
            sig_dist = total_sig_dist + EPS

        normalized_sig_dist = sig_dist / dataset_sig_count
        return tf.constant(normalized_bg_dist / normalized_sig_dist, dtype=tf.float32)

    else:  # no planing, data is preweighted according to cross-section
        dataset_name = name
        if (
            "ChargedHiggs" not in dataset_name
        ):  # get background class weights according to interaction cross-sections
            # sigma = crossection.backgroundCrossSections.crossSection(
            #     dataset_name, energy
            # )
            # assert N0 > 0
            # if sigma == None:
            #     msg = f"No cross-section data found for dataset named {dataset_name}!"
            #     raise Exception(msg)
            # return tf.zeros_like(edges, dtype=tf.float32) + sigma / N0
            return tf.ones_like(edges, dtype=tf.float32)

        # for the signal samples with no planing, we want to have equal weights for all the samples
        return tf.ones_like(edges, dtype=tf.float32)

def match_with_weights(
    name,
    fold,
    planing,
    bkg_hists,
    sig_hists,
    bg_dataset_sizes,
    marginalized,
    energy,
    edges,
):
    if planing:
        total_sig_counts = [
            sum([sum(hist["values"]) for hist in hists.values()]) for hists in sig_hists
        ]
        total_sig_dist = [
            np.sum(
                np.stack([hist["values"] for hist in hists.values()], axis=0), axis=0
            )
            for hists in sig_hists
        ]

        bg_dist = np.array(bkg_hists[fold]["values"])
        bg_count = np.sum(bg_dist)
        normalized_bg_dist = bg_dist / bg_count

        dataset_name = name

        if dataset_name not in sig_hists[fold].keys():
            # sigma = crossection.backgroundCrossSections.crossSection(
            #     dataset_name, energy
            # )
            # N0 = bg_dataset_sizes[dataset_name]
            # if sigma == None:
            #     msg = f"No cross-section data found for dataset named {dataset_name}!"
            #     raise Exception(msg)
            return tf.ones_like(edges, dtype=tf.float32) # + sigma / N0

        if not marginalized:  # planing for each individual signal dataset
            dataset_sig_count = sum(sig_hists[fold][dataset_name]["values"])
            sig_dist = np.array(sig_hists[fold][dataset_name]["values"]) + EPS
        else:  # planing for the combined signal dataset of samples of different masses
            dataset_sig_count = total_sig_counts[fold]
            sig_dist = total_sig_dist[fold] + EPS

        normalized_sig_dist = sig_dist / dataset_sig_count
        return tf.constant(normalized_bg_dist / normalized_sig_dist, dtype=tf.float32)

    else:  # no planing, only need to weight different backgrounds according to cross-sections
        dataset_name = name
        if (
            "ChargedHiggs" not in dataset_name
        ):  # get background class weights according to interaction cross-sections
            # sigma = crossection.backgroundCrossSections.crossSection(
            #     dataset_name, energy
            # )
            # N0 = bg_dataset_sizes[dataset_name]
            # assert N0 > 0
            # if sigma == None:
            #     msg = f"No cross-section data found for dataset named {dataset_name}!"
            #     raise Exception(msg)
            return tf.ones_like(edges, dtype=tf.float32) # + sigma / N0

        # for the signal samples with no planing, we want to have equal weights for all the samples
        return tf.ones_like(edges, dtype=tf.float32)

def load_files_numpy(npy_files):
    dset_name = Path(npy_files[0]).parts[-3]
    arrs = [np.load(f).astype(np.float32) for f in npy_files]
    arr = np.concatenate(arrs, axis=0)
    return dset_name, arr

def load_files(npy_files, feature_names):
    dset_name, arr = load_files_numpy(npy_files)
    size = len(arr)
    mass_idx = feature_names.index("m_true")
    y = arr[..., mass_idx] == arr[..., mass_idx]
    w = arr[..., feature_names.index("w")]
    dataset = tf.data.Dataset.from_tensor_slices((arr, y, w))
    return dataset, (dset_name, size)

def file_in_fold(filepath, i):
    return f"fold_{i}" in filepath

# function to map bin weights onto the event samples as sample weights
def add_weights_dataset(ds, binned_weights, mt_idx, edges):
    import tensorflow_probability as tfp
    return ds.map(
        lambda x, y, w: (
            x, y, w * tf.gather(
                binned_weights,
                tfp.stats.find_bins(
                    x,
                    edges=edges,
                    dtype=tf.int64,
                    extend_upper_interval=True,
                    extend_lower_interval=True,
                )[mt_idx],
            ),
        )
    )

# function to load k-folded datasets, returns a list of tf.data.Datasets
def load(
    datadir,
    k,
    disco_var=None,
    testset=False,
    planing=False,
    marginalized=False,
    energy="13",
    exclude=None,
    wjets_src="HT",
):

    data_files = glob.glob(datadir + "/*/data/")
    x_names = load_input_vars(datadir)
    if disco_var is not None:
        mt_idx = x_names.index(disco_var)
    else:
        mt_idx=0 # NOTE: this must be defined to get scalar weights

    # drop directory names that are in the list to exclude
    if exclude:
        for substr in exclude:
            data_files = [tgt for tgt in data_files if substr not in tgt]

    # keep only the chosen WJetsToLNu source
    if wjets_src == "inclusive":
        data_files = [tgt for tgt in data_files if "WJetsToLNu_HT" not in tgt]
    elif wjets_src == "HT":
        data_files = [
            tgt for tgt in data_files if not ("WJetsToLNu" in tgt and "HT" not in tgt)
        ]

    datasets = []
    final_datasets = []
    files = []
    ds_counts = []
    for fold in range(k):

        # complementary datasets for training and testing
        if testset:
            fold_files = [glob.glob(path + f"fold_{fold}_test.npy") for path in data_files]
            # fold_files = [
            #     [f for f in ds_files if file_in_fold(f, fold)]
            #     for ds_files in fold_files
            # ]
        else:
            fold_files = [glob.glob(path + f"fold_{fold}_train.npy") for path in data_files]
            # fold_files = [
            #     [f for f in ds_files if not file_in_fold(f, fold)]
            #     for ds_files in fold_files
            # ]
        files.append(fold_files)

        fold_datasets = [
            load_files(ds_files, x_names) for ds_files in fold_files
        ]  # load datasets
        fold_datasets, fold_ds_sizes = tuple(zip(*fold_datasets))
        fold_ds_sizes = dict(fold_ds_sizes)
        ds_counts.append(fold_ds_sizes)
        datasets.append(fold_datasets)

    sig_hists, bkg_hists = load_hists(datadir, k, ds_counts, energy)

    # FIXME: updated RDF saved histogram layout
    if planing:
        edges = bkg_hists[0]["bins"].astype(np.float32)
    else:  # no planing, only need to weight different backgrounds according to cross-sections
        edges = [
            0.0,
            1.0,
        ]  # find_bins requires atleast two edges, the weights are all set to the same value.

    edges = tf.constant(edges,dtype=tf.float32)

    for fold in range(k):
        fold_ds_sizes = ds_counts[fold]
        fold_datasets = datasets[fold]
        fold_files = files[fold]
        f_weights = list(
            map(
                lambda data_file: match_with_weights(
                    Path(data_file).parent.stem,
                    fold,
                    planing,
                    bkg_hists,
                    sig_hists,
                    fold_ds_sizes,
                    marginalized,
                    energy,
                    edges,
                ),
                data_files,
            )
        )  # match each name with binned weights

        # normalize background weights s.t. the average weight for an event will be 1:
        bg_mask = np.array(
            [int(not ("ChargedHiggs" in fname[0])) for fname in fold_files]
        )
        bg_unnormalized_w = [
            weights[0] for i, weights in enumerate(f_weights) if bg_mask[i] == 1
        ]

        # map weights from bins onto events
        fold_datasets = [add_weights_dataset(*x, mt_idx, edges) for x in zip(fold_datasets, f_weights)]

        # split into collection of signal datasets and bg datasets
        sig_datasets = [
            ds
            for i, ds in enumerate(fold_datasets)
            if "ChargedHiggs" in fold_files[i][0]
        ]
        bg_datasets = [
            ds
            for i, ds in enumerate(fold_datasets)
            if "ChargedHiggs" not in fold_files[i][0]
        ]

        sig_lens_weights = [get_ds_sumw(ds) for ds in sig_datasets]
        sig_lens, sig_weights = zip(*sig_lens_weights)
        sig_mean_len = tf.math.reduce_mean(sig_lens)

        # balance sum of weights to be equal for each signal mass hypothesis
        sig_datasets = [
            balance_weights(ds, sig_mean_len, ds_sumw)
            for ds, ds_sumw in zip(sig_datasets, sig_weights)
        ]

        # mix different mass hypotheses randomly
        sig_ds = shuffle_datasets(
            sig_datasets, sig_lens
        )

        bg_lens_weights = [get_ds_sumw(ds) for ds in bg_datasets]
        bg_lens, bg_weights = zip(*bg_lens_weights)
        bg_sumw = sum(bg_weights)
        bg_len = sum(bg_lens)
        bg_ds = shuffle_datasets(
            bg_datasets, bg_lens
        )  # sample from background datasets in proportion to how many events passed the preliminary selection

        # normalize average weight to 1 for background
        bg_ds = balance_weights(bg_ds, bg_len, bg_sumw)

        # sig_sf = tf.cast(sig_sumw / sig_len, tf.float32)
        # sig_ds = sig_ds.map(lambda x, y, w: (x, y, w * sig_sf))

        final_datasets.append((sig_ds, bg_ds))
    return final_datasets, x_names

def balance_weights(ds, ds_len, ds_sumw):
        ds_sf = tf.cast(ds_len / ds_sumw, tf.float32)
        return ds.map(lambda x, y, w: (x, y, w * ds_sf))

def load_hists(datadir, k, ds_counts, energy="13", testset=False):
    sig_flag = "ChargedHiggsToTauNu"
    sig_targets = glob.glob(datadir + "/*" + sig_flag + "*/mt_hist.root")
    bkg_targets = set(glob.glob(datadir + "/*/mt_hist.root")) - set(sig_targets)

    ybin = 1 if testset else 0

    bkg_hist = []
    sig_hists = []
    for i in range(k):
        bkg_hist.append({"bins": [], "values": []})
        for fname in bkg_targets:
            with R.TFile.Open(fname, "READ") as f:
                name = fname.split("/")[-2]
                if name not in ds_counts[i]: continue
                # sigma = crossection.backgroundCrossSections.crossSection(name, energy)
                hist = f.Get(f"mt_hist_{i}").ProjectionX(firstybin=ybin,lastybin=ybin)
                if not len(bkg_hist[i]["bins"]):
                    bkg_hist[i]["bins"] = np.array(hist.GetXaxis().GetXbins())
                    bkg_hist[i]["values"] = (
                        np.array(hist)[1:-1] #* sigma / ds_counts[i][name]
                    )
                else:
                    bkg_hist[i]["values"] = bkg_hist[i]["values"] + np.array(hist)[1:-1]

        sig_hists.append({})
        for fname in sig_targets:
            with R.TFile.Open(fname, "READ") as f:
                hist = f.Get(f"mt_hist_{i}").ProjectionX(firstybin=ybin,lastybin=ybin)
                sig_hists[i][fname.split("/")[-2]] = {
                    "bins": np.array(hist.GetXaxis().GetXbins()),
                    "values": np.array(hist)[1:-1],
                }

    return sig_hists, bkg_hist

# function to map bin weights onto the event samples as sample weights
def add_weights(df, binned_weights, x_name, edges):
    df["w"] = df["w"] * binned_weights[np.digitize(df[x_name], edges)]
    return df

# function to load k-folded datasets into memory
def load_pandas(
    datadir,
    k,
    disco_var=None,
    testset=False,
    planing=False,
    marginalized=False,
    energy="13",
    exclude=None,
    wjets_src="HT",
):

    data_files = glob.glob(datadir + "/*/data/")
    x_names = load_input_vars(datadir)
    if disco_var is not None:
        mt_idx = x_names.index(disco_var)
    else:
        mt_idx=None

    # drop directory names that are in the list to exclude
    if exclude:
        for substr in exclude:
            data_files = [tgt for tgt in data_files if substr not in tgt]

    # keep only the chosen WJetsToLNu source
    if wjets_src == "inclusive":
        data_files = [tgt for tgt in data_files if "WJetsToLNu_HT" not in tgt]
    elif wjets_src == "HT":
        data_files = [
            tgt for tgt in data_files if not ("WJetsToLNu" in tgt and "HT" not in tgt)
        ]

    data = []
    final_datasets = []
    files = []
    ds_counts = []
    for fold in range(k):
        fold_files = [glob.glob(path + f"fold_*.npy") for path in data_files]

        # complementary datasets for training and testing
        if testset:
            fold_files = [
                [f for f in ds_files if file_in_fold(f, fold)]
                for ds_files in fold_files
            ]
        else:
            fold_files = [
                [f for f in ds_files if not file_in_fold(f, fold)]
                for ds_files in fold_files
            ]
        files.append(fold_files)

        fold_data = dict(
            load_files_numpy(ds_files) for ds_files in fold_files
        )
        data.append(fold_data)

    sig_hists, bkg_hists = load_hists(datadir, k, ds_counts, energy, testset)

    if planing:
        edges = bkg_hists[0]["bins"].astype(np.float32)
    else:  # no planing, only need to weight different backgrounds according to cross-sections
        edges = np.array([
            0.0,
            1.0,
        ])  # find_bins requires atleast two edges, the weights are all set to the same value.


    for fold in range(k):
        fold_data = data[fold]
        fold_files = files[fold]
        f_weights = [
            match_df_with_weights(
                Path(data_file).parent.stem,
                planing,
                bkg_hists[fold],
                sig_hists[fold],
                marginalized,
                energy,
                edges,
            )
            for data_file in data_files
        ]

        # normalize background weights s.t. the average weight for an event will be 1:
        bg_mask = np.array(
            [int(not ("ChargedHiggs" in fname[0])) for fname in fold_files]
        )
        bg_unnormalized_w = [
            weights[0] for i, weights in enumerate(f_weights) if bg_mask[i] == 1
        ]

        if disco_var is not None:
            fold_data = [add_weights(*x, disco_var, edges) for x in zip(fold_data, f_weights)]

        # split into collection of signal datasets and bg datasets
        sig_data= [
            ds
            for i, ds in enumerate(fold_data)
            if "ChargedHiggs" in fold_files[i][0]
        ]
        bg_data= [
            ds
            for i, ds in enumerate(fold_data)
            if "ChargedHiggs" not in fold_files[i][0]
        ]

        sig_ds = interleave_signals(
            sig_datasets
        )  # interleave elements from the different signal datasets to show the nn an equal amount for each type
        bg_ds, bg_lens = shuffle_backgrounds(
            bg_datasets
        )  # sample from background datasets in proportion to how many events passed the preliminary selection

        bg_len_tot = sum(bg_lens)
        bg_crosssection_tot = np.sum(np.array(bg_unnormalized_w) * np.array(bg_lens))
        scale_factor = tf.cast(bg_len_tot / bg_crosssection_tot, tf.float32)
        bg_ds = bg_ds.map(lambda x, y, w: (x, y, w * scale_factor))

        final_datasets.append((sig_ds, bg_ds))
    return final_datasets, x_names

