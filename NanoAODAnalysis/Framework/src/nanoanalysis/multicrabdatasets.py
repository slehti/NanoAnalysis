#!/usr/bin/env python

import os
import sys
import re
import subprocess
import json
from concurrent.futures import ThreadPoolExecutor as tpe

useROOT = True
try:
    import uproot
    useROOT = False
except ModuleNotFoundError:
    import ROOT

from nanoanalysis.aux import execute

root_re = re.compile("(?P<rootfile>([^/]*events_\d+\.root))")
json_re = re.compile("(?P<jsonfile>files_\S+\.json$)")
mask_re = re.compile("(?P<lumimask>\S*Collisions\S+\.\w+)")

class Dataset:
    def __init__(self,path,run):
        self.path = path
        self.name = os.path.basename(path)
        self.run = run
        self.year = getYear(os.path.dirname(path))
        self.type = getType(path)
        self.runRange = getRunRange(path)
        self.isData = False
        if "Run20" in self.name:
            self.isData = True
        self.lumi = 0
        self.lumimask = ""
        self.files = []
        cands = execute("ls %s"%os.path.join(path,"results"))
        jsonfile = ""
        for c in cands:
            match = root_re.search(c)
            if match:
                self.files.append(os.path.join(path,"results",match.group("rootfile")))
            jsonmatch = json_re.search(c)
            if jsonmatch:
                jsonfile = os.path.join(path,"results",jsonmatch.group("jsonfile"))
#                f = open(os.path.join(path,"results",jsonmatch.group("jsonfile")))
#                filelist = json.load(f)
#                f.close()
#                self.files = filelist["files"]

        if len(self.files) == 0:
            if os.path.exists(jsonfile):
                with open(jsonfile, 'r') as fJSON:
                    data = json.loads(fJSON.read())
                    self.files.extend(data['files'])

        if len(self.files) == 0:
            print("Dataset %s contains no root files"%path)
            return

        self.histograms = {}
        self.fPU = os.path.join(path,"results","PileUp.root")
        """
        first = True
        if useROOT:
            self.fRF = ROOT.TFile.Open(self.files[0])
            self.skimCounter = self.fRF.Get("configInfo/SkimCounter").Clone("skimCounter")
            self.skimCounter.Reset()
        for f in self.files:
            if useROOT:
                rf = ROOT.TFile.Open(f)
                s = rf.Get("configInfo/SkimCounter").Clone("skimCounter")
                self.skimCounter.Add(s)
                rf.Close()
            else:
                with uproot.open(f) as fIN:
                    for key in fIN.keys():
                        if "configInfo/SkimCounter" in key:
                            h_skim = fIN["configInfo"]["SkimCounter"]
                            if first:
                                self.skimCounter = h_skim.to_hist()
                                first = False
                            else:
                                self.skimCounter = self.skimCounter + h_skim.to_hist()
                            break
        if self.isData:
            cands = execute("ls %s"%os.path.join(path,"inputs/"))
            for c in cands:
                match = mask_re.search(c)
                if match:
                    self.lumimask = os.path.join(path,"inputs/",match.group("lumimask"))
        """
        executor = tpe(max_workers= len(self.files))
        skimcounters = executor.map(_readskim, self.files)
        self.skimCounter = sum(
            [c for c in skimcounters if c is not None]
        )
        executor.shutdown()

    def getPileup(self):
        if self.isData:
            return None
        fPU = self.getPileupfile()
        with uproot.open(fPU) as fIN:
            return fIN['pileup']
            #return fIN['configInfo/pileup']

    def getPileupfile(self):
        return self.fPU

    def getFileNames(self):
        return self.files

    def getSkimCounter(self):
        return self.skimCounter

    def getEvents(self):
        nev = None
        if self.skimCounter is not None:
            nev = self.skimCounter["Skim: All events"]
        return nev

    def to_dict(self):
        out = {}
        out["name"] = self.name
        out["path"] = self.path
        out["filenames"] = self.files
        out["files"] = {f: "Events" for f in self.files}
        out["path"] = self.path
        out["run"] = self.run
        out["type"] = self.type
        out["year"] = self.year
        out["runRange"] = self.runRange
        out["isData"] = self.isData
        out["histograms"] = self.histograms
        out["fPU"] = self.fPU
        out["skimCounter"] = self.skimCounter
        return out

    def Print(self):
        print( self.name )
        print( "    is data",self.isData )
        print( "    number of files",len(self.files) )

def _readskim(path):
    try:
        with uproot.open(path) as fIN:
            if "configInfo/SkimCounter" in fIN:
                h_skim = fIN["configInfo"]["SkimCounter"]
                skimCounter = h_skim.to_hist()
                return skimCounter
    except Exception as e:
        print(e)
        print(path)
        exit()
    return None

#def execute(cmd):
#    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,
#                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
#    (s_in, s_out) = (p.stdin, p.stdout)
#
#    f = s_out
#    ret=[]
#    for line in f:
#        line = str(line,'utf-8')
#        ret.append(line.replace("\n", ""))
#    f.close()
#    return ret

def getRun(multicrabdir,dsetnames):
    datasetnames = dsetnames
    datasetnames.append(multicrabdir)

    run = ""
    run_re = re.compile("Run(?P<run>20\d\d\S+?)[_|-]")
    for n in datasetnames:
        match = run_re.search(n)
        if match:
            run = match.group("run")
            break
    if len(run) > 0:
        return run

    # getting run from the lumi.json
    lumijson = open(os.path.join(multicrabdir,"lumi.json"),'r')
    data = json.load(lumijson)
    run_re = re.compile("Run(?P<run>20\d\d\S+?)_")
    runs = []
    for key in data.keys():
        match = run_re.search(key)
        if match:
            run = match.group("run")
            if not run in runs:
                runs.append(run)
    if len(runs) > 0:
        retvalue = runs[0][:4]
        for run in runs:
            retvalue+=run[4:]
        return retvalue
    print("Run not found, exiting..")
    sys.exit()

def getDatasets(multicrabdir,whitelist=[],blacklist=[]):
    datasets = []
    cands = execute("ls %s"%multicrabdir)

    if whitelist != None and len(whitelist) > 0:
        #print "check whitelist 1 ",whitelist,blacklist
        datasets_whitelist = []
        for d in cands:
            for wl in whitelist:
                if len(wl) == 0:
                    continue
                wl_re = re.compile(wl)
                match = wl_re.search(d)
                if match:
                    datasets_whitelist.append(d)
                    break
        #print "check whitelist",datasets_whitelist
        cands = datasets_whitelist

    if blacklist != None and len(blacklist) > 0:
        #print "check blacklist 1 ",whitelist,blacklist
        datasets_blacklist = []
        for d in cands:
            found = False
            for bl in blacklist:
                if len(bl) == 0:
                    continue
                bl_re = re.compile(bl)
                match = bl_re.search(d)
                if match:
                    found = True
                    break
            if not found:
                datasets_blacklist.append(d)
        cands = datasets_blacklist

    run = getRun(multicrabdir,cands)

    # parallelizing this ends up with too much overhead
    # executor = tpe(max_workers=len(cands))
    datasets = map(
        _getDataset,
        [(os.path.join(multicrabdir,c),run) for c in cands]
    )
    datasets = [d for d in datasets if d is not None]
    # for c in cands:
    #     resultdir = os.path.join(multicrabdir,c,"results")
    #     if os.path.exists(resultdir):
    #         datasets.append(Dataset(os.path.join(multicrabdir,c),run))

    return datasets

def _getDataset(dir_run):
    dir, run = dir_run
    resultdir = os.path.join(dir,"results")
    if os.path.exists(resultdir):
        return Dataset(dir,run)
    return None


def getDataPileupROOT(datasets):
    import ROOT
    pileup_data = ROOT.TH1F("pileup_data","",100,0,100)
    for d in datasets:
        if d.isData:
            if hasattr(d, 'pileup'):
                pileup_data.Add(d.pileup)
    return pileup_data

def getDataPileup(multicrabdir,datasets,puHistoName='pileup'):
    hPU = None
    ndatadatasets = 0
    hpudatasets = 0
    for d in datasets:
        if d.isData:
            ndatadatasets += 1
            fPU = d.getPileupfile()
            if not os.path.exists(fPU):
                continue
            with uproot.open(fPU) as fIN:
                if ndatadatasets == 1:
                    hPU = fIN[puHistoName].to_hist()
                else:
                    hPU = hPU + fIN[puHistoName].to_hist()
                hpudatasets += 1
    if ndatadatasets == 0 or hpudatasets != ndatadatasets:
        print("No pileup found for all individual datasets, using multicrab/pileup.root")
        hPU = getDataPileupMulticrab(multicrabdir)

    return hPU

def getDataPileupFile(multicrabdir,datasets=[]):
    if len(datasets) == 0:
        return os.path.join(multicrabdir,"pileup.root")
    pu_files = []
    for d in datasets:
        if not d.isData:
            continue
        pufile = d.getPileupfile()
        if os.path.exists(pufile):
            pu_files.append(pufile)
        else:
            print("Warning, pileup file",pufile,"not found")
    TARGET = "pileup.root"
    SOURCES = ' '.join(pu_files)
    cmd = "hadd -f %s %s"%(TARGET,SOURCES)
    os.system(cmd)
    return TARGET

def getDataPileupMulticrab(multicrabdir):
    with uproot.open(os.path.join(multicrabdir,"pileup.root")) as fIN:
        return fIN['pileup'].to_hist()
    return None
    """
    print("check getDataPileupMulticrab",os.path.join(multicrabdir,"pileup.root"))
    fIN = ROOT.TFile.Open(os.path.join(multicrabdir,"pileup.root"))
    pileup_data = fIN.Get("pileup").Clone("pileup_data")
    pileup_data.SetDirectory(0)
    print("check pu data histo",pileup_data)
    return pileup_data
    """
def loadLuminosity(multicrabdir,datasets):
    lumisum = 0
    lumijson = open(os.path.join(multicrabdir,"lumi.json"),'r')
    data = json.load(lumijson)
    for d in datasets:
        if d.name in data.keys():
            d.lumi = data[d.name]
            lumisum += d.lumi
    lumijson.close()
    return lumisum

def getYear(multicrabdir):
    # getting year from the multicrab path
    year_re = re.compile("Run(?P<year>20\d\d)(?P<runs>\S+)_")
    match = year_re.search(multicrabdir)
    apv = ""
    if match:
        year = match.group("year")
        runs = match.group("runs")
        if year == "2016" and runs == "BCDEF":
            apv = "APV"
        if year == "2022" and ("E" in runs or "F" in runs or "G" in runs):
            apv = "E"
        return year+apv

    # getting year from the first entry in the lumi.json
    lumijson = open(os.path.join(multicrabdir,"lumi.json"),'r')
    data = json.load(lumijson)
    match = year_re.search(list(data.keys())[0])
    lumijson.close()
    if match:
        return match.group("year")

    print("Year not found, exiting..")
    sys.exit()

def getType(multicrabdir):
    # DoubleMuon_Run2017B_UL2017_MiniAODv2_NanoAODv9_v1_297050_299329
    type_re = re.compile("\S+Run20\d\d\S+?_(?P<type>\S*?20\S+?)_")
    match = type_re.search(multicrabdir)
    if match:
        return match.group("type")
    return "-1"

def getRunRange(multicrabdir):
    # DoubleMuon_Run2017B_UL2017_MiniAODv2_NanoAODv9_v1_297050_299329
    rr_re = re.compile("_(?P<rr>\d\d\d\d\d\d_\d\d\d\d\d\d)")
    match = rr_re.search(multicrabdir)
    if match:
        return match.group("rr")
    return "-1"

