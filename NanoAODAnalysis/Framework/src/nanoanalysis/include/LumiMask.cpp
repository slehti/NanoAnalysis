#include "LumiMask.h"
#include <Python.h>
#include <stdexcept>
#include <pybind11/pybind11.h>

namespace py = pybind11;

namespace nanoanalysis {

    nlohmann::json init_json(std::string jsonFile) {
        std::cout << "Initializing JSON file" << std::endl;
        std::ifstream f(jsonFile);
        nlohmann::json mapping = nlohmann::json::parse(f);
        return mapping;
    }

    bool is_good_lumi(const nlohmann::json &golden_json, int run, int lumi) {
        if (!golden_json.contains(std::to_string(run))) {return false;}
        for (auto& lumiRange : golden_json[std::to_string(run)]) {
           if ((lumi >= lumiRange[0]) && (lumi <= lumiRange[1])) {
               return true;
           }
        }
        return false;
    }

}

PYBIND11_MODULE(LumiMask, m) {
    m.doc() = "Extension for Luminosity masking with RDataFrame";

    m.def("init_json", &nanoanalysis::init_json, "Initialize a lumi mask json file");
    m.def("is_good_lumi", &nanoanalysis::is_good_lumi, "Apply mask to given run and lumi");
}

