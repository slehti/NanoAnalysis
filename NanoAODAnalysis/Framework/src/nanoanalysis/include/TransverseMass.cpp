#include "TransverseMass.h"
#include <Python.h>
#include <pybind11/pybind11.h>

namespace py = pybind11;

namespace nanoanalysis {

    float mT_tau_met(
        float tau_mass,
        float tau_pt,
        float tau_phi,
        float met_pt,
        float met_phi
    ) {
        float tau_mt = std::sqrt(tau_mass*tau_mass + tau_pt*tau_pt);
        float myCosPhi = std::cos(ROOT::VecOps::DeltaPhi(tau_phi, met_phi));

        // myCosPhi = std::abs(myCosPhi) > 1. ? std::copysign(1.0,myCosPhi) : myCosPhi;
        float mt_precursor = std::sqrt(2*tau_pt*(1.-myCosPhi));

        return std::isfinite(mt_precursor) ? mt_precursor*std::sqrt(met_pt) : tau_mt;
    }

}

PYBIND11_MODULE(TransverseMass, m) {
    m.doc() = "Extension for calculating transverse mass with RDataFrame";

    m.def("mT_tau_met", &nanoanalysis::mT_tau_met, "Get transverse mass between a tau and MET");

}

