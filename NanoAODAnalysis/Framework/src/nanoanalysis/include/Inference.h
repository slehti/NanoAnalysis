#ifndef TFLITE_INFERENCE
#define TFLITE_INFERENCE

#include <vector>
#include <string>
#include <iostream>

namespace nanoanalysis {

    // initialize the tflite model
    bool load_model(const std::string& model_path);

    // forward pass through a model, thread-safe as long as slot is unique
    std::vector<std::vector<float>> run_inference(
        const std::string& model_path,
        // unsigned int slot,
        const std::vector<float>& x
    );

    // forward pass through nth model
    std::vector<std::vector<float>> run_folded_inference(
        // unsigned int slot,
        std::vector<std::string> model_paths,
        unsigned int fold,
        const std::vector<float>& x
    ) {
        // return run_inference(model_paths[fold], slot, x);
        return run_inference(model_paths[fold], x);
    };


    std::vector<std::string> get_output_names(const std::string& path);
    std::vector<std::string> get_input_names(const std::string& path);

}

#endif
