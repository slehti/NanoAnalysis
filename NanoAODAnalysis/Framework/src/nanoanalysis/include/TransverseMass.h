#ifndef MT_DEFINED
#define MT_DEFINED

#include "ROOT/RVec.hxx"

namespace nanoanalysis {

    float mT_tau_met(float tau_mass, float tau_pt, float tau_phi, float met_pt, float met_phi);

}

#endif
