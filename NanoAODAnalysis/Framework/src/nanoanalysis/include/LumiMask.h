#ifndef LUMIMASK
#define LUMIMASK

#include <nlohmann/json.hpp>
#include <iostream>
#include <fstream>
#include <string>

namespace nanoanalysis {

    nlohmann::json init_json(std::string jsonFile);

    bool is_good_lumi(const nlohmann::json &golden_json, int run, int lumi);

}

#endif

