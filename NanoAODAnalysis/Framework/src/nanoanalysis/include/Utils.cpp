#include "Utils.h"
#include <pybind11/pybind11.h>

namespace py = pybind11;

namespace nanoanalysis {

    float sample_pileup_weight(const TH1D* weights, bool is_data, int n_true) {
        if (is_data) return 1.0;

        return weights->Interpolate(n_true);
    };

    ROOT::RVec<float> dPhi(
        float single_phi,
        ROOT::RVec<float> phi_vec
    ) {
        ROOT::RVec<float> dphi(phi_vec.size());

        for (int i=0; i<dphi.size(); i++) {
            dphi[i] = ROOT::VecOps::DeltaPhi(single_phi, phi_vec[i]);
        }
        return dphi;
    }

    // get R distances between elements in a collection
    ROOT::RVec<float> dPhi_combinations(
        ROOT::RVec<float> phi_vec
    ) {

        ROOT::RVec<ROOT::RVec<size_t>> phi_indices = (
            ROOT::VecOps::Combinations(phi_vec, 2)
        );
        ROOT::RVec<float> phi_0 = ROOT::VecOps::Take(phi_vec, phi_indices[0]);
        ROOT::RVec<float> phi_1 = ROOT::VecOps::Take(phi_vec, phi_indices[1]);
        return ROOT::VecOps::DeltaPhi(phi_0, phi_1);

    }
    bool match(
        float reco_eta,
        float reco_phi,
        ROOT::RVec<float> genpart_eta,
        ROOT::RVec<float> genpart_phi
    ) {
        bool matches = false;
        for (int j=0; j<genpart_eta.size(); j++) {
            float dR = ROOT::VecOps::DeltaR(
                reco_eta,genpart_eta[j],reco_phi,genpart_phi[j]
            );
            matches = matches| (dR < 0.1);
        }
        return matches;

    }

    float Rbb_min(
        ROOT::RVec<float> jet_phi,
        float met_phi,
        float tau_phi
    ) {
        float dphi2_tau_met = std::pow(
            M_PI - std::abs(
                ROOT::VecOps::DeltaPhi(tau_phi, met_phi)
            ),
            2
        );

        float dphi2_jet_met = std::pow( ROOT::VecOps::DeltaPhi(jet_phi[0],met_phi),2);
        for (int i=1; i<jet_phi.size(); i++) {
            float dphi2 = std::pow( ROOT::VecOps::DeltaPhi(jet_phi[i],met_phi),2);
            dphi2_jet_met = dphi2_jet_met < dphi2 ? dphi2_jet_met : dphi2;
        }
        return std::sqrt(dphi2_jet_met + dphi2_tau_met);
    }

    float Rtau(
        float tau_pt,
        float tau_eta,
        float lChTrkPtOverTauPt,
        float lChTrkDeltaEta
    ) {
        float lChTrkPt = tau_pt * lChTrkPtOverTauPt;
        float lChTrkEta = lChTrkDeltaEta - tau_eta;
        float tau_p = tau_pt * std::cosh(tau_eta);

        // Calculate p_z of leading track
        float pz = std::sinh(lChTrkEta)*lChTrkPt;

        // Calcualte p of leading track
        float p = std::sqrt(pz*pz + lChTrkPt*lChTrkPt);

        // Calculate Rtau
        float rtau = -1.0;
        if (tau_p > 0.0) rtau = p / tau_p;
        return rtau;
    }

    ROOT::RDF::RNode apply_classifier(ROOT::RDF::RNode df, const std::vector<std::string>& classifier_paths) {
    auto expression = [classifier_paths](unsigned int k, const std::vector<float>& x) {return nanoanalysis::run_folded_inference(classifier_paths, k, x);};
        return df.Define(
            "classifier_output_raw",
            expression,
            {"fold", "classifier_input"}
        );
    }

}

PYBIND11_MODULE(Utils, m) {
    m.doc() = "Extension for miscellaneous RDataFrame utilities";

    m.def("py_sample_pileup_weight", &nanoanalysis::sample_pileup_weight, "Get interpolated pileup weight for event");
    m.def("py_dPhi", &nanoanalysis::dPhi, "get phi angle between a particle and collection");
    m.def("py_dPhi_combinations", &nanoanalysis::dPhi_combinations, "get phi angles between members of collection");
    m.def("match", &nanoanalysis::dPhi, "match reco particle with a gen particle from collection");
    m.def("Rbb_min", &nanoanalysis::dPhi, "get minimum Rbb angle");
    m.def("Rtau", &nanoanalysis::Rtau, "get tau leading charged track energy fraction");
    // m.def("apply_classifier", &nanoanalysis::apply_classifier, "Run k-folded forward pass with ML models stored at given paths");
}

