#include "Inference.h"
#include <tensorflow/lite/interpreter.h>
#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/model.h>
#include <pybind11/pybind11.h>

namespace py = pybind11;

// struct InterpreterKey {
//     std::string path;
//     unsigned int slot;
//
//     // Equality comparison (required by std::unordered_map)
//     bool operator==(const InterpreterKey& other) const {
//         return path == other.path && slot == other.slot;
//     }
// };
//
// // Specialize std::hash for InterpreterKey
// namespace std {
//     template <>
//     struct hash<InterpreterKey> {
//         std::size_t operator()(const InterpreterKey& key) const {
//
//             std::size_t hash1 = std::hash<std::string>()(key.path);
//             std::size_t hash2 = std::hash<unsigned int>()(key.slot);
//
//             return hash1 ^ (hash2 << 1);
//         }
//     };
// }

namespace nanoanalysis {

    // capability to run different models, with interpreters unique for each thread
    std::unordered_map<std::string, std::unique_ptr<tflite::FlatBufferModel>> inference_model;
    thread_local std::unordered_map<std::string, std::unique_ptr<tflite::Interpreter>> interpreters;

    float get_first_tensor_value(std::vector<TfLiteTensor*> x) {
        float* data = x[0]->data.f; // Do not call with empty vectors
        return data[0];      // or with vectors of empty tensors
    }

    // Function to load a TFLite model
    bool load_model(const std::string& model_path) {
        // Load the model
        inference_model[model_path] = tflite::FlatBufferModel::BuildFromFile(model_path.c_str());
        if (!inference_model[model_path]) {
            std::cerr << "Failed to load model: " << model_path << std::endl;
            return false;
        }
        // std::cout << "Model loaded successfully!" << std::endl;
        return true;
    }

    // Function to build an interpreter for the model
    std::unique_ptr<tflite::Interpreter> build_interpreter(const tflite::FlatBufferModel* model) {
        // Create an interpreter
        tflite::ops::builtin::BuiltinOpResolver resolver;
        std::unique_ptr<tflite::Interpreter> interpreter;

        tflite::InterpreterBuilder builder(*model, resolver);
        if (builder(&interpreter) != kTfLiteOk) {
            std::cerr << "Failed to build interpreter." << std::endl;
            exit(-1);
        }

        // Allocate tensor buffers.
        if (interpreter->AllocateTensors() != kTfLiteOk) {
            std::cerr << "Failed to allocate tensors!" << std::endl;
            exit(-1);
        }

        // std::cout << "Interpreter created and tensors allocated." << std::endl;
        return interpreter;
    }

    std::unique_ptr<tflite::Interpreter>& get_interpreter(const std::string& path ) {
        if (!interpreters[path]) {
            interpreters[path] = build_interpreter(inference_model[path].get());
            if (!interpreters[path]) {
                std::cerr << "Failed to build interpreter " << path << std::endl;
                exit(-1);
            }
        }
        return interpreters[path];
    }

    // Function to set the input data
    void set_inputs(tflite::Interpreter* interpreter, const std::vector<float>& x) {
        // Get input tensor
        float* input = interpreter->typed_input_tensor<float>(0);
        int input_length = interpreter->input_tensor(0)->bytes / sizeof(float);
        assert(x.size() == input_length);
        std::copy(x.begin(), x.end(), input);
    }

    // Function to get the output data
    std::vector<std::vector<float>> get_outputs(tflite::Interpreter* interpreter) {
        // Get the output tensor
        size_t size = interpreter->outputs().size();
        std::vector<std::vector<float>> outputs;

        // convert each output tensor to a flat vector
        for (int i=0; i<size; i++) {

            auto output_tensor = interpreter->output_tensor(i);
            int num_output_elements = 1;
            for (int j = 0; j < output_tensor->dims->size; j++) {
                num_output_elements *= output_tensor->dims->data[j];
            }
            float* data = interpreter->typed_output_tensor<float>(i);
            outputs.emplace_back(data, data+num_output_elements);
        }
        return outputs;
    }

    // Function to run inference
    std::vector<std::vector<float>> run_inference(
        const std::string& model_path,
        // unsigned int slot,
        const std::vector<float>& x
    ) {
        auto& interpreter = get_interpreter(model_path);
        set_inputs(interpreter.get(), x);
        if (interpreter->Invoke() != kTfLiteOk) {
            std::cerr << "Failed to invoke tflite interpreter!" << std::endl;
            exit(-1);
        }
        return get_outputs(interpreter.get());
    }

    // Function to get the output tensor names
    std::vector<std::string> get_output_names(const std::string& path) {
        auto interpreter = build_interpreter(inference_model[path].get());
        size_t size = interpreter->outputs().size();
        std::vector<std::string> outputs(size);
        for (int i=0; i<size; i++) {
            outputs[i] = std::string(interpreter->GetOutputName(i));
        }
        return outputs;
    }

    // Function to get the input tensor names
    std::vector<std::string> get_input_names(const std::string& path) {
        auto interpreter = build_interpreter(inference_model[path].get());
        auto indices = interpreter->inputs();
        std::vector<std::string> outputs(indices.size());
        for (auto i : indices) {
            outputs[i] = std::string(interpreter->GetInputName(i));
        }
        return outputs;
    }

}

PYBIND11_MODULE(Inference, m) {
    m.doc() = "Extension for tflite model inference from C++ for RDataFrame compatibility";

    m.def("py_load_model", &nanoanalysis::load_model, "Initialize the tflite model at a provided path");
    m.def("py_run_inference", &nanoanalysis::run_inference, "Forward pass using previously loaded model");
    m.def("py_get_input_names", &nanoanalysis::get_input_names, "Get signature names of model inputs");
    m.def("py_get_output_names", &nanoanalysis::get_output_names, "get signature names of model outputs");
}

