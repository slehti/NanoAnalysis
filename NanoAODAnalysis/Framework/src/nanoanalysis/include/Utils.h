#ifndef NANOANALYSIS_UTILS
#define NANOANALYSIS_UTILS
#include "Inference.h"
#include "ROOT/RVec.hxx"
#include "ROOT/RDataFrame.hxx"
#include "TH1.h"

namespace nanoanalysis {

    float sample_pileup_weight(const TH1D* weights, bool is_data, int n_true);

    ROOT::RVec<float> dPhi(
        float single_phi,
        ROOT::RVec<float> phi_vec
    );

    ROOT::RVec<float> dPhi_combinations(
        ROOT::RVec<float> phi_vec
    );

    template<class T>
    ROOT::RVec<T> fix_size(
        ROOT::RVec<T> input_vec,
        size_t new_size,
        T value = std::numeric_limits<T>::quiet_NaN()
    ) {
        input_vec.resize(new_size, value);
        return input_vec;
    }

    template<class T>
    T get_leading(
        ROOT::RVec<T> input_vec,
        T fallback = std::numeric_limits<T>::quiet_NaN()
    ) {
        T result = fallback;
        if (input_vec.size() > 0) {result = input_vec[0];}
        return result;
    }

    bool match(
        float reco_eta,
        float reco_phi,
        ROOT::RVec<float> genpart_eta,
        ROOT::RVec<float> genpart_phi
    );

    float Rbb_min(
        ROOT::RVec<float> jet_phi,
        float met_phi,
        float tau_phi
    );

    float Rtau(
        float tau_pt,
        float tau_eta,
        float lChTrkPtOverTauPt,
        float lChTrkEtaDiff
    );

ROOT::RDF::RNode apply_classifier(ROOT::RDF::RNode df, const std::vector<std::string>& classifier_paths);
}

#endif


