import re
from pathlib import Path
from datetime import datetime

timestamp_format = "%a %b %d %H:%M:%S %Y"
era_map = {
    2011: "7TeV",
    2012: "8TeV",
    2016: "13TeV",
    2017: "13TeV",
    2018: "13TeV",
    2022: "13.6TeV",
    2023: "13.6TeV",
    2024: "13.6TeV",
}

def add_dcard_description(file, mass, years, lumis):
    channel_info = ""
    for channel in years.keys():
        channel_info += (
            f", channel {channel[0]} category {channel[1]} "
            f"luminosity = {lumis[channel]} from years [{', '.join(years[channel])}]"
        )
    desc = (
        "Description: NanoAnalysis - automatically generated datacard, "
        f"mass = {mass}{channel_info}"
    )
    date = generate_timestamp()
    delimiter_size = max(len(date), len(desc))
    delimiter = "-" * delimiter_size
    to_prepend = "\n".join((desc, date, delimiter)) + "\n"
    with open(file, "r+") as f:
        datacard = f.read()
        f.seek(0)
        f.write(to_prepend)
        f.write(datacard)
    return

def generate_timestamp():
    now = datetime.now()
    formatted_time = now.strftime(timestamp_format)
    timestamp = f"Date: {formatted_time}"
    return timestamp

def parse_timestamp(timestamp_str):
    # Remove the "Date: " prefix
    timestamp_str = timestamp_str.split(":", maxsplit=1)[1].strip()

    # Define the format string matching the timestamp structure
    format_string = timestamp_format

    # Parse the timestamp string using strptime
    datetime_obj = datetime.strptime(timestamp_str, format_string)
    return datetime_obj

def parse_dcard_descriptions(dcard_dir):
    masses= []
    dates = []
    bins = None
    dcard_dir = Path(dcard_dir)
    dcards = dcard_dir.glob("*/datacard.txt")
    for dcard in dcards:
        info = _parse_dcard_description(dcard)
        masses.append(info["mass"])
        dates.append(info["date"])
        if bins is None:
            bins = info["bins"]
    return {"masses": masses, "dates": dates, "bins": bins}

def _parse_dcard_description(dcard):
    last_line = 1
    with open(dcard, "r") as f:
        line_iter= iter(f)
        descline = next(line_iter)
        dateline = next(line_iter)

    mass_re = r"mass = (\d+)"
    bin_re = r"channel (\w+) category (\w+) luminosity = ((\d+\.?\d*)|(\.\d+)) from years \[([^\]]+)\]"
    mass = re.search(mass_re, descline).group(1)
    bins= re.findall(bin_re, descline)
    bins = {match[0]+match[1]: {"lumi": float(match[2]), "years": match[-1]} for match in bins}
    date = parse_timestamp(dateline)

    return {"mass": mass, "date": date, "bins": bins}

def get_lumis(metadata):
    lumis = {}
    for b in metadata["bins"].values():
        years = b["years"]
        if years in lumis and lumis[years] != b["lumi"]:
            raise ValueError(
                "Cannot reconcile multiple luminosities for a single data taking"
                " period!"
            )
        else:
            lumis[years] = b["lumi"]
    return lumis

def lumis_by_era(metadata):
    lumis = get_lumis(metadata)
    lumis_by_era = {}
    for y in lumis:
        era = get_era(y)
        if era in lumis_by_era:
            lumis_by_era[era] = lumis_by_era[era] + lumis[y]
        else:
            lumis_by_era[era] = lumis[y]
    return lumis_by_era

def get_era(years):
    def strip_year(year):
        return int(re.sub("[^0-9]", "", year))
    years = years.split(",")
    years = map(strip_year, years)
    eras = [era_map[y] for y in years]
    if any(e != eras[0] for e in eras):
        raise ValueError(
            "Including multiple LHC eras in a single analysis bin is not supported!"
        )
    return eras[0]

