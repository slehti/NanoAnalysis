import os
import aux

def get_mass_dirs(basepath):
    masses = [f.name for f in os.scandir() if f.is_dir() and f.name.isdigit()]
    return masses

def for_each_mass(command):
    for m in get_mass_dirs("."):
        os.chdir(m)
        aux.execute(command.format(mass=m), echo=True)
        os.chdir("..")

