from pathlib import Path
import sys
import json

def load_config(custompath):
    #configpath = Path(__file__).parent / "configs"
    defaultpath = Path(sys.argv[0]).parent / "configs"
    if custompath is None:
        with open(defaultpath / "base.json") as f:
            config = json.load(f)
    else:
        with open(custompath) as f:
            config = json.load(f)
    config = fill_inherited_config(config)
    return config

def fill_inherited_config(config):
    if "base" not in config:
        return config
    else:
        return update_config(load_config(config["base"]), config)

def update_config(current, new):
    if isinstance(current, dict) and isinstance(new, dict):
        for key in new:
            if key in current:
                current[key] = update_config(current[key], new[key])
            else:
                current[key] = new[key]
    else:
        current = new
    return current

