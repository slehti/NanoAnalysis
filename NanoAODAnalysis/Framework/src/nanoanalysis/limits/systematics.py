from dataclasses import dataclass, field
from typing import Tuple, Union, Optional
import json

import CombineHarvester.CombineTools.ch as ch


@dataclass
class Systematic:
    name: str
    mapping: ch.SystMap
    syst_type: str = field(default="lnN")
    readable_name: Optional[str] = None

    def __post_init__(self):
        if self.readable_name is None:
            self.readable_name = self.name

    def to_AddSyst(self):
        return self.name, self.syst_type, self.mapping

    def _get_template_index(self, template_type):
        backend_template_type = template_type.replace("$","").lower()
        types = [x.__name__ for x in self.mapping.methodcallers]
        return types.index(backend_template_type)

    def translations(self, process_labels):
        """
        Replace templated systematic names with concrete name alternatives
        """
        templates = ["$ERA", "$PROCESS"]
        original = [self.name]
        translated = [self.readable_name]
        for template in templates:
            if template in self.readable_name:
                i = self._get_template_index(template)
                alternatives = [keytuple[i] for keytuple in self.mapping.tmap.keys()]
                original = [
                    x.replace(template, alt)
                    for x in original
                    for alt in alternatives
                ]
                translated = [
                    x.replace(template, alt)
                    for x in translated
                    for alt in alternatives
                ]
        translations = {k:v for k,v in zip(original,translated)}
        for proc, label in process_labels.items():
            translations = {k: v.replace(proc, label) for k,v in translations.items()}
        return translations

def get_translations(process_labels=None):
    if process_labels is None:
        process_labels = {}
    rename = {k:v for syst in _all for k,v in syst.translations(process_labels).items()}
    rename["r"] = "#mu"
    return rename


_all = [
    Systematic(
        "lumi_$ERA",
        ch.SystMap("era","process")
            (["13TeV"], ["FakeTau"], 0.998)
            (["13TeV"], [
                "TT",
                "Wjets",
                "SingleTop",
                "DY",
                "VV",
                "ChargedHiggsToTauNu_Heavy_M170",
                "ChargedHiggsToTauNu_Heavy_M175",
                "ChargedHiggsToTauNu_Heavy_M200",
                "ChargedHiggsToTauNu_Heavy_M220",
                "ChargedHiggsToTauNu_Heavy_M250",
                "ChargedHiggsToTauNu_Heavy_M400",
                "ChargedHiggsToTauNu_Heavy_M500",
                "ChargedHiggsToTauNu_Heavy_M700",
                "ChargedHiggsToTauNu_Heavy_M800",
                "ChargedHiggsToTauNu_Heavy_M1500",
                "ChargedHiggsToTauNu_Heavy_M2000",
                "ChargedHiggsToTauNu_Heavy_M2500",
                "ChargedHiggsToTauNu_Heavy_M3000",
            ], 1.025),
        readable_name="luminosity ($ERA)"
    ),
    Systematic(
        "CMS_pileup",
        ch.SystMap("era")
            (["13TeV"], 1.050),
        readable_name="Pileup"
    ),
    Systematic(
        "pdf_TT",
        ch.SystMap("process")
            (["TT"], 1.042)
            (["FakeTau"], 0.997),
        readable_name="t#bar{t} PDF scale"
    ),
    Systematic(
        "QCDscale_TT",
        ch.SystMap("process")
            (["TT"],                (0.965,  1.024))
            (["FakeTau"],  (1.003,  0.998)),
        readable_name="t#bar{t} QCD scale"

    ),
    Systematic(
        "mass_top",
        ch.SystMap("process")
            (["TT"],                (0.973,  1.028))
            (["SingleTop"],         (1/1.022,1.022))
            (["FakeTau"],  (1.002,  0.998)),
        readable_name="Top quark mass"
    ),
    Systematic(
        "QCDscale_$PROCESS",
        ch.SystMap("process")
            (["Wjets"],     (0.996,  1.008))
            (["SingleTop"], (1/1.025,1.025))
            (["VV"],        (1/1.032,1.032))
            (["DY"],        (0.996,  1.007)),
        readable_name="QCD scale ($PROCESS)"
    ),
    Systematic(
        "pdf_$PROCESS",
        ch.SystMap("process")
            (["Wjets"],     1.038)
            (["SingleTop"], 1.047)
            (["VV"],        1.044)
            (["DY"],        1.037),
        readable_name="PDF scale ($PROCESS)"
    ),
    Systematic(
        "CMS_eff_t",
        ch.SystMap("process")
            (["FakeTau"], 0.997)
            ([
                "TT",
                "Wjets",
                "SingleTop",
                "DY",
                "VV",
                "ChargedHiggsToTauNu_Heavy_M170",
                "ChargedHiggsToTauNu_Heavy_M175",
                "ChargedHiggsToTauNu_Heavy_M200",
                "ChargedHiggsToTauNu_Heavy_M220",
                "ChargedHiggsToTauNu_Heavy_M250",
                "ChargedHiggsToTauNu_Heavy_M400",
                "ChargedHiggsToTauNu_Heavy_M500",
                "ChargedHiggsToTauNu_Heavy_M700",
                "ChargedHiggsToTauNu_Heavy_M800",
                "ChargedHiggsToTauNu_Heavy_M1500",
                "ChargedHiggsToTauNu_Heavy_M2000",
                "ChargedHiggsToTauNu_Heavy_M2500",
                "ChargedHiggsToTauNu_Heavy_M3000",
            ], 1.040),
        readable_name="Tau identification"

    ),
    Systematic(
        "CMS_eff_t_trg_data",
        ch.SystMap("era")
            (["13TeV"], 1.030),
        readable_name="Data #tau trigger eff."

    ),
    Systematic(
        "CMS_eff_t_trg_MC",
        ch.SystMap("era")
            (["13TeV"], 1.040),
        readable_name="MC #tau trigger eff."

    ),
    Systematic(
        "CMS_eff_met_trg_data",
        ch.SystMap("era")
            (["13TeV"], 1.200),
        readable_name="Data MET trigger eff."
    ),
    Systematic(
        "CMS_eff_met_trg_MC",
        ch.SystMap("era")
            (["13TeV"], 1.010),
        readable_name="MC MET trigger eff."
    ),
    Systematic(
        "CMS_eff_e_veto",
        ch.SystMap("process")
            (["TT"],                              1.007)
            (["Wjets"],                           1.004)
            (["SingleTop"],                       1.006)
            (["VV"],                              1.006)
            (["DY"],                              1.004)
            (["ChargedHiggsToTauNu_Heavy_M170"],  1.005)
            (["ChargedHiggsToTauNu_Heavy_M175"],  1.005)
            (["ChargedHiggsToTauNu_Heavy_M200"],  1.005)
            (["ChargedHiggsToTauNu_Heavy_M220"],  1.005)
            (["ChargedHiggsToTauNu_Heavy_M250"],  1.005)
            (["ChargedHiggsToTauNu_Heavy_M400"],  1.005)
            (["ChargedHiggsToTauNu_Heavy_M500"],  1.005)
            (["ChargedHiggsToTauNu_Heavy_M700"],  1.005)
            (["ChargedHiggsToTauNu_Heavy_M800"],  1.005)
            (["ChargedHiggsToTauNu_Heavy_M1500"], 1.005)
            (["ChargedHiggsToTauNu_Heavy_M2000"], 1.005)
            (["ChargedHiggsToTauNu_Heavy_M2500"], 1.005)
            (["ChargedHiggsToTauNu_Heavy_M3000"], 1.005),
        readable_name="Electron veto eff."

    ),
    Systematic(
        "CMS_eff_m_veto",
        ch.SystMap("process")
            (["TT"],                              1.004)
            (["Wjets"],                           1.002)
            (["SingleTop"],                       1.003)
            (["VV"],                              1.004)
            (["DY"],                              1.006)
            (["ChargedHiggsToTauNu_Heavy_M170"],  1.003)
            (["ChargedHiggsToTauNu_Heavy_M175"],  1.003)
            (["ChargedHiggsToTauNu_Heavy_M200"],  1.003)
            (["ChargedHiggsToTauNu_Heavy_M220"],  1.003)
            (["ChargedHiggsToTauNu_Heavy_M250"],  1.003)
            (["ChargedHiggsToTauNu_Heavy_M400"],  1.003)
            (["ChargedHiggsToTauNu_Heavy_M500"],  1.003)
            (["ChargedHiggsToTauNu_Heavy_M700"],  1.003)
            (["ChargedHiggsToTauNu_Heavy_M800"],  1.003)
            (["ChargedHiggsToTauNu_Heavy_M1500"], 1.003)
            (["ChargedHiggsToTauNu_Heavy_M2000"], 1.003)
            (["ChargedHiggsToTauNu_Heavy_M2500"], 1.003)
            (["ChargedHiggsToTauNu_Heavy_M3000"], 1.003),
        readable_name="Muon veto eff."

    ),
    Systematic(
        "CMS_eff_b",
        ch.SystMap("era")
            (["13TeV"], 1.050),
        readable_name="b jet eff."

    ),
    Systematic(
        "CMS_fake_b",
        ch.SystMap("era")
            (["13TeV"], 1.020),
        readable_name="Fake b jet eff."

    ),
    Systematic(
        "CMS_fake_e_to_t",
        ch.SystMap("process")
            (["TT"],                              1.001)
            (["Wjets"],                           1.001)
            (["SingleTop"],                       1.001)
            (["VV"],                              1.001)
            (["DY"],                              1.001)
            (["ChargedHiggsToTauNu_Heavy_M170"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M175"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M200"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M220"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M250"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M400"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M500"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M700"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M800"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M1500"], 1.001)
            (["ChargedHiggsToTauNu_Heavy_M2000"], 1.001)
            (["ChargedHiggsToTauNu_Heavy_M2500"], 1.001)
            (["ChargedHiggsToTauNu_Heavy_M3000"], 1.001),
        readable_name="e misid. as #tau"

    ),
    Systematic(
        "CMS_fake_m_to_t",
        ch.SystMap("process")
            (["TT"],                              1.001)
            (["Wjets"],                           1.001)
            (["SingleTop"],                       1.001)
            (["VV"],                              1.001)
            (["DY"],                              1.001)
            (["ChargedHiggsToTauNu_Heavy_M170"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M175"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M200"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M220"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M250"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M400"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M500"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M700"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M800"],  1.001)
            (["ChargedHiggsToTauNu_Heavy_M1500"], 1.001)
            (["ChargedHiggsToTauNu_Heavy_M2000"], 1.001)
            (["ChargedHiggsToTauNu_Heavy_M2500"], 1.001)
            (["ChargedHiggsToTauNu_Heavy_M3000"], 1.001),
        readable_name="#mu misid. as #tau"

    ),
    Systematic(
        "CMS_scale_t",
        ch.SystMap("era")
            (["13TeV"], 1.060),
        readable_name="Top quark scale"

    ),
    Systematic(
        "CMS_scale_j",
        ch.SystMap("era")
            (["13TeV"], 1.030),
        readable_name="Jet energy scale"

    ),
    Systematic(
        "CMS_scale_met",
        ch.SystMap("era")
            (["13TeV"], 1.030),
        readable_name="MET scale"

    ),
    Systematic(
        "CMS_res_j",
        ch.SystMap("era")
            (["13TeV"], 1.040),
        readable_name="Jet energy resolution"

    ),
    Systematic(
        "CMS_Hptn_pdf_top",
        ch.SystMap("process")
            (["TT"],                (0.980,  1.003))
            (["FakeTau"],  (1.002,  1.000)),
        readable_name="Top quark PDF scale"

    ),
    Systematic(
        "CMS_Hptn_pdf_ewk",
        ch.SystMap("process")
            (["Wjets"], (0.967,  1.046))
            (["DY"],    (0.967,  1.046))
            (["VV"],    (0.967,  1.046)),
        readable_name="EWK PDF scales"

    ),
    Systematic(
        "CMS_Hptn_pdf_Hptn",
        ch.SystMap("process")
            (["ChargedHiggsToTauNu_Heavy_M170"],  (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M175"],  (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M200"],  (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M220"],  (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M250"],  (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M400"],  (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M500"],  (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M700"],  (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M800"],  (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M1500"], (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M2000"], (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M2500"], (0.996, 1.017))
            (["ChargedHiggsToTauNu_Heavy_M3000"], (0.996, 1.017)),
        readable_name="H^{#pm} PDF scale"

    ),
    Systematic(
        "CMS_Hptn_mu_RF_top",
        ch.SystMap("process")
            (["TT"], 1.020)
            (["SingleTop"], 1.020)
            (["FakeTau"], 0.998),
        readable_name="Top quark RF scale"

    ),
    Systematic(
        "CMS_Hptn_mu_RF_ewk",
        ch.SystMap("process")
            (["Wjets"], 1.050)
            (["DY"],    1.050)
            (["VV"],    1.050),
        readable_name="EWK RF scales"

    ),
    Systematic(
        "CMS_Hptn_mu_RF_Hptn",
        ch.SystMap("process")
            (["ChargedHiggsToTauNu_Heavy_M170"],  1.048)
            (["ChargedHiggsToTauNu_Heavy_M175"],  1.048)
            (["ChargedHiggsToTauNu_Heavy_M200"],  1.048)
            (["ChargedHiggsToTauNu_Heavy_M220"],  1.048)
            (["ChargedHiggsToTauNu_Heavy_M250"],  1.048)
            (["ChargedHiggsToTauNu_Heavy_M400"],  1.048)
            (["ChargedHiggsToTauNu_Heavy_M500"],  1.048)
            (["ChargedHiggsToTauNu_Heavy_M700"],  1.048)
            (["ChargedHiggsToTauNu_Heavy_M800"],  1.048)
            (["ChargedHiggsToTauNu_Heavy_M1500"], 1.048)
            (["ChargedHiggsToTauNu_Heavy_M2000"], 1.048)
            (["ChargedHiggsToTauNu_Heavy_M2500"], 1.048)
            (["ChargedHiggsToTauNu_Heavy_M3000"], 1.048),
        readable_name="H^{#pm} RF scale"

    ),
    Systematic(
        "CMS_topPtReweight",
        ch.SystMap("process")
            (["TT"], 1.250)
            (["FakeTau"], 1.250),
        readable_name="Approximation for top quark p_{T} reweighting"
    ),
]

systematics = {
    s.name: s for s in _all
}


