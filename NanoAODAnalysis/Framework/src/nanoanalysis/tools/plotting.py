#!/usr/bin/env python
#
# Helper for plotting from pseudomulticrab output
#
# 24.11.2022/S.Lehti
#

import sys
import os
import re
import subprocess
from array import array
import math
from itertools import product

import ROOT
import numpy as np

ROOT.gROOT.SetBatch(True)
ROOT.gErrorIgnoreLevel = ROOT.kWarning

root_re = re.compile("(?P<rootfile>([^/]*histograms.root))")

basepath_re = re.compile("(?P<basepath>\S+/NanoAnalysis)/")
match = basepath_re.search(os.getcwd())
if match:
    sys.path.append(os.path.join(match.group("basepath"),"NanoAODAnalysis/Framework/python"))

from nanoanalysis.aux import *
import nanoanalysis.tools.crosssection as crosssection

class Dataset:
    def __init__(self,name,multicrabdir=""):
        self.name = os.path.basename(name)
        self.multicrabdir = multicrabdir
        self.histofiles = []
        self.histo = {}
        self.lumi = 0
        self.energy = "13"
        self.uwcounters = None

        self.run  = ""
        self.year = ""
        self.jec = []
        self.isdata = False

    def readSkim(self):
        eventFiles = execute("ls %s"%(os.path.join(self.multicrabdir,self.name,"res*","events*.root")))

        if len(eventFiles):
            print("Empty dataset",self.name)
            return False

        self.isdata = False
        data_re = re.compile("_Run20")
        match = data_re.search(self.name)
        if match:
            self.isdata = True

        self.skimCounters = None
        for ef in eventFiles:
            fIN = ROOT.TFile.Open(ef)
            if self.skimCounters == None:
                self.skimCounters = fIN.Get(os.path.join("configInfo","skimCounter")).Clone("skimCounter")
                self.skimCounters.SetDirectory(0)
            else:
                self.skimCounters.Add(fIN.Get(os.path.join("configInfo","skimCounter")).Clone("skimCounter"))
            fIN.Close()
        self.counters = self.skimCounters
        self.uwcounters = self.skimCounters
        return True

    def read(self):
        print("Reading",self.name)
        # read cfgInfo
        #histofile = os.path.join(self.multicrabdir,self.name,"results","histograms.root")
        histofile = execute("ls %s"%(os.path.join(self.multicrabdir,self.name,"res*","histograms*.root")))[0]
        if not os.path.exists(histofile):
            return False
        self.histofiles.append(histofile)
        fIN = ROOT.TFile.Open(self.histofiles[0])
        counter_h = fIN.Get(os.path.join("configInfo","weighted_counter"))
        if counter_h:
            self.counters = counter_h.Clone("weighted counters")
            self.counters.SetDirectory(0)
        else:
            self.counters = None
        uwcounter_h = fIN.Get(os.path.join("configInfo","unweighted_counter"))
        if uwcounter_h:
            self.uwcounters = uwcounter_h.Clone("unweighted counters")
            self.uwcounters.SetDirectory(0)
        else:
            self.uwcounters = None

        isdata_h = fIN.Get(os.path.join("configInfo","isdata"))
        if isdata_h:
            isdata_h = isdata_h.Clone("isdata")
            self.isdata = isdata_h.GetBinContent(1)
        else:
            self.isdata = False
        if self.isdata:
            h_lumi = fIN.Get(os.path.join("configInfo","lumi")).Clone("lumi")
            self.lumi = h_lumi.GetBinContent(1)


        run_re = re.compile("(?P<year>20\d\d)\w+?")
        match = run_re.search(self.name)
        if not match:
            match = run_re.search(self.multicrabdir)
        if match:
            self.run = match.group(0)
            self.year = match.group("year")

        if float(self.year) > 2020:
            self.energy = "13.6"

        jec_re = re.compile("JEC used: (?P<jec>\S+)")
        configDir = ROOT.gFile.GetDirectory("configInfo")
        configKeys = configDir.GetListOfKeys()
        for i in range(configDir.GetNkeys()):
            keyname = configKeys.At(i).GetName()
            match = jec_re.search(keyname)
            if match:
                jec = match.group("jec")
                if not jec in self.jec:
                    self.jec.append(jec)

        # histogram directories
        rpaths = []
        basekeys = fIN.GetListOfKeys()
        self.analysis_dirs = [k.GetName() for k in basekeys]
        self.analysis_dirs.remove("configInfo")
        for key in basekeys:
            if key.GetName() == 'configInfo':
                continue
            rpaths.extend(self.getpaths(fIN,key.GetName()))
            """
            nDir = fIN.cd(key.GetName())
            gDir = ROOT.gFile.GetDirectory(key.GetName())
            nextkeys = gDir.GetListOfKeys()
            for nkey in nextkeys:
                keyname = nkey.GetName()
                obj = fIN.Get(os.path.join(key.GetName(),keyname)).Clone(keyname)
                if obj.IsFolder():
                    rpaths.append(os.path.join(key.GetName(),keyname))
                else:
                    if key.GetName() not in rpaths:
                        rpaths.append(key.GetName())
            """

        # read histograms
        histonames = []
        for rpath in rpaths:
            hdir = fIN.cd(rpath)
            gDir = ROOT.gFile.GetDirectory(rpath)
            keys = gDir.GetListOfKeys()
            nkeys = gDir.GetNkeys()
            for i in range(nkeys):
                keyname = keys.At(i).GetName()
                obj = fIN.Get(os.path.join(rpath,keyname)).Clone(keyname)
                if isinstance(obj, ROOT.TH1D):
                    histonames.append(os.path.join(rpath,keyname))
                if isinstance(obj, ROOT.TH2D):
                    histonames.append(os.path.join(rpath,keyname))
                if isinstance(obj, ROOT.TH3D):
                    histonames.append(os.path.join(rpath,keyname))
                if isinstance(obj, ROOT.TProfile):
                    histonames.append(os.path.join(rpath,keyname))
                if isinstance(obj, ROOT.TProfile2D):
                    histonames.append(os.path.join(rpath,keyname))

        #print("Reading histograms templates from file:",len(histonames))
        for histoname in histonames:
            if not histoname in self.histo.keys():
                self.histo[histoname] = fIN.Get(histoname).Clone(histoname)
                self.histo[histoname].Reset()
                self.histo[histoname].SetDirectory(0)
        fIN.Close()

        for fname in self.histofiles:
            fIN = ROOT.TFile.Open(fname)
            for histoname in self.histo.keys():
                hIN = fIN.Get(histoname).Clone("histoname")
                self.histo[histoname].Add(hIN)
            fIN.Close()

        return True

    def getpaths(self,fIN,pathname):
        ret = []
        obj = fIN.Get(pathname)
        if obj.IsFolder():
            ret.append(pathname)
            nextkeys = obj.GetListOfKeys()
            for key in nextkeys:
                nextpaths = self.getpaths(fIN,os.path.join(pathname,key.GetName()))
                ret.extend(nextpaths)
        return ret

    def append(self,dset,norm=1):
        self.isdata = dset.isData()

        if len(self.histo.keys()) > 0:
            for keyname in self.histo.keys():
                self.histo[keyname].Add(dset.histo[keyname])
        else:
            self.histo = dset.histo

        if hasattr(self, 'counters'):
            self.counters.Add(dset.counters)
        else:
            self.counters = dset.counters

        self.lumi += dset.lumi
        self.year = dset.year
        self.energy = dset.energy
        if self.year in self.run:
            #self.run += dset.run[4:]
            self.run = self.run[:5] + '-' + dset.run[4:]
        else:
            self.run = dset.run

        for jec in dset.getJEC():
            if not jec in self.jec:
                self.jec.append(jec)

    def getHistogram(self,histoname):
        return self.histo[histoname]

    def getHistogramNames(self):
        return self.histo.keys()

    def getDirectoryContent(self,directory):
        content = []
        for key in self.histo.keys():
            if directory in key:
                content.append(key)
        return content

    def histogram(self,histoname):
        fIN0 = ROOT.TFile.Open(self.histofiles[0])
        h = fIN0.Get(os.path.join("analysis",histoname)).Clone(histoname)
        h.Reset()
        h.SetDirectory(0)
        fIN0.Close()
        for fname in self.histofiles:
            fIN = ROOT.TFile.Open(fname)
            hIN = fIN.Get(os.path.join("analysis",histoname))
            h.Add(hIN)
            fIN.Close()
        h.SetMarkerSize(self.style.getMarkerSize())
        h.SetMarkerStyle(self.style.getMarkerStyle())
        h.SetFillColor(self.style.getColor())
        return h

    def exists(self,histoname):
        return histoname in self.histo

    def isData(self):
        return self.isdata

    def getLumi(self):
        return self.lumi

    def getEnergy(self):
        return float(self.energy)

    def getYear(self):
        return self.year

    def getRun(self):
        return self.run

    def getJEC(self):
        return self.jec

    def getEvents(self):
        # Skim: All events is the bin with largest number of events (bin labels hist.hist -> ROOT not working in uproot 3)
        # nev = 0
        # for i in range(1,self.counters.GetNbinsX()+1):
        #     label = self.counters.GetXaxis().GetBinLabel(i)
        #     if self.counters.GetBinContent(i) > nev:
        #         nev = self.counters.GetBinContent(i)
        nev = None
        if self.counters is not None:
            nev = self.counters.GetBinContent(
                self.counters.GetXaxis().FindBin("Skim: All events")
            )
        return nev

    def getName(self):
        return self.name

    def setName(self,name):
        self.name = name

    def getCounters(self):
        counter2=None
        if isinstance(self.uwcounters, ROOT.TH1F):
            counter2 = self.uwcounters
        return self.counters,counter2

    def printCounters(self):
        print("Counters")
        for i in range(1,self.counters.GetNbinsX()+1):
            label = self.counters.GetXaxis().GetBinLabel(i)
            while len(label) < 25:
                label += ' '
            print(label,self.counters.GetBinContent(i))

    def normalizeToLumi(self,lumi, xsec_signal=None):
        if self.isData():
            return
        dsetname = self.name
        if not self.isdata and "Run201" in self.name:
            dsetname = dsetname[:-8]

        xsec = crosssection.backgroundCrossSections.crossSection(dsetname, self.energy)
        nev = self.getEvents()
        if nev is None:
            print(f"No event counter found for {self.name}, assuming histogram is already normalized!")
            return
        if xsec is None:
            print(f"Using cross-section {xsec_signal} for {self.name}")
            xsec = xsec_signal
        norm = lumi*xsec/nev
        #print("check norm",dsetname,self.energy,lumi,xsec,nev,norm)

        for keyname in self.histo.keys():
            self.histo[keyname].Scale(norm)

        self.counters.Scale(norm)

    def get_mass(self, config):
        return parse_mass(self.name, config["signal_mass_re"])

    def is_signal(self, config):
        return self.get_mass(config) != "*"

    def is_background(self, config):
        return (not self.isData()) and not self.is_signal(config)


def getDatasetsMany(multicrabdirs,whitelist=[],blacklist=[]):
    datasets = []
    for m in multicrabdirs:
        if 'multicrab' not in m:
            continue
        ds = getDatasets(m,whitelist,blacklist)
        datasets.extend(ds)
    return datasets

def getDatasets(multicrabdir,whitelist=[],blacklist=[]):
    datasets = []
    cands = execute("ls %s"%multicrabdir)

    if len(whitelist) > 0:
        datasets_whitelist = []
        for d in cands:
            for wl in whitelist:
                wl_re = re.compile(wl)
                match = wl_re.search(d)
                if match:
                    datasets_whitelist.append(d)
                    break
        cands = datasets_whitelist

    if len(blacklist) > 0:
        datasets_blacklist = []
        for d in cands:
            found = False
            for bl in blacklist:
                bl_re = re.compile(bl)
                match = bl_re.search(d)
                if match:
                    found = True
                    break
            if not found:
                datasets_blacklist.append(d)
        cands = datasets_blacklist

    for c in cands:
        resultdir = os.path.join(multicrabdir,c,"results")
        if os.path.exists(resultdir):
            datasets.append(Dataset(c,multicrabdir))
        resdir = os.path.join(multicrabdir,c,"res")
        if os.path.exists(resdir):
            datasets.append(Dataset(c,multicrabdir))
    return datasets

def mergeExtDatasets(datasets):
    for d in datasets:
        mergeDsets = []
        name = d.name
        if "ext" in name:
            ext_re = re.compile("(?P<name>\S+)_ext")
            match = ext_re.search(name)
            if match:
                name = match.group("name")
        for dd in datasets:
            if name in dd.name:
                mergeDsets.append(dd)
        if len(mergeDsets) > 1:
            datasets = mergeDatasets(name,name,datasets)
    return datasets

def mergeDatasets(datasetName,datasetRe,datasets,lumi=0,keep=False):
    subset = []
    rest   = []
    dset_re = re.compile(datasetRe)
    isdata = False
    for d in datasets:
        match = dset_re.search(d.name)
        if match:
            if d.isData():
                isdata = True
            subset.append(d)
            if keep:
                rest.append(d)
        else:
            rest.append(d)
    if len(subset) == 0:
        print("Warning, dataset",datasetName,"is empty!")
    else:
        if datasetName in getDatasetNames(datasets):
            print("Dataset with name %s already exists. Not merging."%datasetName)
            return datasets

        print("Merging datasets")
        for d in subset:
            print("    ",d.name)
        print("     as\033[94m",datasetName,'\033[0m')

    if len(subset) > 0:
        mergedDset = Dataset(datasetName)
        for d in subset:
            newd = d
            if not newd.isData() and lumi > 0:
                newd.normalizeToLumi(lumi)
            mergedDset.append(newd)
        rest.append(mergedDset)
    return rest

def removeDatasets(datasetRe,datasets):
    subset = []
    dset_re = re.compile(datasetRe)
    for d in datasets:
        match = dset_re.search(d.name)
        if not match:
            subset.append(d)
    return subset

def read(datasets):
    dsets = []
    for d in datasets:
        if d.read() or d.readSkim():
            dsets.append(d)
    return dsets

def getLumi(datasets):
    lumi = 0
    for d in datasets:
        if d.isData():
            lumi+=d.getLumi()
    return lumi

def normalizeToLumi(datasets, xsec_signal=None):
    lumi = getLumi(datasets)
    for d in datasets:
        d.normalizeToLumi(lumi, xsec_signal=xsec_signal)
    return datasets

def getRuns(datasets):
    runs = []
    for d in datasets:
        if d.isData():
            runs.append(d.getRun())
    return runs

def getYears(datasets):
    years = []
    for d in datasets:
        if d.isData():
            year = d.getYear()
            if year not in years:
                years.append(year)
    return years

def getDataDatasetNames(datasets):
    names = []
    for d in datasets:
        if d.isData():
            names.append(d.getName())
    return names

def getDatasetNames(datasets):
    names = []
    for d in datasets:
        names.append(d.getName())
    return names

def renameDataset(oldname,newname,datasets):
    for d in datasets:
        if d.getName() == oldname:
            d.setName(newname)
    return datasets

def getDatasetByName(name,datasets):
    for d in datasets:
        if d.getName() == name:
            return d
    print("getDatasetByName, dataset %s not found"%name)
    return None

def getSize(d):
    return d.histo[list(d.histo)[0]].Integral()

def orderBySize(datasets):
    ordered = []
    data = []
    mc = []
    for d in datasets:
        if d.isData():
            data.append(d)
        else:
            mc.append(d)

    mc.sort(key=getSize)

    histonames = mc[0].histo.keys()
    for h in histonames:
        for i in range(len(mc)):
            for j in range(0,i):
                mc[i].histo[h].Add(mc[j].histo[h])

    ordered.extend(reversed(mc))
    ordered.extend(data)
    return ordered

def printd(datasets):
    for d in datasets:
        print(d.name)

def getRatio(graph1,graph2):
    nr = graph1.GetN()
    xr = []
    yr = []
    xrerr = []
    yrerr = []
    for i in range(0,nr):
        x = graph1.GetPointX(i)
        y1 = graph1.GetPointY(i)
        y2 = graph2.GetPointY(i)

        yerr1 = graph1.GetErrorY(i)
        yerr2 = graph2.GetErrorY(i)

        xr.append(float(x))
        ratio = 0
        if not y2 == 0:
            ratio = float(y1)/float(y2)
        yr.append(ratio)
        xrerr.append(0)
        ratioerr = 0

        if y1>0.0001 and y2>0.0001:
            ratioerr = math.sqrt(yerr1*yerr1/(y1*y1) + yerr2*yerr2/(y2*y2))
        yrerr.append(ratioerr)

    return ROOT.TGraphErrors(nr,array('d',xr),array('d',yr),array('d',xrerr),array('d',yrerr))

def graph2th1(name,graph,N,xmin,xmax):
    h = ROOT.TH1F(name,"",N,xmin,xmax)
    for i in range(0,N):
        x = graph.GetPointX(i)
        y = graph.GetPointY(i)

        yerr = graph.GetErrorY(i)

        h.Fill(x,y);
        h.SetBinError(i,yerr)
    return h.Clone(name)

def printGraph(name,graph):
    for i in range(0,graph.GetN()):
        x = graph.GetPointX(i)
        y = graph.GetPointY(i)
        print("    ",i,x,y)

def CMS_lumi(pad, lumis):

    lumiTextSize     = 0.6
    lumiTextOffset   = 0.2

    relPosX    = 0.045
    relPosY    = 0.035
    relExtraDY = 1.2

    H = pad.GetWh()
    W = pad.GetWw()
    l = pad.GetLeftMargin()
    t = pad.GetTopMargin()
    r = pad.GetRightMargin()
    b = pad.GetBottomMargin()
    e = 0.025

    pad.cd()

    lumiText = ""
    first = True
    for era in lumis:
        text = f"{lumis[era]*1e-3:.1f} fb^{{-1}} ({era})"
        if first:
            first = False
        else:
            text = " + " + text
        lumiText += text

    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextAngle(0)
    latex.SetTextColor(ROOT.kBlack)

    latex.SetTextFont(42)
    latex.SetTextAlign(31)
    latex.SetTextSize(lumiTextSize*t)

    latex.DrawLatex(1-r,1-t+lumiTextOffset*t,lumiText)

    pad.cd()
    pad.Update()
    return

def create_axis_hist(src, at_limits=True):
    import ctypes

    backup = ROOT.gPad
    tmp = ROOT.TCanvas()
    tmp.cd()
    src.Draw('AP')
    result = src.GetHistogram().Clone('tmp')
    if (at_limits):
        min = 0.
        max = 0.
        x = ctypes.c_double(0.)
        y = ctypes.c_double(0.)
        src.GetPoint(0, x, y)
        min = x.value
        max = x.value
        for i in range(1, src.GetN()):
            src.GetPoint(i, x, y)
            if x.value < min:
                min = float(x.value)
            if x.value > max:
                max = float(x.value)
        result.GetXaxis().SetLimits(min, max)
    ROOT.gPad = backup

    return result

def get_axis_hist(pad):
    pad_obs = pad.GetListOfPrimitives()
    if pad_obs is None:
        return None
    obj = None
    for obj in pad_obs:
        if obj.InheritsFrom(ROOT.TH1.Class()):
            return obj
        if obj.InheritsFrom(ROOT.TMultiGraph.Class()):
            return obj.GetHistogram()
        if obj.InheritsFrom(ROOT.TGraph.Class()):
            return obj.GetHistogram()
        if obj.InheritsFrom(ROOT.THStack.Class()):
            return obj.GetHistogram()
    return None


def DrawCMSLogo(pad, cmsText, extraText, iPosX, relPosX, relPosY, relExtraDY, extraText2='', cmsTextSize=0.8):
    """Draw the standard CMS logo with the optional extra text

    Args:
        pad (TYPE): Description
        cmsText (TYPE): Description
        extraText (TYPE): Description
        iPosX (TYPE): Description
        relPosX (TYPE): Description
        relPosY (TYPE): Description
        relExtraDY (TYPE): Description
        extraText2 (str): Description
        cmsTextSize (float): Description

    Returns:
        TYPE: Description
    """
    pad.cd()
    cmsTextFont = 62  # default is helvetic-bold

    writeExtraText = len(extraText) > 0
    writeExtraText2 = len(extraText2) > 0
    extraTextFont = 52

    # text sizes and text offsets with respect to the top frame
    # in unit of the top margin size
    lumiTextOffset = 0.2
    # cmsTextSize = 0.8
    # float cmsTextOffset    = 0.1;  // only used in outOfFrame version

    # ratio of 'CMS' and extra text size
    extraOverCmsTextSize = 0.76

    outOfFrame = False
    if iPosX / 10 == 0:
        outOfFrame = True

    alignY_ = 3
    alignX_ = 2
    if (iPosX / 10 == 0):
        alignX_ = 1
    if (iPosX == 0):
        alignX_ = 1
    if (iPosX == 0):
        alignY_ = 1
    if (iPosX / 10 == 1):
        alignX_ = 1
    if (iPosX / 10 == 2):
        alignX_ = 2
    if (iPosX / 10 == 3):
        alignX_ = 3
    # if (iPosX == 0): relPosX = 0.14
    align_ = 10 * alignX_ + alignY_

    l = pad.GetLeftMargin()
    t = pad.GetTopMargin()
    r = pad.GetRightMargin()
    b = pad.GetBottomMargin()

    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextAngle(0)
    latex.SetTextColor(ROOT.kBlack)

    extraTextSize = extraOverCmsTextSize * cmsTextSize
    pad_ratio = (float(pad.GetWh()) * pad.GetAbsHNDC()) / \
        (float(pad.GetWw()) * pad.GetAbsWNDC())
    if (pad_ratio < 1.):
        pad_ratio = 1.

    if outOfFrame:
        latex.SetTextFont(cmsTextFont)
        latex.SetTextAlign(11)
        latex.SetTextSize(cmsTextSize * t * (1./pad_ratio))
        latex.DrawLatex(l, 1 - t + lumiTextOffset * t, cmsText)

    posX_ = 0
    if iPosX % 10 <= 1:
        posX_ = l + relPosX * (1 - l - r)
    elif (iPosX % 10 == 2):
        posX_ = l + 0.5 * (1 - l - r)
    elif (iPosX % 10 == 3):
        posX_ = 1 - r - relPosX * (1 - l - r)

    posY_ = 1 - t - relPosY * (1 - t - b)
    if not outOfFrame:
        latex.SetTextFont(cmsTextFont)
        latex.SetTextSize(cmsTextSize * t * pad_ratio)
        latex.SetTextAlign(align_)
        latex.DrawLatex(posX_, posY_, cmsText)
        if writeExtraText:
            latex.SetTextFont(extraTextFont)
            latex.SetTextAlign(align_)
            latex.SetTextSize(extraTextSize * t * pad_ratio)
            latex.DrawLatex(
                posX_, posY_ - relExtraDY * cmsTextSize * t, extraText)
            if writeExtraText2:
                latex.DrawLatex(
                    posX_, posY_ - 1.8 * relExtraDY * cmsTextSize * t, extraText2)
    elif writeExtraText:
        if iPosX == 0:
            posX_ = l + relPosX * (1 - l - r)
            posY_ = 1 - t + lumiTextOffset * t
        latex.SetTextFont(extraTextFont)
        latex.SetTextSize(extraTextSize * t * (1./pad_ratio))
        latex.SetTextAlign(align_)
        latex.DrawLatex(posX_, posY_, extraText)

def fill_pave(pave, text, alignment=10, fillcolor=ROOT.kWhite, fillalpha=0., font=42):
    if text is not None:
        for line in text:
            pave.AddText(line)
        for t in pave.GetListOfLines():
            t.SetTextAlign(alignment)
        pave.SetFillColorAlpha(fillcolor, fillalpha)
        pave.SetTextFont(font)
        pave.Draw()
    return

def positioned_legend(
        width,
        height,
        pos,
        offset,
        horizontaloffset=None,
        description_height=0.2,
        columns=1,
    ):
    do = offset
    o = offset
    ho = horizontaloffset
    if not ho:
        ho = o
    w = width
    h = height
    h2 = description_height
    l = ROOT.gPad.GetLeftMargin()
    t = ROOT.gPad.GetTopMargin()
    b = ROOT.gPad.GetBottomMargin()
    r = ROOT.gPad.GetRightMargin()
    if pos == 1:
        x1 = l + ho
        x2 = l + ho + w
        y1 = 1 - t - o - h
        y2 = 1 - t - o
        y1_extra = y1 - h2 - do
        y2_extra = y1 - do
    if pos == 2:
        c = l + 0.5 * (1 - l - r)
        x1 = c - 0.5*w
        x2 = c + 0.5*w
        y1 = 1 - t - o - h
        y2 = 1 - t - o
        y1_extra = y1 - h2 - do
        y2_extra = y1 - do
    if pos == 3:
        x1 = 1 - r - ho - w
        x2 = 1 - r - ho
        y1 = 1 - t - o - h
        y2 = 1 - t - o
        y1_extra = y1 - h2 - do
        y2_extra = y1 - do
    if pos == 4:
        x1 = l + ho
        x2 = l + ho + w
        y1 = b + o
        y2 = b + o + h
        y1_extra = y2 + do
        y2_extra = y2 + h2 + do
    if pos == 5:
        c = l + 0.5 * (1 - l - r)
        x1 = c - 0.5*w
        x2 = c + 0.5*w
        y1 = b + o
        y2 = b + o + h
        y1_extra = y2 + do
        y2_extra = y2 + h2 + do
    if pos == 6:
        x1 = 1 - r - ho - w
        x2 = 1 - r - ho
        y1 = b + o
        y2 = b + o + h
        y1_extra = y2 + do
        y2_extra = y2 + h2 + do
    x_extra = x1
    leg = ROOT.TLegend(x1,y1,x2,y2, '', 'NBNDC')
    leg.SetNColumns(columns)
    pave = ROOT.TPaveText(x1, y1_extra, x2, y2_extra, 'NBNDC')
    return leg, pave

def FixBothRanges(pad, fix_y_lo, frac_lo, fix_y_hi, frac_hi):
    """Adjusts y-axis range such that a lower and a higher value are located a
    fixed fraction of the frame height away from a new minimum and maximum
    respectively.

    This function is useful in conjunction with GetPadYMax which returns the
    maximum or minimum y value of all histograms and graphs drawn on the pad.

    In the example below, the minimum and maximum values found via this function
    are used as the `fix_y_lo` and `fix_y_hi` arguments, and the spacing fractions
    as 0.15 and 0.30 respectively.

    @code
    FixBothRanges(pad, GetPadYMin(pad), 0.15, GetPadYMax(pad), 0.30)
    @endcode

    ![](figures/FixBothRanges.png)

    Args:
        pad (TPad): A TPad on which histograms and graphs have already been drawn
        fix_y_lo (float): The y value which will end up a fraction `frac_lo` above
                          the new axis minimum.
        frac_lo (float): A fraction of the y-axis height
        fix_y_hi (float): The y value which will end up a fraction `frac_hi` below
                         from the new axis maximum.
        frac_hi (float): A fraction of the y-axis height
    """
    hobj = get_axis_hist(pad)
    ymin = fix_y_lo
    ymax = fix_y_hi
    if ROOT.gPad.GetLogy():
        if ymin == 0.:
            print('Cannot adjust log-scale y-axis range if the minimum is zero!')
            return
        ymin = math.log10(ymin)
        ymax = math.log10(ymax)
    fl = frac_lo
    fh = frac_hi

    ymaxn = (
        (1. / (1. - (fh*fl/((1.-fl)*(1.-fh))))) *
        (1. / (1. - fh)) *
        (ymax - fh*ymin)
        )
    yminn = (ymin - fl*ymaxn) / (1. - fl)
    if ROOT.gPad.GetLogy():
        yminn = math.pow(10, yminn)
        ymaxn = math.pow(10, ymaxn)
    hobj.SetMinimum(yminn)
    hobj.SetMaximum(ymaxn)

def Set(obj, **kwargs):
    for key, value in kwargs.items():
        if value is None:
            getattr(obj, 'Set' + key)()
        elif isinstance(value, (list, tuple)):
            getattr(obj, 'Set' + key)(*value)
        else:
            getattr(obj, 'Set' + key)(value)

def get_pad_ylim_in_range(pad, x_min, x_max, y_min, y_max):
    pad_obs = pad.GetListOfPrimitives()
    if pad_obs is None:
        return 0., 0.
    h_max=-99999.
    h_min=+99999.
    for obj in pad_obs:
        if obj.InheritsFrom(ROOT.TH1.Class()) or obj.InheritsFrom(ROOT.THStack.Class()):
            if obj.InheritsFrom(ROOT.THStack.Class()):
                hobj = obj.GetStack().Last()
            else:
                hobj = obj
            if "tmp" in hobj.GetName():
                continue
            for j in range(1, hobj.GetNbinsX()+1):
                if (hobj.GetBinLowEdge(j) + hobj.GetBinWidth(j) < x_min or
                        hobj.GetBinLowEdge(j) > x_max):
                    continue
                if (hobj.GetBinContent(j) + hobj.GetBinError(j) > h_max):
                    h_max = hobj.GetBinContent(j) + hobj.GetBinError(j)
                if (hobj.GetBinContent(j) - hobj.GetBinError(j) < h_min):
                    h_min = hobj.GetBinContent(j) - hobj.GetBinError(j)
        elif obj.InheritsFrom(ROOT.TGraphAsymmErrors.Class()):
            gobj = obj
            n = gobj.GetN()
            for k in range(0, n):
                x = gobj.GetX()[k]
                y = gobj.GetY()[k]
                if x < x_min or x > x_max:
                    continue
                if (y + gobj.GetEYhigh()[k]) > h_max:
                    h_max = y + gobj.GetEYhigh()[k]
                if (y - gobj.GetEYlow()[k]) < h_min:
                    h_min = y - gobj.GetEYlow()[k]
        elif obj.InheritsFrom(ROOT.TGraphErrors.Class()):
            gobj = obj
            n = gobj.GetN()
            for k in range(0, n):
                x = gobj.GetX()[k]
                y = gobj.GetY()[k]
                if x < x_min or x > x_max:
                    continue
                if (y + gobj.GetEY()[k]) > h_max:
                    h_max = y + gobj.GetEY()[k]
                if (y - gobj.GetEY()[k]) < h_min:
                    h_min = y - gobj.GetEY()[k]
        elif obj.InheritsFrom(ROOT.TGraph.Class()):
            gobj = obj
            n = gobj.GetN()
            for k in range(0, n):
                x = gobj.GetX()[k]
                y = gobj.GetY()[k]
                if x < x_min or x > x_max:
                    continue
                if y > h_max:
                    h_max = y
                if y < h_min:
                    h_min = y
    return max(h_min, y_min), min(h_max, y_max)

def get_pad_ylim(pad, y_min=float('-inf'), y_max=float('inf')):
    pad_obs = pad.GetListOfPrimitives()
    if pad_obs is None:
        return 0, 1
    xmin = get_axis_hist(pad).GetXaxis().GetXmin()
    xmax = get_axis_hist(pad).GetXaxis().GetXmax()
    return get_pad_ylim_in_range(pad, xmin, xmax, y_min, y_max)

def parse_mass(dset_name, mass_re):
    mass_re = re.compile(mass_re)
    match = mass_re.search(dset_name)
    if match:
        return match.group(1)
    return "*"

def save_canvas(
    canvas,
    stack_pad,
    ratio_pad,
    stack,
    out_base,
    ymin,
    ymax,
    name,
    logx="both",
    logy="both",
    above_margin=0.25,
    **kwargs
):
    canvas.cd()
    if not logx:
        x_combinations = [0]
    elif logx=="both":
        x_combinations = [0,1]
    else:
        x_combinations = [1]
    if not logy:
        y_combinations = [0]
    elif logy=="both":
        y_combinations = [0,1]
    else:
        y_combinations = [1]

    logxy = product(x_combinations, y_combinations)
    ymin_plot = ymin

    for logx, logy in logxy:
        out = out_base
        if logx and logy:
            out= out_base / "logxy"
        elif logx:
            out= out_base / "logx"
        elif logy:
            out= out_base / "logy"
        else:
            out= out_base / "linear"
        out = out / name

        stack_pad.SetLogx(logx)
        ratio_pad.SetLogx(logx)
        stack_pad.SetLogy(logy)

        # FIXME: The commented method for selecting y minimum does not work
        # for unknown reasons
        # ymin_plot = ymin
        if logy:
            # if ymin <= 0:
            #     ymin_plot = 0.1
            logmin = np.log10(ymin_plot)
            logmax = np.log10(ymax)
            new_logrange = (logmax - logmin)/(1-above_margin)
            new_logmax = logmin + new_logrange
            newmax = np.power(10, new_logmax)

        else:
            newmax = ymin + (ymax-ymin)/(1-above_margin)
        stack.SetMaximum(newmax)
        stack.SetMinimum(ymin_plot)

        out.parent.mkdir(parents=True,exist_ok=True)
        canvas.SaveAs(str(out.with_suffix(".pdf")))
        canvas.SaveAs(str(out.with_suffix(".png")))
        canvas.SaveAs(str(out.with_suffix(".root")))
    return

def dict_to_stack(hist_dict, name=""):
    stack = ROOT.THStack("stack", name)
    prev = None
    for i, hname in enumerate(hist_dict):
        hist = hist_dict[hname]
        if hist is None:
            continue
        elif (hist.GetNbinsX() == 1):
            continue
        else:
            prev = hist
        hist.SetTitle(hname)
        hist.SetLineWidth(1)
        stack.Add(hist)
    return stack

def plot_stacked_with_signal(
        canvas,
        stack,
        signal,
        data,
        signame,
        xlabel="",
        ylabel="Events / bin",
        stack_area = (0.,0.,1.,1.),
        cms_extra="Internal",
        lumi_by_era=None,
        process_labels = {},
        **kwargs
    ):

    private_work = cms_extra.lower() == "private work"

    pad = ROOT.TPad('pad', 'pad', *stack_area)
    pad.Draw()
    canvas.cd()
    pad.cd()

    # Draw stacked background
    stack.SetTitle(signal.GetTitle())
    stack.Draw("hist pfc")
    xax = stack.GetXaxis()
    yax = stack.GetYaxis()

    # Axis titles
    xax.SetTitle(xlabel)
    xax.SetTitleSize(26)
    xax.SetTitleFont(43)
    yax.SetTitle(ylabel)
    yax.SetTitleSize(26)
    yax.SetTitleFont(43)
    yax.SetTitleOffset(1.8)

    # Axis labels
    xax.SetLabelFont(43)
    xax.SetLabelSize(20)
    yax.SetLabelFont(43)
    yax.SetLabelSize(20)

    # Draw signal
    signal.SetLineColor(ROOT.kRed - 7)
    signal.SetLineWidth(2)
    signal.SetLineStyle(2)
    signal.Draw("hist same")

    # Draw uncertainties
    if "bkg_stat_uncert" in kwargs:
        stat_uncert = kwargs["bkg_stat_uncert"]
        stat_uncert.SetFillColor(ROOT.kGray + 2)
        stat_uncert.SetFillStyle(3344)
        stat_uncert.SetLineWidth(0)
        stat_uncert.Draw("e2 same")
    if "bkg_total_uncert" in kwargs:
        total_uncert = kwargs["bkg_total_uncert"]
        total_uncert.SetFillColor(ROOT.kGray + 3)
        total_uncert.SetFillStyle(3354)
        total_uncert.SetLineWidth(0)
        total_uncert.Draw("e2 same")

    # Draw observations
    data.SetTitle("Data")
    data.SetMarkerStyle(20)
    data.SetLineColor(ROOT.kBlack)
    data.SetMarkerSize(1)
    data.Draw("PE same")

    # Add legend entries
    legend, _ = positioned_legend(
        0.6, 0.2, 3, 0.03, horizontaloffset=0.04,
        description_height=0, columns=2
    )
    legend.SetBorderSize(0)
    legend.SetFillStyle(0)
    legend.AddEntry(data, "Data", "PE")
    legend.AddEntry(signal, signame, "l")
    for hist in stack.GetStack():
        if hist is None:
            hist = 0
            legtype=""
            label = ""
        else:
            legtype="f"
            title = hist.GetTitle()
            label = process_labels[title] if title in process_labels else title
        legend.AddEntry(hist, label, legtype)
    if "bkg_stat_uncert" in kwargs:
        legend.AddEntry(stat_uncert, "bkg. stat.", "f")
    if "bkg_total_uncert" in kwargs:
        legend.AddEntry(total_uncert, "bkg. stat.#oplussyst.", "f")

    legend.Draw()
    extratext=cms_extra
    if not private_work:
        logotext = 'CMS'
        pos=10
    else:
        logotext = ''
        pos=0
    DrawCMSLogo(
        pad,
        logotext,
        extratext,
        pos,
        0.045,
        0.035,
        1.2,
        '',
        1,
    )
    if lumi_by_era is not None:
        CMS_lumi(pad, lumi_by_era)

    return pad, legend

def _createCoverPad(xmin=0.065, ymin=0.285, xmax=0.158, ymax=0.33):
    '''
    Creates a cover pad

    parameters:
        xmin:  X left coordinate
        ymin:  Y lower coordinate
        xmax:  X right coordinate
        ymax:  Y upper coordinate

    source: gitlab.cern.ch/HPlus/HiggsAnalysis/-/blob/master/NtupleAnalysis/python/tools/plots.py
    If distributions and data/MC ratios are plotted on the same TCanvas
    such that the lower X axis of distributions TPad and the upper X
    axis of the ratio TPad coincide, the Y axis labels of the two TPads
    go on top of each others and it may happen that the greatest Y axis
    value of the lower TPad is directly on top of the smallest Y axis
    value of the upper TPad.

    This function can be used to create a blank TPad which is drawn
    after the lower TPad Y axis and before the upper TPad Y axis. Then
    only the smallest Y axis value of the upper TPad is drawn.

    See plots.DataMCPlot.draw() and plots.ComparisonPlot.draw() for
    examples.

    returns: TPad
    '''
    coverPad = ROOT.TPad("coverpad", "coverpad", xmin, ymin, xmax, ymax)
    coverPad.SetBorderMode(0)
    return coverPad

def plot_ratio(
        canvas,
        data,
        bkg,
        pad_area=(0.,0.,1.,1.),
        ratio_ylabel="Data/Bkg.",
        xlabel="",
        blind_region=None,
        standard_ratiorange=False,
        **kwargs
    ):
    canvas.cd()
    pad = ROOT.TPad("ratio", "Ratio pad", *pad_area)
    pad.Draw()
    pad.cd()
    bkg_total = bkg.GetStack().Last()
    for b in range(1,bkg_total.GetNbinsX()+1):
        bkg_total.SetBinError(b,0.0)
    ratio = data.Clone("h_ratio")
    ratio.Divide(bkg_total)
    ratio.SetMarkerStyle(20)
    ratio.SetLineColor(ROOT.kBlack)
    ratio.SetMarkerSize(1)
    keepalives = [ratio]

    # The histogram x-axis limits in log mode are determined
    # differently for th1 compared to thstack
    # but we can match them by drawing a transparent stack here
    # sadly, this also means we must manually set the y limits...
    dummy_stack = ROOT.THStack("dummy stack","")
    dummy_hist = bkg_total.Clone("dummy h")
    dummy_hist.Divide(dummy_hist)
    dummy_hist.SetFillStyle(0)
    dummy_hist.SetLineColorAlpha(0,0)
    dummy_stack.Add(dummy_hist)
    dummy_stack.Draw("hist")
    xax = dummy_stack.GetXaxis()
    yax = dummy_stack.GetYaxis()
    # Axis titles
    xax.SetTitle(xlabel)
    xax.SetTitleSize(26)
    xax.SetTitleFont(43)
    yax.SetTitle(ratio_ylabel)
    yax.SetTitleSize(26)
    yax.SetTitleFont(43)
    yax.SetTitleOffset(1.8)

    # Axis labels
    xax.SetLabelFont(43)
    xax.SetLabelSize(20)
    yax.SetLabelFont(43)
    yax.SetLabelSize(20)
    keepalives.append(dummy_stack)
    # ymin = ratio.GetMinimum()
    # ymax = ratio.GetMaximum()

    if "bkg_total_uncert" in kwargs:
        total_uncert = kwargs["bkg_total_uncert"].Clone("total unc ratio")
        total_uncert.Divide(bkg_total)
        total_uncert.SetFillColor(ROOT.kGray + 2)
        total_uncert.SetMarkerStyle(0)
        total_uncert.SetFillStyle(1001)
        total_uncert.Draw("e2 same")
        # yminn, ymaxn = get_y_range_with_errors(total_uncert)
        # ymin = min(yminn, ymin)
        # ymax = max(ymaxn, ymax)
        keepalives.append(total_uncert)
    if "bkg_stat_uncert" in kwargs:
        stat_uncert = kwargs["bkg_stat_uncert"].Clone("stat unc ratio")
        stat_uncert.Divide(bkg_total)
        stat_uncert.SetFillColor(ROOT.kGray)
        stat_uncert.SetMarkerStyle(0)
        stat_uncert.SetFillStyle(1001)
        stat_uncert.Draw("e2 same")
        # yminn, ymaxn = get_y_range_with_errors(stat_uncert)
        # ymin = min(yminn, ymin)
        # ymax = max(ymaxn, ymax)
        keepalives.append(stat_uncert)

    if standard_ratiorange:
        ymin, ymax = 0,2
        dummy_stack.GetYaxis().SetNdivisions(504,ROOT.kFALSE)
    else:
        ymin, ymax = get_pad_ylim(pad)

    dummy_stack.SetMinimum(ymin)
    dummy_stack.SetMaximum(ymax)

    # draw observations on top of uncertainties
    ratio.Draw("PE same")

    # draw reference line at y=1
    xlim = array("d", (ratio.GetBinLowEdge(1), ratio.GetBinLowEdge(ratio.GetNbinsX()+1)))
    refline = ROOT.TGraph(2, xlim, array("d", (1.,1.)))
    refline.SetLineStyle(3)
    refline.SetLineWidth(2)
    refline.SetLineColor(ROOT.kRed)
    refline.Draw("L")
    keepalives.append(refline)

    # make legend
    legend, _ = positioned_legend(
        0.3, 0.2, 1, 0.0,
        horizontaloffset=0.02, description_height=0,
    )
    legend.SetBorderSize(0)
    legend.SetFillStyle(0)
    if "bkg_stat_uncert" in kwargs:
        legend.AddEntry(stat_uncert, "bkg. stat.", "f")
    if "bkg_total_uncert" in kwargs:
        legend.AddEntry(total_uncert, "bkg. stat.#oplussyst.", "f")
    legend.Draw()
    keepalives.append(legend)
    if blind_region is not None:
        keepalives.append(
            draw_text(
                0.55, 0.35,
                f"Data blinded: {blind_region[0]}-{blind_region[1]}",
                align="center", bold=True, size=0.1
            )
        )

    return pad, keepalives

def draw_text(x, y, text, size=None, bold=False, align="left", color=ROOT.kBlack, font=None):
    '''
    Draw text box in current pad

    parameter x       X coordinate of the text (in NDC)
    parameter y       Y coordinate of the text (in NDC)
    parameter text    String to draw
    parameter size    Size of text (None for the default value, taken from gStyle)
    parameter bold    Should the text be bold?
    parameter align   Alignment of text (left, center, right)
    parameter color   Color of the text
    parameter font    Specify font explicitly
    '''
    l = ROOT.TLatex()
    l.SetNDC()
    if not bold:
        l.SetTextFont(l.GetTextFont()-20) # bold -> normal
    if font is not None:
        l.SetTextFont(font)
    if size is not None:
        l.SetTextSize(size)
    if isinstance(align, str):
        if align.lower() == "left":
            l.SetTextAlign(11)
        elif align.lower() == "center":
            l.SetTextAlign(21)
        elif align.lower() == "right":
            l.SetTextAlign(31)
        else:
            raise Exception("Error: Invalid option '%s' for text alignment! Options are: 'left', 'center', 'right'."%align)
    else:
        l.SetTextAlign(align)
    l.SetTextColor(color)
    l.DrawLatex(x, y, text)
    return l

def plot_shapes(cb, out_dir, scale_width=False, **kwargs):
    bins = cb.bin_set()
    for b in bins:
        cbb = cb.cp().bin([b])
        bkg = cbb.cp().backgrounds()
        h_bkg = get_combine_histos(bkg, uncert=True)
        sig = cbb.cp().signals()
        h_sig = get_combine_histos(sig)
        h_data = cbb.GetObservedShape()
        bkg_tot, bkg_stat = get_bkg_uncert(bkg.deep(), **kwargs)
        if scale_width:
            h_data.Scale(1,option="width")
            bkg_tot.Scale(1,option="width")
            bkg_stat.Scale(1,option="width")
            for sig in h_sig.values():
                sig.Scale(1,option="width")
            for hist in h_bkg.values():
                hist.Scale(1,option="width")

        make_histplots(
            h_bkg,
            h_sig,
            h_data,
            out_dir / b,
            bkg_total_uncert = bkg_tot,
            bkg_stat_uncert = bkg_stat,
            **kwargs
        )
    return

def make_histplots(
    h_bkg,
    h_sig,
    h_data,
    output_dir,
    ymin=0.1,
    **kwargs
):

    rename_signal = ("signal label" in kwargs) and ("signal_mass_re" in kwargs)
    if rename_signal:
        def siglabel_func(histoname):
            mass = parse_mass(histoname, kwargs["signal_mass_re"])
            return kwargs["signal label"].replace("*", mass)
    plot_data = blind_datahist(h_data, **kwargs)
    top_area = (0.,0.3,1.,1.)
    bottom_area = (0.,0.,1.,0.3)

    #FIXME: Plot everything but the signal sample here once, to avoid recomputation
    bkg_stack = dict_to_stack(h_bkg, name="Stacked Backgrounds")
    for signame in h_sig:
        sig = h_sig[signame]
        if rename_signal:
            signame = siglabel_func(signame)
        canvas = ROOT.TCanvas("canvas", "Stacked Backgrounds and Signal", 600, 800)
        stack_pad, legend = plot_stacked_with_signal(
            canvas,
            bkg_stack,
            sig,
            plot_data,
            signame,
            stack_area = top_area,
            **kwargs
        )
        stack_pad.SetBottomMargin(0)
        ratio_pad, ratio_content = plot_ratio(
            canvas,
            plot_data,
            bkg_stack,
            pad_area = bottom_area,
            **kwargs
        )
        ratio_pad.SetTopMargin(0)
        ratio_pad.SetBottomMargin(0.3)
        ratio_pad.RedrawAxis() # make sure ticks are visible
        canvas.cd()
        cover = _createCoverPad()
        cover.Draw() # cover topmost part of ratio pad axis label
        canvas.cd(1)
        stack_pad.Pop() # ensure stack pad is drawn on top of the cover
        ymin, ymax = get_pad_ylim(stack_pad, y_min=ymin)
        save_canvas(
            canvas,
            stack_pad,
            ratio_pad,
            bkg_stack,
            output_dir,
            ymin,
            ymax,
            sig.GetTitle(),
            **kwargs
        )
        canvas.Close()
    print(f"saved plots to {output_dir}")
    return

def blind_datahist(data, blind_region=(-np.inf, np.inf), **kwargs):
    blinded = data.Clone("blinded data")
    if blind_region is not None:
        first_blinded = blinded.FindFixBin(blind_region[0])
        first_unblinded = blinded.FindFixBin(blind_region[1])
        for i in range(first_blinded, first_unblinded):
            blinded.SetBinContent(i, 0)
            blinded.SetBinError(i, 0)
    return blinded

def get_combine_histos(cb, uncert=False):
    histos = []
    names = []
    for p in cb.process_set():
        names.append(p)
        if uncert:
            histos.append(cb.cp().process([p]).GetShapeWithUncertainty())
        else:
            histos.append(cb.cp().process([p]).GetShape())
    totals = [h.Integral() for h in histos]
    order = np.argsort(totals)
    histos = [histos[i] for i in order]
    names = [names[i] for i in order]
    histos = {n: h for n, h in zip(names, histos)}
    return histos

def not_stats_uncert(syst):
    return not bool(re.search("_bin_", syst.name()))

def get_bkg_uncert(bkg_cb, hasBBlite=False, **kwargs):
    import CombineHarvester.CombineTools.ch as ch

    # AutoMCStats are not calculated by CombineHarvester
    # this means we need bin by bin variations to show
    # stat. uncertainty in plots
    if hasBBlite:
        bbb = ch.BinByBinFactory().SetAddThreshold(0)
        bbb.AddBinByBin(bkg_cb, bkg_cb)

    bkg_total_unc = bkg_cb.GetShapeWithUncertainty()
    bkg_stat = bkg_cb.cp()
    bkg_stat.FilterSysts(not_stats_uncert)
    bkg_stat_unc = bkg_stat.GetShapeWithUncertainty()
    return bkg_total_unc, bkg_stat_unc

