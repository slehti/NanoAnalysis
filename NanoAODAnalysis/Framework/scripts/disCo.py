"""
DESCRIPTION:

Run with the option -h or --help for information about the script and options

LAST USED:
python ANN/disCo.py --dataDir /data/aakoss/NNinputsFinal/ --saveDir ~/NN_results/ --gen_save_appendix --batchSize 2048 --optimizer adamw  --standardize --onlyBackground --reduce_on_plateau --use_cache --epochs 300 --shuffle --discoIdx -2 --lr_fac 0.98 --exclude "" --wjets_src HT

python ANN/disCo.py --dataDir /data/aakoss/NNinputs2017CorWeights/ --saveDir ~/NN_results/ --gen_save_appendix --batchSize 2048 --optimizer adamw  --standardize --onlyBackground --reduce_on_plateau --use_cache --epochs 300 --shuffle --discoIdx -2

python ANN/disCo.py --dataDir /data/aakoss/NNinputs161022/ --saveDir ~/NN_results/base_fixedSele171022  --standardize --onlyBackground --reduce_on_plateau --use_cache --epochs 300 --shuffle --dropout_rate 0 --discoIdx -2

python ANN/disCo.py --dataDir /data/aakoss/NNinputsNoBReq --saveDir /data/NN_results/base_031022 --standardize --onlyBackground --reduce_on_plateau --use_cache --epochs 1000

python ANN/disCo.py --dataDir /home/AapoKossi/NNinputsFinal/ --saveDir /home/AapoKossi/NN_results/regress_lam_d5_lam_r1000_epochs_5_DLogLoss0707 --k 2 --epochs 5 -v --standardize --learning_rate 3e-4 --activation "swish,swish,swish,swish,softplus" --reduce_on_plateau --regress_mass --lam_d 5 lam_r 1000 --use_cache

python ANN/disCo.py --dataDir ~/NNinputsFull/ --saveDir ~/NN_results/ --epochs 1000 --reduce_on_plateau --standardize --activation "swish,swish,swish,swish,swish,swish,softplus" --neurons 2048,1536,1024,512,256,128,2 --regress_mass --use_cache --lam_r 1000 --lam_d 5 --optimizer adamw --batchnorm False --batchSize 2048 --learning_rate 1e-3 -- "tau_pt,pt_miss,R_tau,btag_all,mt,dPhiTauNu,dPhiTauJets,dPhiNuJets,dPhiAllJets,Jets_pt,mtrue"
"""

# Base script from gitlab.cern.ch/Hplus/HiggsAnalysis keras neural networks disCo.py
# ================================================================================================
# Imports
# ================================================================================================
import os


import ROOT

# print("=== Importing KERAS")
# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '0'
#os.environ["TF_USE_LEGACY_KERAS"] = "1"
import tensorflow as tf
import keras
#from tensorflow.python import keras
# tf.debugging.disable_traceback_filtering()
# tf.config.run_functions_eagerly(True)
# tf.data.experimental.enable_debug_mode()

from tensorflow.python.platform import tf_logging as logging

# from keras.utils import io_utils
from tensorflow.python.keras import backend
from keras import ops, tree
from keras import backend as k_backend

# tf.config.threading.set_intra_op_parallelism_threads(1)
# tf.config.threading.set_inter_op_parallelism_threads(1)
import numpy as np
import array
import shutil

import json
import random as rn
from keras.layers import (
    Dense,
    Activation,
    BatchNormalization,
    Concatenate,
    Dropout,
)  # , Flatten, Input
import subprocess
from nanoanalysis.ML import plot
import nanoanalysis.ML.func as func
from nanoanalysis.ML.func import (
    Print,
    ss, ns, ts, hs, ls, es, cs
)
from nanoanalysis.tools import tdrstyle
from nanoanalysis.ML.jsonWriter import JsonWriter
import sys
import time
from datetime import datetime
import argparse
import getpass
import socket
from nanoanalysis.ML.Disco_tf import distance_corr
from nanoanalysis.ML import datasetUtils

from nanoanalysis.ML import DNN_model
from nanoanalysis.ML import QML_model

from nanoanalysis.ML.layers import accumulate_mass_max_min

MAX_VRAM = 24576 # for utilizing ~all~ memory on 24 GiB cards
gpus = tf.config.list_physical_devices('GPU')
for gpu in gpus:
  try:
    tf.config.set_logical_device_configuration(
        gpu,
        [tf.config.LogicalDeviceConfiguration(memory_limit=MAX_VRAM)]
    )
  except RuntimeError as e:
    # Virtual devices must be set before GPUs have been initialized
    print(e)

# ================================================================================================
# Class Definition
# ================================================================================================

@keras.saving.register_keras_serializable(package="NanoAnalysis")
class MetricWrapper(tf.keras.metrics.Metric):
    def __init__(self, lossfn, **kwargs):
        super().__init__(**kwargs)
        self.call = lossfn
        self.total = self.add_weight(name="total",initializer="zeros")
        self.count = self.add_weight(name="count",initializer="zeros")

    def update_state(self, y_true, y_pred, sample_weight=None):

        y_pred = tree.map_structure(
            lambda x: ops.convert_to_tensor(x, dtype=self.dtype), y_pred
        )
        y_true = tree.map_structure(
            lambda x: ops.convert_to_tensor(x, dtype=self.dtype), y_true
        )

        # Reduction is left for the loss function implementation
        # (necessary for distance correlation as a distribution level loss)
        self.total.assign_add(self.call(y_true, y_pred, sample_weight=sample_weight))
        self.count.assign_add(1)

    def result(self):
        return self.total / self.count

    def reset_state(self):
        self.total.assign(0)
        self.count.assign(0)

    def get_config(self):
        base_config = super().get_config()
        config = {
            "lossfn": keras.saving.serialize_keras_object(self.call),
        }
        return {**base_config, **config}

    @classmethod
    def from_config(cls, config):
        lossfn_cfg = config.pop("lossfn")
        lossfn = keras.saving.deserialize_keras_object(lossfn_cfg)
        return cls(lossfn, **config)


@keras.saving.register_keras_serializable(package="NanoAnalysis")
class LossFnWrapper(tf.keras.losses.Loss):
    def __init__(self, lossfn, **kwargs):
        super().__init__(reduction="sum_over_batch_size", **kwargs)
        self.call = lossfn

    def __call__(self, y_true, y_pred, sample_weight=None):

        y_pred = tree.map_structure(
            lambda x: ops.convert_to_tensor(x, dtype=self.dtype), y_pred
        )
        y_true = tree.map_structure(
            lambda x: ops.convert_to_tensor(x, dtype=self.dtype), y_true
        )

        # Reduction is left for the loss function implementation
        # (necessary for distance correlation as a distribution level loss)
        value = self.call(y_true, y_pred, sample_weight=sample_weight)
        return value

    def get_config(self):
        base_config = super().get_config()
        base_config.pop("reduction")
        config = {
            "lossfn": keras.saving.serialize_keras_object(self.call),
        }
        return {**base_config, **config}

    @classmethod
    def from_config(cls, config):
        lossfn_cfg = config.pop("lossfn")
        lossfn = keras.saving.deserialize_keras_object(lossfn_cfg)
        return cls(lossfn, **config)


# custom callback that reduces weight decay at the same ratio with learning rate.
@keras.saving.register_keras_serializable(package="NanoAnalysis")
class myReduceLRWDOnPlateau(tf.keras.callbacks.ReduceLROnPlateau):
    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        logs["lr"] = backend.get_value(self.model.optimizer.lr)
        current = logs.get(self.monitor)
        if current is None:
            logging.warning(
                "Learning rate reduction is conditioned on metric `%s` "
                "which is not available. Available metrics are: %s",
                self.monitor,
                ",".join(list(logs.keys())),
            )

        else:
            if self.in_cooldown():
                self.cooldown_counter -= 1
                self.wait = 0

            if self.monitor_op(current, self.best):
                self.best = current
                self.wait = 0
            elif not self.in_cooldown():
                self.wait += 1
                if self.wait >= self.patience:
                    old_lr = backend.get_value(self.model.optimizer.lr)
                    if old_lr > np.float32(self.min_lr):
                        new_lr = old_lr * self.factor
                        new_lr = max(new_lr, self.min_lr)
                        true_factor = new_lr / old_lr
                        new_wd = (
                            backend.get_value(self.model.optimizer.weight_decay)
                            * true_factor
                        )
                        backend.set_value(self.model.optimizer.lr, new_lr)
                        backend.set_value(self.model.optimizer.weight_decay, new_wd)
                        if self.verbose > 0:
                            pass
                            # io_utils.print_msg(
                            #     f'\nEpoch {epoch +1}: '
                            #     f'ReduceLROnPlateau reducing learning rate to {new_lr}.')
                        self.cooldown_counter = self.cooldown
                        self.wait = 0

    def get_config(self):
        return super().get_config()

    @classmethod
    def from_config(cls, config):
        return cls(**config)



# custom keras layer to drop input variables according to user defined model configuration
# DEPRECATED, do not use as this breaks tensorflow serving
class InputSelector(tf.keras.layers.Layer):
    def __init__(self, include, name="InputSelector", **kwargs):
        super().__init__(name=name, **kwargs)
        self.include = tf.constant(include)
        outs = tf.reduce_sum(tf.cast(self.include, tf.int64))
        self.outshape = (None, outs)

    def get_config(self):
        config = super().get_config()
        config.update(include=self.include.numpy().tolist())
        return config

    def call(self, x):
        y = tf.boolean_mask(x, self.include, axis=1)
        return tf.ensure_shape(y, self.outshape)

# custom binary accuracy metric to handle extra targets
@keras.saving.register_keras_serializable(package="NanoAnalysis")
class MyBinaryAccuracy(tf.keras.metrics.BinaryAccuracy):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_true = tf.expand_dims(y_true[..., 0], axis=-1)
        return super().update_state(y_true, y_pred, sample_weight=sample_weight)

    def get_config(self):
        return super().get_config()

    @classmethod
    def from_config(cls, config):
        return cls(**config)


# custom accuracy metric to handle extra targets
@keras.saving.register_keras_serializable(package="NanoAnalysis")
class MyMSE(tf.keras.metrics.MeanSquaredError):
    def __init__(self, log=False, predmin=0, **kwargs):
        super().__init__(**kwargs)
        self.log = log
        self.predmin = predmin

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_true = tf.expand_dims(y_true[..., 0], axis=-1)
        if self.log:
            y_true = tf.math.log(tf.math.log(y_true - self.predmin + 1) + 1)
        return super().update_state(y_true, y_pred, sample_weight=sample_weight)

    def get_config(self):
        return super().get_config()

    @classmethod
    def from_config(cls, config):
        return cls(**config)



# custom AUC metric to handle extra targets
@keras.saving.register_keras_serializable(package="NanoAnalysis")
class MyAUC(tf.keras.metrics.AUC):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_true = tf.expand_dims(y_true[..., 0], axis=-1)
        return super().update_state(y_true, y_pred, sample_weight=sample_weight)


    def get_config(self):
        return super().get_config()

    @classmethod
    def from_config(cls, config):
        return cls(**config)


# custom callback for model checkpointing and metric plotting during training
class ModelMonitoring(tf.keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.losses = []
        self.val_losses = []

    def on_epoch_end(self, epoch, logs=None):

        def ApplyStyle(h, ymin, ymax, color, lstyle=ROOT.kSolid):
            h.SetLineColor(color)
            h.SetLineWidth(2)
            h.SetLineStyle(lstyle)
            if lstyle != ROOT.kSolid:
                h.SetLineWidth(3)
            h.SetMaximum(ymax)
            h.SetMinimum(ymin)
            h.GetXaxis().SetTitle("# epoch")
            h.GetYaxis().SetTitle("Loss")
            return

        self.losses.append(logs.get("loss"))
        self.val_losses.append(logs.get("val_loss"))
        last_epoch = len(self.losses)

        if args.epochs < 50:
            return
        if last_epoch % (args.epochs / 40) != 0:
            return

        plot.CreateDir("%s/modelMonitoring" % args.saveDir)

        # Save the model
        modelName = "model_%sEpochs.h5" % (last_epoch)
        self.model.save(os.path.join("%s/modelMonitoring" % args.saveDir, modelName))

        trainLossList = self.losses
        valLossList = self.val_losses
        epochList = range(last_epoch)
        # jsonWr = JsonWriter(saveDir=".", verbose=args.verbose)

        ROOT.gStyle.SetOptStat(0)
        canvas = ROOT.TCanvas()
        canvas.cd()
        leg = plot.CreateLegend(0.6, 0.70, 0.9, 0.88)

        h_TrainLoss = ROOT.TGraph(
            len(epochList), array.array("d", epochList), array.array("d", trainLossList)
        )
        h_ValLoss = ROOT.TGraph(
            len(epochList), array.array("d", epochList), array.array("d", valLossList)
        )

        ymax = max(
            h_TrainLoss.GetHistogram().GetMaximum(),
            h_ValLoss.GetHistogram().GetMaximum(),
        )
        ymin = min(
            h_TrainLoss.GetHistogram().GetMinimum(),
            h_ValLoss.GetHistogram().GetMinimum(),
        )

        ApplyStyle(h_TrainLoss, 0, ymax * 1.1, ROOT.kGreen + 1)
        ApplyStyle(h_ValLoss, 0, ymax * 1.1, ROOT.kOrange + 3, ROOT.kDashed)

        h_TrainLoss.Draw("AC")
        h_ValLoss.Draw("C same")
        leg.AddEntry(h_TrainLoss, "Loss (train)", "l")
        leg.AddEntry(h_ValLoss, "Loss (val)", "l")

        leg.Draw()
        plot.SavePlot(
            canvas, "%s/modelMonitoring" % args.saveDir, "Loss_%sEpochs" % last_epoch
        )
        canvas.Close()

        return

def str2bool(vec):
    def is_trueish(in_str):
        return in_str.lower().strip() in ("yes", "true", "y", "1")
    return [is_trueish(s) for s in vec]

# keras learning rate schedule example, reduceLROnPlateau preferred over this, can be improved
def lr_scheduler(epoch, lr):
    if epoch < 50:  # keep initial learning rate for 100 epochs
        return lr
    else:
        return lr * tf.math.exp(-0.025)  # decrease exponentially


# https://towardsdatascience.com/custom-loss-function-in-tensorflow-2-0-d8fa35405e4e
# https://github.com/sol0invictus/Blog_stuff/blob/master/custom%20loss/high_level_keras.py
def disco_loss(y_true, y_pred, sample_weight=tf.constant(1.0)):
    def flatten_and_cast(x, type):
        return tf.cast(tf.reshape(x, (-1,)), type)

    # Split given labels to the target and the mass value needed for decorrelation
    y_pred = tf.convert_to_tensor(y_pred)
    dcPred = tf.reshape(y_pred, (-1,))
    sample_weights = flatten_and_cast(y_true[:, 2], y_pred.dtype) * sample_weight
    normed_weights = (
        sample_weights
        * tf.cast(tf.size(sample_weights), tf.float32)
        / tf.reduce_sum(sample_weights)
    )  # ensure weight normalization
    dcMass = flatten_and_cast(y_true[:, 1], y_pred.dtype)
    y_true = flatten_and_cast(y_true[:, 0], y_pred.dtype)

    custom_loss = distance_corr(dcMass, dcPred, normedweight=normed_weights, power=1)
    return custom_loss


def disco_loss_between_preds(y_true, y_pred, sample_weight=tf.constant(1.0)):
    def flatten_and_cast(x, type):
        return tf.cast(tf.reshape(x, (-1,)), type)

    # Split given labels to the target and the mass value needed for decorrelation
    var_1 = y_pred[..., 0]
    var_2 = y_pred[..., 1]
    dcPred = tf.reshape(var_1, (-1,))
    sample_weights = flatten_and_cast(y_true[:, 2], y_pred.dtype) * sample_weight
    dcMass = tf.reshape(var_2, (-1,))

    # The loss
    # crossentropy = tf.losses.binary_crossentropy(y_true, y_pred, label_smoothing=0.0)
    custom_loss = distance_corr(dcMass, dcPred, normedweight=sample_weights, power=1)
    return custom_loss


# wrapper for loss function when y_true contains extra variables
def get_lossfunc_extra_targets(tf_loss):
    @keras.saving.register_keras_serializable(name="custom_loss")
    def custom_loss(y_true, y_pred, sample_weight=None):
        y_true = tf.cast(tf.reshape(y_true[..., 0], (-1, 1)), y_pred.dtype)
        if sample_weight is not None:
            l = tf_loss(y_true, y_pred, sample_weight=sample_weight)
            l = tf.math.reduce_mean(l)
            return l
        l = tf_loss(y_true, y_pred)
        return tf.math.reduce_mean(l)

    return custom_loss


# wrapper for custom loss function for the mass regression head
def get_regression_loss(tf_loss, args, min, max):
    if "squared" in tf_loss.name:
        power = 2
    elif "percentage" in tf_loss.name.lower():
        power = 0
    else:
        power = 1

    scale = tf.cast(
        args.lam_r
        / tf.math.pow(tf.math.log(tf.math.log(max - args.masspred_min + 1) + 1), power),
        tf.float32,
    )

    # the loss function
    def mse_loss(y_true, y_pred):
        log_mass_pred = y_pred
        log_mass_true = tf.expand_dims(
            tf.math.log(tf.math.log(y_true[..., 0] - args.masspred_min + 1) + 1),
            axis=-1,
        )
        sample_weight = tf.expand_dims(y_true[..., 1], axis=-1)
        return (
            tf_loss(log_mass_true, log_mass_pred, sample_weight=sample_weight) * scale
        ) / args.batchSize

    return mse_loss


# wrapper for distance correlation metric
def get_disco(lam_d):

    @tf.function(experimental_relax_shapes=True)
    def DisCo_metric(y_true, y_pred, sample_weights=tf.constant(1.0)):
        y_pred = tf.convert_to_tensor(y_pred)
        mass = tf.cast(tf.reshape(y_true[:, 1], (-1, 1)), y_pred.dtype)
        y_true = tf.cast(tf.reshape(y_true[:, 0], (-1, 1)), y_pred.dtype)

        # Type casting and reshaping
        numEntries = tf.cast(tf.size(y_true), dtype=tf.float32)
        numSignalEntries = tf.cast(tf.reduce_sum(y_true), dtype=tf.float32)
        bitMaskForBkg = 1 - y_true

        # Calculate the weights only for background, while setting signal weights to zero (we're only
        # interested in decorrelating the background from mass, signal doesn't matter)
        # Note that distance_corr function expects the weights to be normalized to the number of events
        weightFactor = sample_weights * numEntries / (numEntries - numSignalEntries)
        weights = tf.multiply(weightFactor, bitMaskForBkg)

        dcPred = tf.reshape(y_pred, [tf.size(y_pred)])
        dcMass = tf.reshape(mass, [tf.size(mass)])
        weights = tf.cast(tf.reshape(weights, [tf.size(weights)]), y_pred.dtype)

        # return distance_corr(dcMass, dcPred, normedweight=weights, power=1)
        return distance_corr(dcMass, dcPred, normedweight=weights, power=1) * lam_d

    return DisCo_metric


def PrintFlushed(msg, printHeader=True):
    """
    Useful when printing progress in a loop
    """
    msg = "\r\t" + msg
    ERASE_LINE = "\x1b[2K"
    if printHeader:
        print("=== aux.py")
    sys.stdout.write(ERASE_LINE)
    sys.stdout.write(msg)
    sys.stdout.flush()
    return


def Verbose(msg, printHeader=True, verbose=False):
    if not args.verbose:
        return
    Print(msg, printHeader)
    return


def PrintXYWTestTrain(
    x_tr, x_val, x_te, y_tr, y_val, y_te, w_tr, w_val, w_te, nEntries, verbose=False
):
    if not verbose:
        return
    table = []
    align = "{:>10} {:>10} {:>10} {:>10} {:>10} {:>10} {:>10} {:>10} {:>10} {:>10}"
    table.append("=" * 110)
    title = align.format(
        "index",
        "X_train",
        "X_val",
        "X_test",
        "Y_train",
        "Y_val",
        "Y_test",
        "W_train",
        "W_val",
        "W_test",
    )
    table.append(title)
    table.append("=" * 110)
    # For-loop: All entries
    # for i,x in enumerate(x_val, 0):
    n = min(len(x_tr), len(x_val), len(x_te))
    for i in range(n):
        msg = align.format(
            "%d" % i,
            "%.2f" % x_tr[i],
            "%.2f" % x_val[i],
            "%.2f" % x_te[i],
            "%.2f" % y_tr[i],
            "%.2f" % y_val[i],
            "%.2f" % y_te[i],
            "%.2f" % w_tr[i],
            "%.2f" % w_val[i],
            "%.2f" % w_te[i],
        )
        if i < (nEntries / 2):
            table.append(ss + msg + ns)
        else:
            table.append(es + msg + ns)
    table.append("=" * 100)
    for i, r in enumerate(table, 0):
        Print(r, i == 0)
    return


def PrintNetworkSummary(args):
    table = []
    msgAlign = "{:^10} {:^10} {:>12} {:>10}"
    title = msgAlign.format("Layer #", "Neurons", "Activation", "Type")
    hLine = "=" * len(title)
    table.append(hLine)
    table.append(title)
    table.append(hLine)
    for i, n in enumerate(args.neurons, 0):
        layerType = "unknown"
        if i == 0:
            layerType = "hidden"
        elif i + 1 == len(args.neurons):
            layerType = "output"
        else:
            layerType = "hidden"
        table.append(
            msgAlign.format(i + 1, args.neurons[i], args.activation[i], layerType)
        )
    table.append("")

    Print("Will construct a DNN with the following architecture", True)
    for r in table:
        Print(r, False)
    return


def GetModelWeightsAndBiases(nInputs, neuronsList):
    """
    https://keras.io/models/about-keras-models/

    returns a dictionary containing the configuration of the model.
    The model can be reinstantiated from its config via:
    model = Model.from_config(config)
    """
    nParamsT = 0
    nBiasT = 0
    nWeightsT = 0

    for i, n in enumerate(neuronsList, 0):
        nParams = 0
        nBias = neuronsList[i]
        if i == 0:
            nWeights = nInputs * neuronsList[i]
        else:
            nWeights = neuronsList[i - 1] * neuronsList[i]
        nParams += nBias + nWeights

        nParamsT += nParams
        nWeightsT += nWeights
        nBiasT += nBias
    return nParamsT, nWeightsT, nBiasT


def GetModelParams(model):
    """
    https://stackoverflow.com/questions/45046525/how-can-i-get-the-number-of-trainable-parameters-of-a-model-in-keras
    """
    # For older keras versions check the link above
    trainable_count = np.sum(
        [tf.keras.backend.count_params(w) for w in model.trainable_weights]
    )
    non_trainable_count = np.sum(
        [tf.keras.backend.count_params(w) for w in model.non_trainable_weights]
    )

    total_count = trainable_count + non_trainable_count
    return total_count, trainable_count, non_trainable_count


def GetModelConfiguration(myModel, verbose):
    """
    NOTE: For some older releases of Keras the
    Model.get_config() method returns a list instead of a dictionary.
    This is fixed in newer releases. See details here:
    https://github.com/keras-team/keras/pull/11133
    """

    config = myModel.get_config()
    for i, c in enumerate(config, 1):
        Verbose("%d) %s " % (i, c), True)
    return config


def GetKwargs(var, standardise=False):

    kwargs = {
        "normalizeToOne": True,
        "xTitle": "DNN output",
        "yTitle": "a.u.",
        "xMin": 0.0,
        "xMax": +1.0,
        "nBins": 200,
        "log": True,
    }

    if "mass" in var.lower():
        kwargs["normalizeToOne"] = True
        kwargs["nBins"] = 50
        kwargs["xMin"] = 0.0
        kwargs["xMax"] = 3100
        kwargs["yMin"] = 0.01
        kwargs["yMax"] = 1.0
        kwargs["xTitle"] = "DNN mass prediction"
        kwargs["yTitle"] = "a.u."
        kwargs["log"] = True
        kwargs["xBins"] = datasetUtils.MT_BINS
        if var.lower().split("_")[0] == "output":
            kwargs["legHeader"] = "all data"
        if var.lower().split("_")[0] == "outputpred":
            kwargs["legHeader"] = "all data"
            kwargs["legEntries"] = ["train", "test"]
        if var.lower().split("_")[0] == "outputtrain":
            kwargs["legHeader"] = "training data"
        if var.lower().split("_")[0] == "outputtest":
            kwargs["legHeader"] = "test data"
        return kwargs

    if "output" in var.lower() or var.lower() == "output":
        kwargs["normalizeToOne"] = True
        kwargs["nBins"] = 50
        kwargs["xMin"] = 0.0
        kwargs["xMax"] = 1.0
        kwargs["yMin"] = 1e-5
        kwargs["yMax"] = 2.0
        kwargs["xTitle"] = "DNN output"
        kwargs["yTitle"] = "a.u."
        kwargs["log"] = True
        if var.lower() == "output":
            kwargs["legHeader"] = "all data"
        if var.lower() == "outputpred":
            kwargs["legHeader"] = "all data"
            kwargs["legEntries"] = ["train", "test"]
        if var.lower() == "outputtrain":
            kwargs["legHeader"] = "training data"
        if var.lower() == "outputtest":
            kwargs["legHeader"] = "test data"
        return kwargs

    if "phi" in var.lower():
        kwargs["normalizeToOne"] = False
        kwargs["xMin"] = 0.0
        kwargs["xMax"] = 1.0
        kwargs["nBins"] = 200
        kwargs["xTitle"] = "DNN output"
        kwargs["yTitle"] = "efficiency"  # (\varepsilon)"
        kwargs["yMin"] = 0.0
        kwargs["log"] = False

    if "efficiency" in var:
        kwargs["normalizeToOne"] = False
        kwargs["xMin"] = 0.0
        kwargs["xMax"] = 1.0
        kwargs["nBins"] = 200
        kwargs["xTitle"] = "DNN output"
        kwargs["yTitle"] = "efficiency"  # (\varepsilon)"
        kwargs["yMin"] = 0.0
        kwargs["log"] = False

    if "roc" in var:
        kwargs["normalizeToOne"] = False
        kwargs["xMin"] = 0.0
        kwargs["xMax"] = 1.0
        kwargs["nBins"] = 200
        kwargs["xTitle"] = "signal efficiency"
        kwargs["yTitle"] = "background efficiency"
        kwargs["yMin"] = 1e-4
        kwargs["yMax"] = 10
        kwargs["log"] = True

    if var == "significance":
        kwargs["normalizeToOne"] = False
        kwargs["xMin"] = 0.0
        kwargs["xMax"] = 1.0
        kwargs["nBins"] = 200
        kwargs["xTitle"] = "DNN output"
        kwargs["yTitle"] = "significance"
        kwargs["yMin"] = 0.0
        kwargs["log"] = False

    if var == "loss":
        kwargs["normalizeToOne"] = False
        kwargs["xMin"] = 0.0
        kwargs["xMax"] = args.fold_epochs
        kwargs["nBins"] = args.fold_epochs
        kwargs["xTitle"] = "epoch"
        kwargs["yTitle"] = "loss"
        kwargs["yMin"] = 0.0
        kwargs["log"] = False

    if var == "acc":
        kwargs["normalizeToOne"] = False
        kwargs["xMin"] = 0.0
        kwargs["xMax"] = args.fold_epochs
        kwargs["nBins"] = args.fold_epochs
        kwargs["xTitle"] = "epoch"
        kwargs["yTitle"] = "accuracy"
        kwargs["yMin"] = 0.0
        kwargs["log"] = False

    if var == "auc":
        kwargs["normalizeToOne"] = False
        kwargs["xMin"] = 0.0
        kwargs["xMax"] = args.fold_epochs
        kwargs["nBins"] = args.fold_epochs
        kwargs["xTitle"] = "epoch"
        kwargs["yTitle"] = "AUC"
        kwargs["yMin"] = 0.0
        kwargs["log"] = False

    if standardise:
        kwargs["xMin"] = -5.0
        kwargs["xMax"] = +5.0
        kwargs["nBins"] = 500  # 1000

    return kwargs


def GetTime(tStart):
    tFinish = time.time()
    dt = int(tFinish) - int(tStart)
    days = divmod(dt, 86400)  # days
    hours = divmod(days[1], 3600)  # hours
    mins = divmod(hours[1], 60)  # minutes
    secs = mins[1]  # seconds
    return days, hours, mins, secs


def SaveModelParameters(myModel, nInputs):
    nParams = myModel.count_params()
    total_count, trainable_count, non_trainable_count = GetModelParams(myModel)
    nParams, nWeights, nBias = GetModelWeightsAndBiases(nInputs, args.neurons)
    args.modelParams = total_count
    args.modelParamsTrain = trainable_count
    args.modelParamsNonTrainable = non_trainable_count
    args.modelWeights = nWeights
    args.modelBiases = nBias

    ind = "{:<6} {:<30}"
    msg = "The model has a total of %s%d parameters%s (neuron weights and biases):" % (
        hs,
        args.modelParams,
        ns,
    )
    msg += "\n\t" + ind.format("%d" % args.modelWeights, "Weights")
    msg += "\n\t" + ind.format("%d" % args.modelBiases, "Biases")
    msg += "\n\t" + ind.format("%d" % args.modelParamsTrain, "Trainable")
    msg += "\n\t" + ind.format("%d" % args.modelParamsNonTrainable, "Non-trainable")
    Print(msg, True)
    return


def writeCfgFile(args):
    # Write to json file
    jsonWr = JsonWriter(saveDir=args.saveDir, verbose=args.verbose)
    #jsonWr.addParameter("keras version", args.keras)
    jsonWr.addParameter("host name", args.hostname)
    jsonWr.addParameter("python version", args.python)
    jsonWr.addParameter("model", "disCo")
    jsonWr.addParameter("model parameters (total)", args.modelParams)
    jsonWr.addParameter("model parameters (trainable)", args.modelParamsTrain)
    jsonWr.addParameter(
        "model parameters (non-trainable)", args.modelParamsNonTrainable
    )
    jsonWr.addParameter("model weights", args.modelWeights)
    jsonWr.addParameter("model biases", args.modelBiases)
    jsonWr.addParameter("planing", args.planing)
    jsonWr.addParameter("comput disco only with bkg", args.onlyBackground)
    jsonWr.addParameter("rndSeed", args.rndSeed)
    jsonWr.addParameter("layers", len(args.neurons))
    jsonWr.addParameter("hidden layers", len(args.neurons) - 2)
    jsonWr.addParameter("activation functions", [a for a in args.activation])
    jsonWr.addParameter("neurons", [n for n in args.neurons])
    jsonWr.addParameter("loss function", args.lossFunction)
    jsonWr.addParameter("optimizer", args.optimizer)
    jsonWr.addParameter("epochs", args.epochs)
    jsonWr.addParameter("batch size", args.batchSize)
    jsonWr.addParameter("lambda_d", args.lam_d)
    jsonWr.addParameter("lambda_r", args.lam_r)
    jsonWr.addParameter("elapsed time", args.elapsedTime)
    for i, b in enumerate(args.input_features, 1):
        jsonWr.addParameter("var%d" % i, b)
    jsonWr.write(args.cfgJSON)
    return


def writeGitFile(args):
    # Write to json file
    path = os.path.join(args.saveDir, "gitBranch.txt")
    gFile = open(path, "w")
    gFile.write(str(args.gitBranch))

    path = os.path.join(args.saveDir, "gitStatus.txt")
    gFile = open(path, "w")
    gFile.write(str(args.gitStatus))

    path = os.path.join(args.saveDir, "gitDiff.txt")
    gFile = open(path, "w")
    gFile.write(str(args.gitDiff))
    return


def PrintXYW(X, Y, W, W_disCo, nEntries, verbose=False):
    if not verbose:
        return

    x = X[0:nEntries, args.input_features.index("mt")].tolist()
    y = Y[0:nEntries, 0].tolist()
    w = W[0:nEntries].tolist()
    wd = W_disCo[0:nEntries].tolist()

    table = []
    align = "{:>10} {:>10} {:>10} {:>10} {:>10}"
    table.append("=" * 100)
    title = align.format("index", "X", "Y", "W", "W DisCo")
    table.append(title)
    table.append("=" * 100)
    # For-loop: All entries
    for i, xx in enumerate(x, 0):
        msg = align.format(
            "%d" % i, "%.2f" % x[i], "%.2f" % y[i], "%.2f" % w[i], "%.2f" % wd[i]
        )
        if i < (nEntries / 2):
            table.append(ss + msg + ns)
        else:
            table.append(es + msg + ns)
    table.append("=" * 100)
    for i, r in enumerate(table, 0):
        Print(r, i == 0)
    return


def add_extra_targets(dataset, feature_names, args):
    """
    This function takes care of dataset threading options and
    transforming the dataset to fit the given nn algorithm
    (parametrization, disCo loss and/or mass regression)
    """
    mass_idx = feature_names.index("m_true")
    disco_idx = None
    if args.disco_var is not None:
        disco_idx = feature_names.index(args.disco_var)

    options = tf.data.Options()
    options.threading.max_intra_op_parallelism = 16
    options.threading.private_threadpool_size = 16

    # IGNORE ALL NEGATIVE WEIGHTS!
    dataset = dataset.map(lambda x,y,w : (x,y,tf.math.abs(w)))

    dataset = dataset.map(lambda X, Y, W: (X, tf.cast(Y, tf.float32), W)).with_options(
        options
    )

    # add variable to decorrelate with to
    if disco_idx is not None:
        dataset = dataset.map(
            lambda X, Y, W: (X, tf.stack((Y, X[disco_idx]), axis=-1), W)
        )
    else:
        dataset = dataset.map(
            lambda X, Y, W: (X, tf.stack((Y, tf.zeros_like(Y)), axis=-1), W)
        )

    # add sample weight to classification targets
    if args.onlyBackground:
        dataset = dataset.map(
            lambda X, Y, W: (X, tf.concat((Y, [(1 - Y[0]) * 2]), axis=-1), W)
        )
    else:
        dataset = dataset.map(
            lambda X, Y, W: (X, tf.concat((Y, [tf.ones_like(W)]), axis=-1), W)
        )

    # save the mass parameter as an extra output for plotting even if it's not used in training
    if not args.param_mass and not args.regress_mass:
        return dataset.map(
            lambda X, Y, W: (
                X,
                {
                    "class_out": tf.concat(
                        (Y, X[mass_idx : mass_idx + 1]), axis=-1
                    )
                },
                W,
            )
        )
    elif args.param_mass:
        return dataset.map(lambda X, Y, W: (X, {"class_out": Y}, W))
    elif (
        args.regress_mass
    ):  # add true mass as targets and set bg weights for regression to 0
        dataset = dataset.map(
            lambda X, Y, W: (
                X,
                {
                    "class_out": Y,
                    "mass_out": tf.stack((X[mass_idx], Y[0] * 2), axis=-1),
                    "combined": Y,
                },
                W,
            )
        )
        return dataset

# optionally drop input features from the dataset, this function replaces the deprecated InputSelector layer
def use_features(dataset, ds_names, used_names):
    indices = [ds_names.index(used) for used in used_names]
    idx = tf.constant(indices, dtype=tf.int64)

    def select(x, y, w):
        return tf.gather(x, idx, axis=-1), y, w

    return dataset.map(select)

# split a dataset into training and validation portions
def validation_split(dataset, split, n_elems):
    n_train = tf.cast((1 - split) * tf.cast(n_elems, tf.float32), tf.int64)
    train_ds = dataset.take(n_train)
    val_ds = dataset.skip(n_train)
    return train_ds, val_ds


# prepare dataset for iteration: caching, shuffling, batching
def finalize(ds, ds_feats, args, cachepath, testset=False, shuffle=False):
    ds = ds.batch(args.batchSize)
    # ds = use_features(ds, ds_feats, args.input_features)
    if testset:
        fridx = [s for s in args.input_features if s != "_"].index("mt")
        toidx = args.mass_index

        def save_mtrue_aslast(x, y, w):
            return x, y, w, x[..., toidx]

        def copy_mt2mtrue(x, y, w, m):
            update = x[..., fridx : fridx + 1]
            x = tf.concat([x[..., :toidx], update, x[..., toidx + 1 :]], axis=-1)
            return x, y, w, m

        ds = ds.map(save_mtrue_aslast).map(copy_mt2mtrue)
    if cachepath is not None:
        ds = ds.cache(cachepath)

    ds = ds.unbatch()
    if shuffle:
        ds = ds.shuffle(3 * args.batchSize, seed=args.rndSeed)
    ds = ds.batch(args.batchSize)
    return ds.prefetch(tf.data.AUTOTUNE)


# combine 2 datasets (signal and background) by interleaving events from each one in turn.
# Optionally transfer parametrized mass from signal events to background
def interleave_2(datasets, args):
    n = 2
    assert len(datasets) == n
    choice_ds = tf.data.Dataset.range(n).repeat()
    if (not args.copy_mass) or (not args.param_mass):
        return tf.data.Dataset.choose_from_datasets(
            datasets, choice_ds, stop_on_empty_dataset=True
        )
    else:
        fridx = (0, args.mass_index)
        toidx = (1, args.mass_index)

        def copy_m_1to2(x, y, w):
            update = x[fridx] - x[toidx]
            x = tf.tensor_scatter_nd_add(x, [toidx], [update])
            return x, y, w

        interleaved = tf.data.Dataset.choose_from_datasets(
            datasets, choice_ds, stop_on_empty_dataset=True
        )
        return interleaved.batch(2, drop_remainder=True).map(copy_m_1to2).unbatch()


def count_elems_list(datasets):
    counts = []
    for ds in datasets:
        counts.append(datasetUtils.get_ds_len(ds))
    return counts



def main(args):
    # Save start time (epoch seconds)
    tStart = time.time()
    Verbose("Started @ " + str(tStart), True)

    # Do not display canvases & disable screen info
    ROOT.gROOT.SetBatch(ROOT.kTRUE)
    ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 1001;")

    # Setup the style
    style = tdrstyle.TDRStyle()
    style.setOptStat(False)
    style.setGridX(args.gridX)
    style.setGridY(args.gridY)

    # Setting the seed for numpy-generated random numbers
    np.random.seed(args.rndSeed)
    # Setting the seed for python random numbers
    rn.seed(args.rndSeed)
    # Setting tensorflow random seed
    tf.random.set_seed(args.rndSeed)

    # For future use
    jsonWr = JsonWriter(saveDir=args.saveDir, verbose=args.verbose)

    # set default batch size
    if args.batchSize == None:
        args.batchSize = 1024

    # load training data
    datasets_train, ds_feats = datasetUtils.load(
        args.datadir,
        args.k,
        disco_var=args.disco_var,
        planing=args.planing,
        marginalized=args.marginalized_planing,
        energy=args.energy,
        exclude=args.exclude,
        wjets_src=args.wjets_src,
    )

    sig_datasets_train = [sig for sig, bg in datasets_train]
    sig_datasets_train = [add_extra_targets(ds, ds_feats, args) for ds in sig_datasets_train]
    sig_datasets_train = [use_features(ds, ds_feats, args.input_features) for ds in sig_datasets_train]
    bg_datasets_train = [bg for sig, bg in datasets_train]
    bg_datasets_train = [add_extra_targets(ds, ds_feats, args) for ds in bg_datasets_train]
    bg_datasets_train = [use_features(ds, ds_feats, args.input_features) for ds in bg_datasets_train]

    datasets_train = [interleave_2(datasets, args) for datasets in datasets_train]
    datasets_train = [add_extra_targets(ds, ds_feats, args) for ds in datasets_train]
    datasets_train = [use_features(ds, ds_feats, args.input_features) for ds in datasets_train]

    # print(datasets_train[0].element_spec)
    # sys.exit()
    # load test data
    datasets_test, _ = datasetUtils.load(
        args.datadir,
        args.k,
        disco_var=args.disco_var,
        testset=True,
        planing=args.planing,
        marginalized=args.marginalized_planing,
        energy=args.energy,
        exclude=args.exclude,
        wjets_src=args.wjets_src,
    )

    sig_datasets_test = [sig for sig, bg in datasets_test]
    sig_datasets_test = [add_extra_targets(ds, ds_feats, args) for ds in sig_datasets_test]
    sig_datasets_test = [use_features(ds, ds_feats, args.input_features) for ds in sig_datasets_train]
    bg_datasets_test = [bg for sig, bg in datasets_test]
    bg_datasets_test = [add_extra_targets(ds, ds_feats, args) for ds in bg_datasets_test]
    bg_datasets_test = [use_features(ds, ds_feats, args.input_features) for ds in bg_datasets_train]

    datasets_test = [interleave_2(datasets, args) for datasets in datasets_test]
    datasets_test = [add_extra_targets(ds, ds_feats, args) for ds in datasets_test]
    datasets_test = [use_features(ds, ds_feats, args.input_features) for ds in datasets_test]

    # get datasets separated to signal, background for later plotting
    sig_datasets_all = list(
        map(
            lambda datasets: datasets[0]
            .concatenate(datasets[1])
            .filter(lambda *X: X[1]["class_out"][0] == 1),
            zip(datasets_train, datasets_test),
        )
    )
    # sig_datasets_all = [
    #     use_features(
    #         ds.batch(args.batchSize), ds_feats, args.input_features
    #     ).unbatch()
    #     for ds in sig_datasets_all
    # ]

    bg_datasets_all = list(
        map(
            lambda datasets: datasets[0]
            .concatenate(datasets[1])
            .filter(lambda *X: X[1]["class_out"][0] == 0),
            zip(datasets_train, datasets_test),
        )
    )
    # bg_datasets_all = [
    #     use_features(
    #         ds.batch(args.batchSize), ds_feats, args.input_features
    #     ).unbatch()
    #     for ds in bg_datasets_all
    # ]

    # get dataset sizes for train/validation split
    if not os.path.exists(args.saveDir + "/train_cache"):
        n_train_plus_val = count_elems_list(datasets_train)
        print(
            f"total number of elements in training + validation datasets in different folds:\n{n_train_plus_val}"
        )
    else:  # datasets will be loaded from cache so there is no need to count elements
        n_train_plus_val = 100000

    # perform train-validation split for each fold
    datasets_train, datasets_val = zip(
        *map(
            lambda x: validation_split(x[0], args.val_split, x[1]),
            zip(datasets_train, n_train_plus_val),
        )
    )

    # decide whether to train all folds or a specific one
    if args.worker == None:
        folds_to_train = range(args.k)  # train all k models
    else:
        folds_to_train = range(
            args.worker, args.worker + 1
        )  # train only the model corresponding to worker

    args.parentDir = args.saveDir
    for n_fold in folds_to_train:
        args.saveDir = (
            args.parentDir + f"/fold_{n_fold}/"
        )  # save results of each model into their own directories

        # make subdirectories if they don't already exist
        if not os.path.exists(args.saveDir):
            os.mkdir(args.saveDir)
        if not os.path.exists(args.saveDir + "train_cache"):
            os.mkdir(args.saveDir + "train_cache")
        if not os.path.exists(args.saveDir + "val_cache"):
            os.mkdir(args.saveDir + "val_cache")
        if not os.path.exists(args.saveDir + "test_cache"):
            os.mkdir(args.saveDir + "test_cache")

        # finalize datasets
        cachedirs = [
            args.saveDir + var + "_cache/" if args.use_cache else None
            for var in ["train", "val", "test"]
        ]
        fold_ds = finalize(
            datasets_train[n_fold],
            ds_feats,
            args,
            cachedirs[0],
            shuffle=args.shuffle,
        )
        fold_val_ds = finalize(datasets_val[n_fold], ds_feats, args, cachedirs[1])
        fold_test_ds = finalize(
            datasets_test[n_fold], ds_feats, args, cachedirs[2], testset=args.param_mass
        )

        nInputs = fold_ds.element_spec[0].shape[-1]

        Verbose("Creating the Keras model", True, args.verbose)
        strategy = tf.distribute.MirroredStrategy()
        n_replicas = strategy.num_replicas_in_sync
        loss_scale = 1/n_replicas
        with strategy.scope():

            myModel = args.model_initializer(fold_ds, args)

            # add losses to model
            if args.disco_var is not None:  # add distance correlation term to loss
                Print(
                    f"Adding distance correlation loss to input {args.disco_var}", True
                )
                lam_d = args.lam_d
                tf_loss = args.tf_loss_func
                @keras.saving.register_keras_serializable(package="NanoAnalysis")
                def lossFunc_(y_true, y_pred, sample_weight=tf.constant(1.0)):
                    return loss_scale * (
                        get_lossfunc_extra_targets(tf_loss)(
                            y_true, y_pred, sample_weight=sample_weight
                        )
                        + lam_d * disco_loss(y_true, y_pred, sample_weight=sample_weight)
                    )
            else:
                @keras.saving.register_keras_serializable(package="NanoAnalysis")
                def lossFunc_(y_true, y_pred, sample_weight=tf.constant(1.0)):
                    return loss_scale * (
                        get_lossfunc_extra_targets(args.tf_loss_func)(y_true, y_pred, sample_weight)
                    )
            if args.regress_mass:  # add loss for the mass regression term
                if args.mass_loss == "MSE":
                    loss_instance = keras.losses.MeanSquaredError(reduction=tf.keras.losses.Reduction.NONE)
                elif args.mass_loss == "MAPE":
                    loss_instance = keras.losses.MeanAbsolutePercentageError(reduction=tf.keras.losses.Reduction.NONE)
                else:
                    raise Exception(
                        "unknown mass regression loss function " + args.mass_loss
                    )
                mass_loss = get_regression_loss(loss_instance, args, mass_min, mass_max)
                corr_loss = lambda y_true, y_pred, sample_weight=tf.constant(
                    1.0
                ): args.lam_d / n_replicas * disco_loss_between_preds(
                    y_true, y_pred, sample_weight=sample_weight
                )
                lossFunc = {
                    "class_out": LossFnWrapper(lossFunc_),
                    "mass_out": LossFnWrapper(mass_loss),
                    "combined": LossFnWrapper(corr_loss),
                }
            else:
                lossFunc = {"class_out": LossFnWrapper(lossFunc_)}

            # add metrics
            metrics = {
                "class_out": [
                    MyBinaryAccuracy(),
                    MyAUC(name="roc_AUC"),
                    MetricWrapper(get_lossfunc_extra_targets(args.tf_loss_func), name=args.lossFunction),
                ]
            }
            if args.disco_var is not None:
                metrics["class_out"].append(MetricWrapper(get_disco(args.lam_d), name="disco"))

            if args.regress_mass:
                metrics["mass_out"] = [
                    MyMSE(log=True),
                    # get_regression_loss(mse_instance, args, mass_min, mass_max)
                ]
                metrics["combined"] = [
                    # lambda y_true, y_pred, sample_weight=tf.constant(1.): args.lam_d * disco_loss_between_preds(y_true, y_pred, sample_weight=sample_weight)
                ]

            # Compile the model
            Print(
                "Compiling the model with the loss function %s and optimizer %s "
                % (ls + args.lossFunction + ns, ls + args.optimizer + ns),
                True,
            )
            optimizer_kwargs = {"learning_rate": args.lr}
            if args.optimizer == "adamw":
                optimizer_kwargs["weight_decay"] = args.wd
            optimizer_instance = args.optimizer_class(**optimizer_kwargs)
            myModel.compile(
                optimizer=optimizer_instance, loss=lossFunc, weighted_metrics=metrics
            )

        # Print a summary representation of your model
        if True:  # args.verbose:
            Print("Printing model summary:", True)
            myModel.summary()

        # Get the number of parameters of the model
        SaveModelParameters(myModel, nInputs)  # Compatible with tensorflow v2

        # Get a dictionary containing the configuration of the model.
        model_cfg = GetModelConfiguration(myModel, args.verbose)

        # Serialize model to JSON (contains arcitecture of model)
        model_json = myModel.to_json()
        with open(args.saveDir + "/model_architecture.json", "w") as json_file:
            json_file.write(model_json)

        # Callbacks (https://keras.io/callbacks/)
        earlystop = tf.keras.callbacks.EarlyStopping(
            monitor="val_loss",
            min_delta=0.00001,
            patience=args.earlystop,
            restore_best_weights=True,
        )
        plot.CreateDir("%s/checkPoint" % args.saveDir)
        checkPoint = tf.keras.callbacks.ModelCheckpoint(
            args.saveDir + "/checkPoint/weights.{epoch:02d}.keras",
            monitor="val_loss",
            verbose=0,
            save_best_only=True,
            save_weights_only=False,
            mode="auto",
            save_freq="epoch",
        )  # pediod -> save_freq when using tf.keras.callbacks
        backup = tf.keras.callbacks.BackupAndRestore("%s/backup" % args.saveDir)
        if not args.reduce_on_plateau:
            lr_callback = tf.keras.callbacks.LearningRateScheduler(lr_scheduler)
        elif args.optimizer == "adamw":
            lr_callback = myReduceLRWDOnPlateau(
                monitor="val_loss", patience=args.patience, factor=args.lr_fac
            )
        else:
            lr_callback = tf.keras.callbacks.ReduceLROnPlateau(
                monitor="val_loss", patience=args.patience, factor=args.lr_fac
            )
        callbacks = [earlystop, ModelMonitoring(), checkPoint, backup, lr_callback]

        if args.load_model:
            seqModel = myModel.load_weights(args.saveDir + "model.weights.h5")
        else:
            seqModel = myModel.fit(
                fold_ds,
                validation_data=fold_val_ds,
                epochs=args.epochs,
                shuffle=False,
                verbose=1,  # 0=silent, 1=progress, 2=mention the number of epoch
                callbacks=callbacks,
            )

            # Retrieve  the training / validation loss / accuracy at each epoch
            Verbose(
                "The available history objects of the model are: %s"
                % (", ".join(seqModel.history.keys())),
                True,
                args.verbose,
            )
            epochList = range(args.epochs)

        out_keys = fold_ds.element_spec[1].keys()

        # get numpy arrays of some of the datasets, possibly truncated to args.max_elems
        X_sig = (
            next(iter(sig_datasets_all[n_fold].batch(args.max_elems)))[0]
            .numpy()
            .reshape((-1, nInputs))
        )
        X_bkg = (
            next(iter(bg_datasets_all[n_fold].batch(args.max_elems)))[0]
            .numpy()
            .reshape((-1, nInputs))
        )
        XYW_train = next(iter(fold_ds.unbatch().batch(args.max_elems)))
        XYW_test = next(iter(fold_test_ds.unbatch().batch(args.max_elems)))
        X_train = XYW_train[0].numpy().reshape((-1, nInputs))
        X_test = XYW_test[0].numpy().reshape((-1, nInputs))
        Y_train = {}
        Y_test = {}
        for key in out_keys:
            n_vals = fold_ds.element_spec[1][key].shape[-1]
            Y_train[key] = XYW_train[1][key].numpy().reshape((-1, n_vals))
            Y_test[key] = XYW_test[1][key].numpy().reshape((-1, n_vals))
        W_sig = (
            next(iter(sig_datasets_all[n_fold].batch(args.max_elems)))[2]
            .numpy()
            .reshape((-1, 1))
        )
        W_bkg = (
            next(iter(bg_datasets_all[n_fold].batch(args.max_elems)))[2]
            .numpy()
            .reshape((-1, 1))
        )
        W_train = XYW_train[2].numpy().reshape((-1, 1))
        W_test = XYW_test[2].numpy().reshape((-1, 1))

        if args.plotInputs:
            std_ = False
            Verbose(
                "Plotting all %d input variables for signal and bacgkround" % (nInputs),
                True,
            )
            i = 0
            for var in args.input_features:

                if var == "_":
                    continue

                # Get the lists
                sig_mask = ~tf.math.is_nan(X_sig[:,i])
                sigList = X_sig[sig_mask, i].tolist()
                bkg_mask = ~tf.math.is_nan(X_bkg[:,i])
                bkgList = X_bkg[bkg_mask, i].tolist()
                train_mask = ~tf.math.is_nan(X_train[:,i])
                trainList = X_train[train_mask, i].tolist()
                test_mask = ~tf.math.is_nan(X_test[:,i])
                testList = X_test[test_mask, i].tolist()

                # Make the plots
                func.PlotInputs(
                    sigList,
                    bkgList,
                    f"{i}_{var}",
                    "%s/%s" % (args.saveDir, "sigVbkg"),
                    args.saveFormats,
                    pType="sigVbkg",
                    standardise=std_,
                    w1=W_sig[sig_mask],
                    w2=W_bkg[bkg_mask],
                )
                func.PlotInputs(
                    trainList,
                    testList,
                    f"{i}_{var}",
                    "%s/%s" % (args.saveDir, "trainVtest"),
                    args.saveFormats,
                    pType="trainVtest",
                    standardise=std_,
                    w1=W_train[train_mask],
                    w2=W_test[test_mask],
                )
                i += 1

        # Write the model
        if not args.load_model:
            modelName = "model_trained.keras"
            litename = "model_trained.tflite"
            savepath = os.path.join(args.saveDir, modelName)
            litepath = os.path.join(args.saveDir, litename)

            # Serialize weights to HDF5
            myModel.save_weights(
                os.path.join(args.saveDir, "model.weights.h5"), overwrite=True
            )
            myModel.save(savepath)
            converter = tf.lite.TFLiteConverter.from_keras_model(myModel)
            converter.target_spec.supported_ops = [
                tf.lite.OpsSet.TFLITE_BUILTINS,
                tf.lite.OpsSet.SELECT_TF_OPS
            ]
            @tf.function(input_signature=[tf.TensorSpec(shape=myModel.input_shape, dtype=myModel.input.dtype)])
            def forward_pass(input_data):
                return myModel(input_data)

            # Extract the concrete function
            concrete_function = forward_pass.get_concrete_function()
            cconverter = tf.lite.TFLiteConverter.from_concrete_functions([concrete_function], myModel)
            cconverter.target_spec.supported_ops = [
                tf.lite.OpsSet.TFLITE_BUILTINS,
                tf.lite.OpsSet.SELECT_TF_OPS
            ]
            lite_model = converter.convert()
            with open(litepath, "wb") as f:
                f.write(lite_model)

            # Write weights and architecture in txt file
            scaler_attributes = "scalerType None\n"
            modelFilename = os.path.join(args.saveDir, "model.txt")
            Print(
                "Writing the model (weights and architecture) in the file %s"
                % (hs + os.path.basename(modelFilename) + ns),
                True,
            )
            func.WriteModel(
                myModel,
                model_json,
                args.input_features,
                scaler_attributes,
                modelFilename,
                verbose=False,
            )
            if args.model_type == "quantum":
                func.visualize(myModel, args.input_features, os.path.join(args.saveDir, "model.tex"))

        # Produce method score (i.e. predict output value for given input dataset). Computation is done in batches.
        Print(
            "Generating output predictions (numpy arrays) for the input samples", True
        )
        pred_train = myModel.predict(
            X_train, batch_size=args.batchSize * args.eval_speedup, verbose=1, steps=None
        )  # DNN output for training data (for both signal & bkg)
        pred_test = myModel.predict(
            X_test, batch_size=args.batchSize * args.eval_speedup, verbose=1, steps=None
        )  # DNN output for test data (for both signal & bkg)
        pred_signal = myModel.predict(
            X_sig, batch_size=args.batchSize * args.eval_speedup, verbose=1, steps=None
        )  # DNN output for signal only (all data)
        pred_bkg = myModel.predict(
            X_bkg, batch_size=args.batchSize * args.eval_speedup, verbose=1, steps=None
        )  # DNN output for data only (all data)

        # produce final validation metrics for hparam optimisation
        result = myModel.evaluate(fold_val_ds, return_dict=True)
        metric_fname = os.path.join(args.saveDir, "metrics.json")
        with open(metric_fname, "w") as fp:
            json.dump(result, fp)

        # produce final test metrics for papers eg.
        result = myModel.evaluate(fold_test_ds, return_dict=True)
        metric_fname = os.path.join(args.saveDir, "testmetrics.json")
        with open(metric_fname, "w") as fp:
            json.dump(result, fp)

        XYW_train = np.concatenate((X_train, Y_train["class_out"], W_train), axis=1)
        XYW_test = np.concatenate((X_test, Y_test["class_out"], W_test), axis=1)

        # Pick events with output = 1
        #    Verbose("Select events/samples which have an output variable Y (last column) equal to 1 (i.e. prediction is combatible with signal)", True)
        X_train_S = XYW_train[XYW_train[:, nInputs] == 1]
        X_train_S = X_train_S[:, 0:nInputs]
        X_test_S = XYW_test[XYW_test[:, nInputs] == 1]
        X_test_S = X_test_S[:, 0:nInputs]

        w_train_S = XYW_train[XYW_train[:, nInputs] == 1]
        w_train_S = w_train_S[:, -1]
        w_test_S = XYW_test[XYW_test[:, nInputs] == 1]
        w_test_S = w_test_S[:, -1]

        #    Verbose("Select events/samples which have an output variable Y (last column) equal to 0 (i.e. prediction is NOT combatible with signal)", False)
        X_train_B = XYW_train[XYW_train[:, nInputs] == 0]
        X_train_B = X_train_B[:, 0:nInputs]
        X_test_B = XYW_test[XYW_test[:, nInputs] == 0]
        X_test_B = X_test_B[:, 0:nInputs]

        w_train_B = XYW_train[XYW_train[:, nInputs] == 0]
        w_train_B = w_train_B[:, -1]
        w_test_B = XYW_test[XYW_test[:, nInputs] == 0]
        w_test_B = w_test_B[:, -1]

        if args.param_mass:
            mtrue_test = (
                next(iter(fold_test_ds.unbatch().batch(args.max_elems)))[3]
                .numpy()
                .reshape((-1, 1))
            )
            mtrue_test_S = mtrue_test[Y_test["class_out"][..., 0] == 1]
        elif args.regress_mass:
            mtrue_test_S = Y_test["mass_out"][XYW_test[:, nInputs] == 1]
            mtrue_test_S = mtrue_test_S[:, 0]
        else:
            mtrue_test_S = Y_test["class_out"][XYW_test[:, nInputs] == 1]
            mtrue_test_S = mtrue_test_S[..., -1]

        # Produce method score for signal (training and test) and background (training and test)
        pred_train_S = myModel.predict(
            X_train_S, batch_size=args.batchSize * args.eval_speedup, verbose=1, steps=None
        )
        pred_train_B = myModel.predict(
            X_train_B, batch_size=args.batchSize * args.eval_speedup, verbose=1, steps=None
        )
        pred_test_S = myModel.predict(
            X_test_S, batch_size=args.batchSize * args.eval_speedup, verbose=1, steps=None
        )
        pred_test_B = myModel.predict(
            X_test_B, batch_size=args.batchSize * args.eval_speedup, verbose=1, steps=None
        )

        # scale regression head outputs to correspond to a mass
        if args.regress_mass:
            pred_train["mass_out"] = np.exp(np.exp(pred_train["mass_out"]) - 1) - 1
            pred_test["mass_out"] = np.exp(np.exp(pred_test["mass_out"]) - 1) - 1
            pred_signal["mass_out"] = np.exp(np.exp(pred_signal["mass_out"]) - 1) - 1
            pred_bkg["mass_out"] = np.exp(np.exp(pred_bkg["mass_out"]) - 1) - 1
            pred_train_S["mass_out"] = np.exp(np.exp(pred_train_S["mass_out"]) - 1) - 1
            pred_train_B["mass_out"] = np.exp(np.exp(pred_train_B["mass_out"]) - 1) - 1
            pred_test_S["mass_out"] = np.exp(np.exp(pred_test_S["mass_out"]) - 1) - 1
            pred_test_B["mass_out"] = np.exp(np.exp(pred_test_B["mass_out"]) - 1) - 1

        # Inform user of early stop
        stopEpoch = earlystop.stopped_epoch
        if stopEpoch != 0 and stopEpoch < args.epochs:
            msg = "Early stop occured after %d epochs!" % (1 + stopEpoch)
            args.fold_epochs = stopEpoch
            Print(cs + msg + ns, True)
        else:
            args.fold_epochs = args.epochs

        # Create json file
        writeGitFile(args)

        # plot input distributions before and after nn cut
        for i in range(X_test.shape[-1]):
            if args.input_features[i] == "_":
                continue
            b_mask = ~tf.math.is_nan(X_test_B[...,i])
            s_mask = ~tf.math.is_nan(X_test_S[...,i])
            func.PlotInputDistortion(
                pred_test_B["class_out"][b_mask],
                X_test_B[b_mask,i],
                w_test_B[b_mask],
                f"bg_{args.input_features[i]}{i}_be4vsafter",
                args.saveDir + "/b4vsafter/",
                args.saveFormats,
            )
            func.PlotInputDistortion(
                pred_test_S["class_out"][s_mask],
                X_test_S[s_mask, i],
                w_test_S[s_mask],
                f"sig_{args.input_features[i]}{i}_be4vsafter",
                args.saveDir + "/b4vsafter/",
                args.saveFormats,
            )

        # Plot selected output and save to JSON file for future use
        for key in out_keys:
            if "combine" in key:
                continue
            func.PlotAndWriteJSON(
                pred_signal[key],
                pred_bkg[key],
                args.saveDir,
                f"Output_{key}",
                jsonWr,
                args.saveFormats,
                **GetKwargs(f"Output_{key}"),
            )  # DNN score (predicted): Signal Vs Bkg (all data)
            func.PlotAndWriteJSON(
                pred_train[key],
                pred_test[key],
                args.saveDir,
                f"OutputPred_{key}",
                jsonWr,
                args.saveFormats,
                **GetKwargs(f"OutputPred_{key}"),
            )  # DNN score (sig+bkg)  : Train Vs Predict
            func.PlotAndWriteJSON(
                pred_train_S[key],
                pred_train_B[key],
                args.saveDir,
                f"OutputTrain_{key}",
                jsonWr,
                args.saveFormats,
                **GetKwargs(f"OutputTrain_{key}"),
            )  # DNN score (training) : Sig Vs Bkg
            func.PlotAndWriteJSON(
                pred_test_S[key],
                pred_test_B[key],
                args.saveDir,
                f"OutputTest_{key}",
                jsonWr,
                args.saveFormats,
                **GetKwargs(f"OutputTest_{key}"),
            )  # DNN score (predicted): Sig Vs Bkg
        if args.regress_mass:
            func.PlotAndWriteJSON_DNNscore(
                pred_test_S["class_out"],
                pred_test_B["class_out"],
                0.5,
                pred_test_S["mass_out"],
                pred_test_B["mass_out"],
                args.saveDir,
                "OutputDist_cut",
                jsonWr,
                args.saveFormats,
                **GetKwargs(f"mass"),
            )

        rocDict = {}

        # Plot overtraining test
        htrain_s, htest_s, htrain_b, htest_b = func.PlotOvertrainingTest(
            pred_train_S["class_out"],
            pred_test_S["class_out"],
            pred_train_B["class_out"],
            pred_test_B["class_out"],
            args.saveDir,
            "OvertrainingTest",
            args.saveFormats,
            w_train_S,
            w_test_S,
            w_train_B,
            w_test_B,
        )

        # Plot summary plot (efficiency & singificance)
        func.PlotEfficiency(htest_s, htest_b, args.saveDir, "Summary", args.saveFormats)

        # Write efficiencies (signal and bkg)
        xVals_S, xErrs_S, effVals_S, effErrs_S = func.GetEfficiency(htest_s)
        xVals_B, xErrs_B, effVals_B, effErrs_B = func.GetEfficiency(htest_b)
        func.PlotTGraph(
            xVals_S,
            xErrs_S,
            effVals_S,
            effErrs_S,
            args.saveDir,
            "EfficiencySig",
            jsonWr,
            args.saveFormats,
            **GetKwargs("efficiency"),
        )
        func.PlotTGraph(
            xVals_B,
            xErrs_B,
            effVals_B,
            effErrs_B,
            args.saveDir,
            "EfficiencyBkg",
            jsonWr,
            args.saveFormats,
            **GetKwargs("efficiency"),
        )

        xVals, xErrs, sig_def, sig_alt = func.GetSignificance(htest_s, htest_b)
        func.PlotTGraph(
            xVals,
            xErrs,
            sig_def,
            effErrs_B,
            args.saveDir,
            "SignificanceA",
            jsonWr,
            args.saveFormats,
            **GetKwargs("significance"),
        )
        func.PlotTGraph(
            xVals,
            xErrs,
            sig_alt,
            effErrs_B,
            args.saveDir,
            "SignificanceB",
            jsonWr,
            args.saveFormats,
            **GetKwargs("significance"),
        )

        # Plot some metrics
        if not args.load_model:
            xErr = [0.0 for i in range(0, args.fold_epochs)]
            yErr = [0.0 for i in range(0, args.fold_epochs)]
            for name, metric in seqModel.history.items():
                if "acc" in name:
                    var = "acc"
                    draw = True
                elif "loss" in name or "DisCo" in name:
                    var = "loss"
                    draw = True
                elif "AUC" in name:
                    var = "auc"
                    draw = True
                else:
                    draw = False
                if draw:
                    func.PlotTGraph(
                        range(args.fold_epochs),
                        xErr,
                        metric,
                        yErr,
                        args.saveDir,
                        name,
                        jsonWr,
                        args.saveFormats,
                        **GetKwargs(var),
                    )

        # Plot ROC curve
        gSig = func.GetROC(htest_s, htest_b)
        gDict = {"graph": [gSig], "name": [os.path.basename(args.saveDir)]}
        func.PlotROC(gDict, args.saveDir, "ROC", args.saveFormats)

        if args.regress_mass:
            func.PlotMassPredictionErrors(
                mtrue_test_S,
                pred_test_S["mass_out"],
                args.saveDir,
                "MassPredErrors",
                jsonWr,
                args.saveFormats,
            )

        # plot distribution widths of the variable of interest for each dataset
        poi = None
        if args.param_mass and args.disco_var is not None:
            poi = X_test_S[..., args.input_features.index(args.disco_var)]
        elif args.regress_mass:
            poi = pred_test_S["mass_out"]
        elif args.disco_var is not None:
            poi = X_test_S[..., args.input_features.index(args.disco_var)]

        if poi is not None:
            plotxMin = 0
            plotxMax = 2
            func.PlotPoivsMassDiffPerDataset(
                mtrue_test_S,
                poi,
                args.saveDir,
                "POIvsTrueMass",
                jsonWr,
                args.saveFormats,
                xMin=plotxMin,
                xMax=plotxMax,
            )
        func.PlotDNNscorePerDataset(
            mtrue_test_S,
            pred_test_S["class_out"],
            args.saveDir,
            "BinnedDNNscores",
            jsonWr,
            args.saveFormats,
        )

        # Write the resultsJSON file!
        jsonWr.write(args.resultsJSON)

        # Print total time elapsed
        days, hours, mins, secs = GetTime(tStart)
        dt = "%s days, %s hours, %s mins, %s secs" % (days[0], hours[0], mins[0], secs)
        Print("Elapsed time: %s" % (hs + dt + ns), True)
        args.elapsedTime = dt
        writeCfgFile(args)

        for name in ["/train_cache", "/val_cache", "/test_cache"]:
            shutil.rmtree(args.saveDir + name)

    return


# ================================================================================================
# Main
# ================================================================================================
if __name__ == "__main__":
    """
    https://docs.python.org/3/library/argparse.html

    name or flags...: Either a name or a list of option strings, e.g. foo or -f, --foo.
    action..........: The basic type of action to be taken when this argument is encountered at the command line.
    nargs...........: The number of command-line arguments that should be consumed.
    const...........: A constant value required by some action and nargs selections.
    default.........: The value produced if the argument is absent from the command line.
    type............: The type to which the command-line argument should be converted.
    choices.........: A container of the allowable values for the argument.
    required........: Whether or not the command-line option may be omitted (optionals only).
    help............: A brief description of what the argument does.
    metavar.........: A name for the argument in usage messages.
    dest............: The name of the attribute to be added to the object returned by parse_args().
    """
    # Default Settings
    STANDARDIZE = False
    DATADIR = "/home/AapoKossi/temp"
    #    INPUTVARS    = "tau_pt,pt_miss,R_tau,bjet_pt,tau_mt,mt,dPhiTauNu,dPhitaub,dPhibNu,mtrue"
    INPUTVARS = "load"
    SAVEDIR = None
    SAVEDIRAPPDX = False
    SAVEFORMATS = ["pdf"]
    URL = False
    SCALEBACK = True
    PLANING = False
    MARGINAL = False
    ONLYBKG = False
    VERBOSE = False
    RNDSEED = 1234
    EPOCHS = 100
    BATCHSIZE = 1024
    ACTIVATION = ["swish","swish","swish","swish","sigmoid"]
    NEURONS = [1024,1024,1024,1024,1]
    BATCHNORM = "True"
    GRIDX = False
    GRIDY = False
    LOSSFUNCTION = "binary_crossentropy"
    OPTIMIZER = "adam"
    CFGJSON = "config.json"
    RESULTSJSON = "results.json"
    PLOTINPUTS = True
    LAMBDAD = 15
    LAMBDAR = 4
    FOLDS = 2
    MAXLOADEDELEMS = 2**18
    DISCOVAR = None  # by default, NOT using distance correlation loss term
    VALSPLIT = 0.1
    WORKERID = None
    ENERGY = "13"
    LEARNINGRATE = 3e-4
    REGRESSMASS = False
    MASSPREDMIN = 0
    PARAMMASS = False
    MODELTYPE = "classical"
    QMLHEAD = "dense"
    EXCLUDE = ["ZZ","WZ","WW","WJets","DYJets"]
    WJETSRC = "inclusive"
    REDUCEONPLATEAU = False
    LOADMODEL = False
    USECACHE = False
    COPYMASS = True
    DROPOUT = 0.15
    WEIGHTDECAY = 3e-6
    MASSLOSS = "MSE"
    MASSACTIVATION = "softplus"
    LRFACTOR = 0.99
    LRPATIENCE = 0
    EARLYSTOP = 50
    SHUFFLE = False
    SINGLE_GATE = "rgate_theoretical"
    ENTANGLER_GATE = "cz"
    NQLAYERS = "2"
    EVAL_SPEEDUP = 4

    # TODO: switch to argumentparser, optparser is python 2 legacy
    parser = argparse.ArgumentParser("DNN training with disCo loss term")

    parser.add_argument(
        "datadir",
        help="Input directory containing the datasets saved with save_training_data.py)"
    )

    parser.add_argument(
        "-x", "--input-features",
        default=INPUTVARS,
        nargs='*',
        help="list of names of the model input variables, input _ in order to specify that the input at that index "
        + "in the loaded dataset should not be used [default: %s]" % INPUTVARS,
    )

    parser.add_argument(
        "--planing",
        action="store_true",
        default=PLANING,
        help="Calculate weights to decorrelate a variable from the training. This is done by reweighting the branches so that the selected variable becomes flat [default: %s]"
        % PLANING,
    )

    parser.add_argument(
        "--marginalized-planing",
        action="store_true",
        default=MARGINAL,
        help="If specified planing, whether to plane the marginalized transverse mass distribution. If false and planing, planes all the signal datasets individually. [default: %s]"
        % MARGINAL,
    )

    parser.add_argument(
        "--onlyBackground",
        action="store_true",
        default=ONLYBKG,
        help="Calculate distance correlation using only the background events (w_sig = 0). [default: %s]"
        % ONLYBKG,
    )

    parser.add_argument(
        "--standardize",
        action="store_true",
        default=STANDARDIZE,
        help="Whether to insert a sanitizer layer in the beginning of the model. [default: %s]"
        % STANDARDIZE,
    )

    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=VERBOSE,
        help="Enable verbose mode (for debugging purposes mostly) [default: %s]"
        % VERBOSE,
    )

    parser.add_argument(
        "--plotInputs",
        action="store_true",
        default=PLOTINPUTS,
        help="Enable plotting of input variables [default: %s]" % PLOTINPUTS,
    )

    parser.add_argument(
        "--resultsJSON",
        default=RESULTSJSON,
        help="JSON file containing the results [default: %s]" % (RESULTSJSON),
    )

    parser.add_argument(
        "--cfgJSON",
        default=CFGJSON,
        help="JSON file containing the configurations [default: %s]" % (CFGJSON),
    )

    parser.add_argument(
        "--saveDir",
        default=SAVEDIR,
        help="Directory where all pltos will be saved [default: %s]" % SAVEDIR,
    )

    parser.add_argument(
        "--url",
        action="store_true",
        default=URL,
        help="Don't print the actual save path the histogram is saved, but print the URL instead [default: %s]"
        % URL,
    )

    parser.add_argument(
        "-s",
        "--saveFormats",
        default=SAVEFORMATS,
        nargs='*',
        help="Save formats for all plots [default: %s]" % SAVEFORMATS,
    )

    parser.add_argument(
        "--rndSeed",
        type=int,
        default=RNDSEED,
        help="Value of random seed (integer) [default: %s]" % RNDSEED,
    )

    parser.add_argument(
        "--epochs",
        type=int,
        default=EPOCHS,
        help='Number of "epochs" to be used (how many times you go through your training set) [default: %s]'
        % EPOCHS,
    )

    parser.add_argument(
        "--eval-speedup",
        type=int,
        default=EVAL_SPEEDUP,
        help='Factor to multiply batch size during inference. [default: %s]'
        % EVAL_SPEEDUP,
    )

    parser.add_argument(
        "--batchSize",
        type=int,
        default=BATCHSIZE,
        help='The "batch size" to be used (= a number of samples processed before the model is updated). Batch size impacts learning significantly; typically networks train faster with mini-batches. However, the batch size has a direct impact on the variance of gradients (the larger the batch the better the appoximation and the larger the memory usage). [default: %s]'
        % BATCHSIZE,
    )

    parser.add_argument(
        "--activation",
        default=ACTIVATION,
        nargs='*',
        help="Type of transfer function that will be used to map the output of one layer to another [default: %s]"
        % ACTIVATION,
    )

    parser.add_argument(
        "--exclude",
        default=EXCLUDE,
        nargs='*',
        help="Exclude datasets that include these substrings [default: %s]" % EXCLUDE,
    )

    parser.add_argument(
        "--wjets-src",
        default=WJETSRC,
        help="Specify whether to use inclusive or HT sample for WJets data  [default: %s]"
        % WJETSRC,
    )

    parser.add_argument(
        "--neurons",
        default=NEURONS,
        nargs='*',
        type=int,
        help="List of neurons to use for each sequential layer (comma-separated integers)  [default: %s]"
        % NEURONS,
    )

    parser.add_argument(
        "--batchnorm",
        default=BATCHNORM,
        nargs='*',
        type=str2bool,
        help="List of Booleans to specify whether to insert batch normalization layers after activations (comma-separated python booleans)  [default: %s]"
        % BATCHNORM,
    )

    parser.add_argument(
        "--lossFunction",
        default=LOSSFUNCTION,
        help="One of the two parameters required to compile a model. The weights will take on values such that the loss function is minimized [default: %s]"
        % LOSSFUNCTION,
    )

    parser.add_argument(
        "--optimizer",
        default=OPTIMIZER,
        help="Name of optimizer function; one of the two parameters required to compile a model: [default: %s]"
        % OPTIMIZER,
    )

    parser.add_argument(
        "--gridX",
        action="store_true",
        default=GRIDX,
        help="Enable x-axis grid [default: %s]" % GRIDX,
    )

    parser.add_argument(
        "--gridY",
        action="store_true",
        default=GRIDY,
        help="Enable y-axis grid [default: %s]" % GRIDY,
    )

    parser.add_argument(
        "--lam-d",
        default=LAMBDAD,
        type=float,
        help="Lambda for disCo loss (Default: %s)" % LAMBDAD,
    )

    parser.add_argument(
        "--lam-r",
        default=LAMBDAR,
        type=float,
        help="Lambda (Default: %s)" % LAMBDAR,
    )

    parser.add_argument(
        "--k",
        default=FOLDS,
        type=int,
        help="K-folding number of folds used when saving the dataset (Default: %s)"
        % FOLDS,
    )

    parser.add_argument(
        "--max-elems",
        default=MAXLOADEDELEMS,
        type=int,
        help="Maximum number of single dataset elements to be loaded in memory at any time, all (Default: %s)"
        % MAXLOADEDELEMS,
    )

    parser.add_argument(
        "--disco-var",
        default=DISCOVAR,
        help="Index in the training data of the variable to decorrelate NN output with disCo from. Note that this is applied before dropping any input variables (Default: %s)"
        % DISCOVAR,
    )

    parser.add_argument(
        "--val-split",
        default=VALSPLIT,
        type=float,
        help="What fraction of the training data to set aside for model validation (Default: %s)"
        % VALSPLIT,
    )

    parser.add_argument(
        "--worker",
        default=WORKERID,
        type=int,
        help="Which fold of the data to train the model on, defaults to training all the models sequentially (Default: %s)"
        % WORKERID,
    )

    parser.add_argument(
        "--energy",
        default=ENERGY,
        help="Center of mass energy for the data, used for fetching event cross sections for different bakcgrounds: [default: %s]"
        % ENERGY,
    )

    parser.add_argument(
        "-lr", "--learning-rate",
        dest="lr",
        type=float,
        default=LEARNINGRATE,
        help="Initial learning rate for the optimizer [default: %s]" % LEARNINGRATE,
    )

    parser.add_argument(
        "--weight-decay",
        type=float,
        default=WEIGHTDECAY,
        help="Initial weight decay rate for the optimizer [default: %s]" % WEIGHTDECAY,
    )

    parser.add_argument(
        "--regress-mass",
        action="store_true",
        default=REGRESSMASS,
        help="Specify this to add additional output to the model for regressing the H+ mass[default: %s]"
        % REGRESSMASS,
    )

    parser.add_argument(
        "--model-type",
        default=MODELTYPE,
        help="Specify whether to train a classical DNN or a quantum model [default: %s]"
        % MODELTYPE,
    )

    parser.add_argument(
        "--qml-head",
        default=QMLHEAD,
        help="Specify the type of prediction head used for the PQC model [default: %s]"
        % QMLHEAD,
    )

    parser.add_argument(
        "--mass-loss",
        type=str,
        default=MASSLOSS,
        help="Which loss function to use for regressing the H+ mass[default: %s]"
        % MASSLOSS,
    )

    parser.add_argument(
        "--mass-act",
        type=str,
        default=MASSACTIVATION,
        help="Which activation function to use for regressing the H+ mass [default: %s]"
        % MASSACTIVATION,
    )

    parser.add_argument(
        "--masspred-min",
        type=int,
        default=MASSPREDMIN,
        help="Minimum output for the nn head regressing the H+ mass[default: %s]"
        % MASSPREDMIN,
    )

    parser.add_argument(
        "--param-mass",
        action="store_true",
        default=PARAMMASS,
        help="Specify this to add additional output to the model for parametrizing the model on H+ mass[default: %s]"
        % PARAMMASS,
    )

    parser.add_argument(
        "--reduce-on-plateau",
        action="store_true",
        default=REDUCEONPLATEAU,
        help="Specify to overwrite lr decay with a keras reduce on plateau callback [default: %s]"
        % REDUCEONPLATEAU,
    )

    parser.add_argument(
        "--load-model",
        action="store_true",
        default=LOADMODEL,
        help="Load the model from the saveDir [default: %s]" % LOADMODEL,
    )

    parser.add_argument(
        "--use-cache",
        action="store_true",
        default=USECACHE,
        help="Use cache for faster dataset loading. Default: %s" % USECACHE,
    )

    parser.add_argument(
        "--gen-save-appendix",
        action="store_true",
        default=SAVEDIRAPPDX,
        help="Add descriptionary appendix to save dir. Default: %s" % SAVEDIRAPPDX,
    )

    parser.add_argument(
        "--copy-mass",
        action=argparse.BooleanOptionalAction,
        default=COPYMASS,
        help="Instead of giving background events mass parameter exactly from the previous signal event, take it from a naive predetermined distribution. Default: %s"
        % COPYMASS,
    )

    parser.add_argument(
        "--dropout",
        type=float,
        default=DROPOUT,
        help="Dropout rate between Dense layers. Default: %s" % DROPOUT,
    )

    parser.add_argument(
        "--lr-fac",
        type=float,
        default=LRFACTOR,
        help="Factor to reduce learning rate (and weight decay) by on plateau. Default: %s"
        % LRFACTOR,
    )

    parser.add_argument(
        "--patience",
        type=float,
        default=LRPATIENCE,
        help="number of epochs to keep training before reducing lr on validation loss plateau. Default: %s"
        % LRPATIENCE,
    )

    parser.add_argument(
        "--earlystop",
        type=float,
        default=EARLYSTOP,
        help="number of epochs to keep training before early stopping on validation loss plateau. Default: %s"
        % EARLYSTOP,
    )

    parser.add_argument(
        "--shuffle",
        action="store_true",
        default=SHUFFLE,
        help="shuffle the input training data each epoch [default: %s]" % SHUFFLE,
    )

    parser.add_argument(
        "--single-gate",
        default=SINGLE_GATE,
        help="[default: %s]" % SINGLE_GATE,
    )

    parser.add_argument(
        "--entangling-gate",
        default=ENTANGLER_GATE,
        help="[default: %s]" % ENTANGLER_GATE,
    )

    parser.add_argument(
        "--n-qlayers",
        type=int,
        default=NQLAYERS,
        help="[default: %s]" % NQLAYERS,
    )

    args  = parser.parse_args()

    # Require at least two arguments (script-name, ...)
    if len(sys.argv) < 1:
        parser.print_help()
        sys.exit(1)
    else:
        pass

    # Input list of discriminatin variables
    if args.input_features == "load":
        args.input_features = datasetUtils.load_input_vars(args.datadir)
    if len(args.input_features) < 1:
        raise Exception(
            "At least one input variable needed to create the DNN. Only %d provided"
            % (len(args.input_features))
        )

    Verbose("Activation = %s" % (args.activation), True)
    Verbose("Excluded datasets = %s" % (args.exclude), True)
    Verbose("Neurons = %s" % (args.neurons), True)
    Verbose("batch normalization = %s" % (args.batchnorm), True)

    # Sanity checks (One activation function and batchnorm option for each layer)
    if (
        len(args.neurons)
        != len(args.activation) | len(args.neurons)
        != len(args.batchnorm)
    ):
        msg = (
            "The list of neurons (size=%d) is not the same size as the list of activation functions (=%d)"
            % (len(args.neurons), len(args.activation))
        )
        raise Exception(es + msg + ns)
    # Sanity check (Last layer)
    if args.neurons[-1] != 1:
        if not args.regress_mass:
            msg = (
                "The number of neurons for the last layer should be equal to 1 (=%d instead)"
                % (args.neurons[-1])
            )
            raise Exception(es + msg + ns)
        elif args.neurons[-1] != 2:
            msg = (
                "The number of neurons for the last layer should be equal to 2 when regressing mass (=%d instead)"
                % (args.neurons[-1])
            )
            raise Exception(es + msg + ns)

    # Define dir/logfile names
    specs = "%dLayers" % (len(args.neurons))
    """
    for i,n in enumerate(args.neurons, 0):
        specs+= "_%s%s" % (args.neurons[i], args.activation[i])
    """
    specs += "_%sEpochs_%sBatchSize" % (args.epochs, args.batchSize)

    #    if args.decorrelate != None:
    #        if args.decorrelate not in args.inputList:
    #            msg = "Cannot apply sample reweighting. The input variable \"%s\" is not in the inputList." % (args.decorrelate)
    #            raise Exception(es + msg + ns)

    # Get the current date and time
    now = datetime.now()
    nDay = now.strftime("%d")
    nMonth = now.strftime("%h")
    nYear = now.strftime("%Y")
    nTime = now.strftime("%Hh%Mm%Ss")  # w/ seconds
    nDate = "%s%s%s_%s" % (nDay, nMonth, nYear, nTime)
    sName = "DNNresults_"
    #    if args.decorrelate != None:
    #        sName += "_%sDecorrelated" % args.decorrelate
    if args.planing:
        sName += "_Planing"
    if args.marginalized_planing:
        sName += "_Marginalized"
    if args.onlyBackground:
        sName += "_OnlyBkg"

    # Add the number of input variables
    sName += "_%dInputs" % (len(args.input_features))
    # Add the lamda value
    if args.disco_var is not None or args.regress_mass:
        sName += "_Lamda_d%s" % (args.lam_d)
    if args.regress_mass:
        sName += "_Lamda_r%s" % (args.lam_r)
    sName += "_lr%s" % (args.lr)
    if args.optimizer == "adamw":
        sName += "_wd%s" % (args.wd)
    # Add the time-stamp last
    sName += "_%s" % (nDate)

    sName += f"_{args.optimizer}"

    if args.dropout > 0:
        d = str(args.dropout).replace("0.", "p")
        sName += f"_dropout{args.dropout}"

    if args.regress_mass:
        sName += "_RegressMass"
    if args.param_mass:
        sName += "_ParamMass"

    # Determine path for saving plots
    if args.saveDir == None:
        usrName = getpass.getuser()
        usrInit = usrName[0]
        myDir = ""
        if "lxplus" in socket.gethostname():
            myDir = "/afs/cern.ch/user/%s/%s/public/html/" % (usrInit, usrName)
        else:
            myDir = os.path.join(os.getcwd())
        args.saveDir = os.path.join(myDir, sName)
    # elif args.saveapdx:
    #     args.saveDir = os.path.join(args.saveDir, sName)

    # Create dir if it does not exist
    if not os.path.exists(args.saveDir):
        os.makedirs(args.saveDir)

    # Write list of input variables to savedir for input conversion at inference time
    with open(args.saveDir + "/input_vars.txt", "w") as f:
        for var in args.input_features:
            f.write(var + "\n")

    if args.model_type == "classical":
        PrintNetworkSummary(args)
    else:
        # TODO: Print actual summary and hyperparameters
        Print("Will construct a Parameterized Quantum Circuit classifier", True)

    # See https://keras.io/activations/
    actList = [
        "elu",
        "softmax",
        "selu",
        "softplus",
        "softsign",
        "PReLU",
        "LeakyReLU",
        "relu",
        "tanh",
        "sigmoid",
        "hard_sigmoid",
        "exponential",
        "linear",
        "swish",
    ]  # Loukas used "relu" for resolved top tagger
    # Sanity checks
    for a in args.activation:
        if a not in actList:
            msg = (
                "Unsupported activation function %s. Please select on of the following:%s\n\t%s"
                % (args.activation, ss, "\n\t".join(actList))
            )
            raise Exception(es + msg + ns)

    if args.mass_act not in actList:
        msg = (
            "Unsupported activation function %s. Please select on of the following:%s\n\t%s"
            % (args.activation, ss, "\n\t".join(actList))
        )
        raise Exception(es + msg + ns)

    # See https://keras.io/losses/
    lossList = [
        "binary_crossentropy",
        "mean_squared_error",
        "mean_absolute_error",
        "mean_absolute_percentage_error",
        "mean_squared_logarithmic_error",
        "squared_hinge",
        "hinge",
        "categorical_hinge",
        "logcosh",
        "huber_loss",
        "categorical_crossentropy",
        "sparse_categorical_crossentropy",
        "kullback_leibler_divergenc",
        "poisson",
        "cosine_proximity",
    ]
    bLossList = [
        "binary_crossentropy",
    ]
    # Sanity checks
    if args.lossFunction not in lossList:
        msg = (
            "Unsupported loss function %s. Please select on of the following:%s\n\t%s"
            % (args.lossFunction, ss, "\n\t".join(lossList))
        )
        raise Exception(es + msg + ns)
    elif args.lossFunction not in bLossList:
        msg = (
            "Binary output currently only supports the following loss fucntions: %s"
            % ", ".join(bLossList)
        )
        raise Exception(es + msg + ns)

    loss_args = {"reduction": tf.keras.losses.Reduction.NONE}
    lossMap = {
        "binary_crossentropy": tf.keras.losses.BinaryCrossentropy(**loss_args),
        "mean_squared_error": tf.keras.losses.MeanSquaredError(**loss_args),
        "mean_absolute_error": tf.keras.losses.MeanAbsoluteError(**loss_args),
        "mean_absolute_percentage_error": tf.keras.losses.MeanAbsolutePercentageError(**loss_args),
        "mean_squared_logarithmic_error": tf.keras.losses.MeanSquaredLogarithmicError(**loss_args),
        "squared_hinge": tf.keras.losses.SquaredHinge(**loss_args),
        "hinge": tf.keras.losses.Hinge(**loss_args),
        "categorical_hinge": tf.keras.losses.CategoricalHinge(**loss_args),
        "logcosh": tf.keras.losses.LogCosh(**loss_args),
        "huber_loss": tf.keras.losses.Huber(**loss_args),
        "categorical_crossentropy": tf.keras.losses.CategoricalCrossentropy(**loss_args),
        "sparse_categorical_crossentropy": tf.keras.losses.SparseCategoricalCrossentropy(**loss_args),
        "kullback_leibler_divergence": tf.keras.losses.KLDivergence(**loss_args),
        "poisson": tf.keras.losses.Poisson(**loss_args),
        "cosine_proximity": tf.keras.losses.CosineSimilarity(**loss_args),
    }

    args.tf_loss_func = lossMap[args.lossFunction]

    # See https://keras.io/optimizers/. Also https://www.dlology.com/blog/quick-notes-on-how-to-choose-optimizer-in-keras/
    optList = {
        "sgd": tf.keras.optimizers.SGD,
        "rmsprop": tf.keras.optimizers.RMSprop,
        "adagrad": tf.keras.optimizers.Adagrad,
        "adadelta": tf.keras.optimizers.Adagrad,
        "adam": tf.keras.optimizers.Adam,
        "adamax": tf.keras.optimizers.Adamax,
        "nadam": tf.keras.optimizers.Nadam,
        "adamw": tf.keras.optimizers.AdamW,
    }
    if args.optimizer not in optList.keys():
        msg = (
            "Unsupported loss function %s. Please select on of the following:%s\n\t%s"
            % (args.optimizer, ss, "\n\t".join(optList))
        )
        raise Exception(es + msg + ns)
    args.optimizer_class = optList[args.optimizer]

    modelList = {
        "classical": DNN_model.initialize,
        "quantum": QML_model.initialize
    }
    if args.model_type not in modelList.keys():
        msg = (
            "Unsupported model type %s. Please select on of the following:%s\n\t%s"
            % (args.model_type, ss, "\n\t".join(modelList))
        )
        raise Exception(es + msg + ns)
    args.model_initializer = modelList[args.model_type]

    # Get some basic information
    # args.keras = keras.__version__  # This is broken since TF v2.16 with legacy keras 2
    args.tensorflow = tf.__version__
    args.hostname = socket.gethostname()
    args.python = "%d.%d.%d" % (
        sys.version_info[0],
        sys.version_info[1],
        sys.version_info[2],
    )
    args.gitBranch = subprocess.check_output(["git", "branch", "-a"])
    args.gitStatus = subprocess.check_output(["git", "status"])
    args.gitDiff = subprocess.check_output(["git", "diff"])

    # Call the main function
    Print("Hostname is %s" % (hs + args.hostname + ns), True)
    #Print("Using Keras %s" % (hs + args.keras + ns), False)
    Print("Using Tensorflow %s" % (hs + args.tensorflow + ns), False)

    main(args)

    Print("Directory %s created" % (ls + args.saveDir + ns), True)
