#!/usr/bin/env python

import os
import sys
import re
import subprocess
import json
import datetime
import tarfile
import math

from optparse import OptionParser

basepath_re = re.compile("(?P<basepath>\S+/NanoAnalysis)/")
match = basepath_re.search(os.getcwd())
if match:
    sys.path.append(os.path.join(match.group("basepath"),"NanoAODAnalysis/Framework/python"))

from nanoanalysis import LumiMask
from nanoanalysis import multicrabdatasets
from nanoanalysis import JetCorrections

GRIDPROXY  = "arcproxy --info"
GRIDSUBMIT = "arcsub -c kale-cms.grid.helsinki.fi grid.xrsl"
GRIDSTATUS = "arcstat"
GRIDGET    = "arcget"

def usage():
    os.system("%s --help"%sys.argv[0])
    sys.exit()

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    f = p.stdout
    ret=[]
    for line in f:
        line = line.decode('utf-8')
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def proxy():
    proxyResult = execute(GRIDPROXY)
    timeleft_re = re.compile("Time left for \S+: (?P<time>.*)")
    for line in proxyResult:
        match = timeleft_re.search(line)
        if match:
            print("Time left for proxy:",match.group("time"))
            if "expired" in match.group("time"):
                sys.exit()

def copy_proxy(destinationpath):
    proxyResult = execute(GRIDPROXY)
    proxypath_re = re.compile("Proxy path: (?P<path>\S+)")
    for line in proxyResult:
        match = proxypath_re.search(line)
        if match:
           cmd = "cp %s %s"%(match.group("path"),destinationpath)
           #print(cmd)
           os.system(cmd)
           return match.group("path")
    return None

def copy_task(destinationpath):
    filename = 'copy2task.py'
    fOUT = open(os.path.join(destinationpath,filename),"w")
    fOUT.write('#!/usr/bin/env python\n')
    fOUT.write('import os,sys\n')
    fOUT.write('import subprocess\n')
    fOUT.write('from optparse import OptionParser\n')
    fOUT.write('\n')
    fOUT.write('def execute(cmd):\n')
    fOUT.write('    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE)\n')
    fOUT.write('    f = p.stdout\n')
    fOUT.write('    ret=[]\n')
    fOUT.write('    for line in f:\n')
    fOUT.write('        line = line.decode("utf-8")\n')
    fOUT.write('        ret.append(line.replace("\\n", ""))\n')
    fOUT.write('    f.close()\n')
    fOUT.write('    return ret\n')
    fOUT.write('\n')
    fOUT.write('def copy(path,opts):\n')
    fOUT.write('    stuff = execute("ls x509up_u*")\n')
    fOUT.write('    stuff+= ["analysis.tgz","coffea.sif"]\n')
    fOUT.write('    for s in stuff:\n')
    fOUT.write('        if os.path.exists(s):\n')
    fOUT.write('            if opts.link and ".sif" in s:\n')
    fOUT.write('                os.system("ln -s %s %s"%(os.path.join(os.getcwd(),s),path))\n')
    fOUT.write('            else:\n')
    fOUT.write('                os.system("cp %s %s"%(s,path))\n')
    fOUT.write('\n')
    fOUT.write('def main():\n')
    fOUT.write('\n')
    fOUT.write('    parser = OptionParser(usage="Usage: %prog [options]")\n')
    fOUT.write('    parser.add_option("--link", dest="link", default=False, action="store_true",\n')
    fOUT.write('                      help="Link files for local processing instead of copying [defaut: False]")\n')
    fOUT.write('    (opts, args) = parser.parse_args()\n')
    fOUT.write('\n')
    fOUT.write('    cands = execute("ls .")\n')
    fOUT.write('    for cand in cands:\n')
    fOUT.write('        if os.path.isdir(cand):\n')
    fOUT.write('            print("Copying to %s"%cand)\n')
    fOUT.write('            subcands = execute("ls %s"%cand)\n')
    fOUT.write('            for i,sc in enumerate(subcands):\n')
    fOUT.write('                path = os.path.join(cand,sc)\n')
    fOUT.write('                if not os.path.exists(path):\n')
    fOUT.write('                    continue\n')
    fOUT.write('                if "task" in sc:\n')
    fOUT.write('                    copy(path,opts)\n')
    fOUT.write('                    print("     %s/%s"%(i+1,len(subcands)))\r')
    fOUT.write('\n')
    fOUT.write('main()\n')
    fOUT.close()

def makeDataTarBall(subdir,filename,files,pileup_data,lumijson,lumimask,dataset,opts):
    #print("    Making data tarball",subdir)
    cdir = dataset.name
    multigrabdir = "multicrab"
    fTAR = tarfile.open(os.path.join(subdir,filename),'x:gz')
    #fTAR.add(proxy,os.path.basename(proxy))
    if 'root' in files[0][:4] or opts.list:
        dict = {}
        dict['files'] = files
        f = open(os.path.join(subdir,"files_%s.json"%cdir), "w")
        json.dump(dict, f, sort_keys=True, indent=2)
        f.close()
        fTAR.add(os.path.join(subdir,"files_%s.json"%cdir),os.path.join(multigrabdir,cdir,"results","files_%s.json"%cdir))
    else:
        for f in files:
            fTAR.add(f,os.path.join(multigrabdir,cdir,"results",os.path.basename(f)))
    fTAR.add(pileup_data,os.path.join(multigrabdir,os.path.basename(pileup_data)))
    fTAR.add(lumijson,os.path.join(multigrabdir,os.path.basename(lumijson)))
    if dataset.isData:
        fTAR.add(lumimask,os.path.join(multigrabdir,cdir,"inputs",os.path.basename(lumimask)))
    if os.path.exists(dataset.getPileupfile()):
        fTAR.add(dataset.getPileupfile(),os.path.join(multigrabdir,cdir,"results",os.path.basename(dataset.getPileupfile())))

    fTAR.close()
    #print("Making data tarball done.")


def makeAnalysisTarBall(subdir,filename,year,opts):
    #print("    Making analysis tarball",subdir)
    codedir = ""
    #nano_re = re.compile("\S+/(?P<file>NanoAnalysis/\S+?/\S+)")
    nano_re = re.compile("(?P<anadir>\S+/NanoAnalysis/\S+?)/(?P<file>\S+)")
    for p in sys.path:
        match = nano_re.search(p)
        if match:
            codedir = match.group("anadir")
            break
    if len(codedir) == 0:
        print("Analysis dir not found, exiting..")
        sys.exit()

    SAVEDFILES = []
    SAVEDFILES = execute("find %s -name '*.py'"%os.path.join(codedir,"Framework","python"))
    SAVEDFILES.extend(execute("ls %s/*.py"%os.getcwd()))

    FRAMEWORK_DATA = execute("ls %s"%os.path.join(codedir,"Framework","data"))
    for sdir in FRAMEWORK_DATA:
        if 'JECDatabase' in sdir:
            jetvetomapfiles = execute("find %s -name '*.root'"%os.path.join(codedir,"Framework","data","JECDatabase","jet_veto_maps"))
            for f in jetvetomapfiles:
                if year[2:] in f:
                    SAVEDFILES.append(f)
            for key in JetCorrections.JEC_CORRECTION_SETS.keys():
                if year in key:
                    for key2 in JetCorrections.JEC_CORRECTION_SETS[key].keys():
                        for corr in JetCorrections.JEC_CORRECTION_SETS[key][key2]:
                            SAVEDFILES.append(os.path.join(codedir,"Framework","data","JECDatabase","textFiles",corr))
            continue
        if 'JRDatabase' in sdir:
            for key in JetCorrections.JER_CORRECTION_SETS.keys():
                if year in key:
                    for key2 in JetCorrections.JER_CORRECTION_SETS[key].keys():
                        for corr in JetCorrections.JER_CORRECTION_SETS[key][key2]:
                            SAVEDFILES.append(os.path.join(codedir,"Framework","data","JRDatabase","textFiles",corr))
            continue
        SAVEDFILES.append(os.path.join(codedir,"Framework","data",sdir))

    fTAR = tarfile.open(os.path.join(subdir,filename),'x:gz')
    fTAR.add(os.path.join(os.path.dirname(codedir),".git"),"NanoAnalysis/.git")
    for f in SAVEDFILES:
        match = nano_re.search(f)
        if match:
            fTAR.add(f,os.path.join("NanoAnalysis/NanoAODAnalysis",match.group("file")))
            #print("Adding file",f)
    fTAR.close()
    #print("Making analysis tarball done.")
    return codedir

def makeGridJob(subdir,filename,opts):
    binddir = "-B ${PWD}:/work"
    if len(opts.map) > 0:
        binddir = "-B ${PWD}:/work,%s"%opts.map
    fOUT = open(os.path.join(subdir,filename),"w")
    fOUT.write("#!/usr/bin/sh\n")
    fOUT.write("\n")
    fOUT.write("echo Begin job\n")
    fOUT.write("pwd\n")
    if opts.image == "":
        fOUT.write("singularity run -i %s docker://coffeateam/coffea-dask bash -c /work/job.sh\n"%binddir)
    else:
        fOUT.write("singularity run -i %s %s bash -c /work/job.sh\n"%(binddir,opts.image))
    fOUT.write("\n")
    fOUT.write('cp `find ./NanoAnalysis -name "histograms.root"` .\n')
    fOUT.write("pwd\n")
    fOUT.write("echo Job finished\n")
    fOUT.close()
    os.chmod(os.path.join(subdir,filename),0o755)

def makeShellJob(subdir,filename,datafile):
    fOUT = open(os.path.join(subdir,filename),"w")
    fOUT.write("#!/usr/bin/sh\n")
    fOUT.write("\n")
    fOUT.write("cd /work\n")
    fOUT.write("\n")
    fOUT.write("tar xfz /work/analysis.tgz\n")
    fOUT.write("tar xfz /work/%s\n"%datafile)
    fOUT.write("\n")
    fOUT.write("export X509_USER_PROXY=`ls /work/x509up_u*`\n")
    fOUT.write("\n")
    analysisName = os.path.basename(os.getcwd())
    fOUT.write("cd /work/NanoAnalysis/NanoAODAnalysis/%s/\n"%analysisName)
    fOUT.write("sed -i 's/^\([[:blank:]]*whitelist = \)\[.*\]/\\1 []/g' analysis.py\n")
    fOUT.write("sed -i 's/^\([[:blank:]]*blacklist = \)\[.*\]/\\1 []/g' analysis.py\n")
    fOUT.write("sed -i 's/^\([[:blank:]]*MAXEVENTS = \).*/\\1 -1/g' analysis.py\n")
    fOUT.write("\n")
    fOUT.write("python analysis.py /work/multicrab\n")
    fOUT.write("cd /work\n")
    fOUT.write('mv `find /work/NanoAnalysis/NanoAODAnalysis -name "histograms.root"` .\n')
    fOUT.write("\n")
    fOUT.close()
    os.chmod(os.path.join(subdir,filename),0o755)

def makeGridCfg(subdir,filename,outputdir,datafile,jobname,proxyfile,opts):
    fOUT = open(os.path.join(subdir,filename),"w")
    fOUT.write('& (executable=grid.job)\n')
    fOUT.write('(jobname = "%s")\n'%jobname)
    fOUT.write('(stdout=analysis.out)\n')
    fOUT.write('(stderr=analysis.err)\n')
    if opts.image == "":
        fOUT.write('(inputfiles= ("grid.job" "") ("job.sh" "") ("%s" "") ("%s" "") ("%s" ""))\n'%("analysis.tgz",datafile,os.path.basename(proxyfile)))
        fOUT.write('(executables= grid.job job.sh)\n')
    else:
        fOUT.write('(inputfiles= ("grid.job" "") ("job.sh" "") ("%s" "") ("%s" "") ("%s" "") ("%s" ""))\n'%("analysis.tgz",datafile,os.path.basename(proxyfile),opts.image))
        fOUT.write('(executables= grid.job job.sh %s)\n'%(opts.image))
    fOUT.write('(outputfiles=("histograms.root" ""))\n')
    fOUT.write('(gmlog=glidlog)\n')
    fOUT.write('(cputime="24 hours")\n')
    fOUT.write('(memory=64)\n')
    fOUT.write('(disk=1)\n')
    fOUT.write('\n')
    fOUT.close()

def gridSubmit(subdir):
    if not os.path.exists(os.path.join(subdir,"jobid.txt")):
        cmd = "cd %s && %s"%(subdir,GRIDSUBMIT)
        submit_string = execute(cmd)
        print("   ",submit_string[0])
        fOUT = open(os.path.join(subdir,"jobid.txt"),"w")
        for s in submit_string:
            fOUT.write("%s\n"%s)
        fOUT.close()

def gridStatus(subdir):
    jobidfile = os.path.join(subdir,'jobid.txt')
    if not os.path.exists(jobidfile):
        print("No jobid found in %s"%subdir)
        return

    gsiftp = execute("grep gsiftp %s"%(jobidfile))
    if len(gsiftp) == 0:
        print("No gsiftp found in %s"%subdir)
        return

    gsiftp_re = re.compile("gsiftp://\S+")
    match = gsiftp_re.search(gsiftp[0])
    if match:
        gsiftp = match.group(0)

    cmd = "%s %s"%(GRIDSTATUS,gsiftp)
    status_string = execute(cmd)
    #print(status_string)
    return status_string

def gridGet(subdir):
    jobidfile = os.path.join(subdir,'jobid.txt')
    if not os.path.exists(jobidfile):
        print("No jobid found in %s"%subdir)
        return

    gsiftp = execute("grep gsiftp %s"%(jobidfile))
    gsiftp_re = re.compile("gsiftp://\S+")
    match = gsiftp_re.search(gsiftp[0])
    if match:
        gsiftp = match.group(0)

    cmd = "cd %s && %s %s"%(subdir,GRIDGET,gsiftp)
    os.system(cmd)

def findhisto(subdir):
    jobidfile = os.path.join(subdir,'jobid.txt')
    gsiftp = execute("grep gsiftp %s"%(jobidfile))
    print("gsiftp",gsiftp)
    if len(gsiftp) > 0:
        gsiftp_re = re.compile("gsiftp://\S+/jobs/(?P<id>\S+)")
        match = gsiftp_re.search(gsiftp[0])
        jobid = ""
        if match:
            jobid = match.group("id")

        rootfile = os.path.join(subdir,jobid,'histograms.root')
        if os.path.exists(rootfile):
            return rootfile
    return None

def printStatus(status):
    format_string = "{} {} {} {} {} {} {} {}"
    format_space = 9
    dataset = "Dataset"
    while len(dataset) < 30:
            dataset += ' '
    print(format_string.format(dataset,"all".rjust(format_space),"submitted".rjust(format_space),"queuing".rjust(format_space),"finished".rjust(format_space),"failed".rjust(format_space),"idle".rjust(format_space),"retrieved".rjust(format_space)))
    for key in status.keys():
        counter_all = 0
        counter_submitted = 0
        #Preparing
        counter_queuing = 0
        counter_finished = 0
        counter_failed = 0
        counter_idle = 0
        counter_done = 0

        for reportlist in status[key]:
            counter_all += 1
            if reportlist == None:
                continue
            if len(reportlist) < 3:
                continue

            counter_submitted += 1

            if 'State: Queuing' in reportlist[2]:
                counter_queuing += 1
            if 'State: Finished' in reportlist[2]:
                counter_finished += 1
            if 'State: Failed' in reportlist[2]:
                counter_failed += 1
            if 'Done' in reportlist[2]:
                counter_done += 1

        dataset = key
        while len(dataset) < 30:
            dataset += ' '
        print(format_string.format(dataset,repr(counter_all).rjust(format_space),repr(counter_submitted).rjust(format_space),repr(counter_queuing).rjust(format_space),repr(counter_finished).rjust(format_space),repr(counter_failed).rjust(format_space),repr(counter_idle).rjust(format_space),repr(counter_done).rjust(format_space)))

def create(multicrabdir, opts, args):
    datasets = multicrabdatasets.getDatasets(multicrabdir,whitelist=opts.whitelist,blacklist=opts.blacklist)
    pileupfile_data = multicrabdatasets.getDataPileupFile(multicrabdir,datasets)
    lumijson = os.path.join(multicrabdir,"lumi.json")
    year = multicrabdatasets.getYear(multicrabdir)

    starttime = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
    outputdir = os.path.basename(os.path.abspath(multicrabdir))+"_processed"+starttime+opts.lepton+"_GRID"
    os.makedirs(outputdir)

    proxyfile = copy_proxy(outputdir)
    os.system("cp %s %s"%(proxyfile,outputdir))

    if len(opts.image) > 0:
        os.system("cp %s %s"%(opts.image,outputdir))

    makeAnalysisTarBall(outputdir,"analysis.tgz",year,opts)
    copy_task(outputdir)

    for i,d in enumerate(datasets):
        print("Dataset %s/%s %s"%(i+1,len(datasets),d.name))
        files = []
        filegroups = 0
        nfiles = opts.nfiles
        if not d.isData:
            nfiles = opts.nmcfiles
        allfilegroups = math.ceil(len(d.files)/nfiles)
        print("    nfiles",len(d.files))
        for f in d.files:
            files.append(f)
            if len(files) >= nfiles or f == d.files[-1]:
                filegroups+=1
                subdir = os.path.join(outputdir,d.name,"task%s"%filegroups)
                if not os.path.exists(subdir):
                    os.makedirs(subdir)

                lumimask = LumiMask.FindLumiJSON(d)
                datafile = "data_%s.tgz"%filegroups
                if not opts.nodata:
                    makeDataTarBall(subdir,datafile,files,pileupfile_data,lumijson,lumimask,d,opts)
                makeGridJob(subdir,"grid.job",opts)
                makeShellJob(subdir,"job.sh",datafile)
                jobname = "%s %s %s/%s"%(os.path.basename(subdir),d.name,filegroups,allfilegroups)
                makeGridCfg(subdir,"grid.xrsl",outputdir,datafile,jobname,proxyfile,opts)
                files = []
    print(outputdir)

def submit(multicrabdir, opts, args):
    datasets = execute("ls %s"%multicrabdir)
    datasets = list(filter(lambda x: os.path.isdir(os.path.join(multicrabdir,x)), datasets))
    for d in datasets:
        print(d)
        subdirs = execute("ls %s"%os.path.join(multicrabdir,d))
        for subdir in subdirs:
            if 'task' in subdir:
                gridSubmit(os.path.join(multicrabdir,d,subdir))

def main(opts, args):

    proxy()

    multicrabdir = ""
    for arg in args:
        if os.path.isdir(arg) and "multi" in arg:
           multicrabdir = arg

    if len(multicrabdir) == 0:
       print("Multicrabdir not found. Given args:",args)
       usage()

    if not opts.get and not opts.submit and 'processed20' in multicrabdir:
        opts.status = True

    if opts.status or opts.get:
        status = {}
        datasets = execute("ls %s"%(multicrabdir))
        for d in datasets:
            cand = os.path.join(os.path.abspath(multicrabdir),d)
            if not os.path.isdir(cand):
                continue
            status[d] = []
            taskdirs = execute("ls %s"%(cand))
            histograms = []
            if os.path.exists(os.path.join(cand,"results","histograms.root")):
                print("Dataset %s Done"%(cand))
                status[d] = ["","","Done"]
                continue

            for taskdir in taskdirs:
                if not 'task' in taskdir:
                    continue

                if opts.status:
                    status[d].append(gridStatus(os.path.join(cand,taskdir)))
                if opts.get:
                    gridGet(os.path.join(cand,taskdir))

                histo = findhisto(os.path.join(cand,taskdir))
                if histo != None:
                    histograms.append(histo)

            # if all done, merge
            if len(histograms) > 0 and len(histograms) == len(taskdirs):
                respath = os.path.join(os.path.abspath(multicrabdir),d,"results")
                if not os.path.exists(respath):
                    os.mkdir(respath)
                cmd = "hadd -v 0 %s/histograms.root"%respath
                for h in histograms:
                    cmd += " %s"%h
                print(cmd)
                os.system(cmd)
        printStatus(status)

    if not "_processed" in multicrabdir:
        opts.create = True

    if opts.create:
        create(multicrabdir, opts, args)

    if opts.submit:
        submit(multicrabdir, opts, args)


if __name__ == "__main__":
    parser = OptionParser(usage="Usage: %prog <multicrabdir> [options]")

    PARTICLE = 13
    NFILES = 25

    parser.add_option("--nfiles", dest="nfiles", default=NFILES, type="int",
                      help="Number of rootfiles run together. Set -1 for the entire dataset. [default: %s]"%NFILES)
    parser.add_option("--nmcfiles", dest="nmcfiles", default=NFILES, type="int",
                      help="Number of MC rootfiles run together. Set -1 for the entire dataset. [default: %s]"%NFILES)
    parser.add_option("--lepton", dest="lepton", default="%s"%PARTICLE, type="string",
                      help="Lepton selection 13 == muon and 11 == electron. [default: %s]"%PARTICLE)
    parser.add_option("--whitelist",dest="whitelist", default="", type="string",
                      help="Whitelist datasets")
    parser.add_option("--blacklist",dest="blacklist", default="", type="string",
                      help="Blacklist datasets")
    parser.add_option("--list", dest="list", default=False, action="store_true",
                      help="Write a list of root files in text files [defaut: False]")
    parser.add_option("--create", dest="create", default=False, action="store_true",
                      help="Create the grid jobs [defaut: False]")
    parser.add_option("--submit", dest="submit", default=False, action="store_true",
                      help="Submit the grid jobs [defaut: False]")
    parser.add_option("--status", dest="status", default=False, action="store_true",
                      help="Get job status [defaut: False]")
    parser.add_option("--get", dest="get", default=False, action="store_true",
                      help="Get job result files [defaut: False]")
    parser.add_option("--map", "--mount",dest="map", default="", type="string",
                      help="Mount singularity dir to local dir")
    parser.add_option("--nodata", dest="nodata", default=False, action="store_true",
                      help="Create without the data tarball [defaut: False]")
    parser.add_option("--image", dest="image", default="", type="string",
                      help="Use local singularity image")
    (opts, args) = parser.parse_args()

    if opts.list and len(opts.map) == 0:
        print("Warning, using option --list, but not mounting datadir to singularity")

    if opts.nfiles < opts.nmcfiles:
        opts.nmcfiles = opts.nfiles

    opts.whitelist = opts.whitelist.split(',')
    opts.blacklist = opts.blacklist.split(',')
    if '' in opts.whitelist:
        opts.whitelist = opts.whitelist.remove('')
    if '' in opts.blacklist:
        opts.blacklist = opts.blacklist.remove('')

    main(opts, args)
