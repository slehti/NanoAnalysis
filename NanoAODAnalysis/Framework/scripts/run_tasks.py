#!/usr/bin/env python

import os,sys,re
import subprocess
import threading

def usage():
    print
    print("### Usage:   ",os.path.basename(sys.argv[0])," <pseudomulticrab dir|crab dir>")
    print
    sys.exit()

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    f = p.stdout
    ret=[]
    for line in f:
        line = line.decode('utf-8')
        #sys.stdout.write(line)                                                                                             
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def runtask(taskdir):
    cmd = "cd %s && ./grid.job"%taskdir
    os.system(cmd)

def main(opts, args):

    pseudomulticrab = sys.argv[1]
    cands = execute("ls %s"%pseudomulticrab)
    datasets = []

    if len(opts.whitelist) > 0:
        datasets_whitelist = []
        for d in cands:
            for wl in opts.whitelist:
                wl_re = re.compile(wl)
                match = wl_re.search(d)
                if match:
                    datasets_whitelist.append(d)
                    break
        cands = datasets_whitelist

    if len(opts.blacklist) > 0:
        datasets_blacklist = []
        for d in cands:
            found = False
            for bl in opts.blacklist:
                bl_re = re.compile(bl)
                match = bl_re.search(d)
                if match:
                    found = True
                    break
            if not found:
                datasets_blacklist.append(d)
        cands = datasets_blacklist

    for cand in cands:
        if "task" in cand:
            continue
        if os.path.isdir(os.path.join(pseudomulticrab,cand)):
            datasets.append(os.path.join(pseudomulticrab,cand))
    if len(datasets) == 0:
        datasets.append(pseudomulticrab)

    for d in datasets:
        if os.path.exists(os.path.join(d,"results","histograms.root")):
            continue
        taskdirs = []
        cands = execute("ls %s"%d)
        for cand in cands:
            if not "task" in cand:
                cands.remove(cand)
                continue
            if os.path.exists(os.path.join(d,cand,"histograms.root")):
                continue

            thread = threading.Thread(target=runtask, args=(os.path.join(d,cand),))
            thread.start()
            thread.join()

        # if all done, merge
        histograms = []
        for cand in cands:
            if not "task" in cand:
                continue
            if os.path.exists(os.path.join(d,cand,"histograms.root")):
                histograms.append(os.path.join(d,cand,"histograms.root"))
        if len(histograms) > 0 and len(histograms) == len(cands):
            respath = os.path.join(d,"results")
            if not os.path.exists(respath):
                os.mkdir(respath)
            cmd = "hadd -v 0 %s/histograms.root"%respath
            for h in histograms:
                cmd += " %s"%h
            print(cmd)
            os.system(cmd)
                
        
if __name__=="__main__":
    from optparse import OptionParser

    parser = OptionParser(usage="Usage: %prog [options]")
    parser.add_option("-i", "--includeTasks", dest="whitelist", default="", type="string",
                      help="Comma separated list of included datasets [default: \"\"]")

    parser.add_option("-e", "--excludeTasks", dest="blacklist", default="", type="string",
                      help="Comma separated list of excluded datasets [default: \"\"]")

    (opts, args) = parser.parse_args()

    if len(opts.whitelist) > 0:
        opts.whitelist = opts.whitelist.split(',')
        if '' in opts.whitelist:
            opts.whitelist = opts.whitelist.remove('')
    if len(opts.blacklist) > 0:
        opts.blacklist = opts.blacklist.split(',')
        if '' in opts.blacklist:
            opts.blacklist = opts.blacklist.remove('')

    main(opts, args)

