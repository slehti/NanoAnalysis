#!/usr/bin/env python

import argparse
import os
import sys
import re
import subprocess
import json
import datetime
import tarfile
import math
from glob import glob


from nanoanalysis import LumiMask
from nanoanalysis import multicrabdatasets

GRIDPROXY  = "arcproxy --info"
GRIDSUBMIT_ELEMENT= "arcsub -c {ce} grid.xrsl"
GRIDSUBMIT_GLOBAL = "arcsub --registry nordugrid.org grid.xrsl"
GRIDSTATUS = "/usr/bin/arcstat"
GRIDGET    = "arcget"
PARTICLE = 13
NFILES = 25

certsfile = "grid-security.tgz"

def parse_args():
    parser = argparse.ArgumentParser(
        usage="Usage: %(prog)s <multicrabdir> [options]",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument("multicrabdir", help="Directory of multicrab project")

    parser.add_argument("--nfiles", default=NFILES, type=int,
                        help=f"Number of rootfiles run together. Set -1 for the entire dataset. [default: {NFILES}]")
    parser.add_argument("--nmcfiles", default=NFILES, type=int,
                        help=f"Number of MC rootfiles run together. Set -1 for the entire dataset. [default: {NFILES}]")
    parser.add_argument("--lepton", default=str(PARTICLE), type=str,
                        help=f"Lepton selection 13 == muon and 11 == electron. [default: {PARTICLE}]")
    parser.add_argument("--whitelist", default=[], type=lambda s: s.split(','),
                        help="Whitelist datasets, separated by commas")
    parser.add_argument("--blacklist", default=[], type=lambda s: s.split(','),
                        help="Blacklist datasets, separated by commas")
    parser.add_argument("--list", default=False, action="store_true",
                        help="Write a list of root files in text files [default: False]")
    parser.add_argument("--create", default=False, action="store_true",
                        help="Create the grid jobs [default: False]")
    parser.add_argument("--submit", default=False, action="store_true",
                        help="Submit the grid jobs [default: False]")
    parser.add_argument("--status", default=False, action="store_true",
                        help="Get job status [default: False]")
    parser.add_argument("--get", default=False, action="store_true",
                        help="Get job result files [default: False]")
    parser.add_argument("--map", "--mount", default="", type=str,
                        help="Mount singularity dir to local dir")
    parser.add_argument("--nodata", default=False, action="store_true",
                        help="Create without the data tarball [default: False]")
    parser.add_argument("--image", default="", type=str,
                        help="Use local singularity image (overrides --image-url)")
    parser.add_argument("--image-url", default="", type=str,
                        help="Singularity-compatible address to publicly available image")
    parser.add_argument("--analysis", default="Hplus2taunuAnalysis", type=str,
                        help="Which analysis directory to run")
    parser.add_argument("-c", "--computing-element", default="kale-cms.grid.helsinki.fi", type=str,
                        help="Computing element to submit jobs to")
    parser.add_argument("--certs", default="/etc/grid-security/", type=str,
                        help="Local grid-security directory containing CA certificates")
    parser.add_argument("--link", default=False, action="store_true",
                        help="Link files for local processing instead of copying [default: False]")
    parser.add_argument("--global", dest="global_submit", default=False, action="store_true",
                        help="Use any computing element that you have permission to on nordugrid.org")

    args, unknown = parser.parse_known_args()
    unknown = " ".join(unknown)
    return args, unknown

def usage():
    os.system("%s --help"%sys.argv[0])
    sys.exit()

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    f = p.stdout
    ret=[]
    for line in f:
        line = line.decode('utf-8')
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def proxy():
    proxyResult = execute(GRIDPROXY)
    timeleft_re = re.compile("Time left for \\S+: (?P<time>.*)")
    for line in proxyResult:
        match = timeleft_re.search(line)
        if match:
            print("Time left for proxy:",match.group("time"))
            if "expired" in match.group("time"):
                sys.exit()

def copy_proxy(destinationpath):
    proxyResult = execute(GRIDPROXY)
    proxypath_re = re.compile("Proxy path: (?P<path>\\S+)")
    for line in proxyResult:
        match = proxypath_re.search(line)
        if match:
           cmd = "cp %s %s"%(match.group("path"),destinationpath)
           #print(cmd)
           os.system(cmd)
           return match.group("path")
    return None

def makeDataTarBall(subdir,filename,files,pileup_data,lumijson,lumimask,dataset,opts):
    # print("    Making data tarball",subdir)
    cdir = dataset.name
    multigrabdir = "multicrab"
    fTAR = tarfile.open(os.path.join(subdir,filename),'x:gz')
    #fTAR.add(proxy,os.path.basename(proxy))
    if 'root' in files[0][:4] or opts.list:
        dict = {}
        dict['files'] = files
        f = open(os.path.join(subdir,"files_%s.json"%cdir), "w")
        json.dump(dict, f, sort_keys=True, indent=2)
        f.close()
        fTAR.add(os.path.join(subdir,"files_%s.json"%cdir),os.path.join(multigrabdir,cdir,"results","files_%s.json"%cdir))
    else:
        for f in files:
            fTAR.add(f,os.path.join(multigrabdir,cdir,"results",os.path.basename(f)))
    fTAR.add(pileup_data,os.path.join(multigrabdir,os.path.basename(pileup_data)))
    fTAR.add(lumijson,os.path.join(multigrabdir,os.path.basename(lumijson)))
    if dataset.isData:
        fTAR.add(lumimask,os.path.join(multigrabdir,cdir,"inputs",os.path.basename(lumimask)))
    if os.path.exists(dataset.getPileupfile()):
        fTAR.add(dataset.getPileupfile(),os.path.join(multigrabdir,cdir,"results",os.path.basename(dataset.getPileupfile())))

    fTAR.close()
    # print("Making data tarball done.")

def makeCertTarBall(file,certdir):
    fTAR = tarfile.open(file,"x:gz")
    fTAR.add(certdir,"grid-security/")
    fTAR.close()
    return file

def makeGridJob(subdir,filename,opts):
    binddir = "-B ${PWD}:/work"
    if len(opts.map) > 0:
        binddir = "-B ${PWD}:/work,%s"%opts.map
    fOUT = open(os.path.join(subdir,filename),"w")
    fOUT.write("#!/usr/bin/sh\n")
    fOUT.write("\n")
    fOUT.write("echo Begin job\n")
    fOUT.write("pwd\n")
    if opts.image == "":
        image = opts.image_url
    else:
        image = "image.sif"
    fOUT.write("singularity run -i %s %s /work/job.sh\n"%(binddir,image))
    fOUT.write("\n")
    #fOUT.write('cp `find . -name "histograms*.root"` .\n')
    fOUT.write("pwd\n")
    fOUT.write("echo Job finished\n")
    fOUT.close()
    os.chmod(os.path.join(subdir,filename),0o755)

def makeShellJob(subdir,filename,datafile,analysistype,passthru):
    fOUT = open(os.path.join(subdir,filename),"w")
    fOUT.write("#!/usr/bin/sh\n")
    fOUT.write("\n")
    fOUT.write("cd /work\n")
    fOUT.write("\n")
    fOUT.write("tar xfz /work/%s\n"%datafile)
    fOUT.write(f"tar xfz /work/{certsfile}\n")
    fOUT.write("\n")
    fOUT.write("export X509_USER_PROXY=`ls /work/x509up_u*`\n")
    fOUT.write("export X509_CERT_DIR=/work/grid-security/certificates/\n")
    fOUT.write("export X509_VOMS_DIR=/work/grid-security/vomsdir/\n")
    fOUT.write("\n")
    # analysisName = os.path.basename(os.getcwd())
    fOUT.write("cd /work\n")
    fOUT.write(
        f"python /NanoAnalysis/NanoAODAnalysis/{analysistype}/analysis.py /work/multicrab {passthru}\n"
    )
    # following collapses every subdirectory of the multicrab, leaving only the name of the multicrab
    # this is needed for cases where the analysis writes multiple result pseudos (Categories, control regions)
    fOUT.write(
        r'''find . -name "histograms*.root" -exec sh -c'''
        r''' 'new=$(echo "{}" | sed "s/\.\/\([^/]\+\).\+.\(histograms.\+\)\.root/\2-\1.root/");'''
        r''' mv -i "{}" "$new"' \;'''
    )
    fOUT.write("\n")
    fOUT.write("ls histograms*.root > output.files")
    fOUT.write("\n")
    fOUT.close()
    os.chmod(os.path.join(subdir,filename),0o755)

def makeGridCfg(subdir,filename,datafile,jobname,proxyfile,certsfile,opts):
    fOUT = open(os.path.join(subdir,filename),"w")
    fOUT.write('& (executable=grid.job)\n')
    fOUT.write('(jobname = "%s")\n'%jobname)
    fOUT.write('(stdout=analysis.out)\n')
    fOUT.write('(stderr=analysis.err)\n')
    proxyname = os.path.basename(proxyfile)
    certsname = os.path.basename(certsfile)
    inputfiles = f'(inputfiles= ("grid.job" "") ("job.sh" "") ("{datafile}" "")'
    inputfiles += f' ("{proxyname}" "") ("{certsname}" ""))'
    executables = '(executables= grid.job job.sh)'
    if len(opts.image):
        imagename = os.path.basename(opts.image)
        inputfiles = inputfiles[:-1] + f' ("image.sif" "../../{imagename}"))'
        executables = executables[:-1] + ' image.sif)'
    fOUT.write(inputfiles + '\n')
    fOUT.write(executables + '\n')
    fOUT.write('(outputfiles=("@output.files" ""))\n')
    fOUT.write('(gmlog=glidlog)\n')
    fOUT.write('(cputime="24 hours")\n')
    fOUT.write('(memory=16000)\n')
    fOUT.write('(disk=100)\n')
    fOUT.write('\n')
    fOUT.close()

def gridSubmit(subdir, args):
    if not os.path.exists(os.path.join(subdir,"jobid.txt")):
        if args.global_submit:
            submit_cmd = GRIDSUBMIT_GLOBAL
        else:
            submit_cmd = GRIDSUBMIT_ELEMENT.format(ce=args.computing_element)
        cmd = f"cd {subdir} && {submit_cmd}"
        submit_string = execute(cmd)
        print("   ",submit_string[0])
        fOUT = open(os.path.join(subdir,"jobid.txt"),"w")

        for s in submit_string:
            fOUT.write("%s\n"%s)
        fOUT.close()

def gridCmd(subdir, cmd):
    jobidfile = os.path.join(subdir,'jobid.txt')
    if not os.path.exists(jobidfile):
        print("No jobid found in %s"%subdir)
        return

    protocols = ["gsiftp", "https"]
    ret = None
    protocol = None
    for p in protocols:
        ret = execute(f"grep {p} {jobidfile}")
        if len(ret):
            protocol = p
            break
    if ret is None:
        print("No submission string found in %s"%subdir)
        return

    addr_re = re.compile(f"{protocol}://\\S+")
    match = addr_re.search(ret[0])
    if match:
        addr = match.group(0)
        cmd_with_target = f"{cmd} {addr}"
        ret = execute(cmd_with_target)
        return ret
    else:
        raise RuntimeError(f"Could not parse job address from {jobidfile}")

def gridStatus(subdir):
    return gridCmd(subdir, GRIDSTATUS)

def gridGet(subdir):
    cmd = f"cd {subdir} && {GRIDGET}"
    return gridCmd(subdir, cmd)

def findhistos(subdir):
    jobidfile = os.path.join(subdir,'jobid.txt')
    gsiftp = execute("grep gsiftp %s"%(jobidfile))
    print("gsiftp",gsiftp)
    if len(gsiftp) > 0:
        gsiftp_re = re.compile(r"gsiftp://\S+/jobs/(?P<id>\S+)")
        match = gsiftp_re.search(gsiftp[0])
        jobid = ""
        if match:
            jobid = match.group("id")

        rootfiles = glob(os.path.join(subdir,jobid,'/*/histograms*.root'))
        if len(rootfiles):
            return list(rootfiles)

    return []

def printStatus(status):
    format_string = "{} {} {} {} {} {} {} {}"
    format_space = 9
    dataset = "Dataset"
    while len(dataset) < 30:
            dataset += ' '
    print(format_string.format(dataset,"all".rjust(format_space),"submitted".rjust(format_space),"queuing".rjust(format_space),"finished".rjust(format_space),"failed".rjust(format_space),"idle".rjust(format_space),"retrieved".rjust(format_space)))
    for key in status.keys():
        counter_all = 0
        counter_submitted = 0
        #Preparing
        counter_queuing = 0
        counter_finished = 0
        counter_failed = 0
        counter_idle = 0
        counter_done = 0

        for reportlist in status[key]:
            counter_all += 1
            if reportlist == None:
                continue
            if len(reportlist) < 3:
                continue

            counter_submitted += 1

            if 'State: Queuing' in reportlist[2]:
                counter_queuing += 1
            if 'State: Finished' in reportlist[2]:
                counter_finished += 1
            if 'State: Failed' in reportlist[2]:
                counter_failed += 1
            if 'Done' in reportlist[2]:
                counter_done += 1

        dataset = key
        while len(dataset) < 30:
            dataset += ' '
        print(format_string.format(dataset,repr(counter_all).rjust(format_space),repr(counter_submitted).rjust(format_space),repr(counter_queuing).rjust(format_space),repr(counter_finished).rjust(format_space),repr(counter_failed).rjust(format_space),repr(counter_idle).rjust(format_space),repr(counter_done).rjust(format_space)))

def create(multicrabdir, args, passthru):
    datasets = multicrabdatasets.getDatasets(multicrabdir,whitelist=args.whitelist,blacklist=args.blacklist)
    pileupfile_data = multicrabdatasets.getDataPileupFile(multicrabdir,datasets)
    lumijson = os.path.join(multicrabdir,"lumi.json")
    # year = multicrabdatasets.getYear(multicrabdir)

    starttime = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
    outputdir = os.path.basename(os.path.abspath(multicrabdir))+"_processed"+starttime+args.lepton+"_GRID"
    os.makedirs(outputdir)

    proxyfile = copy_proxy(outputdir)
    os.system("cp %s %s"%(proxyfile,outputdir))

    if len(args.image) > 0:
        os.system("cp %s %s"%(args.image,outputdir))

    certfile = makeCertTarBall(os.path.join(outputdir,certsfile), args.certs)

    for i,d in enumerate(datasets):
        print("Dataset %s/%s %s"%(i+1,len(datasets),d.name))
        files = []
        filegroups = 0
        nfiles = args.nfiles
        if not d.isData:
            nfiles = args.nmcfiles
        allfilegroups = math.ceil(len(d.files)/nfiles)
        print("    nfiles",len(d.files))
        for f in d.files:
            files.append(f)
            if len(files) >= nfiles or f == d.files[-1]:
                filegroups+=1
                subdir = os.path.join(outputdir,d.name,"task%s"%filegroups)
                if not os.path.exists(subdir):
                    os.makedirs(subdir)

                lumimask = LumiMask.FindLumiJSON(d)
                datafile = "data_%s.tgz"%filegroups
                if not args.nodata:
                    makeDataTarBall(subdir,datafile,files,pileupfile_data,lumijson,lumimask,d,args)
                makeGridJob(subdir,"grid.job",args)
                makeShellJob(subdir,"job.sh",datafile, args.analysis, passthru)
                jobname = "%s %s %s/%s"%(os.path.basename(subdir),d.name,filegroups,allfilegroups)
                makeGridCfg(subdir,"grid.xrsl",datafile,jobname,proxyfile,certfile,args)
                os.system("cp %s %s"%(proxyfile,subdir))
                os.system("cp %s %s"%(certfile,subdir))
                files = []
    print(outputdir)

def submit(multicrabdir, args):
    datasets = execute("ls %s"%multicrabdir)
    datasets = list(filter(lambda x: os.path.isdir(os.path.join(multicrabdir,x)), datasets))
    for d in datasets:
        print(d)
        subdirs = execute("ls %s"%os.path.join(multicrabdir,d))
        for subdir in subdirs:
            if 'task' in subdir:
                gridSubmit(os.path.join(multicrabdir,d,subdir),args)

def check_for_histo(directory):
    matches = glob(os.path.join(directory,"/*/histograms*.root"))
    return len(matches)

def main(args, passthru):

    proxy()

    multicrabdir = args.multicrabdir

    if not args.get and not args.submit and 'processed20' in multicrabdir:
        args.status = True

    if args.status or args.get:
        status = {}
        datasets = execute("ls %s"%(multicrabdir))
        for d in datasets:
            cand = os.path.join(os.path.abspath(multicrabdir),d)
            if not os.path.isdir(cand):
                continue
            status[d] = []
            taskdirs = execute("ls %s"%(cand))
            histograms = []
            if check_for_histo(os.path.join(cand,"results")):
                print("Dataset %s Done"%(cand))
                status[d] = ["","","Done"]
                continue

            for taskdir in taskdirs:
                if not 'task' in taskdir:
                    continue

                if args.status:
                    status[d].append(gridStatus(os.path.join(cand,taskdir)))
                if args.get:
                    gridGet(os.path.join(cand,taskdir))

                histos = findhistos(os.path.join(cand,taskdir))
                if len(histos):
                    histograms.append(histos)

            # if all done, merge
            if len(histograms) > 0 and len(histograms) == len(taskdirs):
                for i in range(len(histograms[0])):
                    first_task_path = histograms[0][i]
                    first_task_mcrab = os.path.basename(os.path.dirname(first_task_path))
                    histoname = os.path.basename(first_task_path)
                    basepath = os.path.dirname(os.path.abspath(multicrabdir))
                    respath = os.path.join(basepath, first_task_mcrab)

                    respath = os.path.join(os.path.abspath(multicrabdir),d,"results")
                    if not os.path.exists(respath):
                        os.mkdir(respath)
                    cmd = f"hadd -v 0 {respath}/{histoname}"
                    for h in histograms:
                        cmd += " %s"%h[i]
                    print(cmd)
                    os.system(cmd)
        printStatus(status)

    if not "_processed" in multicrabdir:
        args.create = True

    if args.create:
        create(multicrabdir, args, passthru)

    if args.submit:
        submit(multicrabdir,args)


if __name__ == "__main__":
    args, unknown_args = parse_args()

    if args.list and len(args.map) == 0:
        print("Warning, using option --list, but not mounting datadir to singularity")

    if args.nfiles < args.nmcfiles:
        args.nmcfiles = args.nfiles

    main(args, unknown_args)

