import os
from setuptools import find_packages

args = dict(
    name="NanoAnalysisFramework",
    version="0.0.2",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    include_package_data=True,
    package_data = {
        "nanoanalysis.data": ["**.txt"],
        "nanoanalysis.include": ["*.h"],
    },
    cmake_source_dir=".",
    cmake_install_dir="src/nanoanalysis/rdf_backend"
)

if 'TF_SRC' in os.environ:
    from skbuild import setup
else:
    from setuptools import setup
    args.pop("cmake_source_dir")
    args.pop("cmake_install_dir")

setup(**args)

