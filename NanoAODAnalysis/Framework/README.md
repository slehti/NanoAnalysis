# NanoAnalysisFramework

Useful tools for data analysis and machine learning with CERN RDataFrames and `coffea`. RDataFrame extensions and machine learning features using tensorflow and C++ are optionally installed.

## Installation

`NanoAnalysisFramework` can be installed in two configurations: a default installation and an extended installation, depending on your analysis needs.

### Default Installation

The default installation does not include C++ extensions or machine learning capabilities. It is intended for use with `coffea` analyses. Latest development is focused on the RDataFrame version.

To install the default configuration, run:

    pip install .

### Extended Installation for RDataFrame Analyses and Machine Learning

The extended installation includes support for CERN RDataFrames and machine learning functionality. It adds C++ extensions for use as common RDataFrame Filter and Define functions.

**Requirements:**
- The TensorFlow source code should be available in a directory specified by the environment variable `TF_SRC`.
- ROOT

To install the extended configuration, run:

    pip install .

The installation script automatically recognises extensions should be included in the installation when `TF_SRC` is defined.

### Environment Variable `TF_SRC`

When installing the extended version, set the environment variable `TF_SRC` to the directory containing the TensorFlow source code. This is necessary to compile the C++ extensions that support machine learning capabilities within the framework.

For example:

    export TF_SRC=/path/to/tensorflow-source

## Usage Notes

- **Default Installation**: Supports analyses using `coffea` but does not include machine learning functionality or RDataFrame support.
- **Extended Installation**: Adds support for RDataFrame and machine learning training/inference, requiring TensorFlow source code for C++ extensions.

## Installation from scratch

```bash
# Default installation inside current python environment
pip install coffea dask distributed bokeh
git clone https://gitlab.cern.ch/HPlus/NanoAnalysis.git
cd NanoAnalysis/NanoAODAnalysis/Framework
pip install .
```

```bash
# make sure pyROOT is available in your python environment
git clone https://github.com/tensorflow/tensorflow.git
export TF_SRC=$(pwd)/tensorflow

git clone https://gitlab.cern.ch/HPlus/NanoAnalysis.git
cd NanoAnalysis/NanoAODAnalysis/Framework
pip install .
```

## Prebuilt analysis environment using Docker image

Prebuilt analysis images are available on the cern gitlab docker registry,
including all dependencies for end-to-end analysis with remote file access.
Make sure to mount your voms proxy when using remote file access for your
datasets. For examples on how to make sure xrootd can read your proxy inside
a container, refer to the grid submission helpers in `scripts/`.

```bash
# For the coffea image
docker pull gitlab-registry.cern.ch/hplus/nanoanalysis:latest
```

```bash
# For the RDataFrame/ML image
docker pull gitlab-registry.cern.ch/hplus/nanoanalysis_rdf:latest
```

