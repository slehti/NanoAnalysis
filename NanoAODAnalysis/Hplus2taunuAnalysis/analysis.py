#!/usr/bin/env python

# grey out deprecation warnings and tensorflow information
print("\033[2;1m")
import gc
import ctypes
import sys
import os,re
import shutil
import getpass
import subprocess
import datetime
import multiprocessing
import time
import shutil
import getpass
import os.path
from functools import reduce
import operator
import json
from contextlib import nullcontext
import warnings
import argparse
import matplotlib.pyplot as plt
import numpy as np
import awkward as ak
import dask_awkward as dak

from coffea import processor
from coffea.dataset_tools import (
    apply_to_dataset,
    apply_to_fileset,
    preprocess,
    max_chunks
)
from coffea import analysis_tools
import coffea.lumi_tools as lumi_tools
import hist.dask as dhist
import dask
import hist
import uproot
from distributed import Client, LocalCluster
from distributed.diagnostics import MemorySampler

#try:
#    import tensorflow as tf
#except:
#    print("failed importing tensorflow, make sure you have it installed if you are running with neural networks!")

from nanoanalysis import multicrabdatasets
from nanoanalysis import PileupWeight
from nanoanalysis.LumiMask import FindLumiJSON
from nanoanalysis import Btag
from nanoanalysis import aux
from analysis_common import (
    parse_args,
    belongs_to,
    name_matches_cat,
    filter_counters,
    getFromDict,
    write_lumi_json,
    get_legacy_name,
    legacy_run_name,
    write_multicrab_cfg,
    navigate_subdirs,
    extend_QCDhist_name,
    SEPARATE,
    ANALYSISTYPE,
    QCD_DIRS,
)

import Hplus2taunuSelection as selection
import Hplus2taunuHistograms as Histograms
# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
ss = "\033[92m"
ns = "\033[0;0m"
ts = "\033[1;34m"
hs = "\033[0;35m"
ls = "\033[0;33m"
es = "\033[1;31m"
cs = "\033[0;44m\033[1;37m"


warnings.filterwarnings("ignore",module="coffea.nanoevents") #FIXME
print(ns)


PRINTED_COUNTERS = ["Inclusive"]

# DISTRIBUTED = False # uncomment if you want to force local execution, for example debugging purposes

class Analysis(processor.ProcessorABC):
    def __init__(self,d, pu_data, nn_path = None, mass_point = None, unblind=False, **analysis_kwargs):
    # def __init__(self,datasets, pu_data, nn_path = None, mass_point = None, **analysis_kwargs):
        self.unblind = unblind
        self.mass_point = mass_point
        self.use_m = "regress_mass" in analysis_kwargs
        self.nn_path = nn_path
        self.nn_kwargs = analysis_kwargs
        run = d.run
        self.year = d.year
        Byear = self.year
        if "APV" in run:
            Byear += "APV"
        self.btab = Btag.Btag("btagDeepB",Byear)
        self.isData = d.isData
        if self.isData:
            lumijson = FindLumiJSON(d)
            self.lumimask = lumi_tools.LumiMask(lumijson)
        else:
            self.pu_mc = d.getPileup()
            self.pu_weight = PileupWeight.PileupWeight(pu_data, self.pu_mc)

        # runs = {d.name: d.run for d in datasets}
        # self.years = {d.name: d.year for d in datasets}
        # Byears = {
        #     name: self.years[name] + "APV" if "APV" in runs[name] else self.years[name]
        #     for name in self.years
        # }
        # # self.btag = {n: Btag.Btag('btagDeepB',Byears[n]) for n in Byears}
        # self.isData = {d.name: d.isData for d in datasets}
        # nn_sele = None
        # if nn_path is not None:
            # nn = selection.ClassifierModel(nn_path, **analysis_kwargs)
            # self.nn_sele = nn.selection
            # use_m = nn.regress_mass


        # pu_mc   = {d.name: d.getPileup() for d in datasets if not self.isData[d.name]}
        # self.pu_weight = {
        #     name: PileupWeight.PileupWeight(pu_data,pu_mc[name])
        #     for name in pu_mc
        # }
        #self.pu_tables = {n: parsePileUpJSONasArray(self.years[n]) for n in self.names}

        # lumijsons = {d.name: FindLumiJSON(d) for d in datasets if d.isData}
        # self.lumimasks = {n: lumi_tools.LumiMask(lumijsons[n]) for n in lumijsons}
#        self.jetCorrections = JetCorrections.JEC(self.run,self.isData)
#        self.jetvetoMap = JetvetoMap.JetvetoMap(self.year,self.isData)

    def make_histogram(self, name, bins, variable_binning = False):
        if variable_binning:
            h = (
                hist.Hist.new
                .Var(bins, name="value", label=name)
                .Weight(label=name, name=name)
            )
        else:
            nbins, binmin, binmax = bins
            h = (
                hist.Hist.new
                .Reg(nbins, binmin, binmax, name="value", label=name,)
                .Weight(label=name, name=name)
            )
        return {name: h}

    def isolation_count(self, events):
        return events[events.TauIsolation > 0]

    def nn_selection(self, events, mass_hypot=None):
        model = selection.ClassifierModelClient(self.nn_path, **self.nn_kwargs)
        return model.selection(events, mass_hypot=mass_hypot)

    @dask.annotate(allow_other_workers=False)
    def process(self, events):

        out = {}
        #name = events.metadata["dataset"]
        counter = {"unweighted": {}, "weighted": {}}
        counter["unweighted"]["Control"] = 1
        counter["weighted"]["Control"] = 1
        isData = self.isData#[name]

        stdsele_histo =  Histograms.AnalysisHistograms(isData, after_std=True) # always unblinded
        out.update(stdsele_histo.book())
        fullsele_histo = Histograms.AnalysisHistograms(
            isData,
            use_nn=self.nn_path is not None,
            use_m=self.use_m,
            unblind=self.unblind,
        )
        out.update(fullsele_histo.book())

        set_count(counter,"Read events",events)
        # Weights
        eweight = analysis_tools.Weights(None,storeIndividual=True)
        if not isData:
            # take into account only the sign of the generator weight
            genw = np.sign(events['genWeight'])
            eweight.add('gen',genw)
            # eweight.weight() has an LRU cache which means that
            # IT MUST NOT BE CALLED BEFORE WEIGHTS ARE FINAL
            # this is why here we first use partial_weight
            events["weight"] = eweight.partial_weight(include=["gen"])
            set_count(counter,"gen weights",events)
            pu = self.pu_weight.getWeight(events.Pileup.nTrueInt)
            # pu = self.pu_weight[name].getWeight(events.Pileup.nTrueInt)
            eweight.add('pileup',pu)
            events["weight"] = eweight.weight()
            set_count(counter,"pu weights",events)

        # selection cuts
        set_count(counter,"All events",events)
        if isData:
            events = events[selection.lumimask(events,self.lumimask)]
        set_count(counter,"JSON filter",events)

        events = events[selection.triggerSelection(events, self.year)]
        set_count(counter,"passed trigger",events)

        events = events[selection.METCleaning(events, self.year)]
        set_count(counter,"MET cleaning",events)

        events = selection.tau_identification(events, isData)
        set_count(counter,"Tau event selection", events)

        events = selection.isolated_electron_veto(events)
        set_count(counter,"electron veto", events)

        events = selection.isolated_muon_veto(events)
        set_count(counter,"muon veto", events)

        events = selection.hadronic_pf_jets(events)
        out = stdsele_histo.fill(events, out)
        set_count(counter,"hadronic jet selection", events)

        events = selection.b_tagged_jets(events)
        set_count(counter,"b-jet selection",events)

        events = selection.met_cut(events)
        set_count(counter,"met selection",events)

        events = selection.Rbb_min(events)
        set_count(counter,"RBB-min selection",events)

        if self.nn_path is not None:
            events = self.nn_selection(events, mass_hypot=self.mass_point)
            if not isData:
                set_count(counter,"DNN",events)

        out["counter"] = counter
        out = fullsele_histo.fill(events,out)

        # Plot PU distributions to see that the pu reweighting works
        if not isData:
            with dask.annotate(priority=10):
                mu = events.Pileup.nTrueInt
                events["mu"] = mu
                out['pu_orig'] = (
                    dhist.Hist.new
                    .Reg(100,0,100,name="value",label="pu_orig")
                    .Double().fill(value=mu)
                )
                out['pu_corr'] = (
                    dhist.Hist.new
                    .Reg(100,0,100,name="value",label="pu_corr")
                    .Double().fill(value=mu,weight=events["weight"])
                )
        return out

    def postprocess(self, accumulator):
#        skim_counter = self.skimCounter.get()
#        for key in skim_counter:
#            accumulator[key] = skim_counter[key] | accumulator[key]
#        if not self.isData:
#            x,y = self.getArrays(self.pu_data)
#            accumulator['pu_data'].fill(value=x,weight=y)
#        Counter.ModularCounters.print(accumulator, PRINTED_COUNTERS)
        pass
#        return accumulator

def event_counts(events):
    # there is no tree-reduction implementation for ak.num in dask_awkward,
    # this results in a lot of unnecessary memory usage
    # here the workaround is to use dak.sum instead even for the unweighted counter
    # unweighted = dak.num(events,axis=0)
    if hasattr(events,"weight"):
        w = events.weight
        weighted = ak.sum(w)
        unweighted = ak.sum(0*w+1)
    else:
        weighted = unweighted = ak.sum(dak.ones_like(events.event))
    return unweighted, weighted

def set_count(counter, label, ev):
    count, wcount = event_counts(ev)
    counter["unweighted"][label] = count
    counter["weighted"][label] = wcount

def setSkimCounter(counterhisto, counters):
    out_counters = {"unweighted": {}, "weighted": {}}
    all_ev_label = "Skim: All events"
    if all_ev_label in counterhisto.axes[0]:
        val = counterhisto[all_ev_label]
        out_counters["unweighted"][all_ev_label] = int(val)
        out_counters["weighted"][all_ev_label] = val
    else:
        print("WARNING: no all event counter found in skim counters!")
    for bin in counterhisto.axes[0]:
        if bin != all_ev_label:
            value = counterhisto[bin]
            out_counters["unweighted"][bin] = int(value)
            out_counters["weighted"][bin] = value
    out_counters["unweighted"].update(counters["unweighted"])
    out_counters["weighted"].update(counters["weighted"])
    return out_counters

def make_counter_h(counters):

    uCounter_axis = hist.axis.StrCategory([], growth=True, name="unweighted", label="")
    h_uCounter = hist.Hist(uCounter_axis, storage="weight", name="ucounter")
    # control = int(counters["unweighted"]["Control"])
    for k in counters["unweighted"].keys():
        count = counters["unweighted"][k]
        # if "Skim" in k:
        #     count = int(count/control)
        h_uCounter.fill(
            unweighted=k, weight=count
        )
    wCounter_axis = hist.axis.StrCategory([], growth=True, name="weighted", label="")
    h_wCounter = hist.Hist(wCounter_axis, storage="weight", name="wcounter")
    for k in counters["weighted"].keys():
        count = counters["weighted"][k]
        # if "Skim" in k:
        #     count = count/control
        h_wCounter.fill(
            weighted=k, weight=count
        )
    return h_uCounter,h_wCounter

def getArrays(histo):
    x = []
    axis  = histo.axes[0]
    edges = axis.edges
    for i in range(0,len(edges)-1):
        bincenter = edges[i] + 0.5*(edges[i+1]-edges[i])
        x.append(bincenter)
    y = histo.values()
    return ak.from_numpy(np.array(x)),y

def usage():

    print()
    print( "### Usage:  ",os.path.basename(sys.argv[0]),"<multicrab skim>" )
    print()

def trim_memory() -> int:
    gc.collect()
    libc = ctypes.CDLL("libc.so.6")
    return libc.malloc_trim(0)

def print_clientsetup(client):
    threads = client.nthreads()
    procs = len(threads)
    threads = map(str, threads.values())
    print(
        f"Using dask cluster with {procs} processes.\n"
        f"Number of threads for each process is:\n{', '.join(threads)}"
    )


def main(args):
    if args.distribute:
        print("successfully imported " + ls + "dask" + ns + " and transferred proxy certificate to home directory, will distribute analysis to an HTCondor cluster with " + ss + f"{MAX_JOBS}" + ns + " workers")

    # configuration of the neural network
    analysis_kwargs = {}
    nn_path = None
    if args.nn_weights is not None:
        nn_path = os.path.abspath(args.nn_weights)
        if 'regress' in nn_path.lower():
            analysis_kwargs["regress_mass"] = True
        elif 'param' in nn_path.lower():
            analysis_kwargs["param_mass"] = True
    analysis_kwargs['nn_working_point'] = args.nn_cutoff

    multicrabdir = os.path.abspath(args.multicrab)
    if not os.path.exists(multicrabdir) or not os.path.isdir(multicrabdir):
        usage()
        sys.exit()

    year = multicrabdatasets.getYear(multicrabdir)

    starttime = datetime.datetime.now().strftime("%Y%m%dT%H%M")

    # blacklist = ["^ST","^ChargedHiggs","^DYJets"]
    # blacklist = ["ChargedHiggsToTauNu_Heavy_M1500","ChargedHiggsToTauNu_Heavy_M2000"]
    # blacklist = ["ChargedHiggs", "DYJets","ST_t_channel_antitop_4f_InclusiveDecays"]
    # whitelist = ["ST_t_channel_top_4f"]
    # whitelist = ["ChargedHiggsToTauNu_Heavy_M170"]
    # whitelist = ["^TT"]
    # whitelist = ["ZZ"]
    # whitelist = ["Tau_Run2017C","ChargedHiggsToTauNu_Heavy_M200$"]
    # whitelist = ["ChargedHiggsToTauNu_Heavy_M1500"]
    whitelist = args.whitelist
    blacklist = args.blacklist
    if args.distribute:
        dash_port = args.dash_port
        sched_port = 8786
        print(ss + "initializing condor cluster" + ns)

        clustermanager = HTCondorCluster
        cluster_kwargs = {
            "cores": args.worker_threads,
            "processes": 1,
            "memory": f'{THREADS*2}000MB',
            "disk": '1000MB',
            "death_timeout": '300',
            "nanny": False,
            "scheduler_options": {
                'port': sched_port,
                'host': socket.gethostname(),
                "dashboard_address": f":{dash_port}",
            },
            "job_extra_directives": {
                'log': 'dask_job_output.log',
                'output': 'dask_job_output.out',
                'error': 'dask_job_output.err',
                'should_transfer_files': 'YES',
                'when_to_transfer_output': 'ON_EXIT',
                '+JobFlavour': '"longlunch"',
                #'environment': f'X509_USER_PROXY={PROXYPATH} PATH=$(ENV(PATH))'
            },
            "worker_extra_args": [
                '--worker-port 10000:10100',
                "--lifetime 115m",
                "--lifetime-stagger 4m",
            ],
            "job_script_prologue": [
                f"source {os.path.dirname(sys.executable)}/activate",
                f'export X509_USER_PROXY={PROXYPATH}',
                'export MALLOC_TRIM_THRESHOLD_=16384',
                'export MALLOC_MMAP_THRESHOLD_=16384',
            ],
        }

    else:
        # dask.config.set({"temporary-directory": "/tmp"})
        threads_per_process = args.worker_threads
        io_factor = 1
        max_workers = multiprocessing.cpu_count() * io_factor // threads_per_process
        n_workers = min(args.max_workers, max_workers)
        clustermanager = LocalCluster
        cluster_kwargs = {}
        cluster_kwargs = {
            "n_workers": n_workers,
            "threads_per_worker": threads_per_process,
        }
    dask.config.set({"distributed.scheduler.default-task-durations.sum-tree-node": "1us"})
    dask.config.set({"distributed.worker.memory.pause": 0.75})
    dask.config.set({"distributed.worker.memory.target": 0.85})
    dask.config.set({"distributed.worker.memory.spill": 0.85})
    dask.config.set({"distributed.worker.memory.terminate": 0.98})

    with clustermanager(**cluster_kwargs) as cluster:
        with Client(cluster) as client:

            datasets = multicrabdatasets.getDatasets(multicrabdir,whitelist=whitelist,blacklist=blacklist)
            if not isinstance(cluster, LocalCluster):
                print(cluster.job_script())
                client.amm.start()

                print(hs + f"scaling cluster to {args.max_jobs} jobs..." + ns)
                cluster.adapt(minimum=args.max_jobs//4,maximum=args.max_jobs)
                #cluster.adapt(minimum=1,maximum=MAX_JOBS)
            else:
                print_clientsetup(client)
            print(f"Dask dashboard at {client.dashboard_link}")

            pileup_data = multicrabdatasets.getDataPileup(multicrabdir,datasets)
            lumi = multicrabdatasets.loadLuminosity(multicrabdir,datasets)

            nntext = ""
            if nn_path is not None:
                nntext = "_" + nn_path.split("/")[-1]
            outputdir = os.path.basename(os.path.abspath(multicrabdir))+"_processed"+nntext+starttime

            t0 = time.time()

            # print("Number of cores used",MAX_WORKERS)
            if len(datasets) == 0:
                print("No datasets to be processed")
                print("  whitelist:",whitelist)
                print("  blacklist:",blacklist)
                sys.exit()

            # initialize nn server
            if nn_path:
                gpus = os.environ["CUDA_VISIBLE_DEVICES"]
                os.environ["CUDA_VISIBLE_DEVICES"] = ""
                from ANN.NNServer import init_server, kill_server
                init_server(nn_path, server_name=nntext[1:], gpus=gpus)

            if not isinstance(cluster, LocalCluster):
                sampler = MemorySampler()
                ms_manager = sampler.sample
            else:
                sampler = None
                ms_manager = nullcontext

            # if client is not None:
            #     job_executor = processor.DaskExecutor(client = client,
            #                                           treereduction = 20)
            #                                         #   worker_affinity = True)
            # else:
            #     #job_executor = processor.IterativeExecutor() # kept here commented for processor debugging purposes
            #     job_executor = processor.FuturesExecutor(workers=MAX_WORKERS)

            # run = processor.Runner(
            #     executor = job_executor,
            #     schema = NanoAODSchema,
            #     chunksize = CHUNKSIZE,
            #     maxchunks = MAXCHUNKS,
            #     processor_compression = None,
            #     mmap=True
            # )

            qcdstr = "QCDMeasurement"
            dataset_dict = {d.name: d.to_dict() for d in datasets}

            t00 = time.time()
            print("preprocessing")
            dataset_dict, datasetdict_partial = preprocess(
                dataset_dict,
                #align_clusters=True,
                step_size=args.chunksize,
                #skip_bad_files=True,
                save_form=True,
            )
            t01 = time.time()
            dt0 = t01-t00
            print("Preprocessing time %s min %s s"%(int(dt0/60),int(dt0%60)))

            # dataset_dict = max_chunks(dataset_dict, 1) #FIXME

            written_to = []
            with ms_manager("run"):
                # processor = Analysis(
                #     datasets, pileup_data, nn_path=nn_path,**analysis_kwargs
                # )
                # to_compute = apply_to_fileset(
                #     processor,
                #     dataset_dict,
                # )
                # results, = dask.compute(to_compute)

                for i, d in enumerate(datasets):
                    name = d.name
                    print("Dataset %s/%s %s"%(i+1,len(datasets),name))
                    t01 = time.time()
                    dataset = dataset_dict[name]
                    processor = Analysis(
                        d, pileup_data, nn_path=nn_path, unblind=args.unblinded,
                        **analysis_kwargs
                    )
                    to_compute = apply_to_dataset(
                        processor,
                        dataset,
                    )
                    print("Built task graph, computing...")
                    (result,), = dask.compute(to_compute)
                    t02 = time.time()
                    dt1 = t02-t01
                    print(f"Processing time for {name} {int(dt1/60)} min {int(dt1%60)} s")
                    # client.restart() # Try to mitigate memory leakage

                    #result = results[name]
                    result["counter"] = setSkimCounter(d.getSkimCounter(), result["counter"])
                    x,y = getArrays(pileup_data)
                    result["pu_data"] = (
                        hist.Hist.new
                        .Reg(100,0,100,name="value",label="pu_data")
                        .Weight()
                        .fill(value=x, weight=y)
                    )

                    is_sig = "ChargedHiggs" in name
                    is_bg = not is_sig and (not d.isData)
                    energy = 13 if "202" not in d.year else 13.6

                    sep_listed = [arr for arr in SEPARATE]
                    # if nn_path is None: # ignore the nn categorizations
                    #     sep_listed.pop(-1)
                    categories = ak.cartesian(sep_listed, axis=0)
                    for cat in categories:
                        cat = cat.tolist()
                        cat_str = "_".join(cat)
                        is_qcd = qcdstr in cat_str
                        analysis_name = "QCDMeasurement" if is_qcd else "SignalAnalysis"
                        run_text = legacy_run_name(d.year)
                        analysis_dir = f"{analysis_name}_{ANALYSISTYPE}_Run{run_text}"

                        if is_qcd:
                            if "ChargedHiggs" in name: continue
                            cat_outputdir = qcdstr + "_" + outputdir + cat_str.replace(qcdstr, "")
                        else:
                            cat_outputdir = outputdir + cat_str

                        if not os.path.exists(cat_outputdir):
                            os.mkdir(cat_outputdir)
                            written_to.append(cat_outputdir)

                        subdir = os.path.join(cat_outputdir,name)

                        if not os.path.exists(subdir):
                            os.mkdir(subdir)
                            os.mkdir(os.path.join(subdir,"results"))

                        with uproot.recreate(os.path.join(subdir,"results",f"histograms-{name}.root")) as fOUT:
                            days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
                            now = datetime.datetime.now()
                            m = "produced: %s %s"%(days[now.weekday()],now)
                            fOUT[f"configInfo/{m}"] = ""

                            try:
                                gitCommit = aux.execute("git rev-parse HEAD")[0]
                                gc = "git commit:"+gitCommit
                                fOUT[f"configInfo/{gc}"] = gitCommit
                            except:
                                print("No git repository or any parent found, skipping writing git commit string.")

                            h_lumi = (
                                hist.Hist.new.Reg(1, 0, 1, name="value", label="x")
                                .Double()
                            )
                            h_lumi.fill(value=0.5,weight=d.lumi)

                            fOUT[f"configInfo/lumi"] = h_lumi

                            h_isdata = (
                                hist.Hist.new.Reg(1, 0, 1, name="value", label="x value")
                                .Double()
                            )
                            h_isdata.fill(value=0.5,weight=d.isData)
                            fOUT[f"configInfo/isdata"] = h_isdata

                            confinfo_h = (
                                hist.Hist.new
                                .StrCat(["control",
                                         "energy",
                                         "isData",
                                         "isPileupReweighted",
                                         "isTopPtReweighted"
                                         ],name="x")
                                .Double()
                            )
                            confinfo_h[:] = [1, energy, d.isData, 1, 0]
                            fOUT[f"configInfo/configinfo"] = confinfo_h

                            h_counters = {}
                            fake_cats = ["", "EWKGenuineTaus", "EWKFakeTaus"]
                            for postfix in fake_cats:
                                h_counters["ForDataDrivenCtrlPlots" + postfix] = {"shapeTransverseMass": [0, 0, 0, None]}
                            if is_qcd:
                                for postfix in fake_cats:
                                #fOUT.mkdir("analysis/ForDataDrivenCtrlPlots" + postfix + "/shapeTransverseMass")
                                    dirs = QCD_DIRS
                                    for dir in dirs:
                                        h_counters[dir] = {}
                                        for subd in dirs[dir]:
                                            h_counters[dir][subd] = [0, 0, 0, None] # binned_both, binned_eta, binned_pt, inclusive


                            for key in result.keys():
                                if key in ['pu_orig','pu_data','pu_corr','lumi','isdata']:
                                    fOUT[f"configInfo/%s"%key] = result[key]
                                    continue

                                if 'counter' in key:
                                    h_uCounter,h_wCounter = make_counter_h(result[key])
                                    fOUT[f"configInfo/unweighted_counter"] = h_uCounter
                                    fOUT[f"configInfo/weighted_counter"] = h_wCounter
                                    continue

                                for sliced_name, sliced_hist in Histograms.slice_variations(result[key], key):

                                    # only write the histo if it matches the category
                                    if not belongs_to(cat, sliced_name):
                                        continue

                                    dname = navigate_subdirs(sliced_name, is_qcd)
                                    newkey = get_legacy_name(dname, sliced_name, h_counters, is_bg, is_qcd)

                                    fOUT[os.path.join(analysis_dir,dname,newkey)] = sliced_hist

                    dt1 = time.time()-t02
                    print("Writing hist time %s min %s s"%(int(dt1/60),int(dt1%60)))

            if sampler is not None:
                sampler.plot(figsize=(15,10), grid=True)
                plt.savefig("dask_memorysample")

    # write multicrab.cfg files to each pseudo-multicrab folder
    for folder in written_to:
        write_multicrab_cfg(folder, datasets)
        write_lumi_json(folder, datasets)
    dt = time.time()-t0

    print("Total processing time %s min %s s"%(int(dt/60),int(dt%60)))
    print("output in",outputdir)
    if nn_path:
        kill_server(nntext[1:])
    return

if __name__ == "__main__":
    args = parse_args()
    main(args)
    #os.system("ls -lt")
    #os.system("pwd")
