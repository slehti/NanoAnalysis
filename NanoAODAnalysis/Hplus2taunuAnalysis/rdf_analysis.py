from pathlib import Path
import time
import datetime
import tempfile
import json
import os
import itertools

import ROOT as R
from ROOT import gDirectory as gdir
import numpy as np

import rdf_selections
from nanoanalysis import (
    LumiMask,
    TransverseMass,
    Inference,
    multicrabdatasets,
    aux,
    cpptools,
)
from nanoanalysis.PileupWeight import RDF_PileupWeight
from nanoanalysis.rdf_backend import Utils

from analysis_common import (
    parse_args,
    belongs_to,
    name_matches_cat,
    filter_counters,
    getFromDict,
    write_lumi_json,
    get_legacy_name,
    legacy_run_name,
    write_multicrab_cfg,
    navigate_subdirs,
    extend_QCDhist_name,
    SEPARATE,
    ANALYSISTYPE,
    QCD_DIRS,
    QCDSTR,
)
import rdf_histograms

def dict_to_root(d,histo_name):
    n = len(d.keys())
    out = R.TH1D(histo_name,"",n,0,n)
    i = 1
    for key in d.keys():
        out.SetBinContent(i,d[key])
        out.GetXaxis().SetBinLabel(i,key)
        i += 1
    return out


def process(df, is_data, year, run, pu_weighter, name, lumimask, inference_runner=None):

    if not is_data:
        df = pu_weighter.apply_weights(df, name)
        df = df.Define("w", "pileup_w * genWeight / abs(genWeight)")
    else:
        df = df.Define("w", "1.0")

    df = lumimask(df, is_data)
    df = rdf_selections.trigger(df, year)
    df = rdf_selections.METCleaning(df, year)
    if int(year) > 2020:
        df = rdf_selections.tau_id(
            df,
            is_data=is_data,
            id_vse = "Tau_idDeepTau2018v2p5VSe",
            id_vsmu = "Tau_idDeepTau2018v2p5VSmu",
            id_vsjet = "Tau_idDeepTau2018v2p5VSjet",
            jet_wp = 4,
            e_wp = 6,
        )
    else:
        df = rdf_selections.tau_id(df, is_data=is_data)

    if int(year) > 2020:
        ele_disc = "Electron_mvaIso_WP80"
        df = rdf_selections.e_veto(df, iso_discriminator=ele_disc)
    else:
        df = rdf_selections.e_veto(df)

    df = rdf_selections.mu_veto(df)
    df = (
        TransverseMass.rdf_reconstruct_transverse_mass(df)
        .Define(
            "rtau",
            """
            nanoanalysis::Rtau(
                leadTau_pt,
                leadTau_eta,
                leadTau_leadTkPtOverTauPt,
                leadTau_leadTkDeltaEta
            )
            """
        )
    )
    stdsele_df = rdf_selections.hadr_jets(df)

    if int(year) > 2020:
        df = rdf_selections.b_jets(stdsele_df, year, tagger="btagPNetB")
    else:
        df = rdf_selections.b_jets(stdsele_df, year)

    df = rdf_selections.met(df)
    df = rdf_selections.Rbb_min(df)
    if inference_runner is not None:
        df = inference_runner.forward(df)
    else:
        df = df.Define("classifier_score", "-1.0")
    if is_data:
        df = df.Define("m_true", "-1.0")
    else:
        df = df.Define("m_true", "Hplus_mass(GenPart_pdgId, GenPart_mass)")

    print("defining big histograms")
    hists = rdf_histograms.get_histograms(df, stdsele_df=stdsele_df)
    print("returning from process")
    return {
        'df': df,
        'name': name,
        'hists': hists,
        "year": year,
        "run": run,
        "is_data": is_data,
        "counter": {"unweighted": {}, "weighted": {}},
        "report": df.Report(),
    }

def setSkimCounter(counterhisto, counters):
    out_counters = {"unweighted": {}, "weighted": {}}
    all_ev_label = "Skim: All events"
    if all_ev_label in counterhisto.axes[0]:
        val = counterhisto[all_ev_label]
        out_counters["unweighted"][all_ev_label] = int(val)
        out_counters["weighted"][all_ev_label] = val
    else:
        print("WARNING: no all event counter found in skim counters!")
    for bin in counterhisto.axes[0]:
        if bin != all_ev_label:
            value = counterhisto[bin]
            out_counters["unweighted"][bin] = int(value)
            out_counters["weighted"][bin] = value
    out_counters["unweighted"].update(counters["unweighted"])
    out_counters["weighted"].update(counters["weighted"])
    return out_counters

def getArrays(histo):
    x = []
    axis  = histo.axes[0]
    edges = axis.edges
    for i in range(0,len(edges)-1):
        bincenter = edges[i] + 0.5*(edges[i+1]-edges[i])
        x.append(bincenter)
    y = histo.values()
    return np.array(x),y

def write_tnamed(name, value):
    R.TNamed(name, value).Write()
    return

def write_confinfo(file, *histos, **tnameds):
    file.cd("configInfo")
    for h in histos:
        h.Write()
    for k,v in tnameds.items():
        write_tnamed(k,v)
    file.cd()
    return

def process_dataset(
    d,
	pileup_data,
	lumis,
	skim_counters,
	output_basedir,
	pu_weighter,
	inference_runner,
    written_to,
    outputdir,
    args
):

    lmask = LumiMask.RDF_LumiMask(d)
    # lumimasks.append(lmask)
    # print("initializing df")
    # breakpoint()
    time.sleep(1) # It doesn't make any sense, but removing this causes the script to hang
    df = R.RDataFrame("Events", d.files)
    result = process(df, d.isData, d.year, d.run, pu_weighter, d.name, lmask, inference_runner)
    # results.append(result)

# histos = [h for res in results for h in res["hists"].values()]
#
# R.RDF.RunGraphs(histos)
# for r in results:
    R.RDF.RunGraphs(result["hists"].values())
    print(f"Dataset {d.name} done at {time.strftime("%c")}")
    result["report"].Print()


# for result in results:
    name = result["name"]
    year = result["year"]
    # run = result["run"]
    is_data = result["is_data"]

    # print("print 1")
    result["counter"] = setSkimCounter(skim_counters[name], result["counter"])
    result["pu_data"] = pileup_data
    result["lumi"] = lumis[name]

    is_sig = "ChargedHiggs" in name
    is_bg = not is_sig and (not is_data)
    energy = 13 if "202" not in year else 13.6

    sep_listed = [arr for arr in SEPARATE]
    # if nn_path is None: # ignore the nn categorizations
    #     sep_listed.pop(-1)
    categories = itertools.product(*sep_listed)
    for cat in categories:
        cat_str = "_".join(cat)
        is_qcd = QCDSTR in cat_str
        analysis_name = "QCDMeasurement" if is_qcd else "SignalAnalysis"
        run_text = legacy_run_name(year)
        analysis_dir = f"{analysis_name}_{ANALYSISTYPE}_Run{run_text}"

        cat_outputdir = output_basedir
        if is_qcd:
            if "ChargedHiggs" in name: continue
            cat_outputdir /= QCDSTR + "_" + str(outputdir) + cat_str.replace(QCDSTR, "")
        else:
            cat_outputdir /= str(outputdir) + cat_str

        if not os.path.exists(cat_outputdir):
            os.mkdir(cat_outputdir)
            written_to.append(cat_outputdir)

        subdir = os.path.join(cat_outputdir,name)

        if not os.path.exists(subdir):
            os.mkdir(subdir)
            os.mkdir(os.path.join(subdir,"results"))

        with R.TFile.Open(os.path.join(subdir,"results",f"histograms-{name}.root"), "RECREATE") as f:
            days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
            now = datetime.datetime.now()
            m = "produced: %s %s"%(days[now.weekday()],now)
            try:
                gitCommit = aux.execute("git rev-parse HEAD")[0]
                gc = "git commit:"+gitCommit
            except:
                print("No git repository or any parent found, skipping writing git commit string.")
                gc = "No git repository found!"

            h_lumi = R.TH1D("lumi", "Luminosity", 1,0,1)
            h_lumi.Fill(0.5, result["lumi"])

            h_isdata = R.TH1D("isdata", "isdata", 1,0,1)
            h_isdata.Fill(0.5, float(result["is_data"]))

            info_dict = {
                "control": 1,
                "energy": energy,
                "isData": int(is_data),
                "isPileupReweighted": 1,
                "isTopPtReweighted": 0
            }
            info_histo = dict_to_root(info_dict, "configinfo")

            dataver_pfx = "data" if is_data else "mc"
            dataver = args.data_version + dataver_pfx

            f.cd()
            if is_qcd:
                f.mkdir(analysis_dir)
                f.cd(analysis_dir)
                dict_to_root(
                    {"Control": 1, "tauPt": 4, "tauEta": 3},
                    "SplittedBinInfo"
                ).Write()

            f.cd()

            f.mkdir("configInfo")
            f.cd("configInfo")
            dict_to_root(result["counter"]["unweighted"], "unweighted_counter").Write()
            dict_to_root(result["counter"]["weighted"], "weighted_counter").Write()

            write_confinfo(
                f,
                info_histo,
                h_lumi,
                h_isdata,
                timestamp=m,
                dataVersion=dataver,
                gitCommit=gc,
            )
            f.cd()

            # initialize naming counters for QCD categorized histograms
            h_counters = {}
            fake_cats = ["", "EWKGenuineTaus", "EWKFakeTaus"]
            for postfix in fake_cats:
                h_counters["ForDataDrivenCtrlPlots" + postfix] = {"shapeTransverseMass": [0, 0, 0, None]}
            if is_qcd:
                for postfix in fake_cats:
                    dirs = QCD_DIRS
                    for dir in dirs:
                        h_counters[dir] = {}
                        for subd in dirs[dir]:
                            h_counters[dir][subd] = [0, 0, 0, None] # binned_both, binned_eta, binned_pt, inclusive

            # Write histograms
            histos = result["hists"]
            for key in histos:

                for sliced_name, sliced_hist in rdf_histograms.slice_variations(histos[key].GetValue(), key):

                    # only write the histo if it matches the category
                    if not belongs_to(cat, sliced_name):
                        continue

                    dname = navigate_subdirs(sliced_name, is_qcd)
                    newkey = get_legacy_name(dname, sliced_name, h_counters, is_bg, is_qcd)

                    target_dir = os.path.join(analysis_dir,dname)

                    if not f.GetDirectory(target_dir):
                        f.mkdir(target_dir)
                    f.cd(target_dir)
                    sliced_hist.SetName(newkey)
                    sliced_hist.Write()
                    f.cd()
            #     # print("histogram written")
            # print("result file written")
        # print("category done")
    # print("dataset done")

def main(args):

    R.EnableImplicitMT()
    starttime = time.strftime("%c")
    print(f"Starting Hp2taunu Analysis script at {starttime}")

    datasets = multicrabdatasets.getDatasets(
        args.multicrab,
        args.whitelist,
        args.blacklist
    )

    cpptools.initialize(Utils)
    cpptools.jit_include("rdf_utils.cpp", "rdf_utils.h")
    LumiMask.RDF_LumiMask.declare()
    TransverseMass.rdf_declare_mT()
    inference_runner = None
    if args.nn_weights is not None:
        modelpath = Path(args.nn_weights)
        inference_runner = Inference.InferenceRunner(modelpath, args.nn_cutoff)
    pu_weighter = RDF_PileupWeight(args.multicrab, datasets)
    pileup_data = multicrabdatasets.getDataPileupROOT(datasets)
    skim_counters = {d.name: d.getSkimCounter() for d in datasets}
    lumis = {d.name: d.lumi for d in datasets}

    # results = []
    # lumimasks = []
    output_basedir = Path(args.output)
    outputdir = Path(
        os.path.basename(os.path.abspath(args.multicrab))
        +"_processed_"+time.strftime("%Y-%m-%d_%H-%M_")
    )
    output_basedir.mkdir(parents=True, exist_ok=True)
    written_to = []
    for d in datasets:
        process_dataset(
            d,
            pileup_data,
            lumis,
            skim_counters,
            output_basedir,
            pu_weighter,
            inference_runner,
            written_to,
            outputdir,
            args
        )


    for folder in written_to:
        write_multicrab_cfg(folder, datasets)
        write_lumi_json(folder, datasets)

    print(f"Finished processing datasets at {time.strftime("%c")}")

    return

if __name__ == "__main__":
    main(parse_args())
