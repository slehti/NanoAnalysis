import itertools

import awkward as ak
import dask_awkward as dak
import numpy as np
import dask

import hist.dask as dhist
import hist

from nanoanalysis import TransverseMass

FILL_THREADS = 1
SUM = slice(None,None,hist.sum)
STDSELE_PREFIX = "AfterStdSelections_"
VARIATION_BINS = {
    "Rtau": ["Rtau75", "Rtau1", "RtauIncl"],
    "pt": ["lowpt", "lmidpt","hmidpt", "highpt","ptIncl"],
    "eta": ["loweta", "mideta", "higheta","etaIncl"],
    "nn_score": ["nnhigh","nnIncl"],
    "tau_isolation": ["Inverted", "Nominal","TauIsoIncl"],
    "genuine_tau": ["EWKFakeTau", "EWKGenuineTau","EWKIncl"],
    "matched_tau": ["NoTauMatch", "MatchedTau","TauMatchIncl"],
}
def get_rBB_min(events):
    tau = events.Tau
    jets = events.Jet
    met = events.MET

    jets_delta_phi = jets.delta_phi(met)**2
    tau_delta_phi = (np.pi - np.absolute(tau.delta_phi(met)))**2

    jets_min = dak.min(jets_delta_phi, axis=1)
    taus_min = dak.min(tau_delta_phi, axis=1)

    return np.sqrt(taus_min + jets_min)

def without(d, key):
    res = d.copy()
    res.pop(key)
    return res

def stripped(hist_name):
    return hist_name.replace(STDSELE_PREFIX,"").replace("vis","")

def get_indexed_name(variables, indices):
    indices = [-1 if i==SUM else i for i in indices]
    nameslist = [VARIATION_BINS[var][idx] for var,idx in zip(variables,indices)]
    return "_".join(nameslist)

def slice_variations(hist, orig_name):
    subhists = []
    variation_axs = hist.axes[1:]
    variation_names = variation_axs.name
    variation_indices = [tuple(range(ax.size)) + (SUM,) for ax in variation_axs]
    combinations = itertools.product(*variation_indices)
    for combination in combinations:
        sliced = hist[(slice(None),) + combination]
        variation_name = get_indexed_name(variation_names, combination)
        name = f"h_{orig_name}_{variation_name}"
        sliced.name = name
        subhists.append((name, sliced))
    return subhists


class AnalysisHistograms():
    def __init__(self, isData, use_nn=False, use_m=False, unblind = False, after_std=False):
        self.isData = isData
        self.use_nn = use_nn
        self.use_m = use_m

        #NOTE: this implementation of blinding makes it so that observed data histograms not filled at all.
        # Turning it off does not equate to unblinding the analysis, but viewing/plotting the resulting histograms will!
        self.unblind = (not self.isData) or unblind or after_std
        self.after_std = after_std

    def book(self):

        prefix = ""
        if self.after_std:
            prefix += STDSELE_PREFIX

        bins_essential = {
            prefix+"mT": np.concatenate([np.arange(0,1000,5), np.array([3000])], axis = 0),
            prefix+"met": np.linspace(0,800,801)
        }

        bins_additional = {
            "eta": np.linspace(-2.1,2.1,22), #FIXME this is not the same binning as in original analysis,
            "jeteta": np.linspace(-4.5,4.5,60), #FIXME this is not the same binning as in original analysis,
            "rBB": np.linspace(0,260,26),
            "pt": [50,60,80,100,150,200,300,400,2000],
            "metvis": [0,20,40,60,80,100,120,140,160,200,250,300,1500],
            "bjetpt": [30,50,70,90,110,130,150,200,300,400,1500],
        }
        if self.use_nn:
            bins_additional.update({
                "nn": np.linspace(0,1,50),
            })
            if self.use_m:
                bins_essential.update({
                    "m_pred": np.geomspace(80,3000,50),
                })


        bins_Rtau = np.array([0,0.75,np.inf])
        bins_Rtau_hist = np.linspace(0.,1.,20)
        bins_pt = np.array([0,60,80,100,np.inf])
        bins_eta = np.array([0,0.6,1.4,np.inf])
        bins_nnvar = np.array([0.5,1])

        met_var_axes = (
            hist.axis.Variable(bins_Rtau, name="Rtau"),
            hist.axis.Variable(bins_pt, name="pt"),
            hist.axis.Variable(bins_eta, name="eta"),
            hist.axis.Boolean(name="genuine_tau"),
            hist.axis.Boolean(name="matched_tau"),
            hist.axis.Boolean(name="tau_isolation"),
        )
        common_var_axes = (
            hist.axis.Variable(bins_Rtau, name="Rtau"),
        )
        if self.use_nn:
            nn_var_ax =hist.axis.Variable(bins_nnvar, name="nn_score"),
            met_var_axes = met_var_axes + (nn_var_ax,)
            common_var_axes = common_var_axes + (nn_var_ax,)

        essential_hists = {
            name: dhist.Hist(
                hist.axis.Variable(
                    bins_essential[name],
                    name=stripped(name),
                ),
                *met_var_axes,
                storage=hist.storage.Weight(),
                name=name
            )
            for name in bins_essential
        }
        self.histograms = essential_hists

        if not self.after_std:
            additional_hists = {
                name: dhist.Hist(
                    hist.axis.Variable(
                        bins_additional[name],
                        name=stripped(name),
                    ),
                    *common_var_axes,
                    storage=hist.storage.Weight(),
                    name=name
                )
                for name in bins_additional
            }
            additional_hists["Rtau"] = dhist.Hist(
                hist.axis.Variable(bins_Rtau_hist, name="Rtau"),
                storage=hist.storage.Weight(),
                name="Rtau"
            )

            self.histograms.update(**additional_hists)

        return self.histograms

    @dask.annotate(priority=10)
    def fill(self,events,out):

        if not self.unblind:
            events = events[events["TauIsolation"] <= 0]
        if hasattr(events, 'weight'):
            weight = events.weight
        else:
            weight = dak.ones_like(events.event)
        leadTau = events.leadTau
        tpt = leadTau.pt
        leadJet = events.Jet[:,0]
        teta = leadTau.eta
        jeteta = leadJet.eta
        Rtau = leadTau.leadTkPtOverTauPt
        genuinetau = leadTau.Genuine
        matchedtau = leadTau.Matched
        tau_isol = events.TauIsolation

        met = events.MET.pt
        mt = TransverseMass.reconstruct_transverse_mass(leadTau, events.MET)

        values = dict(
            mT=mt, pt=tpt, jeteta=jeteta, met=met,
            Rtau=Rtau, eta=teta, genuine_tau=genuinetau,
            matched_tau=matchedtau, tau_isolation=tau_isol,
        )
        if not self.after_std:
            rBB = get_rBB_min(events) * 180 / np.pi
            bjetpt = events.BJet.pt[:,0]
            values.update(
                rBB=rBB, bjetpt=bjetpt
            )
        if self.use_nn:
            values["nn_score"] = events.nn_score
            if self.use_m:
                values["m_pred"] = events.nn_masspred

        for name in self.histograms:
            axes_names = self.histograms[name].axes.name
            this_values = {n: values[n] for n in axes_names}
            out[name].fill(**this_values, weight = weight, threads=FILL_THREADS)
        return out

