#!/usr/bin/env python

###################################################################################
#                                                                                 #
# This script calculates the normalization factors for FakeTauMeasurementAnalysis #
# 02082023/S.Lehti                                                                #
#                                                                                 #
###################################################################################

import os, sys, re
import math

from optparse import OptionParser

import ROOT

ROOT.gROOT.SetBatch(True)

from nanoanalysis.aux import execute
from nanoanalysis.tools.plotting import *
from FitFunctions import *

useWJetsHT = False

selectOnlyBins = []  # use all bins
selectOnlyBins = ["Inclusive"] # use only the inclusive bin


def usage():
    print()
    print("### Usage:   %s <multicrab dir>" % sys.argv[0])
    print()
    sys.exit()


def getModifiedBinLabelString(binLabel):
    label = binLabel
    label = label.replace("#tau p_{T}", "tauPt")
    label = label.replace("#tau eta", "taueta")
    label = label.replace("<", "lt")
    label = label.replace(">", "gt")
    label = label.replace("=", "eq")
    label = label.replace("..", "to")
    label = label.replace(".", "p")
    label = label.replace("/", "_")
    label = label.replace(" ", "_")
    label = label.replace("(", "")
    label = label.replace(")", "")
    return label


def main(opts, args):
    if len(args) == 0:
        usage()

    print("Getting datasets")
    multicrabdirs = [args[0]]

    datasets = getDatasetsMany(multicrabdirs, whitelist=opts.whitelist, blacklist=opts.blacklist)
    print("Datasets received")
    print("number of datasets", len(datasets))
    datasets = read(datasets)
    datasets = mergeExtDatasets(datasets)
    datasets = normalizeToLumi(datasets)

    analysisname = opts.analysisname
    if analysisname != "analysis":
        analysisname = "QCDMeasurement_"+analysisname

    if useWJetsHT:
        datasets = removeDatasets("WJetsToLNu$", datasets)
    else:
        datasets = removeDatasets("WJetsToLNu_HT", datasets)

    datasets = mergeDatasets("Data", "_Run20\d\d\S_", datasets)
    datasets = mergeDatasets("EWK", "^(?!Data)", datasets)

    for dataset in datasets:
        print(dataset.name)

    datasetData = getDatasetByName("Data", datasets)
    datasetEWK = getDatasetByName("EWK", datasets)
    if datasetData is None:
        exit()
    if datasetEWK is None:
        exit()

    COMBINEDHISTODIR = "ForQCDNormalization"
    FAKEHISTODIR = "ForQCDNormalizationEWKFakeTaus"
    GENUINEHISTODIR = "ForQCDNormalizationEWKGenuineTaus"
    DETERMINATIONHISTODIR = "ForDataDrivenCtrlPlots"
    FAKEDETERMINATIONHISTODIR = "ForDataDrivenCtrlPlotsEWKFakeTaus"
    GENUINEDETERMINATIONHISTODIR = "ForDataDrivenCtrlPlotsEWKGenuineTaus"

    BASELINETAUHISTONAME = "NormalizationMETBaselineTauAfterStdSelections/NormalizationMETBaselineTauAfterStdSelections"
    INVERTEDTAUHISTONAME = "NormalizationMETInvertedTauAfterStdSelections/NormalizationMETInvertedTauAfterStdSelections"
    DETERMINATIONHISTONAME= "shapeTransverseMass/shapeTransverseMass"

    FITMIN = None
    FITMAX = None

    histonames = datasetData.getDirectoryContent(
        os.path.join(COMBINEDHISTODIR, "NormalizationMETBaselineTauAfterStdSelections")
    )
    bins = []
    binLabels = []

    for hname in histonames:
        hnametail = hname.split("/")[-1]
        binIndex = hnametail.replace("NormalizationMETBaselineTauAfterStdSelections", "")
        hDummy = datasetData.getHistogram(hname)
        title = hDummy.GetTitle()

        if hDummy.Integral() > 0.0:
            bins.append(binIndex)
            if binIndex == "Inclusive":
                binLabels.append(binIndex)
            else:
                binLabels.append(getModifiedBinLabelString(title))
            if FITMIN == None:
                FITMIN = hDummy.GetXaxis().GetXmin()
                FITMAX = hDummy.GetXaxis().GetXmax()
        else:
            print(
                "Skipping bin '%s' (%s) because it has no entries"
                % (binIndex, getModifiedBinLabelString(title))
            )

    # Select bins by filter
    if len(selectOnlyBins) > 0:
        oldBinLabels = binLabels[:]
        oldBins = bins[:]
        binLabels = []
        bins = []
        for k in selectOnlyBins:
            for i in range(len(oldBinLabels)):
                if k == oldBinLabels[i] or k == oldBins[i]:
                    binLabels.append(oldBinLabels[i])
                    bins.append(oldBins[i])

    print("\nHistogram bins available")
    for i in range(len(binLabels)):
        line = bins[i]
        while len(line) < 10:
            line += " "
        line += ": " + binLabels[i]
        print(line)
    print()

    # Initialize containers for binned result histograms
    fakeTau_mT = ROOT.THStack("FakeTau_mT", "")

    # ===== Initialize normalization calculation
    for i, bin in enumerate(bins):
        histogram_EWKFakeTaus_Baseline = datasetEWK.getHistogram(
            os.path.join(analysisname, FAKEHISTODIR, BASELINETAUHISTONAME + bin)
        )
        histogram_EWKFakeTaus_Inverted = datasetEWK.getHistogram(
            os.path.join(analysisname, FAKEHISTODIR, INVERTEDTAUHISTONAME + bin)
        )
        histogram_EWKGenuineTaus_Baseline = datasetEWK.getHistogram(
            os.path.join(analysisname, GENUINEHISTODIR, BASELINETAUHISTONAME + bin)
        )
        histogram_EWKGenuineTaus_Inverted = datasetEWK.getHistogram(
            os.path.join(analysisname, GENUINEHISTODIR, INVERTEDTAUHISTONAME + bin)
        )
        histogram_TauData_Baseline = datasetData.getHistogram(
            os.path.join(analysisname, COMBINEDHISTODIR, BASELINETAUHISTONAME + bin)
        )
        histogram_TauData_Inverted = datasetData.getHistogram(
            os.path.join(analysisname, COMBINEDHISTODIR, INVERTEDTAUHISTONAME + bin)
        )
        histogram_EWKInclusive_Baseline = histogram_EWKGenuineTaus_Baseline + histogram_EWKFakeTaus_Baseline
        histogram_EWKInclusive_Inverted = histogram_EWKGenuineTaus_Inverted + histogram_EWKFakeTaus_Inverted

        histogram_QCD_Inverted = histogram_TauData_Inverted - histogram_EWKInclusive_Inverted
        nPar = 3
        fit_QCD_Inverted = ROOT.TF1("QCDshape", QCDFunction, FITMIN, FITMAX, nPar)
        fit_QCD_Inverted.SetParameter(0, 1)
        fit_QCD_Inverted.SetParameter(1, 60)
        fit_QCD_Inverted.SetParameter(2, 10)
        histogram_QCD_Inverted.Fit("QCDshape")
        n_QCD_Inverted = fit_QCD_Inverted.Integral(FITMIN, FITMAX)
        fit_QCD_Inverted_parameter1 = fit_QCD_Inverted.GetParameter(
            1
        )  # saving the shape for later use
        fit_QCD_Inverted_parameter2 = fit_QCD_Inverted.GetParameter(
            2
        )  # saving the shape for later use
        print("QCD inverted events", n_QCD_Inverted)

        # fit_EWKGenuineTaus_Inverted = ROOT.TF1("EWKshape", EWKFunction, FITMIN, FITMAX, nPar)
        # fit_EWKGenuineTaus_Inverted.SetParameter(0, 1)
        # fit_EWKGenuineTaus_Inverted.SetParameter(1, 100)
        # fit_EWKGenuineTaus_Inverted.SetParameter(2, 100)
        # histogram_EWKGenuineTaus_Inverted.Fit("EWKshape")
        # n_EWKGenuineTaus_Inverted = fit_EWKGenuineTaus_Inverted.Integral(FITMIN, FITMAX)
        # fit_EWKGenuineTaus_Inverted_parameter1 = fit_EWKGenuineTaus_Inverted.GetParameter(1)
        # fit_EWKGenuineTaus_Inverted_parameter2 = fit_EWKGenuineTaus_Inverted.GetParameter(2)
        # print("EWK genuine tau inverted events", n_EWKGenuineTaus_Inverted)

        # fit_EWKGenuineTaus_Baseline = ROOT.TF1("EWKshape", EWKFunction, FITMIN, FITMAX, nPar)
        # fit_EWKGenuineTaus_Baseline.SetParameter(0, 1)
        # fit_EWKGenuineTaus_Baseline.SetParameter(1, 100)
        # fit_EWKGenuineTaus_Baseline.SetParameter(2, 100)
        # histogram_EWKGenuineTaus_Baseline.Fit("EWKshape")
        # n_EWKGenuineTaus_Baseline = fit_EWKGenuineTaus_Baseline.Integral(FITMIN, FITMAX)
        # fit_EWKGenuineTaus_Baseline_parameter1 = fit_EWKGenuineTaus_Baseline.GetParameter(1)
        # fit_EWKGenuineTaus_Baseline_parameter2 = fit_EWKGenuineTaus_Baseline.GetParameter(2)
        # print("EWK genuine tau baseline events", n_EWKGenuineTaus_Baseline)

        fit_EWKFakeTaus_Inverted = ROOT.TF1("EWKshape", EWKFunction, FITMIN, FITMAX, nPar)
        fit_EWKFakeTaus_Inverted.SetParameter(0, 1)
        fit_EWKFakeTaus_Inverted.SetParameter(1, 100)
        fit_EWKFakeTaus_Inverted.SetParameter(2, 100)
        histogram_EWKFakeTaus_Inverted.Fit("EWKshape")
        n_EWKFakeTaus_Inverted = fit_EWKFakeTaus_Inverted.Integral(FITMIN, FITMAX)
        print("EWK fake tau inverted events", n_EWKFakeTaus_Inverted)

        fit_EWKFakeTaus_Baseline = ROOT.TF1("EWKshape", EWKFunction, FITMIN, FITMAX, nPar)
        fit_EWKFakeTaus_Baseline.SetParameter(0, 1)
        fit_EWKFakeTaus_Baseline.SetParameter(1, 100)
        fit_EWKFakeTaus_Baseline.SetParameter(2, 100)
        histogram_EWKFakeTaus_Baseline.Fit("EWKshape")
        n_EWKFakeTaus_Baseline = fit_EWKFakeTaus_Baseline.Integral(FITMIN, FITMAX)
        print("EWK fake tau baseline events", n_EWKFakeTaus_Baseline)

        histogram_QCD_Baseline = histogram_TauData_Baseline - histogram_EWKInclusive_Baseline
        nPar = 6
        fit_QCD_Baseline = ROOT.TF1(
            "QCD_Baseline", CombinedQCDEWKFunxtion, FITMIN, FITMAX, nPar
        )
        fit_QCD_Baseline.SetParameter(0, 0.5)
        fit_QCD_Baseline.FixParameter(1, fit_QCD_Inverted_parameter1)
        fit_QCD_Baseline.FixParameter(2, fit_QCD_Inverted_parameter2)
        fit_QCD_Baseline.SetParameter(3, 0.5)
        # fit_QCD_Baseline.FixParameter(4, fit_EWKGenuineTaus_Baseline_parameter1)
        # fit_QCD_Baseline.FixParameter(5, fit_EWKGenuineTaus_Baseline_parameter2)
        histogram_QCD_Baseline.Fit("QCD_Baseline")
        nPar = 3
        fit_QCD_Baseline_parameter0 = fit_QCD_Baseline.GetParameter(0)
        fit_QCD_Baseline_parameter3 = fit_QCD_Baseline.GetParameter(3)
        fit_QCD_Baseline = ROOT.TF1("baselinefit", QCDFunction, FITMIN, FITMAX, nPar)
        fit_QCD_Baseline.FixParameter(0, fit_QCD_Baseline_parameter0)
        fit_QCD_Baseline.FixParameter(1, fit_QCD_Inverted_parameter1)
        fit_QCD_Baseline.FixParameter(2, fit_QCD_Inverted_parameter2)
        n_QCD_Baseline = fit_QCD_Baseline.Integral(FITMIN, FITMAX)
        print("QCD baseline events", n_QCD_Baseline)

        fOUT = ROOT.TFile.Open("QCDMeasurement/result/histograms.root", "RECREATE")
        histodir = f"{analysisname}/ForDataDrivenCtrlPlots"
        fOUT.mkdir(histodir)
        fOUT.cd(histodir)
        histogram_QCD_Inverted.Write()
        histogram_EWKGenuineTaus_Baseline.Write()
        histogram_TauData_Baseline.Write()
        fit_QCD_Baseline.Write()
        fOUT.Close()
        canvas = ROOT.TCanvas("canvas", "", 500, 500)
        canvas.cd()
        ROOT.gPad.SetLogy()
        histogram_TauData_Baseline.Draw()
        fit_QCD_Baseline.Draw("same")
        canvas.Print("TauData_Baseline_Fit.pdf")

        # Calculate weight for combined transfer coefficient from determination region
        histogram_TauData_Determination = datasetData.getHistogram(
            os.path.join(analysisname, DETERMINATIONHISTODIR, DETERMINATIONHISTONAME + bin)
        )
        histogram_EWKFakeTaus_Determination = datasetEWK.getHistogram(
            os.path.join(analysisname, FAKEDETERMINATIONHISTODIR, DETERMINATIONHISTONAME + bin)
        )
        histogram_EWKGenuineTaus_Determination = datasetEWK.getHistogram(
            os.path.join(analysisname, GENUINEDETERMINATIONHISTODIR, DETERMINATIONHISTONAME + bin)
        )
        histogram_QCD_Determination = (
            histogram_TauData_Determination
            - histogram_EWKFakeTaus_Determination
            - histogram_EWKGenuineTaus_Determination
        )
        histogram_FakeTau_Determination = histogram_QCD_Determination + histogram_EWKFakeTaus_Determination
        n_QCD_Determination = histogram_QCD_Determination.Integral()
        n_FakeTau_Determination = histogram_FakeTau_Determination.Integral()

        # Calculate combined transfer factor
        r_QCD = n_QCD_Baseline / n_QCD_Inverted
        r_EWKFakeTaus = n_EWKFakeTaus_Baseline / n_EWKFakeTaus_Inverted
        w = n_QCD_Determination / n_FakeTau_Determination
        r_FakeTau = w * r_QCD + (1-w) * r_EWKFakeTaus

        # Calculate estimated fake tau background in signal region
        histogram_FakeTau_Signal = histogram_FakeTau_Determination.Clone()
        histogram_FakeTau_Signal.Scale(r_FakeTau)
        fakeTau_mT.Add(histogram_FakeTau_Signal)

    # Write the result



if __name__ == "__main__":
    parser = OptionParser(usage="Usage: %prog <multicrabdir> [options]")

    parser.add_option(
        "--whitelist", dest="whitelist", default="", type="string", help="Whitelist datasets"
    )
    parser.add_option(
        "--blacklist", dest="blacklist", default="", type="string", help="Blacklist datasets"
    )
    parser.add_option(
        "--analysisname",
        default="analysis",
        type="string",
        help=("Name of the analysis directory in the pseudomulticrabs. " "(default 'analysis')"),
    )
    (opts, args) = parser.parse_args()

    if len(opts.whitelist) > 0:
        opts.whitelist = opts.whitelist.split(",")
        if "" in opts.whitelist:
            opts.whitelist = opts.whitelist.remove("")
    if len(opts.blacklist) > 0:
        opts.blacklist = opts.blacklist.split(",")
        if "" in opts.blacklist:
            opts.blacklist = opts.blacklist.remove("")

    main(opts, args)
