#!/usr/bin/env python
import ROOT

def QCDFunction(x,par):
    return Gaussian(x,par)
    #return Landau(x,par)

def EWKFunction(x,par,paroffset=0):
    #return Gaussian(x,par)
    return Landau(x,par,paroffset)

def CombinedQCDEWKFunxtion(x,par):
    return QCDFunction(x,par) + EWKFunction(x,par,3)

def Linear(x,par):
    return par[0]*x[0] + par[1]

def ErrorFunction(x,par):
    return 0.5*(1 + ROOT.TMath.Erf(par[0]*(x[0] - par[1])))

def ExpFunction(x,par):
    if (x[0] > 280 and x[0] < 300) or x[0] > 360:
        ROOT.TF1.RejectPoint()
        return 0
    return par[0]*ROOT.TMath.Exp(-x[0]*par[1])

def Gaussian(x,par):
    return par[0]*ROOT.TMath.Gaus(x[0],par[1],par[2],1)

def Landau(x,par,paroffset=0):
    return par[paroffset+0]*ROOT.TMath.Landau(x[0],par[paroffset+1],par[paroffset+2],1)


def DoubleGaussian(x,par):
    return par[0]*ROOT.TMath.Gaus(x[0],par[1],par[2],1) + par[3]*ROOT.TMath.Gaus(x[0],par[4],par[5],1)

def SumFunction(x,par):
    return par[0]*ROOT.TMath.Gaus(x[0],par[1],par[2],1) + par[3]*ROOT.TMath.Exp(-x[0]*par[4])

def RayleighFunction(x,par,norm=1):
    if par[0]+par[1]*x[0] == 0.0:
        return 0
    return norm*(par[1]*x[0]/((par[0])*(par[0]))*ROOT.TMath.Exp(-x[0]*x[0]/(2*(par[0])*(par[0]))))

def RayleighFunctionShifted(x,par,norm):
    if par[0]+par[1]*x[0] == 0.0:
        return 0
    return norm*(par[1]*x[0]/((par[0])*(par[0]))*ROOT.TMath.Exp(-x[0]*x[0]/(2*(par[0]*par[0]+2*par[2]*par[0]*x[0]+par[2]*par[2]*x[0]*x[0]))))

def QCDFunctionWithPeakShift(x,par,norm=1):
    return norm*(RayleighFunctionShifted(x,par,1)+par[3]*ROOT.TMath.Gaus(x[0],par[4],par[5],1)+par[6]*ROOT.TMath.Exp(-par[7]*x[0]))

#def EWKFunction(x,par,boundary,norm=1,rejectPoints=0):
#    if x[0] < boundary:
#        return norm*par[0]*ROOT.TMath.Gaus(x[0],par[1],par[2],1)
#    C = norm*par[0]*ROOT.TMath.Gaus(boundary,par[1],par[2],1)*ROOT.TMath.Exp(boundary*par[3])
#    return C*ROOT.TMath.Exp(-x[0]*par[3])

