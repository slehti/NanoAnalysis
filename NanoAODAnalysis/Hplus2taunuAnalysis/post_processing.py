import enum
import ROOT
from ROOT import gDirectory as gdir
import sys
from pathlib import Path
from argparse import ArgumentParser
import copy
import re, os

from nanoanalysis.hist2root import convertCounter
'''
This script adds the dataVersion field to pseudomulticrabs produced in analysis.py
Additionally it can rename the analysis folder in the result rootfiles

USAGE: python post-processing.py  <resultdir1> [<resultdir2> ...] [options]

type the version names without the postfix specifying if the rootfile contains data or mc,
it will automatically be inferred from the configInfo/isData field.

Warning: any previous dataVersion objects in configInfos traversed by this script will be
overwritten! (with the one exception explained below)

Example: python post_processing.py . -v 2017XUL_106 -n 80to1000_Run2017UL -c --qcd_splitted_bins 1,4,3 -p

The above example will look for all rootfiles in the current working directory and add the defined fields and histograms
defined in the options. An exception is with normalised QCDMeasurementMT pseudomulticrabs, which already should have the
dataVersion set as "pseudo". These are left untouched by this script.
'''
ENERGY=13
QCDBINS="Control,tauPt,tauEta"
SIGBINS="Control"

def parse_args():
    parser = ArgumentParser(description="")
    parser.add_argument("pseudo_locations", nargs='+', help='where to look for pseudomulticrab files to process')
    parser.add_argument("-v", "--data_version", help = "for example 2017XUL_106")
    parser.add_argument("-n", "--newname", help='name for the analysis results folder in the rootfile, for example 80to1000_Run2017UL')
    parser.add_argument("-c", "--clean", action = "store_true", help='remove the original analysis folder after results have been copied to the folder with the new name')
    parser.add_argument("--qcd_splitted_bins", default="1", help='comma separated bin counts.')
    parser.add_argument("--qcd_binlabels", default=QCDBINS, help=f'comma separated bin labels for qcd measurement, default: "{QCDBINS}"')
    parser.add_argument("--sig_splitted_bins", default="1", help='comma separated bin counts.')
    parser.add_argument("--sig_binlabels", default=SIGBINS, help=f'comma separated bin labels for signal analysis, default: "{SIGBINS}"')
    parser.add_argument("-E", "--energy", type=int, default=ENERGY, help=f'energy of the data, default: {ENERGY}')
    parser.add_argument("-p", "--isPUReweighted", action="store_true",help='')
    parser.add_argument("-t", "--isTopPtReweighted", action="store_true",help='')
    parser.add_argument("-o", "--old_name", default="analysis", help="original name of the analysis directory in the rootfiles")
    return parser.parse_args()

def copy_dir(src, newname=None):
    src_keys = src.GetListOfKeys()
    savdir = ROOT.gDirectory.GetDirectory('')
    name = src.GetName() if newname is None else newname
    new = savdir.mkdir(name)
    new.cd()
    for key in src_keys:
        classname = key.GetClassName()
        cls = ROOT.gROOT.GetClass(classname)
        if not cls: continue
        if cls.InheritsFrom("TDirectory"):
            src.cd(key.GetName())
            subdir = ROOT.gDirectory.GetDirectory('')
            new.cd()
            copy_dir(subdir)
            new.cd()
        elif cls.InheritsFrom("TTree"):
            t = src.Get(key.GetName())
            new.cd()
            newt = t.CloneTree(-1,"fast")
            newt.Write()
        else:
            src.cd()
            obj = key.ReadObj()
            new.cd()
            obj.Write()
            del obj
    new.SaveSelf(ROOT.kTRUE)
    savdir.cd()

def write_confinfo_h(args):
    gdir.Delete("configinfo;*")
    control=1
    energy = control * args.energy
    isdata = control * gdir.Get("isdata").GetBinContent(1)
    ptreweight = control * (not isdata and args.isPUReweighted)
    topreweight = control * (not isdata and args.isTopPtReweighted)
    d = {
        'control': control,
        'energy': energy,
        'isData': isdata,
        'isPileupReweighted': ptreweight,
        'isTopPtReweighted': topreweight
    }
    return convertCounter(d, "configinfo").Write()


def main(args):
    pseudos = args.pseudo_locations
    assert len(pseudos) > 0
    rfiles = [Path(d).rglob("*.root") for d in pseudos]
    rfiles = (str(filepath) for pseudofiles in rfiles for filepath in pseudofiles)
    for fname in rfiles:
        print("opening "+fname)
        f = ROOT.TFile.Open(fname,"update")
        keys = f.GetListOfKeys().Clone()
        if "configInfo" not in keys: f.Close(); continue
        f.cd("configInfo")
        write_confinfo_h(args)
        f.cd()

        for key in keys:
            if key.GetName() != "configInfo":
                classname = key.GetClassName()
                cls = ROOT.gROOT.GetClass(classname)
                if not cls: continue
                if cls.InheritsFrom("TDirectory"):
                    f.cd(key.GetName())
                    if "counters" in gdir.GetListOfKeys():
                        gdir.rmdir("counters")
                    gdir.mkdir("counters")
                    gdir.cd("counters")
                    unw_counters = f.Get("configInfo/unweighted_counter")
                    unw_counters.SetName("counter")
                    unw_counters.Write()
                    gdir.mkdir("weighted")
                    gdir.cd("weighted")
                    w_counters = f.Get("configInfo/weighted_counter")
                    w_counters.SetName("counter")
                    w_counters.Write()

                    f.cd(key.GetName())
                    if args.qcd_splitted_bins and "QCDMeasurement" in f.GetName():
                        gdir.Delete("SplittedBinInfo;*")
                        gdir.Delete("splittedBinInfo;*")
                        bincounts = [int(n) for n in args.qcd_splitted_bins.split(",")]
                        nbins = len(bincounts)
                        bin_h = ROOT.TH1D("SplittedBinInfo","",nbins,0,nbins)
                        for i, label in enumerate(args.qcd_binlabels.split(",")):
                            n = i+1
                            bin_h.GetXaxis().SetBinLabel(n,label)
                            bin_h.SetBinContent(n,bincounts[i])
                        bin_h.Write()
                    elif args.sig_splitted_bins and "QCDMeasurement" not in f.GetName():
                        gdir.Delete("SplittedBinInfo;*")
                        gdir.Delete("splittedBinInfo;*")
                        bincounts = [int(n) for n in args.sig_splitted_bins.split(",")]
                        nbins = len(bincounts)
                        bin_h = ROOT.TH1D("SplittedBinInfo","",nbins,0,nbins)
                        for i, label in enumerate(args.sig_binlabels.split(",")):
                            n = i+1
                            bin_h.SetBinContent(n,bincounts[i])
                            bin_h.GetXaxis().SetBinLabel(n,label)

                        bin_h.Write()

        f.cd()
        if args.newname:
            if args.old_name in keys:
                is_qcd = "QCDMeasurement" in f.GetName().split("/")[-4]
                prefix = "QCDMeasurement_" if is_qcd else "SignalAnalysis_"
                newname = prefix + args.newname
                if f.Get(newname):
                    assert newname != args.old_name
                    f.rmdir(newname)
                copy_dir(f.Get(args.old_name), newname)
                if args.clean:
                    f.rmdir(args.old_name)
                    [f.rmdir(key.GetName()) for key in keys if any(i in key.GetName() for i in ["QCDMeasurement_", "SignalAnalysis"])]


        if args.data_version:
            ver = args.data_version
            if "QCDMeasurementMT" in f.GetName(): f.Close(); continue
            f.cd("configInfo")
            ROOT.gDirectory.Delete("dataVersion;*")
            isdata = bool(f.Get("configInfo/isdata").GetBinContent(1))
            postfix = "data" if isdata else "mc"
            dataver = ROOT.TNamed("dataVersion", ver + postfix)
            dataver.Write()
            f.cd()
            f.Close()

if __name__ == "__main__":
    args = parse_args()
    main(args)
