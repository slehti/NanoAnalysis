# Guide for processing nanoAOD events with local conda env or on lxplus

## End-to-end instructions for running analysis on lxplus (no neural network)
Because of the space limitations of afs home, the afs workspace is highly recommended.
A containerized version of the environment is planned, but not yet available.

### First time setup
Clone the NanoAnalysis repository to the desired location, it is recommended to have atleast 15G of available disk space!
Note: You cannot have an activated CMSSW release when installing or running the analysis.

        $ git clone https://gitlab.cern.ch/HPlus/NanoAnalysis.git
        $ cd NanoAnalysis

A container with the analysis dependencies is in progress, but for now the analysis can
be distributed to a dask HTCondorCluster running on CERN lxplus batch servers with
a python virtual environment set up as follows:

        $ python3 -m venv venv
        $ . venv/bin/activate
        $ pip install coffea distributed dask-jobqueue xrootd bokeh
        $ pip install -e NanoAODAnalysis/Framework

### run the analysis

With the environment active, all options of the analysis.py are valid, so you can
run on the login node with `--no-distribute` or with a batch cluster by default,
as long as your VOMS proxy is set up as normal.
A list of all available options can be printed out by running

        $ python3 analysis.py -h

By setting up an ssh port forward from the port `31486` (configurable via `--dash-port <port_no>`),
you can monitor the progress
of the processing and the cluster health from your browser by navigating to
`localhost:<forwarded_port>/status`


### post-process the results
For compatibility to the old HiggsAnalysis repo which is still used for the fake tau measurement normalization,
run the post-processing.py script. Instructions on the options are printed out with the -h flag.
Note that the current version of the analysis automatically names the result directories,
so the `--name` argument is not needed. Unlike analysis.py, post processing depends on PyROOT
(ROOT can be obtained via conda or used directly on lxplus without a virtual environment).

        $ python post-processing.py . -v <dataVersion> --qcd_splitted_bins 1,4,3 -p

## Installation with conda environment (use for neural network training)

1. Install miniconda

        $ wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
        $ bash Miniconda3-latest-Linux-x86_64.sh

2. close and reopen terminal for the changes to take effect

3. (Situational) copy your grid certificate files onto your machine, here is demonstrated the situation where you have your certificates already configured on lxplus

        $ scp -r <CERNusername>@lxplus.cern.ch:~/.globus ~/.

4. create conda environment from the template included in this repository

        $ conda config --set channel_priority strict
        $ conda env create -f <PATH_TO_YMLFILE>/conda_env.yml
        $ conda activate <my-environment>

5. Install latest coffea

        $ pip install coffea

6. Local grid access (Not applicable for lxplus)

        $ mkdir localgrid
        $ cd localgrid/
        $ git clone https://github.com/dmwm/dasgoclient.git
        $ cd dasgoclient
        $ make build_all
        $ cd ~
        $ mkdir .grid-security
        $ scp -r <CERNusername>@lxplus.cern.ch:/cvmfs/grid.cern.ch/etc/grid-security/* .grid-security
        $ voms-proxy-init --voms cms --vomses ~/.grid-security/vomses -valid 192:00

7. Install TensorFlow (with optional GPU support)
    1. CUDA support (for NVIDIA GPUS)
        - make sure you have access to a GPU on the machine by running

                $ nvidia-smi

        - The output should show information about your GPU, including power draw, utilization and driver version.
        - if the above step didn't work, make sure you have an nvidia gpu installed on the machine with the appropriate drivers from [nvidia](https://www.nvidia.com/Download/index.aspx) and that your user has access to it.
        - Install Tensorflow-compatible CUDA toolkit and cuDNN versions

                $ conda install -c conda-forge cudatoolkit=11.2 cudnn=8.1.0
                $ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/

        - Automate path configurations

                $ mkdir -p $CONDA_PREFIX/etc/conda/activate.d
                $ echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/' > $CONDA_PREFIX/etc/conda/activate.d/env_vars.sh

    2. TensorFlow Installation

            $ pip install --upgrade pip
            $ pip install tensorflow
            $ pip install tensorflow-probability
            $ pip install tensorflow-addons

8. If you find that modules are still missing after following this guide or run into other issues, please open an issue or better yet, modify this readme yourself and create a merge request!

9. Dependencies for scaling analysis to CERN batch with dask (neural networks not implemented yet)

        $ conda install dask-jobqueue

## Running the analysis (event selection)

- First activate your environment if it is not already activated

        $ conda activate <CONDA_ENV_NAME>
or
        $ source VirtualTestEnvironment/bin/activate

- Training neural DNN classifier (Optional)
    - Instructions in the next section

- Running the event selection and histograms

        $ python analysis.py <multicrabdir> [<nn_savedir> <nn_score_cut>]

## Instructions for training neural networks
1. save training data
    This processes the input multicrabdir and saves the model input variables of events passing the selections defined in the Writer.process() function as tensorflow Datasets in saveDir. Saved data is by default 2-folded, but more folds can be specified.
    - Input variable definitions in Writer.get_tf_inputs() and Writer.get_np_inputs() (TODO: these should be combined)
    - Event selections defined in Writer.process()

            $ python ANN/datasetUtils.py <multicrabdir> <outdir> [<k_folds>] [<nn_input_variables>]

2. train a model
    - you can get a list of the options and instructions by running "python ANN/disCo.py -h"
    loads the datasets from saved in the previous step and trains a DNN to predict signal/background. Parametric DNNs can be trained with the option --param_mass and the DNN can additionally be configured to regress the signal invariant mass with the option --regress_mass.

            $ python ANN/disCo.py --dataDir <datasetUtils_outdir> --saveDir <where/to/save/results> [options]


## Running limit calculation within the new framework

See instructions in the LimitCalc [readme](../LimitCalc/README.md).
Configuration files for simplified systematics (lnN type only) are
included in the repository. This still requires the data-driven background measurement
using the old framework, this is performed in the following steps:

1. after post-processing the results from analysis.py, copy the resulting pseudo-multicrabs to the old HiggsAnalysis framework.
2. Calculate data-driven backgrounds for both categorisations according to the instructions in HiggsAnalysis/NtupleAnalysis/src/QCDMeasurement/work/readme.txt, starting from the normalization (part 3). Use the python3 fork!
3. Now, move the QCDMeasurementMT directories from the created pseudomulticrab into your signal analysis pseudomulticrabs.

When all the required histograms are found inside the pseudomulticrabs, follow the LimitCalc
instructions with the configuration file optionally modified to combine or separate data by
Rtau and/or measurement year.


## Running limit calculation with the modified frequentist CLs method (old framework, being deprecated).

1. after post-processing the results from analysis.py, copy the resulting pseudo-multicrabs to the old HiggsAnalysis framework.
2. Calculate data-driven backgrounds for both categorisations according to the instructions in HiggsAnalysis/NtupleAnalysis/src/QCDMeasurement/work/readme.txt, starting from the normalization (part 3). Use the python3 fork!
- You will need to rename all instances of "AfterStdSelections_h_met" to "h_mt" and "Nominal" to "Inverted" in the normalization factor python file for data-driven pseudomulticrab creation (part 4)
3. Move the results of the data-driven measurement together with the signal analysis into a folder in NtupleAnalysis/src/LimitCalc/work
4. generate datacards with the cmssw8x_ApprovedAnalysis_python3 fork:
        $ ./dcardGenerator.py -x <datacard_template> --dir <directory_with_signalanalysis+qcdresults>
example:
        $ ./dcardGenerator.py -x dcard2017Datacard --dir 2017_results
5. Calculate limits with combine on lxplus, instructions in [twiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/HPlus-SW-Run2Legacy)
