#ifndef RDF_UTILS_INCLUDED
#define RDF_UTILS_INCLUDED
#include "ROOT/RVec.hxx"
#include "TH1.h"
#include <algorithm>
#include <limits>

float mT_tau_met(
    float tau_mass,
    float tau_pt,
    float tau_phi,
    float met_pt,
    float met_phi
);

float Hplus_mass(
    ROOT::RVec<int> genpart_id,
    ROOT::RVec<float> genpart_m
);

ROOT::RVec<bool> filter_taus(
    ROOT::RVec<int> trig_id,
    ROOT::RVec<float> trig_eta,
    ROOT::RVec<float> trig_phi,
    ROOT::RVec<float> tau_eta,
    ROOT::RVec<float> tau_phi,
    ROOT::RVec<int> idVSe,
    ROOT::RVec<int> idVSmu,
    ROOT::RVec<float> tau_pt,
    ROOT::RVec<int> tau_dm,
    ROOT::RVec<float> tau_rtau,
    float pt_min,
    float eta_max,
    int cut_vse,
    int cut_vsmu
);

bool lep_veto(
    ROOT::RVec<float> ele_pt,
    ROOT::RVec<float> ele_eta,
    ROOT::RVec<float> ele_iso,
    ROOT::RVec<bool> loose_id,
    float max_pt,
    float min_eta,
    float min_iso
);

ROOT::RVec<bool> jet_filter(
    ROOT::RVec<float> jet_pt,
    ROOT::RVec<float> jet_eta,
    ROOT::RVec<float> jet_phi,
    float tauh_eta,
    float tauh_phi,
    ROOT::RVec<bool> jet_id,
    float pt_min,
    float eta_max,
    float tau_threshold
);

ROOT::RVec<bool> count_bjets(
    ROOT::RVec<bool> jet_ishadronic,
    ROOT::RVec<float> jet_eta,
    ROOT::RVec<float> jet_btag,
    float cut,
    float eta_max
);

#endif

