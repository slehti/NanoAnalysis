import argparse
import glob
import pickle
from pathlib import Path
import os
import time

from nanoanalysis.PileupWeight import RDF_PileupWeight
import ROOT as R
import numpy as np
import tensorflow as tf

from nanoanalysis.hist2root import convert

import rdf_selections
import nanoanalysis.tools.crosssection as crossection
from nanoanalysis import (
    multicrabdatasets,
    TransverseMass,
    LumiMask,
    cpptools
)
from nanoanalysis.rdf_backend import Utils


# by default this gets overwritten by disCo, where the mass parameter is propagated from the previous signal sample to the background
# If no_copy_mass is specified (running disCo.py), then the mass parameter will be sampled uniformly from MDATA
MDATA = np.array([170,175,200,220,250,400,500,700,800,2500,3000])

# transverse mass bins for planing
MT_BINS = np.array([0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100,105,110,115,120,125,130,135,140,145,150,160,170,180,190,200,210,220,230,240,250,260,280,300,320,340,360,380,400,450,500,550,600,700,800,1000,1500,2000,3000],
                    dtype=float) #TODO: enable user input for bin edge specification or otherwise do it smarter
BOOL_BINS = np.array([0,0.5,1.5])
EPS = 1e-3
#tf.random.set_seed(1234)
TF_OP_SEED = 8765
FULLVARS = [
    "leadTau_pt",
	"MET_pt",
	"rtau",
	"btagDeepB",
	"dPhiTauNu",
	"dPhiTauJets",
	"dPhiNuJets",
	"dPhiAllJets",
	"HJet_pt",
	"mT",
	"m_true",
]
FULLSIZES = [1,1,1,4,1,4,4,6,4,1,1]
COLUMN_KEYS = {
    "btag_all": ['btag'] * 4,
    "dPhiAllJets": ['dPhiJetJet'] * 6,
    "Jets_pt": ['jet_pt'] * 4,
    "dPhiTauJets": ['dPhiTauJet'] * 4,
    "dPhiNuJets": ['dPhiNuJet'] * 4,
}

def parse_args():
    parser = argparse.ArgumentParser(
        "Create ML training dataset from multicrab",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("multicrab", help="The input dataset")
    parser.add_argument("outdir", help="The input dataset")
    parser.add_argument(
        "--whitelist", nargs='*', default=[], help="Whitelisted datasets"
    )
    parser.add_argument(
        "--blacklist", nargs='*', default=[], help="Blacklisted datasets"
    )
    parser.add_argument(
        "-k", "--folds", type=int, default=2, help="cross-validation splits"
    )
    parser.add_argument(
        "-x", "--features", nargs='*', default=FULLVARS,
        help="RDataFrame columns to save for ML model training"
    )
    parser.add_argument(
        "--x-lengths", nargs='*', type=int, default=FULLSIZES,
        help="vector lengths of RDataFrame columns. 1 for non-vector values"
    )
    parser.add_argument(
        "--transfer-factors", default=None,
        help="file containing binned jet -> tau transfer factors"
    )
    return parser.parse_args()

def cols_to_numpy(df, cols, col_lengths):
    nested_cols = [[c] if l == 1 else [f"{c}_{i}" for i in range(l)] for c,l in zip(cols,col_lengths)]
    flat_cols = [c for cols in nested_cols for c in cols]
    return np.stack(list(df.AsNumpy(flat_cols).values()),axis=-1), flat_cols

def vec_to_floats(df, column, length):
    df = df.Redefine(column, f"nanoanalysis::fix_size({column}, {length})")
    for i in range(length):
        df = df.Define(f"{column}_{i}", f"{column}[{i}]")
    return df

def merge_histos(result_list, histo_name):
    added = result_list[0][histo_name].Clone()
    for r in result_list[1:]:
        added.Add(r[histo_name].Clone())
    return added

def get_faketau_histos(results):

    data = [r for r in results if r["dataset"].isData]
    mc = [r for r in results if (not r["dataset"].isData) and (not "ChargedHiggs" in r["dataset"].name)]

    data_count_base = merge_histos(data, "binned_baseline_count")
    data_count_alt = merge_histos(data, "binned_altered_count")
    genuine_count_base = merge_histos(mc, "binned_baseline_count")
    genuine_count_alt = merge_histos(mc, "binned_altered_count")

    data_count = {"baseline": data_count_base, "altered": data_count_alt}
    genuine_count = {"baseline": genuine_count_base, "altered": genuine_count_alt}
    return data_count, genuine_count

def project_yz_continuous(hist, start, stop):
    h = hist.Clone()
    h.GetXaxis().SetRange(start, stop)
    return h.Project3D("zy") # the fact that root reverses the dims is ridiculous

def slice_train_fold(hist, fold):
    upper = hist.GetNbinsX()
    if fold == 0:
        projected = project_yz_continuous(hist, 2, upper)
    elif fold == upper:
        projected = project_yz_continuous(hist, 1, upper-1)
    else:
        projected = project_yz_continuous(hist, 1, fold-1)
        projected.Add(project_yz_continuous(hist, fold+1, upper))
    return projected

# normalization of fake tau event data from the anti-isolated to isolated tau
# region. Binned in tau pt and eta, but not using MET templates and fitting yet
def reweight_faketau_data(
    num_data,
    num_genuine,
    results,
    folds
):
    R.gInterpreter.Declare(f"std::vector<TH2D> faketau_weights({folds});")

    num_data_baseline = num_data["baseline"]
    num_data_altered = num_data["altered"]
    num_genuine_baseline = num_genuine["baseline"]
    num_genuine_altered = num_genuine["altered"]

    for fold in range(folds):

        num_fake_baseline = slice_train_fold(num_data_baseline, fold)
        num_fake_baseline.Add(slice_train_fold(num_genuine_baseline, fold),-1)

        num_fake_altered = slice_train_fold(num_data_altered, fold)
        num_fake_altered.Add(slice_train_fold(num_genuine_altered, fold),-1)

        weight_fake_altered_to_baseline = num_fake_baseline.Clone()
        weight_fake_altered_to_baseline.Divide(num_fake_altered)

        fake_purity = num_fake_baseline.Clone()
        fake_purity.Divide(slice_train_fold(num_data_baseline, fold))

        weight = weight_fake_altered_to_baseline.Clone()
        weight.Multiply(fake_purity)

        R.faketau_weights[fold] = weight.Clone()

        for i, r in enumerate(results):
            if r["dataset"].isData:
                df = r["df"]

                if fold == 0:
                    idx_def = "faketau_weights[fold].FindFixBin(leadTau_pt, leadTau_abseta)"
                    df = df.Define("faketau_bin", idx_def)

                weight_def = "w*faketau_weights[fold].GetBinContent(faketau_bin)"
                df = df.Define(f"w_fold{fold}", weight_def)
                r["df"] = df
            else:
                r["df"] = r["df"].Define(f"w_fold{fold}", "w")
            results[i] = r

    return

def filter_isolated_tau(results):
    for i, r in enumerate(results):

        # for data, only train on inverted tau isolation events
        if r["dataset"].isData:
            df = r["df"]
            df = df.Filter("!Event_tauIso", "Inverted tau isolation")
            results[i] = r

        # for MC, only train on nominal tau isolation, matched with gen lepton
        else:
            r["df"] = (
                r["df"].Filter("Event_tauIso", "Nominal tau isolation")
                .Filter("leadTau_not_misid_jet", "Gen lepton matched tau")
            )

    return results


def process(d, pu_weighter, args, lumimask, lumi):
    year = int(d.year)
    energy = "13.6" if year > 2020 else "13"

    time.sleep(1)
    df = R.RDataFrame("Events",d.getFileNames())

    if not d.isData:
        df = pu_weighter.apply_weights(df, d.name)
        xsec = crossection.backgroundCrossSections.crossSection(d.name, energy)
        nev = d.getEvents()
        if xsec is None:
            print(f"Using cross-section 1 pb for {d.name}")
            xsec = 1
        norm = lumi*xsec/nev

        df = df.Define("w", f"pileup_w * genWeight / abs(genWeight) * {norm}")
    else:
        df = df.Define("w", "1.0")

    df = lumimask(df, d.isData)
    df = rdf_selections.trigger(df, d.year)
    df = rdf_selections.METCleaning(df, d.year)
    if int(year) > 2020:
        df = rdf_selections.tau_id(
            df,
            is_data=d.isData,
            id_vse = "Tau_idDeepTau2018v2p5VSe",
            id_vsmu = "Tau_idDeepTau2018v2p5VSmu",
            id_vsjet = "Tau_idDeepTau2018v2p5VSjet",
            jet_wp = 4,
            e_wp = 6,
        )
    else:
        df = rdf_selections.tau_id(df, is_data=is_data)

    if int(year) > 2020:
        ele_disc = "Electron_mvaIso_WP80"
        df = rdf_selections.e_veto(df, iso_discriminator=ele_disc)
    else:
        df = rdf_selections.e_veto(df)

    df = rdf_selections.mu_veto(df)
    df = rdf_selections.hadr_jets(df)
    df = TransverseMass.rdf_reconstruct_transverse_mass(df)

    # for data, we can train to discriminate the background enriched phase space
    if d.isData:
        # df = df.Filter("!Event_tauIso", "Inverted tau isolation")
        df = df.Define("m_true", "std::numeric_limits<double>::quiet_NaN()")
    # for mc, we can train to discriminate against isolated taus
    else:
        # df = df.Filter("Event_tauIso", "Nominal tau isolation")
        df = df.Define("m_true", "Hplus_mass(GenPart_pdgId, GenPart_mass)")

    df = df.Define(
        "rtau",
        """
        nanoanalysis::Rtau(
            leadTau_pt,
            leadTau_eta,
            leadTau_leadTkPtOverTauPt,
            leadTau_leadTkDeltaEta
        )
        """
    )

    df = df.Define("fold", f"event % {args.folds}")
    if "btagPNetB" in args.features:
        df = df.Define("btagPNetB", "Jet_btagPNetB[Jet_passed]")
    if "btagDeepB" in args.features:
        df = df.Define("btagDeepB", "Jet_btagDeepB[Jet_passed]")
    if "dPhiTauNu" in args.features:
        df = df.Define("dPhiTauNu", "ROOT::VecOps::DeltaPhi(leadTau_phi, MET_phi)")
    if "dPhiTauJets" in args.features:
        df = df.Define("dPhiTauJets", "nanoanalysis::dPhi(leadTau_phi, HJet_phi)")
    if "dPhiNuJets" in args.features:
        df = df.Define("dPhiNuJets", "nanoanalysis::dPhi(MET_phi, HJet_phi)")
    if "dPhiAllJets" in args.features:
        s = args.x_lengths[args.features.index("dPhiAllJets")]
        df = df.Define("dPhiAllJets", f"nanoanalysis::dPhi_combinations(nanoanalysis::fix_size(HJet_phi, {s}))")


    # df.AsNumpy does not support vector form columns
    for s, x_name in zip(args.x_lengths, args.features):
        if x_name == "dPhiAllJets":
            s = (s-1)*s//2
        if s > 1:
            df = vec_to_floats(df, x_name, s)

    ret = {"df": df, "dataset": d}

    pt_bins = np.array([0.,60.,80.,100.,np.inf])
    nbins_pt = len(pt_bins)-1
    eta_bins = np.array([0.,0.6,1.4,np.inf])
    nbins_eta = len(eta_bins)-1
    fold_bins = np.arange(args.folds+1, dtype=float) - 0.5
    nbins_fold = len(fold_bins)-1


    if d.isData:
        ret["binned_baseline_count"] = (
            df.Filter("Event_tauIso")
            .Histo3D(
                (f"{d.name}_baseline", "baseline count;fold;pT;#eta",
                 nbins_fold, fold_bins,nbins_pt, pt_bins,nbins_eta, eta_bins),
                "fold", "leadTau_pt", "leadTau_abseta", "w"
            )
        )
        ret["binned_altered_count"] = (
            df.Filter("!Event_tauIso")
            .Histo3D(
                (f"{d.name}_altered", "baseline count;fold;pT;#eta",
                 nbins_fold, fold_bins,nbins_pt, pt_bins,nbins_eta, eta_bins),
                "fold", "leadTau_pt", "leadTau_abseta", "w"
            )
        )
    else:
        ret["binned_baseline_count"] = (
            df.Filter("Event_tauIso").Filter("leadTau_genuine")
            .Histo3D(
                (f"{d.name}_baseline", "baseline count;fold;pT;#eta",
                 nbins_fold, fold_bins,nbins_pt, pt_bins,nbins_eta, eta_bins),
                "fold", "leadTau_pt", "leadTau_abseta", "w"
            )
        )
        ret["binned_altered_count"] = (
            df.Filter("!Event_tauIso").Filter("leadTau_genuine")
            .Histo3D(
                (f"{d.name}_altered", "baseline count;fold;pT;#eta",
                 nbins_fold, fold_bins,nbins_pt, pt_bins,nbins_eta, eta_bins),
                "fold", "leadTau_pt", "leadTau_abseta", "w"
            )
        )

    return ret

def main(args):
    R.EnableImplicitMT()
    print(f"Starting ML training dataset saving script at {time.strftime("%c")}")

    datasets = multicrabdatasets.getDatasets(
        args.multicrab,
        args.whitelist,
        args.blacklist
    )
    lumi = multicrabdatasets.loadLuminosity(args.multicrab,datasets)
    cpptools.jit_include("rdf_utils.cpp", "rdf_utils.h")
    LumiMask.RDF_LumiMask.declare()
    TransverseMass.rdf_declare_mT()
    cpptools.initialize(Utils)

    pu_weighter = RDF_PileupWeight(args.multicrab, datasets)

    col_keys=[]
    results = []
    lmasks = []
    for i, d in enumerate(datasets):
        lumimask = LumiMask.RDF_LumiMask(d)
        lmasks.append(lumimask) # stop gc
        result = process(d, pu_weighter, args, lumimask, lumi)
        results.append(result)

    num_data, num_genuine = get_faketau_histos(results)

    reweight_faketau_data(
        num_data,
        num_genuine,
        results,
        args.folds
    )
    print(f"Calculated fake tau transfer factors at {time.strftime("%c")}")

    # we only want to train on isolated/anti-isolated tau events for MC/data
    results = filter_isolated_tau(results)

    for i, r in enumerate(results):

        d = r["dataset"]
        full_df = r["df"]
        suboutpath = Path(args.outdir) / d.name
        (suboutpath / "data").mkdir(parents=True,exist_ok=True)

        with R.TFile.Open(str(suboutpath / "mt_hist.root"), "RECREATE"):

            for fold in range(args.folds):
                df = full_df.Define("test", f"fold == {fold}")
                mt_hist= df.Histo2D(
                    (
                        f"mt_hist_{fold}", "m_T;m_T [GeV];test;Events",
                         len(MT_BINS)-1,MT_BINS,len(BOOL_BINS)-1,BOOL_BINS
                    ),
                    "mT", "test", f"w_fold{fold}"
                )
                fold_testdata, col_keys = cols_to_numpy(
                    df.Filter("test"),
                    args.features + [f"w_fold{fold}"], args.x_lengths + [1]
                )
                fold_traindata, _ = cols_to_numpy(
                    df.Filter("!test"),
                    args.features + [f"w_fold{fold}"], args.x_lengths + [1]
                )
                np.save(suboutpath / f"data/fold_{fold}_test", fold_testdata)
                np.save(suboutpath / f"data/fold_{fold}_train", fold_traindata)
                mt_hist.GetValue().Write()

        print(f"Finished processing dataset {d.name} at {time.strftime("%c")}")

    # make sure the weights for each fold are all saved with the column name 'w'
    col_keys = ["w" if "w_fold" in x else x for x in col_keys]
    with open(Path(args.outdir) / "input_columns.txt", "w") as f:
        f.write('\n'.join(col_keys) + '\n')
    return

if __name__ == "__main__":
    main(parse_args())

