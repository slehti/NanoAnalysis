import os
import argparse
import multiprocessing
import subprocess
import shutil
import getpass
import operator
from functools import reduce
import json

MAX_JOBS = 12
THREADS = 4
DASH_PORT = 31486
MAX_WORKERS = max(multiprocessing.cpu_count() // THREADS,1)

CHUNKSIZE = 50_000

# defining magic variables, should some day be moved to some kind of config (file, script arguments?)
MEASUREMENT_BINS = [["Rtau75", "Rtau1", "RtauIncl"],]
QCDSTR = "QCDMeasurement"
QCDSEP = ["SignalAnalysis", QCDSTR]
QCD_DIRS = { # for compatibility with old HiggsAnalysis code
    'ForQCDNormalization': [
        "NormalizationMETBaselineTauAfterStdSelections",
        "NormalizationMtBaselineTauAfterStdSelections",
        "NormalizationMETInvertedTauAfterStdSelections",
        "NormalizationMtInvertedTauAfterStdSelections",
    ], 'ForQCDNormalizationEWKFakeTaus': [
        "NormalizationMETBaselineTauAfterStdSelections",
        "NormalizationMtBaselineTauAfterStdSelections",
        "NormalizationMETInvertedTauAfterStdSelections",
        "NormalizationMtInvertedTauAfterStdSelections",
    ], 'ForQCDNormalizationEWKGenuineTaus': [
        "NormalizationMETBaselineTauAfterStdSelections",
        "NormalizationMtBaselineTauAfterStdSelections",
        "NormalizationMETInvertedTauAfterStdSelections",
        "NormalizationMtInvertedTauAfterStdSelections",
    ], 'ForQCDMeasurement': [
        "BaselineTauShapeTransverseMass",
    ], 'ForQCDMeasurementEWKFakeTaus': [
        "BaselineTauShapeTransverseMass",
    ], 'ForQCDMeasurementEWKGenuineTaus': [
        "BaselineTauShapeTransverseMass",
    ],
}

SEPARATE = MEASUREMENT_BINS + [QCDSEP,]#, ["nn_50", "nn_90", "nn_1"]])
ANALYSISTYPE = "80to1000"
ss = "\033[92m"
ns = "\033[0;0m"
ts = "\033[1;34m"
hs = "\033[0;35m"
ls = "\033[0;33m"
es = "\033[1;31m"
cs = "\033[0;44m\033[1;37m"


try:
    from dask_jobqueue import HTCondorCluster
    import socket
    submission_command = shutil.which("condor_submit")
    assert submission_command is not None
    try:
        usr = getpass.getuser()
        initial = usr[0]
        bash_command = "voms-proxy-info -path"
        proxy_stored_path = subprocess.run(bash_command.split(), check=True, text=True, capture_output=True).stdout.strip()
        proxy_name = proxy_stored_path.split("/")[-1]
        PROXYPATH = f"/afs/cern.ch/user/{initial}/{usr}/private/{proxy_name}"
        shutil.copyfile(proxy_stored_path, PROXYPATH)
        DISTRIBUTED=True
    except Exception as e:
        DISTRIBUTED = False
        print(e)
        print(ls + "Failed transferring grid proxy to available location, will run analysis iteratively" + ns)
except:
    DISTRIBUTED = False
    print(ss + "Missing dependencies for distributing to HTCondor, will run analysis iteratively" + ns)



def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("multicrab", type=str, help="Input dataset path")

    parser.add_argument("-o", "--output", type=str, default="", help="Output pseudomulticrab location")

    parser.add_argument("--unblinded", action="store_true", help="Include this flag to unblind the data")

    parser.add_argument(
        "--distribute", action=argparse.BooleanOptionalAction, default=DISTRIBUTED,
        help="Specify if the analysis should be distributed via HTCondor"
    )

    parser.add_argument("--nn-cutoff", "-c", type=float, default=0.0, help="DNN selection working point")

    parser.add_argument("--nn-weights", default=None, help="location of the saved DNN model weights")

    parser.add_argument("--chunksize", type=int, default=CHUNKSIZE, help="Chunk size for columnar processing")

    parser.add_argument("--max-jobs", type=int, default=MAX_JOBS, help="Number of distributed workers")

    parser.add_argument("--max-workers", type=int, default=MAX_WORKERS, help="Number of local workers")

    parser.add_argument("--worker-threads", type=int, default=THREADS, help="Number of threads per worker")

    parser.add_argument("--dash-port", type=int, default=DASH_PORT, help="Port where the dask dashboard is served")

    parser.add_argument('--whitelist', nargs='*', help='List of datasets to process')

    parser.add_argument('--blacklist', nargs='*', help='List of datasets to exclude from processing')

    parser.add_argument("-v", "--data_version", default="20XX_YYY", help="data version/release. For example 2017XUL_106")

    return parser.parse_args()

def legacy_run_name(year):
    if "202" in year:
        ul_text = ""
    else:
        ul_text = "UL"
    if "APV" in year:
        run = year.replace("APV", ul_text + "APV")
    else:
        run = year + ul_text
    return run

def name_matches_cat(name, cat, alts):
    return (cat in name) or all(not altcat in name for altcat in alts)

def belongs_to(category, name): # split results into QCDMeasurement and SignalAnalysis, for backwards compatibility
    all_cats = SEPARATE
    qcd_identifiers = ["Inverted", "StdSelections"]
    name_inQCD = any((i in name) for i in qcd_identifiers)
    QCD_matched = not (("QCDMeasurement" in category) ^ name_inQCD) # XNOR of cat matches qcd and name matches qcd
    cond = QCD_matched or (not name_inQCD and any((i in name) for i in ["h_mt_", "h_met"]))
    matched = cond & all([name_matches_cat(name, cat, alternatives) or (cat in ["QCDMeasurement", "SignalAnalysis"]) for cat, alternatives in zip(category, all_cats)])
    return matched

def extend_QCDhist_name(name):
    vars_search = ["pt", "eta"] #NOTE: magic strings
    categories = re.findall("|".join([f"[^_]+{var}" for var in vars_search]), name)
    post = "_".join(categories)
    if len(post) > 0: post = "_"+post
    return post

def navigate_subdirs(key, is_qcd_dir): # for backwards compatibility
    if "vis_" in key: return ""
    is_mt = ("_mt_" in key) or ("_mT_" in key)
    is_met = ("_met_" in key) or ("_MET_pt_" in key)
    is_inverted = "Inverted" in key
    is_nominal = "Nominal" in key
    if not (is_mt or is_met):
        return ""
    if is_qcd_dir:
        dirname = "ForQCD"
        sub = "/Normalization"
        if "AfterStdSelections" in key:
            dirname += "Normalization"
            sub += "Mt" if is_mt else "MET"
            if is_inverted:
                sub += "Inverted"
            elif is_nominal:
                sub += "Baseline"
            else:
                return ""
            sub += "TauAfterStdSelections"
        elif is_nominal and is_mt:
            dirname += "Measurement"
            sub = "/BaselineTauShapeTransverseMass"
        elif is_inverted and is_mt:
            dirname = "ForDataDrivenCtrlPlots"
            sub = "/shapeTransverseMass"
        else:
            dirname = "ForDataDrivenCtrlPlots"
            sub = ""
        if ("MatchedTau" in key) or ("NoTauMatch" in key):
            dirname = "ForDataDrivenCtrlPlots"
            sub = ""
        if "EWKFakeTau" in key:
            dirname += "EWKFakeTaus"
        elif "EWKGenuineTau" in key:
            dirname += "EWKGenuineTaus"
    else:
        if (is_mt or is_met) and not all((i in key) for i in ["ptIncl", "etaIncl"]):
            dirname = "qcd_stuff"; sub = ""
        else:
            dirname = "ForDataDrivenCtrlPlots"
            sub = ""
            if "EWKFakeTau" in key:
                dirname += "EWKFakeTaus"
            elif "EWKGenuineTau" in key:
                dirname += "EWKGenuineTaus"

    # return the current location
    return dirname + sub

def getFromDict(d, keys):
    return reduce(operator.getitem, keys, d)

def get_legacy_name(dname, hname, counts, is_bg, is_qcd): # for backwards compatibility
    if not dname:
        return hname
    path = dname.split("/")
    rename = ["_met_", "_mT_"]
    if (
        all(var not in hname for var in rename)
        or (
            any(i in dname for i in ["ForDataDrivenCtrlPlots", "qcd_stuff"])
            and "shapeTransverseMass" not in dname
        )
    ): # Measurement shape histograms
        if is_bg and not is_qcd: # Genuine tau + isolated lepton fake background
            if all(
                i in hname for i in ["_mT_", "Nominal", "MatchedTau", "EWKIncl"]
            ):
                return "shapeTransverseMass"
            else: return hname
        else: # signal shape histograms
            if all(i in hname for i in ["_mT_", "Nominal", "TauMatchIncl", "EWKIncl"]):
                return "shapeTransverseMass"
            else: return hname
    else: # binned histograms for data driven fake tau background
        newname = path[-1]
    count_idx = 0
    if "ptIncl" in hname and "etaIncl" in hname: newname += "Inclusive"; count_idx = 3
    elif "ptIncl" in hname: newname += "ptInclusive"; count_idx = 1
    elif "etaIncl" in hname: newname += "etaInclusive"; count_idx = 2
    count = getFromDict(counts, path)
    try:
        count[count_idx] += 1
        getFromDict(counts, path[:-1])[path[-1]] = count
        return newname + str(count[count_idx]-1)
    except TypeError:
        return newname

def write_multicrab_cfg(pseudo_dir, datasets):
    d_names = [d.name for d in datasets]
    def is_dataset(folder):
        return folder in d_names

    def get_dset_block(name):
        return f"[{name}]\n\n"

    subfolders = next(os.walk(pseudo_dir))[1]
    names = filter(is_dataset, subfolders)

    with open(os.path.join(pseudo_dir, "multicrab.cfg"), "w") as f:
        [f.write(get_dset_block(name)) for name in names]
    return

def write_lumi_json(pseudo_dir, datasets):
    def is_data(dataset):
        return dataset.isData
    datasets = filter(is_data, datasets)
    lumi = {d.name: d.lumi for d in datasets}

    names_in_pseudo = next(os.walk(pseudo_dir))[1]
    lumi_in_pseudo = {k: v for k, v in lumi.items() if k in names_in_pseudo}
    lumi_json = json.dumps(lumi_in_pseudo, indent = 4)
    with open(os.path.join(pseudo_dir, "lumi.json"), "w") as f:
        return f.write(lumi_json)

def filter_counters(category, counters):
    no_qcd_cat = [x for x in category if all(x != s for s in QCDSEP)]
    no_qcd_alts = MEASUREMENT_BINS

    common = ['Skim', 'JSON', 'trigger', 'MET cleaning']
    def isin(cat, countername):
        return any(
            i in countername for i in common
        ) or (
            all(
                name_matches_cat(countername, c, alts) for c, alts in zip(cat, no_qcd_alts)
            )
        )

    return dict([(key, val) for key, val in counters.items() if isin(no_qcd_cat, key)])

