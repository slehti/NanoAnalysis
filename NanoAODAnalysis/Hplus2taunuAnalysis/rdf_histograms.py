import itertools
import numpy as np

STDSELE_PREFIX = "AfterStdSelections_"

# The bin index 0 is used as the magic variable for integrating the dimension out
VARIATION_BINS = {
    "rtau": ["RtauIncl", "Rtau75","Rtau1",],
    "leadTau_pt": ["ptIncl", "lowpt", "lmidpt","hmidpt", "highpt",],
    "leadTau_abseta": ["etaIncl", "loweta", "mideta", "higheta",],
    "nn_score": ["nnIncl", "nnhigh",],
    "Event_tauIso": ["TauIsoIncl", "Inverted", "Nominal",],
    "leadTau_genuine": ["EWKIncl", "EWKFakeTau", "EWKGenuineTau",],
    "leadTau_not_misid_jet": ["TauMatchIncl", "NoTauMatch", "MatchedTau",],
}

def get_histograms(df, stdsele_df=None):

    # Define primary histogram binning
    mt_bins = np.arange(10,1000,10)
    mt_bins = np.insert(mt_bins,0,0)
    mt_bins = np.append(mt_bins, 3000).astype(float)
    met_bins = np.arange(0,805,5).astype(float)
    nn_bins = np.arange(0,1,0.05)
    bins_essential = {
        "mT": mt_bins,
        "MET_pt": met_bins,
        "classifier_score": nn_bins,
    }

    # Define visualization histogram binning
    bins_additional = {
        "leadTau_eta": np.linspace(-2.1,2.1,22), #FIXME this is not the same binning as in original analysis,
        "leadJet_eta": np.linspace(-4.5,4.5,60),
        "Rbb_min": np.linspace(0,2*np.pi,36),
        "rtau": np.linspace(0,1,40),
        "leadTau_pt": np.array([50,60,80,100,150,200,300,400,2000],dtype=float),
        "MET_pt": np.array([0,20,40,60,80,100,120,140,160,200,250,300,1500],dtype=float),
        "leadBjet_pt": np.array([30,50,70,90,110,130,150,200,300,400,1500],dtype=float),
    }

    # Define variations of histograms as bins
    rtau_vars = (-1.,0.75,np.inf)
    pt_vars = (0.,60.,80.,100.,np.inf)
    eta_vars = (0.,0.6,1.4,np.inf)
    bool_vars = (0., 0.5, np.inf)
    genuine_vars = bool_vars
    not_misid_jet_vars = bool_vars
    tauiso_vars = bool_vars
    essential_vars = {
        "rtau": rtau_vars,
        "leadTau_pt": pt_vars,
        "leadTau_abseta": eta_vars,
        "leadTau_genuine": genuine_vars,
        "leadTau_not_misid_jet": not_misid_jet_vars,
        "Event_tauIso": tauiso_vars
    }
    additional_vars = {
        "rtau": rtau_vars,
        "leadTau_not_misid_jet": not_misid_jet_vars,
        "leadTau_genuine": genuine_vars,
        "Event_tauIso": tauiso_vars
    }

    ess_var_nbins = {k: len(v)-1 for k,v in essential_vars.items()}
    add_var_nbins = {k: len(v)-1 for k,v in additional_vars.items()}

    hists = {}

    # Create primary histograms with variations
    for name in bins_essential:
        hists[name] = df.HistoND(
            (
                name,
                ";".join((name,name) + tuple(essential_vars.keys()) + ("Events/bin",)),
                len(essential_vars) + 1,
                (len(bins_essential[name])-1,) + tuple(ess_var_nbins.values()),
                (bins_essential[name],) + tuple(essential_vars.values())
            ),
            (name,) + tuple(essential_vars.keys())
        )

    # Create visualization histograms with their variations
    for name in bins_additional:
        hists[name] = df.HistoND(
            (
                name,
                ";".join((name,name) + tuple(additional_vars.keys()) + ("Events/bin",)),
                len(additional_vars) + 1,
                (len(bins_additional[name])-1,) + tuple(add_var_nbins.values()),
                (bins_additional[name],) + tuple(additional_vars.values())
            ),
            (name,) + tuple(additional_vars.keys())
        )

    if stdsele_df is not None:
        for name in bins_essential:
            hists[STDSELE_PREFIX + name] = df.HistoND(
                (
                    STDSELE_PREFIX + name,
                    ";".join((name,name) + tuple(essential_vars.keys()) + ("Events/bin",)),
                    len(essential_vars) + 1,
                    (len(bins_essential[name])-1,) + tuple(ess_var_nbins.values()),
                    (bins_essential[name],) + tuple(essential_vars.values())
                ),
                (name,) + tuple(essential_vars.keys())
            )

    return hists

def without(d, key):
    res = d.copy()
    res.pop(key)
    return res

def stripped(hist_name):
    return hist_name.replace(STDSELE_PREFIX,"")

def get_indexed_name(variables, indices):
    nameslist = [VARIATION_BINS[var][idx] for var,idx in zip(variables,indices)]
    return "_".join(nameslist)

def slice_variations(hist, orig_name):

    subhists = []

    # Get the list of variation axes, excluding the primary axis (first one)
    var_axs = hist.GetListOfAxes().Clone()
    var_axs.RemoveFirst()  # Primary axis is removed

    # Get the names of the variation axes
    var_names = [ax.GetTitle() for ax in var_axs]

    # Create a list of indices for each axis, including 0 to indicate summing over
    var_indices = [tuple(range(ax.GetNbins() + 1)) for ax in var_axs]

    # Create all combinations of bin indices (including 0 for SUM) for each variation axis
    combinations = itertools.product(*var_indices)

    # Iterate over all combinations of variation axis bins (including sum over)
    for combination in combinations:
        # Set ranges for each variation axis based on the combination
        for i, (axis, idx) in enumerate(zip(var_axs, combination)):
            if idx == 0:
                # Summing over the axis: Set range to cover all bins
                hist.GetAxis(i+1).SetRange(0, axis.GetNbins()+1)  # Set the full range
            else:
                # Slicing the axis to a single bin
                hist.GetAxis(i+1).SetRange(idx, idx)  # Bins in ROOT are 1-indexed

        # Project onto the first axis (the primary variable axis)
        sliced = hist.Projection(0)  # Project on the first axis (index 0)

        # Generate a unique name for the sliced histogram
        var_name = get_indexed_name(var_names, combination)
        name = f"h_{orig_name}_{var_name}"
        sliced.SetName(name)

        # Store the resulting 1D histogram and its name
        subhists.append((name, sliced))

    # Reset the ranges on the axes to include all bins (optional, for safety)
    for axis in var_axs:
        axis.SetRange(1, axis.GetNbins())

    return subhists

#def get_indexed_name(var_names, combination):
#    parts = []
#    for var_name, idx in zip(var_names, combination):
#        if idx == 0:
#            parts.append(f"{var_name}_SUM")
#        else:
#            parts.append(f"{var_name}_bin{idx}")
#    return "_".join(parts)

