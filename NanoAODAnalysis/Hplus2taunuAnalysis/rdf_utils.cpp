#include "rdf_utils.h"

float mT_tau_met(
    float tau_mass,
    float tau_pt,
    float tau_phi,
    float met_pt,
    float met_phi
) {
    float tau_mt = std::sqrt(tau_mass*tau_mass + tau_pt*tau_pt);
    float myCosPhi = std::cos(ROOT::VecOps::DeltaPhi(tau_phi, met_phi));

    // myCosPhi = std::abs(myCosPhi) > 1. ? std::copysign(1.0,myCosPhi) : myCosPhi;
    float mt_precursor = std::sqrt(2*tau_pt*(1.-myCosPhi));

    return std::isfinite(mt_precursor) ? mt_precursor*std::sqrt(met_pt) : tau_mt;
}

float Hplus_mass(
    ROOT::RVec<int> genpart_id,
    ROOT::RVec<float> genpart_m
) {
    // pdg index 37 is the H+
    ROOT::RVec<float> Hp_m = genpart_m[ROOT::VecOps::abs(genpart_id)==37];
    // return nan if there was no H+ in the event
    return Hp_m.size()==0 ? std::numeric_limits<float>::quiet_NaN() : Hp_m[0];
}

ROOT::RVec<bool> filter_taus(
    ROOT::RVec<int> trig_id,
    ROOT::RVec<float> trig_eta,
    ROOT::RVec<float> trig_phi,
    ROOT::RVec<float> tau_eta,
    ROOT::RVec<float> tau_phi,
    ROOT::RVec<int> idVSe,
    ROOT::RVec<int> idVSmu,
    ROOT::RVec<float> tau_pt,
    ROOT::RVec<int> tau_dm,
    ROOT::RVec<float> tau_ldTkPtoverTauPt,
    float pt_min,
    float eta_max,
    int cut_vse,
    int cut_vsmu
) {
    ROOT::RVec<float> trig_taueta = trig_eta[trig_id == 15];
    ROOT::RVec<float> trig_tauphi = trig_phi[trig_id == 15];
    ROOT::RVec<bool> keep(tau_eta.size());

    for (int i=0; i<tau_eta.size(); i++) {
        float dR_nearest = 1.0;
        for (int j=0; j<trig_taueta.size(); j++) {
            float dR = ROOT::VecOps::DeltaR(tau_eta[i], trig_taueta[j], tau_phi[i], trig_tauphi[j]);
            dR_nearest = std::min(dR, dR_nearest);
        }
        keep[i] = dR_nearest < 0.1;
    }
    keep = keep && (idVSe >= cut_vse) && (idVSmu >= cut_vsmu) && (tau_pt > pt_min);
    keep = keep && (ROOT::VecOps::abs(tau_eta) < eta_max);
    keep = keep && ((tau_pt * tau_ldTkPtoverTauPt) > 30);
    keep = keep && (tau_dm >= 0) && (tau_dm <= 2);
    return keep;
}

bool lep_veto(
    ROOT::RVec<float> ele_pt,
    ROOT::RVec<float> ele_eta,
    ROOT::RVec<float> ele_iso,
    ROOT::RVec<bool> loose_id,
    float max_pt,
    float min_eta,
    float min_iso
) {
    return ROOT::VecOps::All(
        (ele_pt <= max_pt) ||
        (ROOT::VecOps::abs(ele_eta) >= min_eta) ||
        (ele_iso >= min_iso) ||
        !loose_id
    );
}

ROOT::RVec<bool> jet_filter(
    ROOT::RVec<float> jet_pt,
    ROOT::RVec<float> jet_eta,
    ROOT::RVec<float> jet_phi,
    float tauh_eta,
    float tauh_phi,
    ROOT::RVec<bool> jet_id,
    float pt_min,
    float eta_max,
    float tau_threshold

) {
    ROOT::RVec<bool> close_to_tau(jet_pt.size());
    for (int i=0; i<jet_pt.size(); i++) {
        close_to_tau[i] = ROOT::VecOps::DeltaR(
            tauh_eta,jet_eta[i],tauh_phi,jet_phi[i]
        ) < tau_threshold;
    }
    ROOT::RVec<bool> keep = (
        !close_to_tau &&
        (jet_pt > pt_min) &&
        (ROOT::VecOps::abs(jet_eta) < eta_max) &&
        (jet_id >= 1)
    );
    return keep;
}

ROOT::RVec<bool> count_bjets(
    ROOT::RVec<bool> jet_ishadronic,
    ROOT::RVec<float> jet_eta,
    ROOT::RVec<float> jet_btag,
    float cut,
    float eta_max
) {
    ROOT::RVec<bool> is_bjet = (
        jet_ishadronic &&
        (ROOT::VecOps::abs(jet_eta) < eta_max) &&
        (jet_btag > cut)
    );
    return is_bjet;
}

