from builtins import breakpoint
from http import server
from typing import overload
import awkward as ak
import dask_awkward as dak
import numpy as np
import sys
import os
import re
import glob
from functools import partial

from nanoanalysis.JetCorrections import JEC
from nanoanalysis.Btag import Btag

# basepath_re = re.compile("(?P<basepath>\S+/NanoAnalysis)/")
# match = basepath_re.search(os.getcwd())
# if match:
#     sys.path.append(os.path.join(match.group("basepath"),"NanoAODAnalysis/Hplus2taunuAnalysis/ANN"))

from nanoanalysis.CommonSelection import *

# machine learning workflows currently only supported with the RDataFrame analysis framework
# class ClassifierModelClient():
#     def __init__(self, savedir, param_mass = False, regress_mass = False, nn_working_point = -1.0):
#
#         self.parametrized = param_mass
#         self.regress_mass = regress_mass
#         folds = glob.glob(savedir + "/fold_*/model_trained.h5")
#         self.folds = folds
#         input_vars = load_input_vars(savedir)
#         input_feature_keys = []
#         for var in input_vars:
#             if var in COLUMN_KEYS.keys():
#                 for basename in COLUMN_KEYS[var]:
#                     input_feature_keys.append(basename)
#             else: input_feature_keys.append(var)
#         self.input_feature_keys = input_feature_keys
#
#         try:
#             self.mass_idx = input_feature_keys.index("mtrue")
#         except:
#             if param_mass: raise Exception("cannot find mass index in neural network inputs")
#             else: pass
#
#         try:
#             nn_mt_idx = input_feature_keys.index("mt")
#         except:
#             raise Exception("cannot find variable of interest (transverse mass) in neural network inputs")
#
#         self.k = len(folds)
#
#         self.feature_generating_func = partial(datasetUtils.Writer.convert, input_vars, datasetUtils.MDATA, inference=True)
#
#         self.cut = nn_working_point
#         self.nn_mt_idx = nn_mt_idx
#
#     def selection(self, events, cut=0, mass_hypot=None):
#
#         if len(events) == 0: return events
#
#         in_tensor = np.array(self.feature_generating_func(events))
#
#         # replace mass parameter if using a single mass point
#         if mass_hypot is not None:
#             if not self.parametrized:
#                 msg = "Cannot specify mass hypothesis when using a model that isn't parametrized w.r.t. the H+ mass"
#                 raise Exception(msg)
#             mass_hypot = np.zeros_like(in_tensor[...,0]) + mass_hypot
#             in_tensor[...,self.mass_idx] = mass_hypot
#             # in_tensor = np.concatenate([in_tensor[...,:self.mass_idx], mass_hypot, in_tensor[...,self.mass_idx+1:]], axis=-1)
#
#         # for parametrized nn, use transverse mass as an approximation for the H+ mass
#         elif self.parametrized:
#             mass_approx = in_tensor[...,self.nn_mt_idx]
#             in_tensor[...,self.mass_idx] = mass_approx
#             # in_tensor = np.concatenate([in_tensor[...,:self.mass_idx], mass_approx, in_tensor[...,self.mass_idx+1:]], axis=-1) # remnant from tensorflow
#
#         event_folds = events.event % self.k
#         predfolds = np.unique(event_folds)
#
#         # get predictions using models that haven't seen the events during training
#         predslist = []
#         for fold in predfolds:
#             fold_x = in_tensor[(events.event % self.k == fold).to_numpy()]
#             predslist.append(self.call(fold_x, fold))
#
#         # collect predictions from different folds into single arrays
#         preds = {}
#         for key in predslist[0]:
#             # instantiate numpy array that will contain predictions for all events
#             preds[key] = np.empty_like(events.event, dtype=float)
#
#             # fill the array with predictions
#             for n, fold in enumerate(predfolds):
#
#                 # get indices where to put preds from this fold
#                 idx = np.asarray(event_folds == fold).nonzero()[0]
#
#                 # place fold preds into larger array
#                 np.put(preds[key], idx, np.squeeze(predslist[n][key]))
#
#         # check which events passed the selection
#         passed = preds['class_out'] >= cut
#
#         # fill event fields for histos
#         events['nn_score'] = preds['class_out']
#         if self.regress_mass:
#             events['nn_masspred'] = preds['mass_out']
#
#         return events[passed]
#
#     def call(self, x, n):
#         out = server_predict(x, n)
#         return out

def triggerSelection(events,year):
    if '2016' in year:
        return events.HLT.LooseIsoPFTau50_Trk30_eta2p1_MET90
    if "2017" in year:
        return events.HLT.MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET90
    if "2018" in year:
        return events.HLT.MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET100

    print("Problem with triggerSelection")


from  nanoanalysis.METCleaning import METCleaning

def tau_identification(events, is_data, pt_min = 50, eta_max = 2.1):
    # step 4: Identify tau particles.

    tau = events.Tau
#    out['unweighted_counter']['before tau selection taus'] = dak.sum(ak.num(tau) > 0)

    #check that offline tau matches deltaR < 0.1 with HLT tau object
    HLT_tau = events.TrigObj[events.TrigObj.id==15]
    # delta_r = dak.min(tau.metric_table(HLT_tau), axis=2)
    # delta_r = ak.min(tau.delta_r(HLT_tau), axis=1)
    # hlt_matched_tau = tau.nearest(HLT_tau)
    hlt_matches = tau.nearest(HLT_tau, axis=1, threshold=0.1)
    keep = ~ak.is_none(hlt_matches,axis=1)
    #keep = ak.fill_none(delta_r < 0.1, value=False) # metric_table returns None in case of no HLT tau in event
    tau = tau[
        keep
    ]

#    out['unweighted_counter']['tau selection passed trigger matching'] = dak.sum(ak.num(tau) > 0)

    # AntiEle, tight
    tau = tau[
        (tau.idDeepTau2017v2p1VSe >= 32)
    ]

#    out['unweighted_counter']['tau selection passed ele discriminator'] = dak.sum(ak.num(tau) > 0)

    # AntiMuon, loose
    tau = tau[
        (tau.idDeepTau2017v2p1VSmu >= 2)
    ]

#    out['unweighted_counter']['tau selection passed mu discriminator'] = dak.sum(ak.num(tau) > 0)

    # pt and eta cuts
    tau = tau[
        (tau.pt > pt_min)&
        (np.absolute(tau.eta) < eta_max)
    ]

#    out['unweighted_counter']['tau selection passed eta and pt cut'] = dak.sum(ak.num(tau) > 0)

    # leading electrically charged particle with pt_ldgTk > 30 GeV
    pt_ldgTk = tau.pt * tau.leadTkPtOverTauPt
    tau = tau[
        (pt_ldgTk > 30)
    ]

#    out['unweighted_counter']['tau selection passed leading track pt cut'] = dak.sum(ak.num(tau) > 0)

    # decay mode cuts (keep one prong taus)
    dm = tau.decayMode
    tau = tau[
        (dm == 0) |
        (dm == 1) |
        (dm == 2)
    ]

#    out['unweighted_counter']['tau selection passed nprongs'] = dak.sum(ak.num(tau) > 0)

    # loose isolation criteria
    isolation = tau.idDeepTau2017v2p1VSjet >= 8
    tau["isolated"] = isolation

    # for events with isolated taus, reject anti-isolated taus
    eventIso = dak.sum(isolation, axis=1) > 0
    keep = (eventIso & isolation) | ~eventIso
    tau = tau[
        (keep)
    ]

    tau['abseta'] = np.absolute(tau.eta)

    events["Tau"] = tau
    events["TauIsolation"] = eventIso

    keep = dak.num(tau, axis=1) > 0

    events = events[
        (keep)
    ]

    if not is_data: # generator level particle matching
        tau = events.Tau
        gen_tau = events.GenVisTau
        genuine_matches = tau.nearest(gen_tau, threshold=0.1)
        gen_fakes = events.GenPart[
            (abs(events.GenPart.pdgId) == 11) |
            (abs(events.GenPart.pdgId) == 13) &
            (events.GenPart.pt > 10)
        ]
        lep_matches = tau.nearest(gen_fakes,threshold = 0.1)
        genuinetau = ~ak.is_none(genuine_matches,axis=1)
        tau["Genuine"] = genuinetau
        tau["Matched"] = ~ak.is_none(lep_matches,axis=1) | genuinetau
        events['Tau'] = tau
    else: # give arbitrary labels to data to ensure the fields are defined
        tau = events.Tau
        tau["Genuine"] = False
        tau["Matched"] = False
        events["Tau"] = tau

    # Define the leading tau to correspond to a tau matching
    # the selection path the event passed (baseline or inverted)
    leadTau = tau[:,0]
    events["leadTau"] = leadTau

    return events


def R_tau_selection(events):
    # Selects events where R_Tau > 0.75.
    # This is done by selecting leading charged tau particle,
    # which has over 75% energy of the event.
    tau = events.Tau

    tau = tau[
        (tau.leadTkPtOverTauPt > 0.75)
    ]

    events['Tau'] = tau

    events = events[
        (ak.num(tau,axis=1) > 0)
    ]

    return events


def isolated_electron_veto(events):
    # step 5: isolated electron veto
    # _mvaFall17V2noIso_WPL
    max_pt = 15
    min_eta = 2.5

    electron = events.Electron

    electron = electron[
        (electron.pt > max_pt) &
        (np.absolute(electron.eta) < min_eta) &
        (electron.miniPFRelIso_all < 0.4)
    ]

    events['Electron'] = electron

    events = events[
        (ak.num(electron) == 0)
    ]

    return events


def isolated_muon_veto(events):
    # step 6: isolated muon veto
    # looseId
    max_pt = 10
    min_eta = 2.5

    muon = events.Muon

    muon = muon[
        (muon.pt > max_pt) &
        (np.absolute(muon.eta) < min_eta) &
        (muon.miniPFRelIso_all < 0.4)
    ]

    events['Muon'] = muon

    events = events[
        (ak.num(muon) == 0)
    ]

    return events


def hadronic_pf_jets(events):
    # step 7: choose hadronic pf jets with loose ID.
    # Choose jets with jetId > 4.
    pt_min = 30
    eta_max = 4.7

    jets = events.Jet

    tau = events.Tau
    matches = jets.nearest(tau, threshold = 0.5)
    not_tau = dak.is_none(matches, axis=1)

    jets = jets[
        (not_tau) &
        (jets.pt > pt_min) &
        (np.absolute(jets.eta) < eta_max) &
        (jets.jetId >= 1)
    ]

    events["Jet"] = jets


    events = events[
        (ak.num(jets,axis=1) >= 3)
    ]

    return events


def b_tagged_jets(events, wp=None):
    # step 8: B tagged jets, using CSVv2 with medium Working Point
    jets = events.Jet
    eta_max = 2.4

    btag = Btag('btagDeepB', "2017")

    if wp == 'loose':
        cut = btag.loose()
    elif wp == 'tight':
        cut = btag.tight()
    else: cut = btag.medium()

    jets = jets[
        (np.absolute(jets.eta) < eta_max) &
        (jets.btagDeepB > cut)
    ]

    events["BJet"] = jets

    events = events[
        dak.num(jets, axis=1) > 0
    ]

    return events


def met_cut(events, pt_min = 90):
    # step 9: Missing transverse energy cut
    met = events.MET

    keep = met.pt > pt_min

    events = events[
        keep
    ]

    return events

def Rbb_min(events):
    # step 10: back-to-back angular cut
    # Select angular discriminant angle = 40 degrees = 0.698 radians
    tau = events.Tau
    jets = events.Jet
    met = events.MET

    # Select smallest taus and jets for each event when calculating Delta_Phi.
    # This assures that each event that passes, has minimum value for R_bb min.
    jets_delta_phi = jets.delta_phi(met)**2
    tau_delta_phi = (np.pi - np.absolute(tau.delta_phi(met)))**2

    jets_min = dak.min(jets_delta_phi, axis=1)
    taus_min = dak.min(tau_delta_phi, axis=1)

    events = events[
        (np.sqrt(taus_min + jets_min) > 0.689)
    ]

    return events

def fake_tau_pt_selection(events, pt_cut):
    # perform pt cut for given pt. Tau pt as variable for bins
    # pt < 60 GeV
    # 60 < pt < 80 GeV
    # 80 < pt < 100 GeV and
    # pt > 100 Gev

    tau = events.Tau

    if pt_cut == 60:
        tau = tau[
            (tau.pt < 60)
        ]

    elif pt_cut == 80:
        tau = tau[
            (tau.pt < 80) &
            (tau.pt > 60)
        ]

    elif pt_cut == 100:
        tau = tau[
            (tau.pt < 100) &
            (tau.pt > 80)
        ]

    else:
        tau = tau[
            (tau.pt > 100)
        ]

    events = events[
        (ak.num(tau) > 0)
    ]

    events['Tau'] = tau

    return events


def fake_tau_eta_selection(events, eta_cut):
    # Selection of Tau eta binning, from
    # |eta| < 0.6
    # 0.6 < |eta| < 1.4 and
    # |eta| > 1.4

    tau = events.Tau

    if eta_cut == 0.6:
        tau = tau[
            (np.absolute(tau.eta) < 0.6)
        ]

    elif eta_cut == 1.4:
        tau = tau[
            (np.absolute(tau.eta) > 0.6) &
            (np.absolute(tau.eta) < 1.4)
        ]

    else:
        tau = tau[
            (np.absolute(tau.eta) > 1.4)
        ]

    events = events[
        (ak.num(tau) > 0)
    ]

    events['Tau'] = tau

    return events

def met_fake_tau_selection(events, pt_cut=None):
    # perform pt cut for given pt. MET pt as variable for bins
    # pt < 60 GeV
    # 60 < pt < 80 GeV
    # 80 < pt < 100 GeV and
    # pt > 100 Gev
    met = events.MET

    if pt_cut == 60:
        met = met[
            (met.pt < 60)
        ]

    elif pt_cut == 80:
        met = met[
            (met.pt < 80) &
            (met.pt > 60)
        ]

    elif pt_cut == 100:
        met = met[
            (met.pt < 100) &
            (met.pt > 80)
        ]

    else:
        met = met[
            (met.pt > 100)
        ]

    events = events[
        (ak.num(met) > 0)
    ]

    events.MET = met

    return events



def tau_selection(events):
    # Uses all criteria to identify desired tau events

    events = tau_identification(events)

    events = R_tau_selection(events)

    events = isolated_electron_veto(events)

    events = isolated_muon_veto(events)

    events = hadronic_pf_jets(events)

    events = b_tagged_jets(events)

    events = met_cut(events)

    events = Rbb_min(events)

    return events
