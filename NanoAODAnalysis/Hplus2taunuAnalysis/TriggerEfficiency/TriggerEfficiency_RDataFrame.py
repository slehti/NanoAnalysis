#!/usr/bin/env python3
#
# Read NanoAOD skim and make histograms in RDataFrame
# Usage:
#       analysis.py <multicrabdir> [options]
# Example:
#       analysis.py multicrab_ZbAnalysis_v1405_Run2024BCDE_20240605T1127 -i 2024B,DY
#
# 20.6.2024/S.Lehti
#

import os
import sys
import re
import subprocess
import datetime
import json

from optparse import OptionParser

import ROOT

basepath_re = re.compile("(?P<basepath>\S+/NanoAnalysis)/")
match = basepath_re.search(os.getcwd())
if match:
    sys.path.append(os.path.join(match.group("basepath"),"NanoAODAnalysis/Framework/python"))

import multicrabdatasets


ROOT.ROOT.EnableImplicitMT()

def main(opts, args):

    if len(args) == 0:
        usage()
        sys.exit()

    multicrabdir = os.path.abspath(args[0])
    if not os.path.exists(multicrabdir) or not os.path.isdir(multicrabdir):
        usage()
        sys.exit()
    year = multicrabdatasets.getYear(multicrabdir)

    blacklist = []
    whitelist = []
    if opts.includeTasks != 'None':
        whitelist.extend(opts.includeTasks.split(','))
    if opts.excludeTasks != 'None':
        blacklist.extend(opts.excludeTasks.split(','))

    datasets = multicrabdatasets.getDatasets(multicrabdir,whitelist=whitelist,blacklist=blacklist)
    pileup_data = multicrabdatasets.getDataPileupROOT(datasets)
    lumi = multicrabdatasets.loadLuminosity(multicrabdir,datasets)

    time = datetime.datetime.now().strftime("%Y%m%dT%H%M")
#    LEPTONFLAVOR = int(opts.lepton)
#    lepton = "_"
#    if LEPTONFLAVOR == 11:
#        lepton+="Electron"
#    if LEPTONFLAVOR == 13:
#        lepton+="Muon"


    outputmulticrab = os.path.basename(os.path.abspath(multicrabdir))+"_processed"+time
    if len(opts.name) > 0:
        outputmulticrab = outputmulticrab + "_"+opts.name
    if not os.path.exists(outputmulticrab):
        os.mkdir(outputmulticrab)

    print("whitelist",whitelist)
    print("blacklist",blacklist)

    print("Number of datasets: %s"%len(datasets))
    for i,dataset in enumerate(datasets):
        print(dataset.name)
#        print(dataset.files)
#        print(lepton)
        txt = "Dataset %s/%s"%(i+1,len(datasets))
        analysisLoop(year,outputmulticrab,dataset,pileup_data,lumi,txt)

    print( "Output written in",outputmulticrab )
    print( "Workdir:",os.getcwd() )


def analysisLoop(year,outputmulticrab,dataset,pileup_data,lumi,txt):
    subdir = os.path.join(outputmulticrab,dataset.name)
    outputdir = os.path.join(subdir,"results")
    if not os.path.exists(subdir):
        os.mkdir(subdir)
        os.mkdir(outputdir)

    # RDataFrame begin
    df_x = ROOT.RDataFrame("Events", dataset.files)
    df_x = df_x.Define("Tau_LeadingJetTrigger", "ROOT::VecOps::Multiply(Tau_leadTkPtOverTauPt,Tau_pt)")

    counter1 = df_x.Count()


    df = (df_x.Filter("HLT_IsoMu24 > 0.5", "Events passing trigger").Filter("abs(Tau_eta[0]) < 2.1 ","Need the taus in this region").Filter("Tau_pt[0] > 50","Tranverse tau momentum trigger"))
    df = (df.Filter("Tau_decayMode[0] <= 2","Number of prongs to be limited to one neutral pion").Filter("Tau_leadTkPtOverTauPt[0]*Tau_pt[0] > 20","Checking the leading jet trigger"))



    df = (df_x.Filter("HLT_IsoMu24 > 0.5", "Events passing trigger").Filter(lambda vec: any(abs(val) > 2.1 for val in vec), ["Tau_eta"]).Filter(lambda vec: any(val > 2.1 for val in vec), ["Tau_pt"]).Filter(lambda vec: any(val > 2 for val in vec), ["Tau_decayMode"]).Filter(lambda vec: any(val > 20 for val in vec), ["Tau_LeadingJetTrigger"]))

#    df = (df.Filter("abs(Tau_eta[1]) < 2.1 ","Need the taus in this region").Filter("Tau_pt[1] > 50","Tranverse tau momentum trigger"))
#    df = (df.Filter("Tau_decayMode[1] <= 2","Number of prongs to be limited to one neutral pion").Filter("Tau_leadTkPtOverTauPt[1]*Tau_pt[1] > 20","Checking the leading jet trigger"))
#    for i in range(10):
#        df = (df.Filter("HLT_IsoMu24 > 0.5", "Events passing trigger").Filter("abs(Muon_eta[i]) < 2.1","Need the muons in this region").Filter("abs(Tau_eta[i]) < 2.1 ","Need the taus in this region").Filter("Tau_pt[i] > 50","Tranverse tau momentum trigger").Filter("Muon_pt[i] > 17","Tranverse muon momentum trigger"))
#        df = (df.Filter("Tau_decayMode[i] <= 2","Number of prongs to be limited to one neutral pion").Filter("Tau_leadTkPtOverTauPt[i]*Tau_pt[i] > 20","Checking the leading jet trigger"))

    df_a = df.Filter("HLT_LooseDeepTauPFTauHPS180_L2NN_eta2p1 > 0.5", "Events passing signal")
    counter2 = df.Count()
    counter3 = df_a.Count()

    histo = df_x.Histo1D(("h_pileup_pre", ";x-axis;y-axis", 100, 0, 100), "PV_npvs")
    histo2 = df.Histo1D(("h_pileup", ";x-axis;y-axis", 100, 0, 100), "PV_npvs")
    histo_pt_before = df.Histo1D(("pt_tau_before", ";x-axis pt;y-axis count", 500, 0, 500), "Tau_pt")
    histo_pt_after = df_a.Histo1D(("pt_tau_after", ";x-axis pt;y-axis count", 500, 0, 500), "Tau_pt")

    print
    print(txt)
    print(dataset.name)
    print("All events",counter1.GetValue())
    print("trigger   ",counter2.GetValue())
    print("signal    ",counter3.GetValue())

    fOUT = ROOT.TFile.Open(os.path.join(outputdir,"histograms.root"),"RECREATE")

    days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    now = datetime.datetime.now()
    m = "produced: %s %s"%(days[now.weekday()],now)
    timestamp = ROOT.TNamed(m,"")
    timestamp.Write()

    histo.Write()
    histo2.Write()
    histo_pt_before.Write()
    histo_pt_after.Write()
    sigmoid = histo_pt_after.Clone("sigmoid")
    sigmoid.Divide(histo_pt_before.GetPtr())
    sigmoid.Write()


    fOUT.Close()
if __name__ == "__main__":
    import time
    t0 = time.time()

    parser = OptionParser(usage="Usage: %prog [options]")
    parser.add_option("-i", "--includeTasks", dest="includeTasks", default="None", type="string",
                      help="Only perform action for this dataset(s) [default: \"\"]")
    parser.add_option("-e", "--excludeTasks", dest="excludeTasks", default="None", type="string",
                      help="Exclude this dataset(s) from action [default: \"\"]")
    parser.add_option("--name",dest="name", default="", type="string",
                      help="Ending for the multicrabdir [default: \"\"]")
    parser.add_option("-l", "--lepton",dest="lepton", default="13", type="string",
                      help="Electron (11) or Muon (13) [default: \"13\"]")

    (opts, args) = parser.parse_args()

    main(opts, args)

    t1 = time.time()
    dt = t1-t0
    print("Processing time %s s"%(int(dt%60)))
