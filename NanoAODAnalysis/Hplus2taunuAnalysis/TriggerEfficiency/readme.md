# Guide for using the trigger efficiency code

## Setup
Make sure you have proxys and everything about grids and VOs in order before running the code. Next, set up python3 venv for using the necessary packages as (if you don't have it already):
```
python3 -m venv VirtualAnalysisEnvironment
source VirtualAnalysisEnvironment/bin/activate
pip install wheel
pip install xrootd
pip install coffea
pip install requests
```
On other times when venv has been set up:
```
source VirtualAnalysisEnvironment/bin/activate
```

Exiting venv:
```
deactivate
```
Other possible way is to, for example setup a conda environment and running the code from there.

## Running the analysis for a multicrab file
```
python3 TriggerEfficiency.py <multicrab skim>
```

## Examples command for running the code
```
python3 TriggerEfficiency.py multicrab_Hplus2taunuTrgMETLegAnalysis_v1255p1_Run2017BCDEF_20230810T0910
```
