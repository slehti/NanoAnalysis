import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec

import uproot
import awkward as ak
import time
import hist
import mplhep as hep
from collections import Counter
from scipy.optimize import curve_fit

import coffea
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema
from coffea.processor import ProcessorABC
from coffea.processor import Runner, IterativeExecutor, FuturesExecutor

import os, re, sys
import json
import os.path
import glob
import multiprocessing as mp
from hist.intervals import clopper_pearson_interval, ratio_uncertainty
from itertools import repeat
import warnings


def fit_function(x, k, b, q, v, m):
    return k/((1+q*np.exp(-b*(x-m)))**(1/v))
    
def savingvalues(x, histogram1, histogram2):
    savelist = []
    for i in range(len(x)-1):
        savedict = {
            "pt": float(x[i]),
            "efficiency": (histogram2/histogram1)[i],
            "uncertaintyPlus": np.abs(clopper_pearson_interval(histogram2, histogram1) - histogram2/histogram1)[1][i],
            "uncertaintyMinus": np.abs(clopper_pearson_interval(histogram2, histogram1) - histogram2/histogram1)[0][i]
        }
        savelist.append(savedict)
    return savelist
    
def savingfit(x, fitvalues, minuserrorvalues, pluserrorvalues):
    savelist = []
    for i in range(len(x)):
        savedict = {
            "pt": float(x[i]),
            "efficiency": fit_function(x[i], *fitvalues),
            "uncertaintyPlus": np.abs(fit_function(x[i], *pluserrorvalues)-fit_function(x[i], *fitvalues)),
            "uncertaintyMinus": np.abs(fit_function(x[i], *minuserrorvalues)-fit_function(x[i], *fitvalues))
        }
        savelist.append(savedict)
    return savelist
    
def produce_json_dict(era, firstrun, lastrun, luminosity, data, mc):
    datadict = {
        "era": era,
        "firstRun": int(firstrun),
        "lastRun": int(lastrun),
        "luminosity": float(luminosity),
        "bins": data
    }

    mcdict = {
        era: {
            "bins": mc
        }
    }
    
    jsondict = {
        "_timestamp": "Generated on "+time.ctime()+" by NanoAnalysis/NanoAODAnalysis/Hplus2taunuAnalysis/TriggerEfficiency/TriggerEfficiency.py",
        "_coffeaversion": str(coffea.__version__),
        "dataParameters": {
            "runs_"+firstrun+"_"+lastrun: datadict
        },
        "mcParameters": mcdict
    }
    
    return jsondict

def usage():

    print()
    print( "### Usage:  ",os.path.basename(sys.argv[0]),"<multicrab skim>", "or", os.path.basename(sys.argv[0]),"results", "<Leg(Cut)> (i.e. MET100 or Tau)","<year>")
    print()
    
def flatten_and_list(listname):
    finallist = ak.flatten(listname, axis=None)
    finallist = ak.to_numpy(finallist)
    
    return finallist
    
def lumis_and_runs(lumifile):
    f = open(lumifile)
    lumidata = json.load(f)
    
    firstrun = list(lumidata.keys())[0].split("_")[-2]
    lastrun = list(lumidata.keys())[-1].split("_")[-1]
    
    lumi = format(sum(lumidata.values())/1000, ".1f")
    luminosity = format(sum(lumidata.values()), ".3f")
    
    return firstrun, lastrun, lumi, luminosity

def plot_efficiency_and_make_jsons(alldata, accepteddata, allmc, acceptedmc, leg, year, multicrabpath, num=""):
    
    lumifile = multicrabpath+r"/lumi.json"
    firstrun, lastrun, lumi, luminosity = lumis_and_runs(lumifile)
    
    hep.style.use("CMS")
    
    gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    
    nbins = [20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180, 200, 240, 280, 320, 360, 400, 500, 800, 1100]
    #nbins = [20, 30, 40, 50, 60, 80, 100, 120, 200, 250, 300, 400, 500]
    #nbins = [20, 30, 40, 50, 60, 70, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 400]
    
    pt_hist = np.histogram(alldata, bins=nbins)
    hist, bins = pt_hist
    width = bins[1:] - bins[0:-1]
    center = (bins[:-1] + bins[1:]) / 2
    
    accepted_hist = np.histogram(accepteddata, bins=nbins)
    hist2, bins2 = accepted_hist
    
    all_hist = np.histogram(allmc, bins=nbins)
    hist3, bins3 = all_hist
    width2 = bins3[1:] - bins3[0:-1]
    center2 = (bins3[:-1] + bins3[1:]) / 2
    
    accepted_hist2 = np.histogram(acceptedmc, bins=nbins)
    hist4, bins4 = accepted_hist2
    
    errorhist11 = hist2/hist-np.abs(clopper_pearson_interval(hist2, hist) - hist2/hist)[0]
    errorhist12 = hist2/hist+np.abs(clopper_pearson_interval(hist2, hist) - hist2/hist)[1]
    
    errorhist21 = hist4/hist3-np.abs(clopper_pearson_interval(hist4, hist3) - hist4/hist3)[0]
    errorhist22 = hist4/hist3+np.abs(clopper_pearson_interval(hist4, hist3) - hist4/hist3)[1]
       
    xspace = list(range(20, 1300))
    
    if leg == "MET":
        era = year+"_"+leg+num
        filename = "metLegTriggerEfficiency_"+era
        yaxis = "L1+HLT efficiency"
        xaxis = r"MET Type 1 (GeV)"
        
        popt, pcov = curve_fit(fit_function, xdata=center[:-2], ydata=(hist2/hist)[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
        popt2, pcov2 = curve_fit(fit_function, xdata=center2[:-2], ydata=(hist4/hist3)[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
        
        errorpopt, errorpcov = curve_fit(fit_function, xdata=center[:-2], ydata=errorhist11[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
        errorpopt2, errorpcov2 = curve_fit(fit_function, xdata=center[:-2], ydata=errorhist12[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
    
        errorpopt3, errorpcov3 = curve_fit(fit_function, xdata=center2[:-2], ydata=errorhist21[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
        errorpopt4, errorpcov4 = curve_fit(fit_function, xdata=center2[:-2], ydata=errorhist22[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
        
    elif leg == "Tau":
        era = year
        filename = "tauLegTriggerEfficiency_"+era+num
        yaxis = "HLT efficiency"
        xaxis = r"$\tau_{h} p_{T} (GeV/c)$"
        
        popt, pcov = curve_fit(fit_function, xdata=center[:-2], ydata=(hist2/hist)[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
        popt2, pcov2 = curve_fit(fit_function, xdata=center2[:-2], ydata=(hist4/hist3)[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
        
        errorpopt, errorpcov = curve_fit(fit_function, xdata=center[:-2], ydata=errorhist11[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
        errorpopt2, errorpcov2 = curve_fit(fit_function, xdata=center[:-2], ydata=errorhist12[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
    
        errorpopt3, errorpcov3 = curve_fit(fit_function, xdata=center2[:-2], ydata=errorhist21[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
        errorpopt4, errorpcov4 = curve_fit(fit_function, xdata=center2[:-2], ydata=errorhist22[:-2], p0=[1.0, 0.1, 2**(0.5), 0.5, 130])
        
    
    fig = plt.figure()
    ax1 = plt.subplot(gs[0])
    hep.cms.label(label="Preliminary", loc=0, data=True, lumi=lumi) #, year="2023")
    
    ax1.errorbar(
        x=center,
        y=hist2/hist,
        xerr=width/2,
        yerr=np.abs(clopper_pearson_interval(hist2, hist) - hist2/hist),
        linestyle="None",
        color="black",
        fmt="o", alpha=1, fillstyle="none", markeredgewidth=1.5,
        label="Data "+year
    )
    
    ax1.errorbar(
        x=center2,
        y=hist4/hist3,
        xerr=width2/2,
        yerr=np.abs(clopper_pearson_interval(hist4, hist3) - hist4/hist3),
        linestyle="None",
        color="red",
        fmt="o", alpha=1, fillstyle="none", markeredgewidth=1.5,
        label="Simulation "+year
    )
    
    ax1.plot(xspace, fit_function(xspace, *popt), color="black", linewidth=1.5, linestyle="-")#, label=r"Data fit")
    
    ax1.fill_between(xspace, fit_function(xspace, *errorpopt), fit_function(xspace, *errorpopt2), alpha=0.3)
    
    ax1.fill_between(xspace, fit_function(xspace, *errorpopt3), fit_function(xspace, *errorpopt4), alpha=0.3)
    
    ax1.plot(xspace, fit_function(xspace, *popt2), color="red", linewidth=1.5, linestyle="-")#, label=r"Simulation fit")

    ax1.set_xlim(right=1300)
    ax2 = plt.subplot(gs[1], sharex = ax1)
    
    ax2.errorbar(
        x=center2,
        y=(hist2/hist)/(hist4/hist3),
        xerr=width2/2,
        yerr=list(map(max, np.abs(clopper_pearson_interval(hist2, hist) - hist2/hist)[0], np.abs(clopper_pearson_interval(hist2, hist) - hist2/hist)[1])),
        linestyle="None",
        color="black",
        fmt="o", alpha=1, fillstyle="none", markeredgewidth=1.5,
    
    )
    
    ax2.plot(xspace, fit_function(xspace, *popt)/fit_function(xspace, *popt2), color="black", linewidth=1.5, linestyle="-")
    
    ax2.fill_between(xspace, fit_function(xspace, *errorpopt)/fit_function(xspace, *errorpopt3), fit_function(xspace, *errorpopt2)/fit_function(xspace, *errorpopt4), color="green", alpha=0.3)
    
    #ax.set_title()
    ax2.set_xlabel(xaxis)
    ax1.set_ylabel(yaxis)
    ax2.set_ylabel("Data/Simulation")
    ax1.legend()
    ax2.set_xlim(right=1300)
    ax2.axhline(1, linestyle = "--", color="red")
    if num == "90" or num == "100" or num == "110":
    	ax2.axvline(int(num), linestyle = "--", color="green")
    ax2.set_ylim(bottom=0.3, top=1.7)
    ax1.grid()
    ax2.grid()
    fig.subplots_adjust(hspace=0)
    plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
    
    #hep_args = {'label': 'Private Work', 'loc': 0, 'data': False, 'year': '2022', 'rlabel': '2022 (13.6 TeV)'}
    plt.savefig(os.getcwd()+"/results/plots/"+leg+num+"_TE_"+year+"_final.png", bbox_inches="tight")
    plt.savefig(os.getcwd()+"/results/plots/"+leg+num+"_TE_"+year+"_final.pdf")
    plt.show()
    
    savedata = savingvalues(bins, hist, hist2)
    savemc = savingvalues(bins, hist3, hist4)
    binjson = produce_json_dict(era, firstrun, lastrun, luminosity, savedata, savemc)
    
    savefitdata = savingfit(xspace, popt, errorpopt, errorpopt2)
    savefitmc = savingfit(xspace, popt2, errorpopt3, errorpopt4)
    fitjson = produce_json_dict(era, firstrun, lastrun, luminosity, savefitdata, savefitmc)
    
    with open("results/JSONfiles/"+filename+"_bin.json", "w+") as f2:
        json.dump(binjson, f2, indent=4)
    f2.close()
    
    with open("results/JSONfiles/"+filename+"_fit.json", "w+") as f3:
        json.dump(fitjson, f3, indent=4)
    f3.close()

def get_pts(fname, leg, year):
    rootfile = uproot.open(fname)
    events = NanoEventsFactory.from_root(rootfile,schemaclass=NanoAODSchema.v6,metadata={"dataset": leg}).events()
    
    if leg == "MET":
        hlt = events.HLT
        met = events.MET
        
        if year == "2017" or year == "2018":
            trigger = hlt.MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET90
            trigger2 = hlt.MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET100
            ref = hlt.MediumChargedIsoPFTau50_Trk30_eta2p1_1pr
        
        else:
            trigger = hlt.LooseIsoPFTau50_Trk30_eta2p1_MET90
            trigger2 = hlt.LooseIsoPFTau50_Trk30_eta2p1_MET110
            ref = hlt.LooseIsoPFTau50_Trk30_eta2p1
    
        ref_pts = met[(ref == True),"pt"][:]
        accepted_pts = met[(trigger == True) & (ref == True),"pt"][:]
        accepted_pts2 = met[(trigger2 == True) & (ref == True),"pt"][:]
        
        ref_pts = ak.to_numpy(ref_pts)
        accepted_pts = ak.to_numpy(accepted_pts)
        accepted_pts2 = ak.to_numpy(accepted_pts2)
        
        return ref_pts, accepted_pts, accepted_pts2
        
    elif leg == "Tau":
    
        tau = events.Tau
        hlt = events.HLT
        trigobj = events.TrigObj
        
        pts = tau.pt
        etas = abs(tau.eta)
        decays = tau.decayMode
        deepjet = tau.idDeepTau2017v2p1VSjet
        deepe = tau.idDeepTau2017v2p1VSe
        deepmu = tau.idDeepTau2017v2p1VSmu
        
        if year == "2017" or year == "2018":
            trigger = hlt.IsoMu24_eta2p1_MediumChargedIsoPFTau50_Trk30_eta2p1_1pr
            ref = hlt.IsoMu24_eta2p1
            ref2 = hlt.IsoMu27
            
            ref_pts = ak.flatten(tau[((ref == True) | (ref2 == True)) & (etas < 2.1) & (decays == (0 or 1 or 2)) & (deepjet >= 8) & (deepe >= 32) & (deepmu >= 2) & (tau.delta_r(tau.nearest(trigobj)) < 0.5),"pt"][:,:1])
            accepted_pts = ak.flatten(tau[(trigger == True) & ((ref == True) | (ref2 == True)) & (etas < 2.1) & (decays == (0 or 1 or 2)) & (deepjet >= 8) & (deepe >= 32) & (deepmu >= 2) & (tau.delta_r(tau.nearest(trigobj)) < 0.5),"pt"][:,:1])
            
        else:
            trigger = hlt.IsoMu16_eta2p1_MET30_LooseIsoPFTau50_Trk30_eta2p1
            ref = hlt.IsoMu16_eta2p1_MET30
            
            ref_pts = ak.flatten(tau[(ref == True) & (etas < 2.1) & (decays == (0 or 1 or 2)) & (deepjet >= 8) & (deepe >= 32) & (deepmu >= 2) & (tau.delta_r(tau.nearest(trigobj)) < 0.5),"pt"][:,:1])
            accepted_pts = ak.flatten(tau[(trigger == True) & (ref == True) & (etas < 2.1) & (decays == (0 or 1 or 2)) & (deepjet >= 8) & (deepe >= 32) & (deepmu >= 2) & (tau.delta_r(tau.nearest(trigobj)) < 0.5),"pt"][:,:1])
        
        ref_pts = ak.to_numpy(ref_pts)
        accepted_pts = ak.to_numpy(accepted_pts)
        
        return ref_pts, accepted_pts
    
def read_files(filelist, leg, datatype, year):
    
    refvals = {}
    ref2vals = {}
    trigvals = {}
    trig2vals = {}
    
    print("Obtaining pT values for",leg,"leg",datatype)
    
    for fname in filelist:
        if datatype == "Data":
            searchstring = r"files_[\S]*_Run\d{4}\S{1}"
            reg = re.search(searchstring, fname)
            namestart = int(reg.end()-5)
            nameend = reg.end()
                
        elif datatype == "MC":
            searchstring = r"files_[\S]*.json"
            reg = re.search(searchstring, fname)
            namestart = int(reg.start()+6)
            nameend = int(reg.end()-5)
                
        legera = leg+"_"+fname[namestart:nameend]
        
        if leg == "MET":
            with mp.Pool() as p:
                legref, legtrig, legtrig2 = zip(*p.starmap(get_pts, zip(json.load(open(fname,'r'))["files"], repeat(leg), repeat(year)), chunksize=1))
            list3 = flatten_and_list(legtrig2)
            trig2vals[legera] = list3
            
        elif leg == "Tau":  
            with mp.Pool() as p:
                legref, legtrig = zip(*p.starmap(get_pts, zip(json.load(open(fname,'r'))["files"], repeat(leg), repeat(year)), chunksize=1))  
               
        list1 = flatten_and_list(legref)
        list2 = flatten_and_list(legtrig)
        
        refvals[legera] = list1
        trigvals[legera] = list2
        
        print(datatype,"file",int(filelist.index(fname)+1),"out of",len(filelist),"read")
    
    if leg == "MET":
        return refvals, trigvals, trig2vals
    else:
        return refvals, trigvals

def main():
    multibool = False
    
    if bool(re.search(r"multicrab", sys.argv[1])) and os.path.isdir(os.path.abspath(sys.argv[1])):
        current_directory = os.getcwd()
        result_directory = os.path.join(current_directory, r"results")
        
        if not os.path.exists(result_directory):
            os.makedirs(result_directory)
            json_directory = os.path.join(result_directory, r"JSONfiles")
            os.makedirs(json_directory)
            plot_directory = os.path.join(result_directory, r"plots")
            os.makedirs(plot_directory)
        
        multicrabfile = sys.argv[1]
        multicrabpath = os.path.abspath(multicrabfile)
        reg1 = re.search(r"Run\d{4}", multicrabfile)
        reg2 = re.search(r"Trg\S{3}Leg", multicrabfile)
    
        leg = multicrabfile[int(reg2.start()+3):int(reg2.end()-3)]
        year = multicrabfile[int(reg1.start()+3):int(reg1.end())]
        multibool = True
        
        if year == "2016":
            reg1 = re.search(r"Run\d{4}\S{1}", multicrabfile)
            if multicrabfile[int(reg1.end()-1):int(reg1.end())] == "F":
                year = "2016FGH"
            else:
                year = "2016BCDEF"
        
        if leg == "MET":
            datafilelist = glob.glob(multicrabpath+r"/Tau*/results/files_Tau*.json")
            simfilelist = list(np.concatenate([glob.glob(multicrabpath+simfile+"/results/files_*.json") for simfile in [r"/TT*", r"/ST*", r"/W*", r"/ZZ", r"/DY*"]], axis=None))
            #simfilelist = list(np.concatenate([glob.glob(multicrabpath+simfile+"/results/files_*.json") for simfile in [r"/TT*"]], axis=None))
        
            datarefvals, datatrigvals, datatrig2vals = read_files(datafilelist, leg, "Data", year)
            finaldatatrig2list = flatten_and_list(list(datatrig2vals.values()))
        
            mcrefvals, mctrigvals, mctrig2vals = read_files(simfilelist, leg, "MC", year)
            finalmctrig2list = flatten_and_list(list(mctrig2vals.values()))
        
        elif leg == "Tau":
            datafilelist = glob.glob(multicrabpath+r"/SingleMuon*/results/files_SingleMuon*.json")
            simfilelist = list(np.concatenate([glob.glob(multicrabpath+simfile+"/results/files_*.json") for simfile in [r"/TT*", r"/ST*", r"/W*", r"/DY*"]], axis=None))
            
            datarefvals, datatrigvals = read_files(datafilelist, leg, "Data", year)
            mcrefvals, mctrigvals = read_files(simfilelist, leg, "MC", year)
                
                
        finaldatareflist = flatten_and_list(list(datarefvals.values()))
        finaldatatriglist = flatten_and_list(list(datatrigvals.values()))
        finalmcreflist = flatten_and_list(list(mcrefvals.values()))
        finalmctriglist = flatten_and_list(list(mctrigvals.values()))
        
        #if multibool:
            #if leg == "Tau":
                #file1 = uproot.recreate(result_directory+"/rootfiles/"+leg+"_TE_"+year+"_final.root")
                #file1["Events"] = {"data_ref": {"pt": [ak.Array(finaldatareflist)]}, "data_trigger": {"pt": [ak.Array(finaldatatriglist)]}, "mc_trigger": {"pt": [ak.Array(finalmctriglist)]}, "mc_ref": {"pt": [ak.Array(finalmcreflist)]}}
            #elif leg == "MET":
                #file1 = uproot.recreate(result_directory+"/rootfiles/"+leg+"90_TE_"+year+"_final.root")
                #file1["Events"] = {"data_ref": {"pt": [ak.Array(finaldatareflist)]}, "data_trigger": {"pt": [ak.Array(finaldatatriglist)]}, "mc_trigger": {"pt": [ak.Array(finalmctriglist)]}, "mc_ref": {"pt": [ak.Array(finalmcreflist)]}}
                #file2 = uproot.recreate(result_directory+"/rootfiles/"+leg+"100_TE_"+year+"_final.root")
                #file2["Events"] = {"data_ref": {"pt": [ak.Array(finaldatareflist)]}, "data_trigger": {"pt": [ak.Array(finaldatatrig2list)]}, "mc_trigger": {"pt": [ak.Array(finalmctrig2list)]}, "mc_ref": {"pt": [ak.Array(finalmcreflist)]}}
                
                
        print("pT values obtained")
        print("Plotting the trigger efficiency.")
        
        if leg == "MET":
            plot_efficiency_and_make_jsons(finaldatareflist, finaldatatriglist, finalmcreflist, finalmctriglist, leg, year, multicrabpath, "90")
            if year == "2017" or year == "2018":
                plot_efficiency_and_make_jsons(finaldatareflist, finaldatatrig2list, finalmcreflist, finalmctrig2list, leg, year, multicrabpath, "100")
            else:
                plot_efficiency_and_make_jsons(finaldatareflist, finaldatatrig2list, finalmcreflist, finalmctrig2list, leg, year, multicrabpath, "110")
        elif leg == "Tau":
            plot_efficiency_and_make_jsons(finaldatareflist, finaldatatriglist, finalmcreflist, finalmctriglist, leg, year, multicrabpath)
            
            
        
    #elif bool(re.search(r"results", sys.argv[1])) and len(sys.argv) == 4:
        #resultfile = sys.argv[1]
        #leg = sys.argv[2][0:3]
        #cut = sys.argv[2][3:]
        #year = sys.argv[3]
        #resultfilepath = os.path.abspath(resultfile)
        
        #fname = glob.glob(resultfilepath+r"/JSONfiles/"+leg+cut+"*"+year+"*")[0]
        
        #warnings.filterwarnings("ignore")
        
        #rootfile = uproot.open(fname)
        #events = NanoEventsFactory.from_root(rootfile,schemaclass=NanoAODSchema.v6,metadata={"dataset": leg}).events()
        
        #finaldatareflist = flatten_and_list(events.data.ref_pt)
        #finaldatatriglist = flatten_and_list(events.data.trigger_pt)
        #finalmcreflist = flatten_and_list(events.mc.ref_pt)
        #finalmctriglist = flatten_and_list(events.mc.trigger_pt)
        
        #print("pT values obtained")
        #print("Plotting the trigger efficiency.")
        #if leg == "MET":
            #plot_efficiency_and_make_jsons(finaldatareflist, finaldatatriglist, finalmcreflist, finalmctriglist, leg, year, multicrabpath, cut)
        #elif leg == "Tau":
            #plot_efficiency_and_make_jsons(finaldatareflist, finaldatatriglist, finalmcreflist, finalmctriglist, leg, year, multicrabpath)
        
        
    else:
        print() 
        print("Error")
        usage()
        sys.exit()
    
    #if not os.path.exists(multicrabfile) or not os.path.isdir(multicrabfile):
        #usage()
        #sys.exit()

main()
