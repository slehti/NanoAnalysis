from pathlib import Path
import os

import numpy as np
import ROOT as R

from nanoanalysis.Btag import Btag
from nanoanalysis.METCleaning import rdf_METCleaning as METCleaning

def trigger(df, year=None):

    if year is None:
        cols = df.GetColumnNames()

        dynamic_trigger = ""

        if "HLT_LooseIsoPFTau50_Trk30_eta2p1_MET90" in cols:
            dynamic_trigger += "((year == \"2016\") & HLT_LooseIsoPFTau50_Trk30_eta2p1_MET90) | "
        if "HLT_MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET90" in cols:
            dynamic_trigger += "((year == \"2017\") & HLT_MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET90) | "
        if "HLT_MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET100" in cols:
            dynamic_trigger += "((year == \"2018\") & HLT_MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET100) | "
        dynamic_trigger = dynamic_trigger[:-3]

        return df.Filter(dynamic_trigger)

    flag = "raise error"
    if '2016' in year:
        flag = "HLT_LooseIsoPFTau50_Trk30_eta2p1_MET90"
    elif "2017" in year:
        flag = "HLT_MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET90"
    elif "2018" in year:
        flag = "HLT_MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET100"

    # Run 3 triggers are preliminary
    elif "2022" in year:
        flag = "HLT_PFMET120_PFMHT120_IDTight"
    elif "2023" in year:
        flag = "HLT_PFMET120_PFMHT120_IDTight"
    elif "2024" in year:
        flag = "HLT_PFMET120_PFMHT120_IDTight"

    return df.Filter(flag, "Trigger selection")

def tau_id(
    df,
    id_vse="Tau_idDeepTau2017v2p1VSe",
    id_vsmu="Tau_idDeepTau2017v2p1VSmu",
    id_vsjet="Tau_idDeepTau2017v2p1VSjet",
    e_wp=32,
    mu_wp=2,
    jet_wp=8,
    is_data=None,
    pt_min = 50,
    eta_max = 2.1
):

    df = df.Define(
        "Tau_passed",
        f"""
        filter_taus(
            TrigObj_id,
            TrigObj_eta,
            TrigObj_phi,
            Tau_eta,
            Tau_phi,
            {id_vse},
            {id_vsmu},
            Tau_pt,
            Tau_decayMode,
            Tau_leadTkPtOverTauPt,
            {pt_min},
            {eta_max},
            {e_wp},
            {mu_wp}
        )
        """
    )
    df = df.Define("Tau_isolated", f"{id_vsjet} >= {jet_wp}")
    df = df.Define("Event_tauIso", "ROOT::VecOps::Any(Tau_isolated)")

    df = df.Filter("ROOT::VecOps::Any(Tau_passed)", "Tau id before isolation")
    # For events that pass nominal isolation criterion, only keep the taus that passed
    df = df.Redefine("Tau_passed", "Tau_passed && ((Tau_isolated && Event_tauIso) || !Event_tauIso)")

    df = df.Filter("ROOT::VecOps::Any(Tau_passed)", "Tau id (nominal or inverted)")

    df  = df.Define("leadTau_pt", "Tau_pt[Tau_passed][0]")
    df  = df.Define("leadTau_mass", "Tau_mass[Tau_passed][0]")
    df  = df.Define("leadTau_eta", "Tau_eta[Tau_passed][0]")
    df  = df.Define("leadTau_phi", "Tau_phi[Tau_passed][0]")
    df  = df.Define("leadTau_leadTkPtOverTauPt", "Tau_leadTkPtOverTauPt[Tau_passed][0]")
    df  = df.Define("leadTau_leadTkDeltaEta", "Tau_leadTkDeltaEta[Tau_passed][0]")
    df = df.Define("leadTau_abseta", "abs(leadTau_eta)")

    if is_data is not None:
        if not is_data: # generator level particle matching
            df = df.Define("leadTau_genuine", "nanoanalysis::match(leadTau_eta, leadTau_phi, GenVisTau_eta, GenVisTau_phi)")
            df = df.Define(
                "GenLep",
                """
                ROOT::VecOps::abs(GenPart_pdgId) == 11 |
                (ROOT::VecOps::abs(GenPart_pdgId) == 13 &
                GenPart_pt > 10)
                """
            )
            df = df.Define("GenLep_eta", "GenPart_eta[GenLep]")
            df = df.Define("GenLep_phi", "GenPart_phi[GenLep]")
            df = df.Define("leadTau_lepfake", "nanoanalysis::match(leadTau_eta, leadTau_phi, GenLep_eta, GenLep_phi)")
            df = df.Define("leadTau_not_misid_jet", "leadTau_genuine | leadTau_lepfake")
        else: # give arbitrary labels to data to ensure the fields are defined
            df = df.Define("leadTau_genuine", "false")
            df = df.Define("leadTau_lepfake", "false")
            df = df.Define("leadTau_not_misid_jet", "false")
    else:
        # is_data is defined per-sample
        df = df.Define("leadTau_genuine", "!is_data && nanoanalysis::match(leadTau_eta, leadTau_phi, GenVisTau_eta, GenVisTau_phi)")
        df = df.Define(
            "GenLep",
            """
            !is_data && (
                ROOT::VecOps::abs(GenPart_pdgId) == 11 |
                (ROOT::VecOps::abs(GenPart_pdgId) == 13 &
                GenPart_pt > 10)
            )
            """
        )
        df = df.Define("GenLep_eta", "GenPart_eta[GenLep]")
        df = df.Define("GenLep_phi", "GenPart_phi[GenLep]")
        df = df.Define("leadTau_lepfake", "nanoanalysis::match(leadTau_eta, leadTau_phi, GenLep_eta, GenLep_phi)")
        df = df.Define("leadTau_not_misid_jet", "leadTau_genuine | leadTau_lepfake")

    return df

def e_veto(df, max_pt=15, min_eta=2.5, min_iso=0.4, iso_discriminator="Electron_mvaFall17V2Iso_WPL"):
    return df.Filter(
        f"""
        lep_veto(
            Electron_pt,
            Electron_eta,
            Electron_miniPFRelIso_all,
            {iso_discriminator},
            {max_pt},
            {min_eta},
            {min_iso}
        )
        """,
        "Reject isolated electrons"
    )

def mu_veto(df, max_pt=10, min_eta=2.5, min_iso=0.4):
    return df.Filter(
        f"""
        lep_veto(
            Muon_pt,
            Muon_eta,
            Muon_miniPFRelIso_all,
            Muon_looseId,
            {max_pt},
            {min_eta},
            {min_iso}
        )
        """,
        "Reject isolated muons"
    )

def hadr_jets(df, pt_min=30, eta_max=4.7, tau_threshold=0.5, n_required=3):
    df = df.Define(
        "Jet_passed",
        f"""
        jet_filter(
            Jet_pt,
            Jet_eta,
            Jet_phi,
            leadTau_eta,
            leadTau_phi,
            Jet_jetId,
            {pt_min},
            {eta_max},
            {tau_threshold}
        )
        """
    )
    df = (
        df.Define("HJet_pt", "Jet_pt[Jet_passed]")
        .Define("HJet_eta", "Jet_eta[Jet_passed]")
        .Define("HJet_phi", "Jet_phi[Jet_passed]")
    )
    df = df.Define("nHadronicJets", "ROOT::VecOps::Sum(Jet_passed)")
    df = df.Filter(f"nHadronicJets >= {n_required}", f"require at least {n_required} hadronic jet(s)")
    df = df.Define("leadJet_pt", "nanoanalysis::get_leading(HJet_pt)")
    df = df.Define("leadJet_eta", "nanoanalysis::get_leading(HJet_eta)")
    return df


def b_jets(df, year, eta_max=2.4, tagger="btagDeepB", wp="medium", n_required=1):
    btag = Btag(tagger, year)
    cut = -1.
    if wp=="loose":
        cut = btag.loose()
    elif wp=="medium":
        cut = btag.medium()
    elif wp=="tight":
        cut = btag.tight()
    else:
        raise ValueError(f"unexpected working point {wp}!")

    df = df.Define("Jet_isbjet", f"count_bjets(Jet_passed, Jet_eta, Jet_{tagger}, {cut}, {eta_max})")
    df = df.Define("nBjets", "ROOT::VecOps::Sum(Jet_isbjet)")
    df = df.Filter(f"nBjets >= {n_required}", f"Require at least {n_required} b-jet(s)")
    df = df.Define("leadBjet_pt", "nanoanalysis::get_leading(Jet_pt[Jet_isbjet])")
    return df

def met(df, pt_min=90):

    df = df.Filter(f"MET_pt >= {pt_min}", "offline MET selection")

    return df


def Rbb_min(df, cut_angle=40*np.pi/180):

    df = df.Define("Rbb_min", "nanoanalysis::Rbb_min(HJet_phi, MET_phi, leadTau_phi)")
    df = df.Filter(f"Rbb_min >= {cut_angle}", "Rbb_min selection")

    return df

