#!/bin/bash

# job.sh:
# arguments: 1. directory containing the multicrab to analyse,
# 2. path of the NanoAnalysis repo, if this script is being run from elsewhere
# 3. path to your grid proxy (needed if the input data is accessed using xrootd)

echo "Running Hplus2taunu analysis in Singularity container environment."
echo "Example usage:"
echo "$ bash job.sh /afs/cern.ch/work/n/name/private/data/HplusSkimMulticrab"

if [ -z "$1" ]
then
  echo "please supply this script with the path to the skim multicrab to analyse"
  exit 1
fi

if [ -z "$2" ]
then
  echo "No analysis directory specified, assuming this script is being run from the Hplus2taunuAnalysis directory"
  analysisdir=./../..
else
  echo "Using $2 as the analysis directory."
  analysisdir=$2
fi
if [ -z "$3" ]
then
  proxymount=""
else
  proxymount="-B "$3
fi
echo $analysisdir

singularity exec --home $analysisdir/../..:/work/NanoAnalysis \
                  -B /usr -B /etc $proxymount \
                  -B $1:/data/multicrab \
                  -B $analysisdir:/work/NanoAnalysis/NanoAODAnalysis/analysis \
                 --pwd /work/NanoAnalysis/NanoAODAnalysis/analysis \
                 --env X509_USER_PROXY=$3 \
                  $analysisdir/../../root.sif \
                 python3 analysis.py /data/multicrab
