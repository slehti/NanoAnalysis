import awkward as ak
import numpy as np
import sys
import os
import re
import math

from nanoanalysis.CommonSelection import *

import tools.Print

def triggerSelection(events,year,leptonflavor):
    if leptonflavor == 11:
        return (events.HLT.Ele23_Ele12_CaloIdL_TrackIdL_IsoVL &
                events.HLT.Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ
        )
    if leptonflavor == 13:
        if "2016" in year:
            if hasattr(events.HLT,"Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ"):
                return events.HLT.Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ
        else:
            if hasattr(events.HLT,"Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8"):
                return events.HLT.Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8

    return (events.event<0)


def leptonSelection(events, LEPTONFLAVOR):

    if LEPTONFLAVOR == 13:
        leptons = events.Muon

        leptons = leptons[leptons.pt > 8]
        leptons = leptons[leptons.tightId]
        pfRelIsoMax = 0.15
        leptons = leptons[leptons.pfRelIso04_all < pfRelIsoMax]
        retEvents = events
        retEvents["Muon"] = leptons
        retEvents = retEvents[(ak.num(leptons) >= 2) & (ak.num(leptons) <= 3)
            & (ak.sum(leptons.charge, axis=1) <= 1)
            & (ak.sum(leptons.charge, axis=1) >= -1)]
        return retEvents

    if LEPTONFLAVOR == 11:
        print("leptonSelection for electrons not implemented")
        return (
            (ak.num(events.Electron) == 2)
            & (ak.num(events.Electron) <= 3)
            & (ak.sum(events.Electron.charge, axis=1) <= 1)
            & (ak.sum(events.Electron.charge, axis=1) >= -1)
        )
    return None

def leptonPtCut(events, LEPTONFLAVOR):
    if LEPTONFLAVOR == 13:
        cut1 = 20
        cut2 = 10
        etamax = 2.3
        lepton = ak.concatenate([events.lepton1,events.lepton2], axis=1)
    if LEPTONFLAVOR == 11:
        cut1 = 25
        cut2 = 15
        etamax = 2.4
        lepton = events.Electron

    retEvents = events[(ak.sum((np.absolute(lepton.eta) < etamax) & (lepton.pt > cut1), axis=1) >= 1)
                     & (ak.sum((np.absolute(lepton.eta) < etamax) & (lepton.pt > cut2), axis=1) >= 2)]
    return retEvents

def Zboson(events,LEPTONFLAVOR):
    mZ = 90 #91.1876
    if LEPTONFLAVOR == 13:
        leptons = events.Muon
    if LEPTONFLAVOR == 11:
        leptons = events.Electron
    combinations = ak.combinations(leptons, 2, fields=["first", "second"])
    combinations["Zboson"] = combinations.first + combinations.second
    combinations = combinations[(combinations.Zboson.charge == 0)]
    combinations["diffZboson"] = np.absolute(combinations.Zboson.mass - mZ)

    keep = ak.min(combinations.diffZboson,axis=1)
    combinations = combinations[(keep == combinations.diffZboson)]
    lepton1 = combinations.first
    lepton2 = combinations.second

    retEvents = events
    retEvents["Zboson"] = lepton1+lepton2
    retEvents["lepton1"] = lepton1
    retEvents["lepton2"] = lepton2

    retEvents = retEvents[(ak.num(retEvents.Zboson)>0)] # removing events with all leptons same charge
    retEvents = retEvents[ak.flatten(retEvents.Zboson.pt >= 15.0)]
    retEvents = retEvents[ak.flatten(np.absolute(retEvents.Zboson.mass - mZ) <= 20.0)]

    return retEvents


def Jetid(candidate):
    # Int_t Jet ID flags bit1 is loose, bit2 is tight, bit3 is tightLepVeto
    return (
        (candidate.jetId >= 4)
    )

def PhiBB(events):
    phiBB_cut = 0.44 # 0.34
    #phiBB = abs(events.Zboson.delta_phi(events.leadingJet) - math.pi)
    phiBB = abs(events.leadingJet.delta_phi(events.Zboson) - math.pi)
    return events[ak.flatten(
        (phiBB <= phiBB_cut) | (phiBB >= 2*math.pi-phiBB_cut)
    )]
