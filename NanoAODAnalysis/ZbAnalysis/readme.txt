# Virtual environment
python -m venv VirtualTestEnvironment
source VirtualTestEnvironment/bin/activate
pip install wheel
pip install xrootd==5.4.2
pip install coffea


python -m venv VirtualEnvironmentUPROOT5
source VirtualEnvironmentUPROOT5/bin/activate
pip install wheel
pip install xrootd
pip install coffea
git clone https://github.com/scikit-hep/uproot5.git
cd uproot5
pip install .
pip install hist

(module versions: pip install xrootd -v)

# Singularity container
singularity build coffea.sif docker://coffeateam/coffea-dask
singularity shell --home /home/slehti/public/JME/NanoAnalysis/NanoAODAnalysis/ZbAnalysis -B /jme coffea.sif

pip install ..
...
ctrl-D

singularity run -i -B ${PWD}:/work coffea.sif python analysis.py /jme/multicrab_JMENano_EOS_v2p1_Run2022CDEF_20221117T1137

singularity build root.sif docker://rootproject/root:6.26.06-conda
singularity shell --home <path/to/NanoAnalysis> -B /eos/cms/store/user/slehti root.sif
	    # Examples
	    dx7-hip-02 > singularity shell --home $HOME/test/NanoAnalysis -B /jme root.sif
	    lxplus748 > singularity shell --home $TMP/NanoAnalysis -B /eos/cms/store/user/slehti root.sif
pip install coffea

# Getting JEC/JER db's
cd ../Framework/data

git clone https://github.com/cms-jet/JECDatabase
git clone https://github.com/cms-jet/JRDatabase

# Getting Rochester corrections for muons
wget https://twiki.cern.ch/twiki/pub/CMS/RochcorMuon/roccor.Run2.v5.tgz
tar xfvz roccor*.tgz
cd -

analysis.py <multicrabdir>

# grid job submission
arcproxy -S <your-VO>

arcsub -c kale-cms.grid.helsinki.fi grid.xrsl
arcstat gsiftp://kale-cms.grid.helsinki.fi:2811/jobs/...
arcget gsiftp://kale-cms.grid.helsinki.fi:2811/jobs/...

grid_submit.py /data/multicrab_JMENano_EOS_v2p1_Run2022CDEF_20221117T1137_Madhatter/ --whitelist=Run2022F --submit
grid_submit.py multicrab_JMENano_EOS_v2p1_Run2022CDEF_20221117T1137_Madhatter_processed20221124T12361513_GRID --status
../Framework/scripts/grid_submit.py /data/multicrab_JMENano_EOS_v2p1_Run2022CDEF_20221128T1333/ --submit --whitelist=DYJetsToLL,Muon_Run2022E,Muon_Run2022F --image=coffea.sif
../Framework/scripts/grid_submit.py /data/multicrab_JMENano_EOS_v2p1_Run2022CDEF_20221128T1333/ --whitelist=DYJetsToLL,Muon_Run2022E,Muon_Run2022F --image=coffea.sif
../Framework/scripts/grid_submit.py /jme/multicrab_ZbAnalysis_v10630p1_Run2018ABCD_20230109T0827/ --blacklist=herwig7,EGamma --image=coffea.sif --nmcfiles=10
../Framework/scripts/grid_submit.py /jme/multicrab_ZbAnalysis_v10630p1_Run2016FGH_20221221T1215/ --blacklist=herwig7,EG --image=coffea.sif --nfiles=5 --nmcfiles=5

rsync -a multicrab_here remote_host:public/JME/
rsync -a --include "/results/*"  --include="histograms.root" --exclude="*" remote_host:public/JME/multicrab_here multicrab_here
rsync -a dx1-915-hep.hip.helsinki.fi:public/JME/multicrab_ZbAnalysis_v10630p1_Run2018ABCD_20230109T0827_processed20230116T15393013_GRID/ .
rsync -a dx1-915-hep.hip.helsinki.fi:public/JME/`basename ${PWD}` .

# condor job submission
condor_submit MULTICRAB=$workdir/private/multicrabdir/multicrab_test ANALYSIS=Hplus2taunuAnalysis PROXY=~/private/x509up_u123456 grid_analyse.sub

# Plotting
post_process.py <pseudomulticrab> # combined mc and data and makes response histograms
plotjsons.py <plot-json-file> <post_process-rootfile>
