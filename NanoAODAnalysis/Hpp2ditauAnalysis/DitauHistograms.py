import itertools
import sys
import re

import awkward as ak
import numpy as np

import hist


class AnalysisHistograms():
    def __init__(self, isData):
        self.histograms = {}
        self.isData = isData

    def book(self):
        bins_y = np.arange(-3.99,6.01,0.02)
        bins_zpt = np.array([12, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 85, 105, 130, 175, 230, 300, 400, 500, 700, 1000, 1500])
        bins_mu = np.arange(1,100,1)
        bins_01 = np.arange(0,1.01,0.01)
        bins_mass = np.arange(200,1100,1)
        self.addHistogram('Hpp_mass', 60, 200, 1100)
#        self.add2DHistogram('h_Zpt_RpT',bins_zpt,bins_y)
#        self.add2DHistogram('h_Zpt_RMPF',bins_zpt,bins_y)

        variation_alpha = {'alpha30': '(alpha < 0.3)','alpha100': '(alpha < 1.0)'}
        variation_eta = {'eta13': '(eta < 1.3) & (eta > -1.3)','eta25': '(eta < 2.5) & (eta > -2.5)'}
        # variation_btag = {'incl': '', 'btagDeepBtight': '(btag == True)', 'btagDeepCtight': '(ctag == True)', 'gluontag': '(gluontag == True)', 'quarktag': '(quarktag == True)', 'notag': '(notag == True)'}
        variation_gen = {'incl': ''}
        variation_psweight= {'PSWeight':''}

        variation_etaNarrow = {'eta_00_03': '(abseta > 0.000) & (abseta < 0.261)',
                               'eta_03_05': '(abseta > 0.261) & (abseta < 0.522)',
                               'eta_05_08': '(abseta > 0.522) & (abseta < 0.783)',
                               'eta_08_10': '(abseta > 0.783) & (abseta < 1.044)',
                               'eta_10_13': '(abseta > 1.044) & (abseta < 1.305)',
                               'eta_13_16': '(abseta > 1.305) & (abseta < 1.566)',
                               'eta_16_17': '(abseta > 1.566) & (abseta < 1.740)',
                               'eta_17_19': '(abseta > 1.740) & (abseta < 1.930)',
                               'eta_19_20': '(abseta > 1.930) & (abseta < 2.043)',
                               'eta_20_22': '(abseta > 2.043) & (abseta < 2.172)',
                               'eta_22_23': '(abseta > 2.172) & (abseta < 2.322)',
                               'eta_23_25': '(abseta > 2.322) & (abseta < 2.500)',
                               'eta_25_27': '(abseta > 2.500) & (abseta < 2.650)',
                               'eta_27_29': '(abseta > 2.650) & (abseta < 2.853)',
                               'eta_29_30': '(abseta > 2.853) & (abseta < 2.964)',
                               'eta_30_31': '(abseta > 2.964) & (abseta < 3.139)',
                               'eta_31_35': '(abseta > 3.139) & (abseta < 3.489)',
                               'eta_35_38': '(abseta > 3.489) & (abseta < 3.839)',
                               'eta_38_52': '(abseta > 3.839) & (abseta < 5.191)'
        }
        variation_etaWide = {'eta_13_19': '(abseta > 1.305) & (abseta < 1.93)',
                             'eta_19_25': '(abseta > 1.93) & (abseta < 2.5)',
                             'eta_25_30': '(abseta > 2.5) & (abseta < 2.964)',
                             'eta_30_32': '(abseta > 2.964) & (abseta < 3.2)',
                             'eta_32_52': '(abseta > 3.2) & (abseta < 5.191)'
        }

        variation_alpha_etaNarrowWide = {'a10': '(alpha < 1.0)'}
  
        # if not self.isData:
        #     variation_psweight = {'PSWeight':'', 'PSWeight0': '', 'PSWeight1': '', 'PSWeight2': '', 'PSWeight3': ''}
        #     variation_gen = {
        #         'incl': '',
        #         'genb': '(ljetGenFlavor == 5)',
        #         'genc': '(ljetGenFlavor == 4)',
        #         'genuds': '(ljetGenFlavor > 0) & (ljetGenFlavor < 4)',
        #         'geng': '(ljetGenFlavor == 21)',
        #         'unclassified': '(ljetGenFlavor == 0)'
        #     }

        variationSet = {
            'alpha': variation_alpha,
            'eta': variation_eta,
            # 'btag': variation_btag,
            'gen': variation_gen,
            'psweight': variation_psweight
        }

        variationSet_etaNarrowWide = {
            'alpha': variation_alpha_etaNarrowWide,
            'eta': {**variation_etaNarrow, **variation_etaWide}
        }

        prefix = 'MC'
        if self.isData:
            prefix = 'DATA'
        # Narrow and Wide eta bins

        # self.histograms.update(self.Hpp_mass.histograms)

        self.Jet_pT = VariationHistograms(prefix,{"JetpT":bins_zpt},variationSet_etaNarrowWide,self.isData)
        self.histograms.update(self.Jet_pT.histograms)

        self.JetDCH_pT = VariationHistograms(prefix,{"JetpT":bins_zpt,"DCHpT":bins_zpt},variationSet_etaNarrowWide,self.isData)
        self.histograms.update(self.JetDCH_pT.histograms)

        self.RpT = VariationHistograms(prefix,{"RpT":bins_zpt},variationSet_etaNarrowWide,self.isData)
        self.histograms.update(self.RpT.histograms)

                
        # self.DCH_Hppmass = VariationHistograms(prefix,{"DCHMass":bins_mass,"DCHpT":bins_zpt},variationSet_etaNarrowWide,self.isData)
        # self.histograms.update(self.DCH_Hppmass.histograms)

        # self.DCH_pT = VariationHistograms(prefix,{"DCHpT":bins_zpt},variationSet_etaNarrowWide,self.isData)
        # self.histograms.update(self.DCH_pT.histograms)



        
        # self.DCH_Hppmass = VariationHistograms(prefix,{"DCHMass":bins_mass},variationSet_etaNarrowWide, self.isData)
        # self.histograms.update(self.DCH_Hppmass.histograms)
        # self.DCH_zpt_Hppmass = VariationHistograms(prefix,{"DCHpT":bins_zpt,"DCHMass":bins_mass},variationSet_etaNarrowWide, self.isData)
        
        # self.DCHJeta_zpt_rawNEvents = VariationHistograms(prefix+'_RawNEvents',{"DCHpT":bins_zpt},variationSet_etaNarrowWide, self.isData)
        # self.histograms.update(self.DCHJeta_zpt_rawNEvents.histograms)
        
        # self.histograms.update(self.DCH_zpt_Hppmass.histograms)

        self.DCHJeta_zpt_rmpf = VariationHistograms(prefix,{"DCHpT":bins_zpt,"MPF":bins_y},variationSet_etaNarrowWide, self.isData)
        self.histograms.update(self.DCHJeta_zpt_rmpf.histograms)
        self.DCHJeta_zpt_ptbal = VariationHistograms(prefix,{"DCHpT":bins_zpt,"PtBal":bins_y},variationSet_etaNarrowWide, self.isData)
        self.histograms.update(self.DCHJeta_zpt_ptbal.histograms)
        self.DCHJeta_zpt_rmpfnotypeI = VariationHistograms(prefix,{"DCHpT":bins_zpt,"MPFnotypeI":bins_y},variationSet_etaNarrowWide, self.isData)
        self.histograms.update(self.DCHJeta_zpt_rmpfnotypeI.histograms)

        
        self.DCH_Hppmass = VariationHistograms(prefix,{"DCHMass":bins_mass},variationSet_etaNarrowWide,self.isData)
        self.histograms.update(self.DCH_Hppmass.histograms)

        
        self.HppmassTauPt = VariationHistograms(prefix,{"DCHMass":bins_mass,"TaupT":bins_zpt},variationSet_etaNarrowWide,self.isData)
        self.histograms.update(self.HppmassTauPt.histograms)

        # self.DCHJeta_zpt_npv = VariationHistograms(prefix,{"DCHpT":bins_zpt,"NPV":bins_mu},variationSet_etaNarrowWide, self.isData)
        # self.histograms.update(self.DCHJeta_zpt_npv.histograms)
        # self.DCHJeta_zpt_rho = VariationHistograms(prefix,{"DCHpT":bins_zpt,"Rho":bins_mu},variationSet_etaNarrowWide, self.isData)
        # self.histograms.update(self.DCHJeta_zpt_rho.histograms)
        # self.DCHJeta_zpt_mu = VariationHistograms(prefix,{"DCHpT":bins_zpt,"Mu":bins_mu},variationSet_etaNarrowWide, self.isData)
        # self.histograms.update(self.DCHJeta_zpt_mu.histograms)

        # self.DCHJeta_zpt_jetchf = VariationHistograms(prefix,{"DCHpT":bins_zpt,"JetCHF":bins_01},variationSet_etaNarrowWide, self.isData)
        # self.histograms.update(self.DCHJeta_zpt_jetchf.histograms)
        # self.DCHJeta_zpt_jetnhf = VariationHistograms(prefix,{"DCHpT":bins_zpt,"JetNHF":bins_01},variationSet_etaNarrowWide, self.isData)
        # self.histograms.update(self.DCHJeta_zpt_jetnhf.histograms)
        # self.DCHJeta_zpt_jetef = VariationHistograms(prefix,{"DCHpT":bins_zpt,"JetEF":bins_01},variationSet_etaNarrowWide, self.isData)
        # self.histograms.update(self.DCHJeta_zpt_jetef.histograms)
        # self.DCHJeta_zpt_jetmf = VariationHistograms(prefix,{"DCHpT":bins_zpt,"JetMF":bins_01},variationSet_etaNarrowWide, self.isData)
        # self.histograms.update(self.DCHJeta_zpt_jetmf.histograms)
        # self.DCHJeta_zpt_jethfhf = VariationHistograms(prefix,{"DCHpT":bins_zpt,"JethfHF":bins_01},variationSet_etaNarrowWide, self.isData)
        # self.histograms.update(self.DCHJeta_zpt_jethfhf.histograms)
        # self.DCHJeta_zpt_jethfemf = VariationHistograms(prefix,{"DCHpT":bins_zpt,"JethfEMF":bins_01},variationSet_etaNarrowWide, self.isData)
        # self.histograms.update(self.DCHJeta_zpt_jethfemf.histograms)

#        self.DCHJeta_zpt_rho_npumean = VariationHistograms(prefix,{"DCHpT":bins_zpt,"Rho":bins_mu,"npumean":bins_mu},variationSet_etaNarrowWide, self.isData)
#        self.histograms.update(self.DCHJeta_zpt_rho_npumean.histograms)
#        self.DCHJeta_zpt_npv_npumean = VariationHistograms(prefix,{"DCHpT":bins_zpt,"NPV":bins_mu,"npumean":bins_mu},variationSet_etaNarrowWide, self.isData)
#        self.histograms.update(self.DCHJeta_zpt_npv_npumean.histograms)
        """
        DCHJetPtDiff

        JetPF
        Rho_vs_npumean
        NPV_vs_npumean
        DCHJetPtDiff_vs_npumean
        """
        
        # self.h_zpt_rpt = VariationHistograms("h",{"DCHpT":bins_zpt,"RpT":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_rpt.histograms)
        # self.h_zpt_rmpf = VariationHistograms("h",{"DCHpT":bins_zpt,"RMPF":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_rmpf.histograms)
        # self.h_zpt_rmpfjet1 = VariationHistograms("h",{"DCHpT":bins_zpt,"RMPF":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_rmpfjet1.histograms)
        # self.h_zpt_rmpfjetn = VariationHistograms("h",{"DCHpT":bins_zpt,"RMPF":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_rmpfjetn.histograms)
        # self.h_zpt_rmpfuncl = VariationHistograms("h",{"DCHpT":bins_zpt,"RMPF":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_rmpfuncl.histograms)
        # self.h_zpt_rmpfx = VariationHistograms("h",{"DCHpT":bins_zpt,"RMPF":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_rmpfx.histograms)
        # self.h_zpt_rmpfjet1x = VariationHistograms("h",{"DCHpT":bins_zpt,"RMPF":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_rmpfjet1x.histograms)

        # self.h_jpt_rpt = VariationHistograms("h",{"JetPt":bins_zpt,"RpT":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_rpt.histograms)
        # self.h_jpt_rmpf = VariationHistograms("h",{"JetPt":bins_zpt,"RMPF":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_rmpf.histograms)
        # self.h_jpt_rmpfjet1 = VariationHistograms("h",{"JetPt":bins_zpt,"RMPFjet1":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_rmpfjet1.histograms)
        # self.h_jpt_rmpfjetn = VariationHistograms("h",{"JetPt":bins_zpt,"RMPFjet2":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_rmpfjetn.histograms)
        # self.h_jpt_rmpfuncl = VariationHistograms("h",{"JetPt":bins_zpt,"RMPFuncl":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_rmpfuncl.histograms)
        # self.h_jpt_rmpfx = VariationHistograms("h",{"JetPt":bins_zpt,"RMPFx":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_rmpfx.histograms)
        # self.h_jpt_rmpfjet1x = VariationHistograms("h",{"JetPt":bins_zpt,"RMPFjet1x":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_rmpfjet1x.histograms)

        # self.h_ptave_rpt = VariationHistograms("h",{"PtAve":bins_zpt,"RpT":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_rpt.histograms)
        # self.h_ptave_rmpf = VariationHistograms("h",{"PtAve":bins_zpt,"RMPF":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_rmpf.histograms)
        # self.h_ptave_rmpfjet1 = VariationHistograms("h",{"PtAve":bins_zpt,"RMPFjet1":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_rmpfjet1.histograms)
        # self.h_ptave_rmpfjetn = VariationHistograms("h",{"PtAve":bins_zpt,"RMPFjetn":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_rmpfjetn.histograms)
        # self.h_ptave_rmpfuncl = VariationHistograms("h",{"PtAve":bins_zpt,"RMPFuncl":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_rmpfuncl.histograms)
        # self.h_ptave_rmpfx = VariationHistograms("h",{"PtAve":bins_zpt,"RMPFx":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_rmpfx.histograms)
        # self.h_ptave_rmpfjet1x = VariationHistograms("h",{"PtAve":bins_zpt,"RMPFjet1x":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_rmpfjet1x.histograms)

        # self.h_mu_rpt = VariationHistograms("h",{"Mu":bins_mu,"RpT":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_mu_rpt.histograms)
        # self.h_mu_rmpf = VariationHistograms("h",{"Mu":bins_mu,"RMPF":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_mu_rmpf.histograms)
        # self.h_mu_rmpfjet1 = VariationHistograms("h",{"Mu":bins_mu,"RMPFjet1":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_mu_rmpfjet1.histograms)
        # self.h_mu_rmpfjetn = VariationHistograms("h",{"Mu":bins_mu,"RMPFjetn":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_mu_rmpfjetn.histograms)
        # self.h_mu_rmpfuncl = VariationHistograms("h",{"Mu":bins_mu,"RMPFuncl":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_mu_rmpfuncl.histograms)
        # self.h_mu_rmpfx = VariationHistograms("h",{"Mu":bins_mu,"RMPFx":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_mu_rmpfx.histograms)
        # self.h_mu_rmpfjet1x = VariationHistograms("h",{"Mu":bins_mu,"RMPFjet1x":bins_y},variationSet, self.isData)
        # self.histograms.update(self.h_mu_rmpfjet1x.histograms)

        # #
        # self.h_zpt_QGL = VariationHistograms("h",{"DCHpT":bins_zpt,"QGL":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_QGL.histograms)
        # self.h_zpt_muEF = VariationHistograms("h",{"DCHpT":bins_zpt,"muEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_muEF.histograms)
        # self.h_zpt_chEmEF = VariationHistograms("h",{"DCHpT":bins_zpt,"chEmEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_chEmEF.histograms)
        # self.h_zpt_chHEF = VariationHistograms("h",{"DCHpT":bins_zpt,"chHEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_chHEF.histograms)
        # self.h_zpt_neEmEF = VariationHistograms("h",{"DCHpT":bins_zpt,"neEmEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_neEmEF.histograms)
        # self.h_zpt_neHEF = VariationHistograms("h",{"DCHpT":bins_zpt,"neHEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_zpt_neHEF.histograms)

        # self.h_jpt_QGL = VariationHistograms("h",{"JetPt":bins_zpt,"QGL":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_QGL.histograms)
        # self.h_jpt_muEF = VariationHistograms("h",{"JetPt":bins_zpt,"muEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_muEF.histograms)
        # self.h_jpt_chEmEF = VariationHistograms("h",{"JetPt":bins_zpt,"chEmEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_chEmEF.histograms)
        # self.h_jpt_chHEF = VariationHistograms("h",{"JetPt":bins_zpt,"chHEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_chHEF.histograms)
        # self.h_jpt_neEmEF = VariationHistograms("h",{"JetPt":bins_zpt,"neEmEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_neEmEF.histograms)
        # self.h_jpt_neHEF = VariationHistograms("h",{"JetPt":bins_zpt,"neHEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_jpt_neHEF.histograms)

        # self.h_ptave_QGL = VariationHistograms("h",{"PtAve":bins_zpt,"QGL":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_QGL.histograms)
        # self.h_ptave_muEF = VariationHistograms("h",{"PtAve":bins_zpt,"muEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_muEF.histograms)
        # self.h_ptave_chEmEF = VariationHistograms("h",{"PtAve":bins_zpt,"chEmEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_chEmEF.histograms)
        # self.h_ptave_chHEF = VariationHistograms("h",{"PtAve":bins_zpt,"chHEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_chHEF.histograms)
        # self.h_ptave_neEmEF = VariationHistograms("h",{"PtAve":bins_zpt,"neEmEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_neEmEF.histograms)
        # self.h_ptave_neHEF = VariationHistograms("h",{"PtAve":bins_zpt,"neHEF":bins_01},variationSet, self.isData)
        # self.histograms.update(self.h_ptave_neHEF.histograms)

        return self.histograms

    def addHistogram(self, name, nbins, binmin, binmax):
        self.histograms[name] = hist.Hist(
            hist.axis.StrCategory([], growth=True, name=name, label="label"),
            hist.axis.Regular(nbins, binmin, binmax, name="value", label="x value"),
            storage="weight",
            name=name
        )

    def add2DHistogram(self, name, xbins, ybins):
        self.histograms[name] = hist.Hist(
            hist.axis.StrCategory([], growth=True, name=name, label="label"),
            hist.axis.Variable(xbins, name="x", label="x label"),
            hist.axis.Variable(ybins, name="y", label="y label"),
            storage="weight",
            name=name
        )

    def cloneHistogram(self, nameOrig, nameClone):
        self.histograms[nameClone] = self.histograms[nameOrig].copy()

    def fill(self,events,out):

        if len(events) > 0:
            # variables
            weight     = events.weight
            mu         = events.mu
            rho        = np.ones(len(events)) #FIXME events.fixedGridRhoFastjetAll
            npv        = events.PV.npvsGood
            MET        = events.MET
            leadingJet = events.leadingJet
            DCHboson     = events.DCHboson


            # Adding a pt-0 jet in cases there are no subleading jets
            events1j = events[(ak.num(events.Jet) == 1)]
            zerojets = events1j.Jet
            zerojets['pt'] = 0
            zerojets['px'] = 0
            zerojets['py'] = 0
            zerojets['phi'] = 0
            jets = ak.concatenate([events1j.Jet,zerojets], axis=1)
            events1j['Jet'] = jets
            eventsNj = events[(ak.num(events.Jet) > 1)]
            events = ak.concatenate([eventsNj,events1j])

            subLeadingJet = events.Jet[:, 1]

            x = ak.sum(events.Jet.pt*np.cos(events.Jet.phi), axis=1)
            y = ak.sum(events.Jet.pt*np.sin(events.Jet.phi), axis=1)
            jets_all = ak.zip({"pt": np.hypot(x, y), "phi": np.arctan2(y, x), "px": x, "py": y})

            x2 = x - leadingJet.px
            y2 = y - leadingJet.py
            jets_notLeadingJet = ak.zip({"pt": np.hypot(x2, y2), "phi": np.arctan2(y2, x2), "px": x2, "py": y2})


            mx = -MET.px -jets_all.px -DCHboson.px
            my = -MET.py -jets_all.py -DCHboson.py
            METuncl = ak.zip({"pt": np.hypot(mx, my), "phi": np.arctan2(my, mx), "px": mx, "py": my})

            # alpha = 0 if subLeadingJet.pt < 15
            subLeadingJetpt = np.array(subLeadingJet.pt)
            subLeadingJetpt[(subLeadingJetpt < 15)] = 0
            events["alpha"] = ak.flatten(subLeadingJetpt/DCHboson.pt)
            events["eta"] = events.leadingJet.eta
            events["abseta"] = np.abs(events.leadingJet.eta)

            R_pT = ak.flatten(leadingJet.pt/DCHboson.pt)
            R_MPF = ak.flatten(1 + MET.pt*(np.cos(MET.phi)*DCHboson.px + np.sin(MET.phi)*DCHboson.py)/(DCHboson.pt*DCHboson.pt))
            R_MPFjet1 = ak.flatten(-leadingJet.pt*(np.cos(leadingJet.phi)*DCHboson.px + np.sin(leadingJet.phi)*DCHboson.py)/(DCHboson.pt*DCHboson.pt))
            R_MPFjetn = ak.flatten(-jets_notLeadingJet.pt*(np.cos(jets_notLeadingJet.phi)*DCHboson.px + np.sin(jets_notLeadingJet.phi)*DCHboson.py)/(DCHboson.pt*DCHboson.pt))
            R_MPFuncl = ak.flatten(-METuncl.pt*(np.cos(METuncl.phi)*DCHboson.px + np.sin(METuncl.phi)*DCHboson.py)/(DCHboson.pt*DCHboson.pt))

            pi2 = np.pi/2
            R_MPFx = ak.flatten(1 + MET.pt*(np.cos(MET.phi+pi2)*DCHboson.px + np.sin(MET.phi+pi2)*DCHboson.py)/(DCHboson.pt*DCHboson.pt))
            R_MPFjet1x = ak.flatten(-leadingJet.pt*(np.cos(leadingJet.phi+pi2)*DCHboson.px + np.sin(leadingJet.phi+pi2)*DCHboson.py)/(DCHboson.pt*DCHboson.pt))

            R_MPFnotypeI = R_MPF #FIXME

            jpt = np.array(leadingJet.pt)
            zpt = np.array(ak.flatten(DCHboson.pt))
            jpt[(jpt < 12) | (zpt < 12)] = 0
            zpt[(jpt < 12) | (zpt < 12)] = 0
            ptave = 0.5*(jpt + zpt)

            #filling
            self.Jet_pT.fill(events,jpt,weight=weight)
            self.RpT.fill(events,R_pT,weight=weight)
            self.JetDCH_pT.fill(events,jpt,zpt,weight=weight)
            # self.DCH_Hppmass.fill(events,ak.flatten(DCHboson.mass,axis=1),zpt,weight=weight)

            # self.DCH_pT.fill(events,zpt,weight=weight)

            

            # self.Hpp_mass.fill(ak.flatten(DCHboson.mass,axis=1))
            # self.DCH_Hppmass.fill(events,ak.flatten(events.DCHboson.mass,axis=1),weight=weight)
            # self.DCHJeta_zpt_rawNEvents.fill(events,zpt,weight=weight)
            # self.DCH_zpt_Hppmass.fill(events,zpt,ak.flatten(events.DCHboson.mass,axis=1),weight=weight)
            self.DCHJeta_zpt_rmpf.fill(events,zpt,R_MPF,weight=weight)
            self.DCHJeta_zpt_ptbal.fill(events,zpt,R_pT,weight=weight)
            self.DCHJeta_zpt_rmpfnotypeI.fill(events,zpt,R_MPFnotypeI,weight=weight)
            self.DCH_Hppmass.fill(events,ak.flatten(DCHboson.mass,axis=1),weight=weight)
            self.HppmassTauPt.fill(events,ak.flatten(DCHboson.mass,axis=1),ak.flatten(events.Tau.pt,axis=1),weight=weight)
            # self.DCHJeta_zpt_npv.fill(events,zpt,npv,weight=weight)
            # self.DCHJeta_zpt_rho.fill(events,zpt,rho,weight=weight)
            # self.DCHJeta_zpt_mu.fill(events,zpt,mu,weight=weight)

            # self.h_zpt_rpt.fill(events,zpt,R_pT,weight=weight)
            # self.h_zpt_rmpf.fill(events,zpt,R_MPF,weight=weight)
            # self.h_zpt_rmpfjet1.fill(events,zpt,R_MPFjet1,weight=weight)
            # self.h_zpt_rmpfjetn.fill(events,zpt,R_MPFjetn,weight=weight)
            # self.h_zpt_rmpfuncl.fill(events,zpt,R_MPFuncl,weight=weight)
            # self.h_zpt_rmpfx.fill(events,zpt,R_MPFx,weight=weight)
            # self.h_zpt_rmpfjet1x.fill(events,zpt,R_MPFjet1x,weight=weight)

            # self.h_jpt_rpt.fill(events,jpt,R_pT,weight=weight)
            # self.h_jpt_rmpf.fill(events,jpt,R_MPF,weight=weight)
            # self.h_jpt_rmpfjet1.fill(events,jpt,R_MPFjet1,weight=weight)
            # self.h_jpt_rmpfjetn.fill(events,jpt,R_MPFjetn,weight=weight)
            # self.h_jpt_rmpfuncl.fill(events,jpt,R_MPFuncl,weight=weight)
            # self.h_jpt_rmpfx.fill(events,jpt,R_MPFx,weight=weight)
            # self.h_jpt_rmpfjet1x.fill(events,jpt,R_MPFjet1x,weight=weight)

            # self.h_ptave_rpt.fill(events,ptave,R_pT,weight=weight)
            # self.h_ptave_rmpf.fill(events,ptave,R_MPF,weight=weight)
            # self.h_ptave_rmpfjet1.fill(events,ptave,R_MPFjet1,weight=weight)
            # self.h_ptave_rmpfjetn.fill(events,ptave,R_MPFjetn,weight=weight)
            # self.h_ptave_rmpfuncl.fill(events,ptave,R_MPFuncl,weight=weight)
            # self.h_ptave_rmpfx.fill(events,ptave,R_MPFx,weight=weight)
            # self.h_ptave_rmpfjet1x.fill(events,ptave,R_MPFjet1x,weight=weight)

            # self.h_mu_rpt.fill(events,mu,R_pT,weight=weight)
            # self.h_mu_rmpf.fill(events,mu,R_MPF,weight=weight)
            # self.h_mu_rmpfjet1.fill(events,mu,R_MPFjet1,weight=weight)
            # self.h_mu_rmpfjetn.fill(events,mu,R_MPFjetn,weight=weight)
            # self.h_mu_rmpfuncl.fill(events,mu,R_MPFuncl,weight=weight)
            # self.h_mu_rmpfx.fill(events,mu,R_MPFx,weight=weight)
            # self.h_mu_rmpfjet1x.fill(events,mu,R_MPFjet1x,weight=weight)

            # self.h_zpt_QGL.fill(events,zpt,leadingJet.qgl,weight=weight)
            # self.h_zpt_muEF.fill(events,zpt,leadingJet.muEF,weight=weight)
            # self.h_zpt_chEmEF.fill(events,zpt,leadingJet.chEmEF,weight=weight)
            # self.h_zpt_chHEF.fill(events,zpt,leadingJet.chHEF,weight=weight)
            # self.h_zpt_neEmEF.fill(events,zpt,leadingJet.neEmEF,weight=weight)
            # self.h_zpt_neHEF.fill(events,zpt,leadingJet.neHEF,weight=weight)

            # self.h_jpt_QGL.fill(events,jpt,leadingJet.qgl,weight=weight)
            # self.h_jpt_muEF.fill(events,jpt,leadingJet.muEF,weight=weight)
            # self.h_jpt_chEmEF.fill(events,jpt,leadingJet.chEmEF,weight=weight)
            # self.h_jpt_chHEF.fill(events,jpt,leadingJet.chHEF,weight=weight)
            # self.h_jpt_neEmEF.fill(events,jpt,leadingJet.neEmEF,weight=weight)
            # self.h_jpt_neHEF.fill(events,jpt,leadingJet.neHEF,weight=weight)

            # self.h_ptave_QGL.fill(events,ptave,leadingJet.qgl,weight=weight)
            # self.h_ptave_muEF.fill(events,ptave,leadingJet.muEF,weight=weight)
            # self.h_ptave_chEmEF.fill(events,ptave,leadingJet.chEmEF,weight=weight)
            # self.h_ptave_chHEF.fill(events,ptave,leadingJet.chHEF,weight=weight)
            # self.h_ptave_neEmEF.fill(events,ptave,leadingJet.neEmEF,weight=weight)
            # self.h_ptave_neHEF.fill(events,ptave,leadingJet.neHEF,weight=weight)

            #self.histograms['DCH_mass'].fill('DCH_mass',value=ak.flatten(DCHboson.mass),weight=weight)
        return self.histograms

class VariationHistograms():
    def __init__(self,name,xybins,var,isData):
        self.histograms = {}
        self.isData = isData
        self.selection = {}
        self.weights = {}

        variations = []
        varSelections = {}

        for key in var.keys():
            variations.append(list(var[key].keys()))
            for k2 in var[key].keys():
                varSelections[k2] = var[key][k2]

        nameBase = name
        xKey = ''
        yKey = ''
        zKey = ''
        for i,key in enumerate(xybins.keys()):
            if i > 0:
                nameBase = nameBase + '_' + key
            if xKey == '':
                xKey = key
            elif yKey == '':
                yKey = key
            else:
                zKey = key

        for combination in list(itertools.product(*variations)):
            hname = nameBase
            sele = None

            for comb in combination:
                if not (comb == '' or 'incl' in comb or comb == 'PSWeight'):
                    hname = hname + '_%s'%comb

                    if 'weight' in comb or 'Weight' in comb:
                        self.weights[hname] = comb
                        continue
                    if sele == None:
                        sele = varSelections[comb]
                    else:
                        sele = sele + " & " + varSelections[comb]
            self.selection[hname] = sele
            if len(xybins.keys()) == 1:
                self.book1d(hname,xKey,xybins[xKey])
            if len(xybins.keys()) == 2:
                self.book2d(hname,xKey,xybins[xKey],yKey, xybins[yKey])
            if len(xybins.keys()) == 3:
                self.book3d(hname,xKey,xybins[xKey],yKey,xybins[yKey],zKey,xybins[zKey])

        self.selection_re = re.compile("^\((?P<variable>\S+)\s*(?P<operator>\S+)\s*(?P<value>\S+)\)")

    def book1d(self,name,xname,xbins):
        self.histograms[name] = hist.Hist(
            hist.axis.StrCategory([], growth=True, name=name, label="label"),
            hist.axis.Variable(xbins, name="x", label=xname)
        )
    def book2d(self,name,xname,xbins,yname,ybins):
        self.histograms[name] = hist.Hist(
            hist.axis.StrCategory([], growth=True, name=name, label="label"),
            hist.axis.Variable(xbins, name="x", label=xname),
            hist.axis.Variable(ybins, name="y", label=yname),
            storage="weight",
            name="Counts"
        )
    def book3d(self,name,xname,xbins,yname,ybins,zname,zbins):
        self.histograms[name] = hist.Hist(
            hist.axis.StrCategory([], growth=True, name=name, label="label"),
            hist.axis.Variable(xbins, name="x", label=xname),
            hist.axis.Variable(ybins, name="y", label=yname),
            hist.axis.Variable(zbins, name="z", label=zname),
            storage="weight",
            name="Counts"
        )

    def fill(self,events,x,y=[],z=[],weight=[]):
        for key in self.histograms:
            sele = self.select(events,key)
            selected_x = x[sele]
            selected_w = np.ones(len(selected_x))
            if len(weight) > 0:
                w = self.getweight(events,key)
                selected_w = weight[sele]*w[sele]
            if len(y) == 0:
                self.histograms[key].fill(key,selected_x,weight=selected_w)
            elif len(z) == 0:
                selected_y = y[sele]
                self.histograms[key].fill(key,selected_x,
                                          selected_y,
                                          weight=selected_w)
            else:
                selected_y = y[sele]
                selected_z = z[sele]
                self.histograms[key].fill(key,selected_x,
                                          selected_y,
                                          selected_z,
                                          weight=selected_w)
        return self.histograms

    def getweight(self,events,variable):
        w = np.array([1.0]*len(events))
        if variable in self.weights.keys():
            if 'PSWeight' in self.weights[variable] and len(self.weights[variable]) > 8:
                weightnumber = int(self.weights[variable].replace('PSWeight',''))
                w = events['PSWeight'][:,weightnumber]
        return w

    def select(self,events,variable):
        return_sele = (events["event"] > 0)
        if self.selection[variable] == None:
            return return_sele
        selections = self.selection[variable].split('&')

        for s in selections:
            s = s.strip()
            match = self.selection_re.search(s)
            if match:
                variable = match.group('variable')
                operator = match.group('operator')
                value    = eval(match.group('value'))

                if operator == '<':
                    return_sele = return_sele & (events[variable] < value)
                if operator == '>':
                    return_sele = return_sele & (events[variable] > value)
                if operator == '==':
                    return_sele = return_sele & (events[variable] == value)
            else:
                print("Problem with VariationHistograms selection",s)
                sys.exit()
        return return_sele

