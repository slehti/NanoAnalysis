#!/usr/bin/env python

import sys
import re
import array
import os
import json

from optparse import OptionParser

import ROOT
ROOT.gROOT.SetBatch(True)

basepath_re = re.compile("(?P<basepath>\S+/NanoAnalysis)/")
match = basepath_re.search(os.getcwd())
if match:
    sys.path.append(os.path.join(match.group("basepath"),"NanoAODAnalysis/Framework/python"))

import tools.tdrstyle as tdrstyle

root_re = re.compile("(?P<rootfile>(\S+\.root))")
json_re = re.compile("(?P<jsonfile>(\S+\.json))")

def usage():
    print
    print("### Usage:  ",sys.argv[0],"<rootfile> <json file(s)>")
    print
    print
    sys.exit()

def main(opts,args):

    rootfiles = []
    jsonfiles = []
    for arg in args:
        if not os.path.isfile(arg):
            continue
        match_root = root_re.search(arg)
        if match_root:
            rootfiles.append(arg)
        match_json = json_re.search(arg)
        if match_json:
            jsonfiles.append(arg)
    #print(rootfiles)
    #print(jsonfiles)
    if len(rootfiles) == 0:
        print("No rootfile given as an argument")
        usage()
    if len(jsonfiles) == 0:
        print("No jsonfile given as an argument")
        usage()

    style = tdrstyle.TDRStyle()

    for jsonfile in jsonfiles:
        #print("check jsonfile",jsonfile)
        with open(jsonfile, 'r') as fJSON:
            config = json.load(fJSON)
            #print(config)
            #style = tdrstyle.TDRStyle()
            canvas = ROOT.TCanvas(config["name"],"",500,700)
            lumi = 0
            energy = 0
            ratioplot = False
            if "ratioframe_ymin" in config.keys():
                ratioplot = True
                a1 = 0.01
                b1 = 0.33
                a2 = 0.99
                b2 = 0.98
                mainpad = ROOT.TPad("mainpad","mainpad",a1,b1,a2,b2)
                mainpad.SetTopMargin(0.05)
                mainpad.SetRightMargin(0.04)
                mainpad.SetLeftMargin(0.15)
                mainpad.SetBottomMargin(0)
                mainpad.Draw()
                #mainpad.SetLogx()
                #mainpad.SetGridx()
                #mainpad.SetGridy()

                a1 = 0.01
                b1 = 0.01
                a2 = 0.99
                b2 = 0.33
                ratiopad = ROOT.TPad("ratiopad","",a1,b1,a2,b2)
                ratiopad.SetTopMargin(0)
                ratiopad.SetRightMargin(0.04)
                ratiopad.SetLeftMargin(0.15)
                ratiopad.SetBottomMargin(0.3)
                ratiopad.SetFrameBorderMode(0)
                ratiopad.Draw()
    
                mainpad.cd()
                
            else:
                canvas.cd()
            #print("check frame",config["frame_xmin"],type(config["frame_xmin"]))
            frame = ROOT.TH2F("frame","",2,config["frame_xmin"],config["frame_xmax"],2,config["frame_ymin"],config["frame_ymax"])
            frame.SetStats(0)
            frame.GetYaxis().SetTitleOffset(1.05)
            frame.GetXaxis().SetTitle(config["xlabel"])
            frame.GetYaxis().SetTitle(config["ylabel"])
            if "xNdivisions" in config.keys():
                frame.GetXaxis().SetNdivisions(config["xNdivisions"])
            frame.Draw()

            if ratioplot:
                rframe = ROOT.TH2F("ratioframe","",2,config["frame_xmin"],config["frame_xmax"],2,config["ratioframe_ymin"],config["ratioframe_ymax"])
                rframe.SetStats(0)
                titlescale = 0.75 #2
                #rframe.GetXaxis().SetTitleSize(frame.GetXaxis().GetTitleSize())
                #rframe.GetXaxis().SetLabelSize(titlescale*frame.GetXaxis().GetLabelSize())
                #rframe.GetXaxis().SetTitleOffset(0) 
                #rframe.GetYaxis().SetTitleSize(titlescale*frame.GetYaxis().GetTitleSize())
                rframe.GetYaxis().SetLabelSize(titlescale*frame.GetYaxis().GetLabelSize())
                rframe.GetYaxis().SetTitleOffset(1.05)
                rframe.GetYaxis().SetNdivisions(505)                                                                                   
                #rframe.GetXaxis().SetTitleSize(1.2)
                #rframe.GetXaxis().SetTitle(config["xlabel"])                                                                           
                #rframe.GetXaxis().SetMoreLogLabels()                                                                                   
                #rframe.GetXaxis().SetNoExponent()                                                                                      
                rframe.GetXaxis().SetTitle(config["xlabel"])
                rframe.GetYaxis().SetTitle(config["ratio_label"])
                if "xNdivisions" in config.keys():
                    rframe.GetXaxis().SetNdivisions(config["xNdivisions"])
                                

            referencehisto = None
            for rootfile in rootfiles:
                fROOT = ROOT.TFile.Open(rootfile, 'r')
                if "legend_xmin" in config.keys():
                    legend = ROOT.TLegend(config["legend_xmin"],config["legend_ymin"],config["legend_xmax"],config["legend_ymax"])
                for histoname in config["histograms"].keys():
                    print("Plotting histogram",histoname)
                    lumi = readMetaData(fROOT,config["lumi"])
                    energy = readMetaData(fROOT,config["energy"])
                    run = readMetaData(fROOT,config["run"])
                    histogram = fROOT.Get(histoname)
                    if histogram==None:
                        print("Histogram not found in the rootfile",histoname)
                        sys.exit()
                    histostyles = config["histograms"][histoname]
                    drawoptions = "SAME"
                    if "DrawOptions" in histostyles.keys():
                        drawoptions = histostyles["DrawOptions"]
                    if "MarkerStyle" in histostyles.keys():
                        histogram.SetMarkerStyle(histostyles["MarkerStyle"])
                    if "MarkerColor" in histostyles.keys():
                        histogram.SetMarkerColor(histostyles["MarkerColor"])
                    if "MarkerSize" in histostyles.keys():
                        histogram.SetMarkerSize(histostyles["MarkerSize"])
                    if "LineStyle" in histostyles.keys():
                        histogram.SetLineStyle(histostyles["LineStyle"])
                    if "LineColor" in histostyles.keys():
                        histogram.SetLineColor(histostyles["LineColor"])
                    if "LineWidth" in histostyles.keys():
                        histogram.SetLineWidth(histostyles["LineWidth"])
                    histogram.Draw(drawoptions)
                    if "legend_xmin" in config.keys():
                        drawoptionslegend = drawoptions[:-4] # drop 'SAME'
                        if len(drawoptionslegend) == 0:
                            drawoptionslegend = "L"
                        legend.AddEntry(histogram,config["histograms"][histoname]["Legend"],drawoptionslegend)

                    if ratioplot:
                        ratiopad.cd()
                        if referencehisto == None:
                            referencehisto = fROOT.Get(config["ratio_denominator"])
                            if referencehisto == None:
                                print("Ref histogram not found in the rootfile",config["ratio_denominator"])
                                sys.exit()
                            rframe.Draw()
                            line = ROOT.TLine(rframe.GetXaxis().GetXmin(),1,frame.GetXaxis().GetXmax(),1)
                            line.SetLineColor(33)
                            line.SetLineStyle(2)
                            line.Draw("SAME")
                        if not histoname == config["ratio_denominator"]:
                            if isinstance(referencehisto,ROOT.TGraph) or isinstance(referencehisto,ROOT.TGraphErrors):
                                x = referencehisto.GetX()
                                y = referencehisto.GetY()
                                xprime = histogram.GetX()
                                yprime = histogram.GetY()
                                xratio = array.array('d')
                                yratio = array.array('d')
                                #print(x,y,yprime)
                                for i in range(0,referencehisto.GetN()):
                                    for j in range(0,histogram.GetN()):
                                        if abs(x[i] - xprime[j]) < 0.01:
                                            xratio.append(x[i])
                                            value = 0
                                            if not y[i] == 0:
                                                value = yprime[i]/y[i]
                                            yratio.append(value)
                                ratio = ROOT.TGraph(len(xratio),xratio,yratio)
                                ratio.SetMarkerStyle(histogram.GetMarkerStyle())
                                ratio.SetMarkerColor(histogram.GetMarkerColor())
                                ratio.SetMarkerSize(0.5*histogram.GetMarkerSize())
                                ratio.SetLineStyle(histogram.GetLineStyle())
                                ratio.SetLineColor(histogram.GetLineColor())
                                #ratio.SetLineWidth(0.5*histogram.GetLineWidth())
                                ratio.Draw(drawoptions)
                            else:
                                ratio = histogram.Divide(referencehisto)
                                ratio.Draw(drawoptions)
                        mainpad.cd()
                if "legend_xmin" in config.keys():
                    legend.SetLineColor(0)
                    legend.SetFillColor(0)
                    legend.SetShadowColor(0)
                    legend.Draw()
                fROOT.Close()

            canvas.cd()
            if lumi > 0:
                text = ROOT.TLatex(0.45,0.95,run)
                text.SetTextSize(25)
                text.SetNDC()
                text.DrawClone()

                text = ROOT.TLatex(0.2,0.95,"%s fb^{-1}"%round(lumi/1000,1))
                text.SetTextSize(25)
                text.SetNDC()
                text.DrawClone()

                e = "%s"%energy
                e = e.rstrip('0')
                e = e.rstrip('.')
                text = ROOT.TLatex(0.75,0.95,"%s TeV"%e)
                text.SetTextSize(25)
                text.SetNDC()
                text.DrawClone()

            for key in config.keys():
                if "text" in key:
                    text = ROOT.TLatex(config[key]["xpos"],config[key]["ypos"],config[key]["text"])
                    text.SetTextSize(config[key]["size"])
                    text.SetNDC()
                    text.DrawClone()

            for suffix in config["endings"]:
                canvas.Print(config["name"]+'.'+suffix)

            #print("check end")

def readMetaData(fIN,datahistoname):
    histo = fIN.Get(datahistoname)
    if isinstance(histo,ROOT.TH1D):
        return histo.GetBinContent(1)
    if isinstance(histo,ROOT.TNamed):
        return histo.GetTitle()

if __name__ == "__main__":
    if len(sys.argv) == 1:
        usage()
    parser = OptionParser(usage="Usage: %prog [options]")
    (opts, args) = parser.parse_args()
    main(opts,args)
