#!/usr/bin/env python

import os,sys,re
import datetime
import ROOT

ROOT.gROOT.SetBatch(True)

basepath_re = re.compile("(?P<basepath>\S+/NanoAnalysis)/")
match = basepath_re.search(os.getcwd())
if match:
    sys.path.append(os.path.join(match.group("basepath"),"NanoAODAnalysis/Framework/python"))

from aux import execute
from tools.plotting import *

OUTPUTDIR = "figures"

root_re = re.compile("(?P<rootfile>([^/]*histograms.root))")

def usage():
    print()
    print("### Usage:  ",os.path.basename(sys.argv[0]),"<multicrab histograms>")
    print()

def writeMetadata(rootfile,path,dataset):
    if path != "":
        tdir = rootfile.Get(path)
        if not tdir:
            tdir = rootfile.mkdir(path)
        ROOT.gFile.cd(path)

    days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    now = datetime.datetime.now()
    m = "produced: %s %s-%s-%s %02d:%02d:%02d"%(days[now.weekday()],now.year,now.month,now.day,now.hour,now.minute,now.second)

    h_timestamp = ROOT.TNamed(m,"")
    h_timestamp.Write()

    if dataset.isData():
        h_lumi = ROOT.TH1D("lumi","",1,0,1)
        h_lumi.SetBinContent(1,dataset.getLumi())
        h_lumi.Write()

        h_energy = ROOT.TH1D("energy","",1,0,1)
        h_energy.SetBinContent(1,dataset.getEnergy())
        h_energy.Write()

        h_run = ROOT.TNamed("run",dataset.getRun())
        h_run.Write()

    h_year = ROOT.TNamed("year",dataset.getYear())
    h_year.Write()

    jec = dataset.getJEC()
    s_jec = ""
    for jec in dataset.getJEC()[1:]:
        if len(s_jec) > 0:
            s_jec += ';'
        s_jec += jec
    h_jec = ROOT.TNamed("JEC",s_jec)
    h_jec.Write()

    rootfile.cd()

def writeResponse(rootfile,histogram,newname):
    if not isinstance(histogram, ROOT.TH2D):
        return

    nbinsx = histogram.GetNbinsX()
    x     = []
    x_err = []
    y     = []
    y_err = []
    firstBin = 1
    for ibin in range(1,nbinsx):
        bincenter = histogram.GetXaxis().GetBinCenter(ibin)
        x.append(bincenter)
        x_err.append(0.0)
        firstBin = ibin
        yslice = histogram.ProjectionY("",firstBin,ibin)
        y.append(yslice.GetMean())
        y_err.append(yslice.GetMeanError())
    rootfile.cd()
    graph = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',y),array.array('d',x_err),array.array('d',y_err))
    if '/' in newname:
        path = os.path.dirname(newname)
        name = os.path.basename(newname)
        tdir = rootfile.Get(path)
        if not tdir:
            tdir = rootfile.mkdir(path)
        tdir.cd()
        graph.Write(name)
    else:
        graph.Write(newname)
    rootfile.cd()

def writeHistogram(rootfile,histogram,newname):
    rootfile.cd()
    if '/' in newname:
        path = os.path.dirname(newname)
        name = os.path.basename(newname)
        tdir = rootfile.Get(path)
        if not tdir:
            tdir = rootfile.mkdir(path)
        tdir.cd()
        histogram.Write(name)
    else:
        histogram.Write(newname)
    rootfile.cd()

def main():

    if len(sys.argv) == 1:
        usage()
        sys.exit()

    multicrabdirs = sys.argv[1:]
    for multicrabdir in multicrabdirs:
        if not os.path.exists(multicrabdir) or not os.path.isdir(multicrabdir):
            usage()
            sys.exit()
        else:
            print(multicrabdir)

    whitelist = []
    blacklist = []

    print("Getting datasets")
    datasets = getDatasetsMany(multicrabdirs,whitelist=whitelist,blacklist=blacklist)
    print("Datasets received")

    datasets = read(datasets)
    datasets = mergeExtDatasets(datasets)
    datasets = normalizeToLumi(datasets)

    runs = getRuns(datasets)
    #for run in runs:
    #    datasets = mergeDatasets("Data_%s"%run,run,datasets,keep=True)
    datasets = mergeDatasets("Data_MERGED",'_Run20\d\d\S_',datasets)
    Data_MERGED = getDatasetByName("Data_MERGED",datasets)
    Data_MERGED_name = "Data_%s"%Data_MERGED.getRun()
    if Data_MERGED != None:
        datasets = renameDataset("Data_MERGED",Data_MERGED_name,datasets)
    datasets = mergeDatasets("MC","^(?!Data).*",datasets)
    #printd(datasets)

    respselection = []
    respselection.append(re.compile("analysis/(?P<label>\S+)_MPF_a10_eta_\d\d_\d\d"))
    respselection.append(re.compile("analysis/(?P<label>\S+)_PtBal_a10_eta_\d\d_\d\d"))

    respselection.append(re.compile("analysis/(?P<label>\S+)_RpT_alpha\d+_eta\d\d"))
    respselection.append(re.compile("analysis/(?P<label>\S+)_RMPF_alpha\d+_eta\d\d"))
    respselection.append(re.compile("analysis/(?P<label>\S+)_RMPFjet1_alpha\d+_eta\d\d"))
    respselection.append(re.compile("analysis/(?P<label>\S+)_RMPFjetn_alpha\d+_eta\d\d"))
    respselection.append(re.compile("analysis/(?P<label>\S+)_RMPFuncl_alpha\d+_eta\d\d"))

    histoselection = []
    histoselection.append(re.compile("analysis/(?P<label>\S+)_RawNEvents_a10_eta_\d\d_\d\d"))
    histoselection.append(re.compile("analysis/(?P<label>\S+)_ZMass_a10_eta_\d\d_\d\d"))
    histoselection.append(re.compile("analysis/(?P<label>\S+)_Mu_a10_eta_\d\d_\d\d"))
    histoselection.append(re.compile("analysis/(?P<label>\S+)_Rho_a10_eta_\d\d_\d\d"))
    histoselection.append(re.compile("analysis/(?P<label>\S+)_NPV_a10_eta_\d\d_\d\d"))


    fNAME = "test.root"
    fOUT = ROOT.TFile.Open(fNAME,"RECREATE")
    for dataset in datasets:
        prefix = "MC"
        if dataset.isData():
            prefix = "DATA"

        jec = dataset.getJEC()
        jecname = jec[0]
        rootdir = Data_MERGED_name+'/'+jecname

        writeMetadata(fOUT,rootdir,dataset)

        #Howto make one response histogram
        #plotResponse(fOUT,dataset.getHistogram('analysis/%s_Mu_a10_eta_00_03'%prefix),'%s/DATA_Mu_a10_eta_00_03'%rootdir)

        #Howto plot response for matching histograms
        for reg in respselection:
            for histoname in dataset.getHistogramNames():
                match = reg.search(histoname)
                if match:
                    newname = os.path.join(rootdir,os.path.basename(histoname))
                    writeResponse(fOUT,dataset.getHistogram(histoname),newname)
        #Renaming/replacing matching histograms
        for reg in histoselection:
            for histoname in dataset.getHistogramNames():
                match = reg.search(histoname)
                if match:
                    newname = os.path.join(rootdir,os.path.basename(histoname))
                    writeHistogram(fOUT,dataset.getHistogram(histoname),newname)

    fOUT.Close()
    print("Wrote output in %s"%fNAME)
    
if __name__ == "__main__":
    main()
