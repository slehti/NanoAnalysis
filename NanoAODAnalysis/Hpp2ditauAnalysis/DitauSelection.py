import awkward as ak
import numpy as np
import sys
import os
import re
import math
import Counter

from CommonSelection import *

import tools.Print

def triggerSelection(events,year):
    return events.HLT.MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET100 #events.HLT.DoubleLooseChargedIsoPFTau35_Trk1_eta2p1_Reg # now at loose, add lmt to arg if switches required


    # if lmt == 'Loose':
    #     return events[events.HLT_DoubleLooseChargedIsoPFTau35_Trk1_eta2p1_Reg]
    # if lmt == 'Medium':
    #     return events[events.HLT_DoubleMediumChargedIsoPFTau35_Trk1_eta2p1_Reg]
    # if lmt == 'Tight':
    #     return events[events.HLT_DoubleTightChargedIsoPFTau35_Trk1_eta2p1_Reg]
    #     # return events.HLT.Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ                                                              
    # return None


def leptonSelection(events):
    leptons = events.Tau

    leptons = leptons[leptons.pt > 20]
    # leptons = leptons[leptons.tightId]
    pfRelIsoMax = 0.15
    # leptons = leptons[leptons.pfRelIso04.all < pfRelIsoMax]
    retEvents = events
    retEvents["Tau"] = leptons
    retEvents = retEvents[(ak.num(leptons) >= 3)
                          & (ak.num(leptons) <= 5)
                          & (ak.sum(leptons.charge, axis=1) <= 1)
                          & (ak.sum(leptons.charge, axis=1) >= -1)]
    return retEvents    

def leptonPtCut(events):
    cut1 = 20
    cut2 = 10
    etamax = 2.3
    lepton = events.Tau
    
    retEvents = events[(ak.sum((np.absolute(lepton.eta) < etamax) & (lepton.pt > cut1), axis=1) >= 1)
                     & (ak.sum((np.absolute(lepton.eta) < etamax) & (lepton.pt > cut2), axis=1) >= 2)]
    return retEvents

def DCHboson(events):
    mDCH = 300

    leptons = events.Tau
    combinations = ak.combinations(leptons, 2, fields=["first", "second"])
    combinations["DCHboson"] = combinations.first + combinations.second
    combinations["diffDCHboson"] = np.absolute(combinations.DCHboson.mass - mDCH)

    combinations = combinations[(abs(combinations.DCHboson.charge) == 2)]

    keep = ak.min(combinations.diffDCHboson,axis=1)
    combinations = combinations[(keep == combinations.diffDCHboson)]

    lepton1 = combinations.first
    lepton2 = combinations.second

    retEvents = events
    retEvents["DCHboson"] = lepton1+lepton2
    retEvents["lepton1"] = lepton1
    retEvents["lepton2"] = lepton2

    retEvents = retEvents[ak.flatten(retEvents.DCHboson.pt > 15)]
    retEvents = retEvents[ak.flatten(np.absolute(retEvents.DCHboson.mass - mDCH) < 1000)]
    return retEvents


def Jetid(candidate):
    return (
        (Tau_decayMode <= 4)
    )



def electronVeto(events,year,lmt):
    ele = ak.zip({
            "pt": events.Electron.pt,
            "eta": events.Electron.eta,
            "phi": events.Electron.phi,
            "mass": events.Electron.mass,
            "charge": events.Electron.charge,
            "miniPF": events.Electron.miniPFRelIso_all
    }, with_name="PtEtaPhiMCandidate")


    if lmt == 'Loose':
        ptcut = ele.pt < 20
        etacut = ele.eta > 2
        miniPFcut = ele.miniPF > 0.5

    elif lmt == 'Medium':
        ptcut = ele.pt < 20
        etacut = ele.eta > 2
        miniPFcut = ele.miniPF > 0.5

    elif lmt == 'Tight':
        ptcut = ele.pt < 20
        etacut = ele.eta > 2
        miniPFcut = ele.miniPF > 0.5

    else:
        # print('No electron veto applied, check the criterion!')
        ptcut = ele.pt < 99999
        etacut = ele.eta > -99999
        miniPFcut = ele.miniPF > -99999

    cut = ptcut & etacut & miniPFcut

    ele = ele[cut]
    events = events[ak.num(ele) == 0]

    return events


def muonVeto(events,year,lmt):
    muon = ak.zip({
            "pt": events.Muon.pt,
            "eta": events.Muon.eta,
            "phi": events.Muon.phi,
            "mass": events.Muon.mass,
	    "charge": events.Muon.charge,
            "miniPF": events.Muon.miniPFRelIso_all
    }, with_name="PtEtaPhiMCandidate")


    if lmt == 'Loose':
        ptcut = muon.pt < 20
        etacut = muon.eta > 2
        miniPFcut = muon.miniPF > 0.5

    elif lmt == 'Medium':
        ptcut = muon.pt < 20
        etacut = muon.eta > 2
        miniPFcut = muon.miniPF > 0.5

    elif lmt == 'Tight':
        ptcut = muon.pt < 20
        etacut = muon.eta > 2
        miniPFcut = muon.miniPF > 0.5

    else:
        # print('No muon veto applied, check the criterion!')
        ptcut = muon.pt < 99999
        etacut = muon.eta > -99999
        miniPFcut = muon.miniPF > -99999

    cut = ptcut & etacut & miniPFcut

    muon = muon[cut]
    events = events[ak.num(muon) == 0]

    return events


def ditauSel(events, year, counter):

    year = year
    counter = counter
    
    events = events[triggerSelection(events,  year)] #korjaa ylempää LEPTONFLAVOR pois
    counter.increment('passed trigger',events)
    
    events = events[METCleaning(events,  year)]
    counter.increment('MET cleaning',events)
    
    events = muonVeto(events, year,'Loose')
    counter.increment('Muon veto',events)
    
    events = electronVeto(events, year,'Loose')
    counter.increment('Electron veto',events)

    events = leptonSelection(events)
    counter.increment('lepton selection',events)

    # events['Tau'] =  rochesterCorrections.apply(events)
    events = leptonPtCut(events)
    counter.increment('lepton pt cut',events)

    events = DCHboson(events)
    counter.increment('DCH boson',events)
    

    events = JetSelection(events)
    counter.increment('jet selection',events)

    return events,counter
