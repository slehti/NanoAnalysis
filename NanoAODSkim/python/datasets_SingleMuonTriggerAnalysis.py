import sys,os
JSONPATH = os.path.realpath(os.path.join(os.getcwd(),"../data"))

# The name of this file MUST contain the analysis name: datasets_<name>Analysis.py.
# E.g. NanoAOD_Hplus2taunuAnalysisSkim.py -> datasets_Hplus2taunuAnalysis.py
# lumijsons /eos/home-c/cmsdqm/www/CAF/certification/Collisions22
# https://cms-service-dqmdc.web.cern.ch/CAF/certification/Collisions24/DCSOnly_JSONS/dailyDCSOnlyJSON/Collisions24_13p6TeV_378981_379355_DCSOnly_TkPx.json
#/eos/home-c/cmsdqm/www/CAF/certification/Collisions24/Cert_Collisions2024_378981_379075_Golden.json

from Dataset import Dataset
"""
class Dataset:
    def __init__(self, url, dbs="global", dataVersion="94Xmc", lumiMask="", name=""):
        self.URL = url
        self.DBS = dbs
        self.dataVersion = dataVersion
        self.lumiMask = lumiMask
        self.name = name
        self.files = []

    def isData(self):
        if "Run20" in self.URL:
            return True
        return False

    def getName(self):
        return self.name

    def getYear(self):
        year_re = re.compile("/Run(?P<year>20\d\d)\S")
        match = year_re.search(self.URL)
        if match:
            return match.group("year")
        else:
            return None

    def setFiles(self, files):
        self.files = files

    def getFiles(self):
        return self.files
"""

datasets = {}

lumijson2024 = "Collisions24_13p6TeV_378981_386951_DCSOnly_TkPx.json"
datasets["2024"] = []
datasets["2024"].append(Dataset('/Muon0/Run2024B-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraB_Golden.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024B-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraB_Golden.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024C-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_379415-379866_Golden_Run2024C1.json"), name="Muon0_Run2024C_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon1/Run2024C-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_379415-379866_Golden_Run2024C1.json"), name="Muon1_Run2024C_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon0/Run2024C-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_379984-380238_Golden_Run2024C2.json"), name="Muon0_Run2024C_PromptReco_v2"))
datasets["2024"].append(Dataset('/Muon1/Run2024C-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_379984-380238_Golden_Run2024C2.json"), name="Muon1_Run2024C_PromptReco_v2"))
datasets["2024"].append(Dataset('/Muon0/Run2024D-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380255-380947_Golden_Run2024D.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024D-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380255-380947_Golden_Run2024D.json")))

datasets["2024"].append(Dataset('/Muon0/Run2024E-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380956-381380_Golden_Run2024E1.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024E-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380956-381380_Golden_Run2024E1.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024E-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381384-381591_Golden_Run2024E2.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024E-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381384-381591_Golden_Run2024E2.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381984-382262_Golden_Run2024F1.json"), name="Muon0_Run2024F_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon1/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381984-382262_Golden_Run2024F1.json"), name="Muon1_Run2024F_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon0/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_382298-383246_Golden_Run2024F2.json"), name="Muon0_Run2024F_PromptReco_v2"))
datasets["2024"].append(Dataset('/Muon1/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_382298-383246_Golden_Run2024F2.json"), name="Muon1_Run2024F_PromptReco_v2"))
datasets["2024"].append(Dataset('/Muon0/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_383247-383779_Golden_Run2024F3.json"), name="Muon0_Run2024F_PromptReco_v3"))
datasets["2024"].append(Dataset('/Muon1/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_383247-383779_Golden_Run2024F3.json"), name="Muon1_Run2024F_PromptReco_v3"))

datasets["2024"].append(Dataset('/Muon0/Run2024G-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_383811-384932_Golden_Run2024G1.json"), name="Muon0_Run2024G_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon1/Run2024G-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_383811-384932_Golden_Run2024G1.json"), name="Muon1_Run2024G_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon0/Run2024G-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_384933-385801_Golden_Run2024G2.json"), name="Muon0_Run2024G_PromptReco_v2"))
datasets["2024"].append(Dataset('/Muon1/Run2024G-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_384933-385801_Golden_Run2024G2.json"), name="Muon1_Run2024G_PromptReco_v2"))

datasets["2024"].append(Dataset('/Muon0/Run2024H-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_385836-385933_Golden_Run2024H.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024H-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_385836-385933_Golden_Run2024H.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024I-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_386423-386693_Golden_Run2024I1.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024I-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_386423-386693_Golden_Run2024I1.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024I-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_386694-386974_Golden_Run2024I2.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024I-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_386694-386974_Golden_Run2024I2.json")))


datasets["2024"].append(Dataset('/DYto2L-4Jets_MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Winter24NanoAOD-JMENanoV14_133X_mcRun3_2024_realistic_v10_ext2-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_2024_JMENanoV14_ext2_v1"))
datasets["2024"].append(Dataset('/DYto2Mu-4Jets_Bin-MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/RunIII2024Summer24NanoAOD-140X_mcRun3_2024_realistic_v26-v2/NANOAODSIM', dataVersion="mc", name="DYto2Mu_Summer24"))
datasets["2024"].append(Dataset('/DYto2E-4Jets_Bin-MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/RunIII2024Summer24NanoAOD-140X_mcRun3_2024_realistic_v26-v2/NANOAODSIM', dataVersion="mc", name="DYto2E_Summer24"))
datasets["2024"].append(Dataset('/DYto2Tau-4Jets_Bin-MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/RunIII2024Summer24NanoAOD-140X_mcRun3_2024_realistic_v26-v2/NANOAODSIM', dataVersion="mc", name="DYto2Tau_Summer24"))

datasets["2023"] = []
datasets["2023"].append(Dataset('/Muon0/Run2023B-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_366403-367079_Golden_Run2023B.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023B-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_366403-367079_Golden_Run2023B.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367094-367515_Golden_Run2023C1.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367094-367515_Golden_Run2023C1.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367516-367619_Golden_Run2023C2.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367516-367619_Golden_Run2023C2.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-22Sep2023_v3-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367661-367758_Golden_Run2023C3.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v3-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367661-367758_Golden_Run2023C3.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v4-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367770-369694_Golden_Run2023C4.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v4-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367770-369694_Golden_Run2023C4.json")))
datasets["2023"].append(Dataset('/DYto2L-4Jets_MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer23NanoAODv12-130X_mcRun3_2023_realistic_v14-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50"))
#datasets["2023"].append(Dataset('', dataVersion="mc"))

datasets["2023BPix"] = []
datasets["2023BPix"].append(Dataset('/Muon0/Run2023D-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_369844-370580_Golden_Run2023D1.json")))
datasets["2023BPix"].append(Dataset('/Muon1/Run2023D-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_369844-370580_Golden_Run2023D1.json")))
datasets["2023BPix"].append(Dataset('/Muon0/Run2023D-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_370616-371225_Golden_Run2023D2.json")))
datasets["2023BPix"].append(Dataset('/Muon1/Run2023D-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_370616-371225_Golden_Run2023D2.json")))
datasets["2023BPix"].append(Dataset('/DYto2L-4Jets_MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer23BPixNanoAODv12-JMENano12p5_132X_mcRun3_2023_realistic_postBPix_v4-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_postBPix"))
datasets["2023BPix"].append(Dataset('/TT_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer23BPixNanoAODv12-130X_mcRun3_2023_realistic_postBPix_v2-v2/NANOAODSIM', dataVersion="mc"))

datasets["2022E"] = []
datasets["2022E"].append(Dataset('/Muon/Run2022E-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraE_359022_360331_Golden.json")))
datasets["2022E"].append(Dataset('/Muon/Run2022F-22Sep2023-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraF_360390_362167_Golden.json")))
datasets["2022E"].append(Dataset('/Muon/Run2022G-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraG_362433_362760_Golden.json")))
datasets["2022E"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8/Run3Summer22EENanoAODv12-forPOG_130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_Summer22EF"))

datasets["2022"] = []
#datasets["2022"].append(Dataset('/DoubleMuon/Run2022C-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_355828-356386_Golden_Run2022C.json")))
datasets["2022"].append(Dataset('/Muon/Run2022C-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraC_355862_357482_Golden.json")))
datasets["2022"].append(Dataset('/Muon/Run2022D-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraD_357538_357900_Golden.json")))
#datasets["2022"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8/Run3Summer22NanoAODv13-133X_mcRun3_2022_realistic_ForNanov13_v1-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_Summer22BCD"))
datasets["2022"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8/Run3Summer22NanoAODv12-forPOG_130X_mcRun3_2022_realistic_v5-v2/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50"))



def getDatasets(dataVersion):
    if len(dataVersion) == 0:
        alldatasets = []
        for key in datasets.keys():
            alldatasets.extend(datasets[key])
        return alldatasets
    if not dataVersion in datasets.keys():
        print("Unknown dataVersion",dataVersion,", dataVersions available",datasets.keys())
        sys.exit()
    return datasets[dataVersion]
