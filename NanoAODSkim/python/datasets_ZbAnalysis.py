import sys,os
JSONPATH = os.path.realpath(os.path.join(os.getcwd(),"../data"))

# The name of this file MUST contain the analysis name: datasets_<name>Analysis.py.
# E.g. NanoAOD_Hplus2taunuAnalysisSkim.py -> datasets_Hplus2taunuAnalysis.py
# lumijsons /eos/home-c/cmsdqm/www/CAF/certification/Collisions22
# https://cms-service-dqmdc.web.cern.ch/CAF/certification/Collisions24/DCSOnly_JSONS/dailyDCSOnlyJSON/Collisions24_13p6TeV_378981_379355_DCSOnly_TkPx.json
#/eos/home-c/cmsdqm/www/CAF/certification/Collisions24/Cert_Collisions2024_378981_379075_Golden.json

from Dataset import Dataset
"""
class Dataset:
    def __init__(self, url, dbs="global", dataVersion="94Xmc", lumiMask="", name=""):
        self.URL = url
        self.DBS = dbs
        self.dataVersion = dataVersion
        self.lumiMask = lumiMask
        self.name = name
        self.files = []

    def isData(self):
        if "Run20" in self.URL:
            return True
        return False

    def getName(self):
        return self.name

    def getYear(self):
        year_re = re.compile("/Run(?P<year>20\d\d)\S")
        match = year_re.search(self.URL)
        if match:
            return match.group("year")
        else:
            return None

    def setFiles(self, files):
        self.files = files

    def getFiles(self):
        return self.files
"""

datasets = {}


lumijson2024 = "Collisions24_13p6TeV_378981_386951_DCSOnly_TkPx.json"
datasets["2024"] = []

datasets["2024"].append(Dataset('/Muon0/Run2024C-2024CDEReprocessing-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_379415-380238_Golden_Run2024C.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024D-2024CDEReprocessing-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380255-380947_Golden_Run2024D.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024E-2024CDEReprocessing-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380956-381380_Golden_Run2024E1.json"), name="Muon0_Run2024Ev1_2024CDEReprocessing_v1"))
datasets["2024"].append(Dataset('/Muon0/Run2024E-2024CDEReprocessing-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381384-381591_Golden_Run2024E2.json"), name="Muon0_Run2024Ev2_2024CDEReprocessing_v1"))

datasets["2024"].append(Dataset('/Muon1/Run2024C-2024CDEReprocessing-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_379415-380238_Golden_Run2024C.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024D-2024CDEReprocessing-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380255-380947_Golden_Run2024D.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024E-2024CDEReprocessing-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380956-381380_Golden_Run2024E1.json"), name="Muon1_Run2024Ev1_2024CDEReprocessing_v1"))
datasets["2024"].append(Dataset('/Muon1/Run2024E-2024CDEReprocessing-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381384-381591_Golden_Run2024E2.json"), name="Muon1_Run2024Ev2_2024CDEReprocessing_v1"))


datasets["2024"].append(Dataset('/Muon0/Run2024B-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraB_Golden.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024B-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraB_Golden.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024C-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_379415-379866_Golden_Run2024C1.json"), name="Muon0_Run2024C_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon1/Run2024C-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_379415-379866_Golden_Run2024C1.json"), name="Muon1_Run2024C_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon0/Run2024C-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_379984-380238_Golden_Run2024C2.json"), name="Muon0_Run2024C_PromptReco_v2"))
datasets["2024"].append(Dataset('/Muon1/Run2024C-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_379984-380238_Golden_Run2024C2.json"), name="Muon1_Run2024C_PromptReco_v2"))
datasets["2024"].append(Dataset('/Muon0/Run2024D-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraD_Golden.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024D-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraD_Golden.json")))

datasets["2024"].append(Dataset('/Muon0/Run2024E-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380956-381380_Golden_Run2024E1.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024E-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380956-381380_Golden_Run2024E1.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024E-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381384-381591_Golden_Run2024E2.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024E-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381384-381591_Golden_Run2024E2.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381984-382262_Golden_Run2024F1.json"), name="Muon0_Run2024F_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon1/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381984-382262_Golden_Run2024F1.json"), name="Muon1_Run2024F_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon0/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_382298-383246_Golden_Run2024F2.json"), name="Muon0_Run2024F_PromptReco_v2"))
datasets["2024"].append(Dataset('/Muon1/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_382298-383246_Golden_Run2024F2.json"), name="Muon1_Run2024F_PromptReco_v2"))
datasets["2024"].append(Dataset('/Muon0/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_383247-383779_Golden_Run2024F3.json"), name="Muon0_Run2024F_PromptReco_v3"))
datasets["2024"].append(Dataset('/Muon1/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_383247-383779_Golden_Run2024F3.json"), name="Muon1_Run2024F_PromptReco_v3"))

datasets["2024"].append(Dataset('/Muon0/Run2024G-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_383811-384932_Golden_Run2024G1.json"), name="Muon0_Run2024G_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon1/Run2024G-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_383811-384932_Golden_Run2024G1.json"), name="Muon1_Run2024G_PromptReco_v1"))
datasets["2024"].append(Dataset('/Muon0/Run2024G-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_384933-385801_Golden_Run2024G2.json"), name="Muon0_Run2024G_PromptReco_v2"))
datasets["2024"].append(Dataset('/Muon1/Run2024G-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_384933-385801_Golden_Run2024G2.json"), name="Muon1_Run2024G_PromptReco_v2"))

datasets["2024"].append(Dataset('/Muon0/Run2024H-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_385836-385933_Golden_Run2024H.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024H-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_385836-385933_Golden_Run2024H.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024I-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_386423-386693_Golden_Run2024I1.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024I-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_386423-386693_Golden_Run2024I1.json")))
datasets["2024"].append(Dataset('/Muon0/Run2024I-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_386694-386974_Golden_Run2024I2.json")))
datasets["2024"].append(Dataset('/Muon1/Run2024I-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_386694-386974_Golden_Run2024I2.json")))

#datasets["2024"].append(Dataset('/EGamma0/Run2024B-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraB_Golden.json")))
#datasets["2024"].append(Dataset('/EGamma1/Run2024B-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraB_Golden.json")))
#datasets["2024"].append(Dataset('/EGamma0/Run2024C-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraC_Golden.json")))
#datasets["2024"].append(Dataset('/EGamma1/Run2024C-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraC_Golden.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024D-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraD_Golden.json")))
datasets["2024"].append(Dataset('/EGamma1/Run2024D-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraD_Golden.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024E-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380956-381380_Golden_Run2024E1.json")))
datasets["2024"].append(Dataset('/EGamma1/Run2024E-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_380956-381380_Golden_Run2024E1.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024E-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381384-381591_Golden_Run2024E2.json")))
datasets["2024"].append(Dataset('/EGamma1/Run2024E-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381384-381591_Golden_Run2024E2.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381984-383779_Golden_Run2024F.json")))
datasets["2024"].append(Dataset('/EGamma1/Run2024F-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381984-383779_Golden_Run2024F.json")))
#datasets["2024"].append(Dataset('/EGamma0/Run2024F-ECAL_CC_HCAL_DI_JMENano-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381984-383779_Golden_Run2024F.json")))
#datasets["2024"].append(Dataset('/EGamma0/Run2024F-ECAL_CC_HCAL_DI_JMENano-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381984-383779_Golden_Run2024F.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024F-ECAL_CC_HCAL_DI_JMENano-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_381984-383779_Golden_Run2024F.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024G-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_383811-385801_Golden_Run2024G.json")))
datasets["2024"].append(Dataset('/EGamma1/Run2024G-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_383811-385801_Golden_Run2024G.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024H-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_385836-385933_Golden_Run2024H.json")))
datasets["2024"].append(Dataset('/EGamma1/Run2024H-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_385836-385933_Golden_Run2024H.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024I-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_386423-386693_Golden_Run2024I1.json")))
datasets["2024"].append(Dataset('/EGamma1/Run2024I-PromptReco-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_386423-386693_Golden_Run2024I1.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024I-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,lumijson2024)))
datasets["2024"].append(Dataset('/EGamma1/Run2024I-PromptReco-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,lumijson2024)))

datasets["2024"].append(Dataset('/EGamma0/Run2024B-2024ECALRATIO_JMENano-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraB_Golden.json")))
datasets["2024"].append(Dataset('/EGamma1/Run2024B-2024ECALRATIO_JMENano-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraB_Golden.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024C-2024ECALRATIO_JMENano-v4/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraC_Golden.json")))
datasets["2024"].append(Dataset('/EGamma1/Run2024C-2024ECALRATIO_JMENano-v4/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraC_Golden.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024C-ECAL_CC_HCAL_DI_JMENano-v3/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraC_Golden.json")))
datasets["2024"].append(Dataset('/EGamma1/Run2024C-ECAL_CC_HCAL_DI_JMENano-v3/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraC_Golden.json")))
datasets["2024"].append(Dataset('/EGamma0/Run2024C-ECAL_R_HCAL_DI_JMENano-v3/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraC_Golden.json")))
datasets["2024"].append(Dataset('/EGamma1/Run2024C-ECAL_R_HCAL_DI_JMENano-v3/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2024_eraC_Golden.json")))
#datasets["2024"].append(Dataset('/DYTo2L_MLL-50_TuneCP5_13p6TeV_pythia8/Run3Winter24NanoAOD-KeepSi_133X_mcRun3_2024_realistic_v8-v2/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50"))
#datasets["2024"].append(Dataset('/DYto2L-4Jets_MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer23BPixNanoAODv12-JMENano12p5_132X_mcRun3_2023_realistic_postBPix_v4-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_postBPix"))
datasets["2024"].append(Dataset('/DYto2L-4Jets_MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Winter24NanoAOD-JMENanoV14_133X_mcRun3_2024_realistic_v10_ext2-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_Winter24"))
datasets["2024"].append(Dataset('/DYto2Mu-4Jets_Bin-MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/RunIII2024Summer24NanoAOD-140X_mcRun3_2024_realistic_v26-v2/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_Summer24"))
#datasets["2024"].append(Dataset('/DYto2L-4Jets_MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Winter24NanoAOD-JMENano_133X_mcRun3_2024_realistic_v10_ext1-v2/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_2024_JMENano_ext1_v2"))

datasets["2023"] = []
datasets["2023"].append(Dataset('/Muon0/Run2023B-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_366403-367079_Golden_Run2023B.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023B-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_366403-367079_Golden_Run2023B.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367094-367515_Golden_Run2023C1.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367094-367515_Golden_Run2023C1.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367516-367619_Golden_Run2023C2.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367516-367619_Golden_Run2023C2.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-22Sep2023_v3-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367661-367758_Golden_Run2023C3.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v3-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367661-367758_Golden_Run2023C3.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v4-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367770-369694_Golden_Run2023C4.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v4-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367770-369694_Golden_Run2023C4.json")))
datasets["2023"].append(Dataset('/EGamma0/Run2023B-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_366403-367079_Golden_Run2023B.json")))
datasets["2023"].append(Dataset('/EGamma1/Run2023B-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_366403-367079_Golden_Run2023B.json")))
datasets["2023"].append(Dataset('/EGamma0/Run2023C-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367094-367515_Golden_Run2023C1.json")))
datasets["2023"].append(Dataset('/EGamma1/Run2023C-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367094-367515_Golden_Run2023C1.json")))
datasets["2023"].append(Dataset('/EGamma0/Run2023C-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367516-367619_Golden_Run2023C2.json")))
datasets["2023"].append(Dataset('/EGamma1/Run2023C-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367516-367619_Golden_Run2023C2.json")))
datasets["2023"].append(Dataset('/EGamma0/Run2023C-22Sep2023_v3-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367661-367758_Golden_Run2023C3.json")))
datasets["2023"].append(Dataset('/EGamma1/Run2023C-22Sep2023_v3-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367661-367758_Golden_Run2023C3.json")))
datasets["2023"].append(Dataset('/EGamma0/Run2023C-22Sep2023_v4-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367770-369694_Golden_Run2023C4.json")))
datasets["2023"].append(Dataset('/EGamma1/Run2023C-22Sep2023_v4-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367770-369694_Golden_Run2023C4.json")))
datasets["2023"].append(Dataset('/DYto2L-4Jets_MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer23NanoAODv12-130X_mcRun3_2023_realistic_v14-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50"))
#datasets["2023"].append(Dataset('', dataVersion="mc"))

datasets["2023BPix"] = []
datasets["2023BPix"].append(Dataset('/Muon0/Run2023D-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_369844-370580_Golden_Run2023D1.json")))
datasets["2023BPix"].append(Dataset('/Muon1/Run2023D-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_369844-370580_Golden_Run2023D1.json")))
datasets["2023BPix"].append(Dataset('/Muon0/Run2023D-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_370616-371225_Golden_Run2023D2.json")))
datasets["2023BPix"].append(Dataset('/Muon1/Run2023D-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_370616-371225_Golden_Run2023D2.json")))
datasets["2023BPix"].append(Dataset('/EGamma0/Run2023D-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_369844-370580_Golden_Run2023D1.json")))
datasets["2023BPix"].append(Dataset('/EGamma1/Run2023D-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_369844-370580_Golden_Run2023D1.json")))
datasets["2023BPix"].append(Dataset('/EGamma0/Run2023D-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_370616-371225_Golden_Run2023D2.json")))
datasets["2023BPix"].append(Dataset('/EGamma1/Run2023D-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_370616-371225_Golden_Run2023D2.json")))
datasets["2023BPix"].append(Dataset('/DYto2L-4Jets_MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer23BPixNanoAODv12-JMENano12p5_132X_mcRun3_2023_realistic_postBPix_v4-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_postBPix"))
datasets["2023BPix"].append(Dataset('/TT_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer23BPixNanoAODv12-130X_mcRun3_2023_realistic_postBPix_v2-v2/NANOAODSIM', dataVersion="mc"))

datasets["2022E"] = []
datasets["2022E"].append(Dataset('/Muon/Run2022E-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraE_359022_360331_Golden.json")))
datasets["2022E"].append(Dataset('/Muon/Run2022F-22Sep2023-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraF_360390_362167_Golden.json")))
datasets["2022E"].append(Dataset('/Muon/Run2022G-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraG_362433_362760_Golden.json")))
datasets["2022E"].append(Dataset('/EGamma/Run2022E-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraE_359022_360331_Golden.json")))
datasets["2022E"].append(Dataset('/EGamma/Run2022F-22Sep2023-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraF_360390_362167_Golden.json")))
datasets["2022E"].append(Dataset('/EGamma/Run2022G-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraG_362433_362760_Golden.json")))
#datasets["2022E"].append(Dataset('/Muon/Run2022F-19Dec2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraF_360390_362167_Golden.json")))
#datasets["2022E"].append(Dataset('/Muon/Run2022G-19Dec2023-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraG_362433_362760_Golden.json")))
datasets["2022E"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8/Run3Summer22EENanoAODv12-forPOG_130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_Summer22EF"))

datasets["2022"] = []
#datasets["2022"].append(Dataset('/DoubleMuon/Run2022C-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_355828-356386_Golden_Run2022C.json")))
datasets["2022"].append(Dataset('/Muon/Run2022C-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraC_355862_357482_Golden.json")))
datasets["2022"].append(Dataset('/Muon/Run2022D-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraD_357538_357900_Golden.json")))
datasets["2022"].append(Dataset('/EGamma/Run2022C-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraC_355862_357482_Golden.json")))
datasets["2022"].append(Dataset('/EGamma/Run2022D-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraD_357538_357900_Golden.json")))
datasets["2022"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8/Run3Summer22NanoAODv13-133X_mcRun3_2022_realistic_ForNanov13_v1-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_Summer22BCD"))

datasets["2018UL"] = []
datasets["2018UL"].append(Dataset('/DoubleMuon/Run2018A-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_315257-316995_13TeV_Legacy2018_Collisions18_JSON_Run2018A.txt")))
datasets["2018UL"].append(Dataset('/DoubleMuon/Run2018B-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_317080-319310_13TeV_Legacy2018_Collisions18_JSON_Run2018B.txt")))
datasets["2018UL"].append(Dataset('/DoubleMuon/Run2018C-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_319337-320065_13TeV_Legacy2018_Collisions18_JSON_Run2018C.txt")))
#datasets["2018UL"].append(Dataset('/DoubleMuon/Run2018D-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_320413-325172_13TeV_Legacy2018_Collisions18_JSON_Run2018D.txt")))
datasets["2018UL"].append(Dataset('/DoubleMuon/Run2018D-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_320413-321500_13TeV_Legacy2018_Collisions18_JSON_Run2018D1.txt")))
datasets["2018UL"].append(Dataset('/DoubleMuon/Run2018D-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_321501-323000_13TeV_Legacy2018_Collisions18_JSON_Run2018D2.txt")))
datasets["2018UL"].append(Dataset('/DoubleMuon/Run2018D-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_323001-325172_13TeV_Legacy2018_Collisions18_JSON_Run2018D3.txt")))


datasets["2018UL"].append(Dataset('/EGamma/Run2018A-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_315257-316995_13TeV_Legacy2018_Collisions18_JSON_Run2018A.txt")))
datasets["2018UL"].append(Dataset('/EGamma/Run2018B-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_317080-319310_13TeV_Legacy2018_Collisions18_JSON_Run2018B.txt")))
datasets["2018UL"].append(Dataset('/EGamma/Run2018C-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_319337-320065_13TeV_Legacy2018_Collisions18_JSON_Run2018C.txt")))
#datasets["2018UL"].append(Dataset('/EGamma/Run2018D-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_320413-325172_13TeV_Legacy2018_Collisions18_JSON_Run2018D.txt")))
datasets["2018UL"].append(Dataset('/EGamma/Run2018D-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_320413-321500_13TeV_Legacy2018_Collisions18_JSON_Run2018D1.txt")))
datasets["2018UL"].append(Dataset('/EGamma/Run2018D-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_321501-323000_13TeV_Legacy2018_Collisions18_JSON_Run2018D2.txt")))
datasets["2018UL"].append(Dataset('/EGamma/Run2018D-UL2018_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_323001-325172_13TeV_Legacy2018_Collisions18_JSON_Run2018D3.txt")))


#datasets["2018UL"].append(Dataset('/SingleMuon/Run2018A-UL2018_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_315257-316995_13TeV_Legacy2018_Collisions18_JSON_Run2018A.txt")))
#datasets["2018UL"].append(Dataset('/SingleMuon/Run2018B-UL2018_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_317080-319310_13TeV_Legacy2018_Collisions18_JSON_Run2018B.txt")))
#datasets["2018UL"].append(Dataset('/SingleMuon/Run2018C-UL2018_MiniAODv2_NanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_319337-320065_13TeV_Legacy2018_Collisions18_JSON_Run2018C.txt")))
#datasets["2018UL"].append(Dataset('/SingleMuon/Run2018D-UL2018_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_320413-325172_13TeV_Legacy2018_Collisions18_JSON_Run2018D.txt")))


#datasets["2018UL"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18NanoAODv9-20UL18JMENano_Pilot_106X_upgrade2018_realistic_v16', dataVersion="mc"))
datasets["2018UL"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18NanoAODv9-20UL18JMENano_106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_LO"))
datasets["2018UL"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18NanoAODv9-20UL18JMENano_106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_NLO"))
datasets["2018UL"].append(Dataset('/DYJetsToLL_M-50_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL18NanoAODv9-20UL18JMENano_HerwigJetPartonBugFix_106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_herwig7"))
datasets["2018UL"].append(Dataset('/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18NanoAODv9-20UL18JMENano_106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM', dataVersion="mc"))

#2017

datasets["2017UL"] = []
datasets["2017UL"].append(Dataset('/DoubleMuon/Run2017B-UL2017_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_297050-299329_13TeV_UL2017_Collisions17_GoldenJSON_Run2017B.txt")))
datasets["2017UL"].append(Dataset('/DoubleMuon/Run2017C-UL2017_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_299368-302029_13TeV_UL2017_Collisions17_GoldenJSON_Run2017C.txt")))
datasets["2017UL"].append(Dataset('/DoubleMuon/Run2017D-UL2017_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_302031-302663_13TeV_UL2017_Collisions17_GoldenJSON_Run2017D.txt")))
datasets["2017UL"].append(Dataset('/DoubleMuon/Run2017E-UL2017_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_303824-304797_13TeV_UL2017_Collisions17_GoldenJSON_Run2017E.txt")))
datasets["2017UL"].append(Dataset('/DoubleMuon/Run2017F-UL2017_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_305040-306460_13TeV_UL2017_Collisions17_GoldenJSON_Run2017F.txt")))

datasets["2017UL"].append(Dataset('/DoubleEG/Run2017B-UL2017_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_297050-299329_13TeV_UL2017_Collisions17_GoldenJSON_Run2017B.txt")))
datasets["2017UL"].append(Dataset('/DoubleEG/Run2017C-UL2017_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_299368-302029_13TeV_UL2017_Collisions17_GoldenJSON_Run2017C.txt")))
datasets["2017UL"].append(Dataset('/DoubleEG/Run2017D-UL2017_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_302031-302663_13TeV_UL2017_Collisions17_GoldenJSON_Run2017D.txt")))
datasets["2017UL"].append(Dataset('/DoubleEG/Run2017E-UL2017_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_303824-304797_13TeV_UL2017_Collisions17_GoldenJSON_Run2017E.txt")))
datasets["2017UL"].append(Dataset('/DoubleEG/Run2017F-UL2017_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_305040-306460_13TeV_UL2017_Collisions17_GoldenJSON_Run2017F.txt")))

#datasets["2017UL"].append(Dataset('/SingleMuon/Run2017B-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_297050-299329_13TeV_UL2017_Collisions17_GoldenJSON_Run2017B.txt")))
#datasets["2017UL"].append(Dataset('/SingleMuon/Run2017C-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_299368-302029_13TeV_UL2017_Collisions17_GoldenJSON_Run2017C.txt")))
#datasets["2017UL"].append(Dataset('/SingleMuon/Run2017D-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_302031-302663_13TeV_UL2017_Collisions17_GoldenJSON_Run2017D.txt")))
#datasets["2017UL"].append(Dataset('/SingleMuon/Run2017E-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_303824-304797_13TeV_UL2017_Collisions17_GoldenJSON_Run2017E.txt")))
#datasets["2017UL"].append(Dataset('/SingleMuon/Run2017F-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_305040-306460_13TeV_UL2017_Collisions17_GoldenJSON_Run2017F.txt")))

#datasets["2017UL"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17NanoAODv9-Pilot_106X_mc2017_realistic_v9-v1/NANOAODSIM', dataVersion="mc"))                                                                 
datasets["2017UL"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL17NanoAODv9-20UL17JMENano_106X_mc2017_realistic_v9-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_LO"))
datasets["2017UL"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17NanoAODv9-20UL17JMENano_106X_mc2017_realistic_v9-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_NLO"))
datasets["2017UL"].append(Dataset('/DYJetsToLL_M-50_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL17NanoAODv9-20UL17JMENano_HerwigJetPartonBugFix_106X_mc2017_realistic_v9-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_herwig7"))
datasets["2017UL"].append(Dataset('/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17NanoAODv9-20UL17JMENano_106X_mc2017_realistic_v9-v1/NANOAODSIM', dataVersion="mc"))
#datasets["2017UL"].append(Dataset('/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer19UL17NanoAODv2-106X_mc2017_realistic_v8-v1/NANOAODSIM', dataVersion="mc"))

datasets["2017ULlowPU"] = []
#datasets["2017ULlowPU"].append(Dataset('/SingleMuon/Run2017H-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Custommix.txt")))
#datasets["2017ULlowPU"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL17NanoAODv2-FlatPU0to75_106X_mc2017_realistic_v8-v2/NANOAODSIM', dataVersion="mc"))                                                       
#datasets["2017ULlowPU"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v1/NANOAODSIM', dataVersion="mc"))
#datasets["2017ULlowPU"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9_ext1-v1/NANOAODSIM', dataVersion="mc"))
#datasets["2017ULlowPU"].append(Dataset('/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL17NanoAODv9-PUForMUOVal_106X_mc2017_realistic_v9-v1/NANOAODSIM', dataVersion="mc"))

# 2016                                                                                                                                                                                                                                           
datasets["2016ULAPV"] = []
#datasets["2016ULAPV"].append(Dataset('/DoubleMuon/Run2016B-ver1_HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_273150-275376_13TeV_Legacy2016_Collisions16_JSON_Run2016B.txt")))
datasets["2016ULAPV"].append(Dataset('/DoubleMuon/Run2016B-ver2_HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_273150-275376_13TeV_Legacy2016_Collisions16_JSON_Run2016B.txt")))
datasets["2016ULAPV"].append(Dataset('/DoubleMuon/Run2016C-HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_275656-276283_13TeV_Legacy2016_Collisions16_JSON_Run2016C.txt")))
datasets["2016ULAPV"].append(Dataset('/DoubleMuon/Run2016D-HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_276315-276811_13TeV_Legacy2016_Collisions16_JSON_Run2016D.txt")))
datasets["2016ULAPV"].append(Dataset('/DoubleMuon/Run2016E-HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_276831-277420_13TeV_Legacy2016_Collisions16_JSON_Run2016E.txt")))
datasets["2016ULAPV"].append(Dataset('/DoubleMuon/Run2016F-HIPM_UL2016_MiniAODv2_JMENanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_277932-278800_13TeV_Legacy2016_Collisions16_JSON_Run2016F_HIP.txt")))

#datasets["2016ULAPV"].append(Dataset('/DoubleEG/Run2016B-ver1_HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_273150-275376_13TeV_Legacy2016_Collisions16_JSON_Run2016B.txt")))
datasets["2016ULAPV"].append(Dataset('/DoubleEG/Run2016B-ver2_HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_273150-275376_13TeV_Legacy2016_Collisions16_JSON_Run2016B.txt")))
datasets["2016ULAPV"].append(Dataset('/DoubleEG/Run2016C-HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_275656-276283_13TeV_Legacy2016_Collisions16_JSON_Run2016C.txt")))
datasets["2016ULAPV"].append(Dataset('/DoubleEG/Run2016D-HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_276315-276811_13TeV_Legacy2016_Collisions16_JSON_Run2016D.txt")))
datasets["2016ULAPV"].append(Dataset('/DoubleEG/Run2016E-HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_276831-277420_13TeV_Legacy2016_Collisions16_JSON_Run2016E.txt")))
datasets["2016ULAPV"].append(Dataset('/DoubleEG/Run2016F-HIPM_UL2016_MiniAODv2_JMENanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_277932-278800_13TeV_Legacy2016_Collisions16_JSON_Run2016F_HIP.txt")))

#datasets["2016ULAPV"].append(Dataset('/SingleMuon/Run2016B-ver1_HIPM_UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_273150-275376_13TeV_Legacy2016_Collisions16_JSON_Run2016B.txt")))
#datasets["2016ULAPV"].append(Dataset('/SingleMuon/Run2016B-ver2_HIPM_UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_273150-275376_13TeV_Legacy2016_Collisions16_JSON_Run2016B.txt")))
#datasets["2016ULAPV"].append(Dataset('/SingleMuon/Run2016C-HIPM_UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_275656-276283_13TeV_Legacy2016_Collisions16_JSON_Run2016C.txt")))
#datasets["2016ULAPV"].append(Dataset('/SingleMuon/Run2016D-HIPM_UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_276315-276811_13TeV_Legacy2016_Collisions16_JSON_Run2016D.txt")))
#datasets["2016ULAPV"].append(Dataset('/SingleMuon/Run2016E-HIPM_UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_276831-277420_13TeV_Legacy2016_Collisions16_JSON_Run2016E.txt")))
#datasets["2016ULAPV"].append(Dataset('/SingleMuon/Run2016F-HIPM_UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_277932-278800_13TeV_Legacy2016_Collisions16_JSON_Run2016F_HIP.txt")))

datasets["2016ULAPV"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL16NanoAODAPVv9-20UL16APVJMENano_106X_mcRun2_asymptotic_preVFP_v11-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_LO"))
datasets["2016ULAPV"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL16NanoAODAPVv9-20UL16APVJMENano_106X_mcRun2_asymptotic_preVFP_v11-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_NLO"))
datasets["2016ULAPV"].append(Dataset('/DYJetsToLL_M-50_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL16NanoAODAPVv9-20UL16APVJMENano_HerwigJetPartonBugFix_106X_mcRun2_asymptotic_preVFP_v11-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_herwig7"))
datasets["2016ULAPV"].append(Dataset('/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL16NanoAODAPVv9-20UL16APVJMENano_106X_mcRun2_asymptotic_preVFP_v11-v1/NANOAODSIM', dataVersion="mc"))                                                                  

datasets["2016UL"] = []
datasets["2016UL"].append(Dataset('/DoubleMuon/Run2016F-UL2016_MiniAODv2_JMENanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_278801-278808_13TeV_Legacy2016_Collisions16_JSON_Run2016F_HIPfixed.txt")))
datasets["2016UL"].append(Dataset('/DoubleMuon/Run2016G-UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_278820-280385_13TeV_Legacy2016_Collisions16_JSON_Run2016G.txt")))
datasets["2016UL"].append(Dataset('/DoubleMuon/Run2016H-UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_281613-284044_13TeV_Legacy2016_Collisions16_JSON_Run2016H.txt")))

datasets["2016UL"].append(Dataset('/DoubleEG/Run2016F-UL2016_MiniAODv2_JMENanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_278801-278808_13TeV_Legacy2016_Collisions16_JSON_Run2016F_HIPfixed.txt")))
datasets["2016UL"].append(Dataset('/DoubleEG/Run2016G-UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_278820-280385_13TeV_Legacy2016_Collisions16_JSON_Run2016G.txt")))
datasets["2016UL"].append(Dataset('/DoubleEG/Run2016H-UL2016_MiniAODv2_JMENanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_281613-284044_13TeV_Legacy2016_Collisions16_JSON_Run2016H.txt")))

#datasets["2016UL"].append(Dataset('/SingleMuon/Run2016F-UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_278801-278808_13TeV_Legacy2016_Collisions16_JSON_Run2016F.txt")))
#datasets["2016UL"].append(Dataset('/SingleMuon/Run2016G-UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_278820-280385_13TeV_Legacy2016_Collisions16_JSON_Run2016G.txt")))
#datasets["2016UL"].append(Dataset('/SingleMuon/Run2016H-UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_281613-284044_13TeV_Legacy2016_Collisions16_JSON_Run2016H.txt")))

datasets["2016UL"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL16NanoAODv9-20UL16JMENano_106X_mcRun2_asymptotic_v17-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_LO"))
datasets["2016UL"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL16NanoAODv9-20UL16JMENano_106X_mcRun2_asymptotic_v17-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_NLO"))
datasets["2016UL"].append(Dataset('/DYJetsToLL_M-50_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL16NanoAODv9-20UL16JMENano_HerwigJetPartonBugFix_106X_mcRun2_asymptotic_v17-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_herwig7"))
datasets["2016UL"].append(Dataset('/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL16NanoAODv9-20UL16JMENano_106X_mcRun2_asymptotic_v17-v1/NANOAODSIM', dataVersion="mc"))

def getDatasets(dataVersion):
    if len(dataVersion) == 0:
        alldatasets = []
        for key in datasets.keys():
            alldatasets.extend(datasets[key])
        return alldatasets
    if not dataVersion in datasets.keys():
        print("Unknown dataVersion",dataVersion,", dataVersions available",datasets.keys())
        sys.exit()
    return datasets[dataVersion]
