#!/usr/bin/env python
import re

class Dataset:
    def __init__(self, url, dbs="global", dataVersion="94Xmc", lumiMask="", name=""):
        self.URL = url
        self.DBS = dbs
        self.dataVersion = dataVersion
        self.lumiMask = lumiMask
        self.name = name

    def isData(self):
        if "Run20" in self.URL:
            return True
        return False

    def getName(self):
        return self.name

    def getYear(self):
        year_re = re.compile("/Run(?P<year>20\d\d)\S")
        match = year_re.search(self.URL)
        if match:
            return match.group("year")
        else:
            return None

    def getRun(self):
        run_re = re.compile("/Run(?P<year>20\d\d\S)")
        match = run_re.search(self.URL)
        if match:
            return match.group("year")
        else:
            return None

    def setFiles(self, files):
        self.files = files

    def getFiles(self):
        return self.files
