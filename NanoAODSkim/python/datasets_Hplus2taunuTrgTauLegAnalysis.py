import sys,os
JSONPATH = os.path.realpath(os.path.join(os.getcwd(),"../data"))

# The name of this file MUST contain the analysis name: datasets_<name>Analysis.py.
# E.g. NanoAOD_Hplus2taunuAnalysisSkim.py -> datasets_Hplus2taunuAnalysis.py

from Dataset import Dataset


datasets = {}

datasets['2016ULAPV'] = []
datasets["2016ULAPV"].append(Dataset('/SingleMuon/Run2016B-ver2_HIPM_UL2016_MiniAODv2_NanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_273150-275376_13TeV_Legacy2016_Collisions16_JSON_Run2016B.txt")))
datasets["2016ULAPV"].append(Dataset('/SingleMuon/Run2016C-HIPM_UL2016_MiniAODv2_NanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_275656-276283_13TeV_Legacy2016_Collisions16_JSON_Run2016C.txt")))
datasets["2016ULAPV"].append(Dataset('/SingleMuon/Run2016D-HIPM_UL2016_MiniAODv2_NanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_276315-276811_13TeV_Legacy2016_Collisions16_JSON_Run2016D.txt")))
datasets["2016ULAPV"].append(Dataset('/SingleMuon/Run2016E-HIPM_UL2016_MiniAODv2_NanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_276831-277420_13TeV_Legacy2016_Collisions16_JSON_Run2016E.txt")))
datasets["2016ULAPV"].append(Dataset('/SingleMuon/Run2016F-HIPM_UL2016_MiniAODv2_NanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_277932-278800_13TeV_Legacy2016_Collisions16_JSON_Run2016F_HIP.txt")))

datasets["2016ULAPV"].append(Dataset('/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL16NanoAODAPVv9-106X_mcRun2_asymptotic_preVFP_v11-v1/NANOAODSIM'))

datasets["2016ULAPV"].append(Dataset('/ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIISummer20UL16NanoAODAPVv9-106X_mcRun2_asymptotic_preVFP_v11-v1/NANOAODSIM'))
datasets["2016ULAPV"].append(Dataset('/ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIISummer20UL16NanoAODAPVv9-106X_mcRun2_asymptotic_preVFP_v11-v1/NANOAODSIM'))
datasets["2016ULAPV"].append(Dataset('/ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL16NanoAODAPVv9-106X_mcRun2_asymptotic_preVFP_v11-v1/NANOAODSIM'))
datasets["2016ULAPV"].append(Dataset('/ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL16NanoAODAPVv9-106X_mcRun2_asymptotic_preVFP_v11-v1/NANOAODSIM'))

datasets["2016ULAPV"].append(Dataset('/WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL16NanoAODAPVv9-106X_mcRun2_asymptotic_preVFP_v11-v2/NANOAODSIM'))

datasets["2016ULAPV"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL16NanoAODAPVv9-106X_mcRun2_asymptotic_preVFP_v11-v1/NANOAODSIM'))


datasets['2016UL'] = []
datasets["2016UL"].append(Dataset('/SingleMuon/Run2016F-UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_278801-278808_13TeV_Legacy2016_Collisions16_JSON_Run2016F_HIPfixed.txt")))
datasets["2016UL"].append(Dataset('/SingleMuon/Run2016G-UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_278820-280385_13TeV_Legacy2016_Collisions16_JSON_Run2016G.txt")))
datasets["2016UL"].append(Dataset('/SingleMuon/Run2016H-UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_281613-284044_13TeV_Legacy2016_Collisions16_JSON_Run2016H.txt")))

datasets["2016UL"].append(Dataset('/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL16NanoAODv9-106X_mcRun2_asymptotic_v17-v1/NANOAODSIM'))

datasets["2016UL"].append(Dataset('/ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIISummer20UL16NanoAODv9-106X_mcRun2_asymptotic_v17-v1/NANOAODSIM'))
datasets["2016UL"].append(Dataset('/ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIISummer20UL16NanoAODv9-106X_mcRun2_asymptotic_v17-v1/NANOAODSIM'))
datasets["2016UL"].append(Dataset('/ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL16NanoAODv9-106X_mcRun2_asymptotic_v17-v2/NANOAODSIM'))
datasets["2016UL"].append(Dataset('/ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL16NanoAODv9-106X_mcRun2_asymptotic_v17-v2/NANOAODSIM'))

datasets["2016UL"].append(Dataset('/WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL16NanoAODv9-106X_mcRun2_asymptotic_v17-v2/NANOAODSIM'))

datasets["2016UL"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL16NanoAODv9-106X_mcRun2_asymptotic_v17-v1/NANOAODSIM'))


datasets['2017UL'] = []
datasets["2017UL"].append(Dataset('/SingleMuon/Run2017B-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_297050-299329_13TeV_UL2017_Collisions17_GoldenJSON_Run2017B.txt")))
datasets["2017UL"].append(Dataset('/SingleMuon/Run2017C-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_299368-302029_13TeV_UL2017_Collisions17_GoldenJSON_Run2017C.txt")))
datasets["2017UL"].append(Dataset('/SingleMuon/Run2017D-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_302031-302663_13TeV_UL2017_Collisions17_GoldenJSON_Run2017D.txt")))
datasets["2017UL"].append(Dataset('/SingleMuon/Run2017E-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_303824-304797_13TeV_UL2017_Collisions17_GoldenJSON_Run2017E.txt")))
datasets["2017UL"].append(Dataset('/SingleMuon/Run2017F-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_305040-306460_13TeV_UL2017_Collisions17_GoldenJSON_Run2017F.txt")))

datasets['2017UL'].append(Dataset('/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v1/NANOAODSIM'))

datasets['2017UL'].append(Dataset('/ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v1/NANOAODSIM'))
datasets['2017UL'].append(Dataset('/ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v1/NANOAODSIM'))
datasets['2017UL'].append(Dataset('/ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v2/NANOAODSIM'))
datasets['2017UL'].append(Dataset('/ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v2/NANOAODSIM'))

datasets['2017UL'].append(Dataset('/WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v2/NANOAODSIM'))

datasets['2017UL'].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v2/NANOAODSIM'))


datasets["2018UL"] = []
datasets["2018UL"].append(Dataset('/SingleMuon/Run2018A-UL2018_MiniAODv2_NanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_315257-316995_13TeV_Legacy2018_Collisions18_JSON_Run2018A.txt")))
datasets["2018UL"].append(Dataset('/SingleMuon/Run2018B-UL2018_MiniAODv2_NanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_317080-319310_13TeV_Legacy2018_Collisions18_JSON_Run2018B.txt")))
datasets["2018UL"].append(Dataset('/SingleMuon/Run2018C-UL2018_MiniAODv2_NanoAODv9-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_319337-320065_13TeV_Legacy2018_Collisions18_JSON_Run2018C.txt")))
datasets["2018UL"].append(Dataset('/SingleMuon/Run2018D-UL2018_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_320413-325172_13TeV_Legacy2018_Collisions18_JSON_Run2018D.txt")))

datasets['2018UL'].append(Dataset('/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM'))

datasets['2018UL'].append(Dataset('/ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM'))
datasets['2018UL'].append(Dataset('/ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM'))
datasets['2018UL'].append(Dataset('/ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM'))
datasets['2018UL'].append(Dataset('/ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM'))

datasets['2018UL'].append(Dataset('/WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM'))

datasets['2018UL'].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM'))


datasets["2022"] = []
datasets["2022"].append(Dataset('/Muon/Run2022C-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraC_355862_357482_Golden.json")))
datasets["2022"].append(Dataset('/Muon/Run2022D-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraD_357538_357900_Golden.json")))
datasets["2022"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8/Run3Summer22NanoAODv13-133X_mcRun3_2022_realistic_ForNanov13_v1-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_Summer22BCD"))

datasets["2022E"] = []
datasets["2022E"].append(Dataset('/Muon/Run2022E-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraE_359022_360331_Golden.json")))
datasets["2022E"].append(Dataset('/Muon/Run2022F-22Sep2023-v2/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraF_360390_362167_Golden.json")))
datasets["2022E"].append(Dataset('/Muon/Run2022G-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraG_362433_362760_Golden.json")))
datasets["2022E"].append(Dataset('/DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8/Run3Summer22EENanoAODv12-forPOG_130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_Summer22EF"))

datasets["2023"] = []
datasets["2023"].append(Dataset('/Muon0/Run2023B-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_366403-367079_Golden_Run2023B.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023B-22Sep2023-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_366403-367079_Golden_Run2023B.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367094-367515_Golden_Run2023C1.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367094-367515_Golden_Run2023C1.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367516-367619_Golden_Run2023C2.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367516-367619_Golden_Run2023C2.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-22Sep2023_v3-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367661-367758_Golden_Run2023C3.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v3-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367661-367758_Golden_Run2023C3.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-22Sep2023_v4-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367770-369694_Golden_Run2023C4.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-22Sep2023_v4-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_367770-369694_Golden_Run2023C4.json")))
datasets["2023"].append(Dataset('/DYto2L-4Jets_MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer23NanoAODv12-130X_mcRun3_2023_realistic_v14-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50"))

datasets["2023BPix"] = []
datasets["2023BPix"].append(Dataset('/Muon0/Run2023D-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_369844-370580_Golden_Run2023D1.json")))
datasets["2023BPix"].append(Dataset('/Muon1/Run2023D-22Sep2023_v1-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_369844-370580_Golden_Run2023D1.json")))
datasets["2023BPix"].append(Dataset('/Muon0/Run2023D-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_370616-371225_Golden_Run2023D2.json")))
datasets["2023BPix"].append(Dataset('/Muon1/Run2023D-22Sep2023_v2-v1/NANOAOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"Cert_Collisions2023_370616-371225_Golden_Run2023D2.json")))
datasets["2023BPix"].append(Dataset('/DYto2L-4Jets_MLL-50_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer23BPixNanoAODv12-JMENano12p5_132X_mcRun3_2023_realistic_postBPix_v4-v1/NANOAODSIM', dataVersion="mc", name="DYJetsToLL_M_50_postBPix"))
datasets["2023BPix"].append(Dataset('/TT_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer23BPixNanoAODv12-130X_mcRun3_2023_realistic_postBPix_v2-v2/NANOAODSIM', dataVersion="mc"))


def getDatasets(dataVersion):
    if not dataVersion in datasets.keys():
        print("Unknown dataVersion",dataVersion,", dataVersions available",datasets.keys())
        sys.exit()
    return datasets[dataVersion]
