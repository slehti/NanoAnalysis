#!/usr/bin/env python2

# Needs the EOS multicrab dir as an argument. Made in lxplus with a script multicrabCreateFromEOS.py
# ### Usage:   multicrabGetFromEOS.py <eos-multicrab dir>
# S.Lehti 25.8.2022

import os
import sys
import re
import subprocess
import json

from optparse import OptionParser

STORAGE_ELEMENT_PATH = "gsiftp://madhatter.csc.fi/pnfs/csc.fi/data/cms/store/"
GRIDLS = "arcls"

def usage():
    print
    print("### Usage:   ",os.path.basename(sys.argv[0])," <multicrab dir>")
    print
    sys.exit()

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    f = p.stdout
    ret=[]
    for line in f:
        line = line.decode('utf-8')
        #sys.stdout.write(line)
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def getMulticrabDirName(path):
    if not os.path.exists(path):
        print("Path",path,"does not exist")
        sys.exit()
    multicrab_re = re.compile("(?P<basedir>.+)(?P<mg>multicrab(_|-)\S+?)(/|$)")
    match = multicrab_re.search(os.path.abspath(path))
    #match = multicrab_re.search(path)
    if match:
        if ' ' in match.group("basedir"):
            return "./",match.group("mg")
        return match.group("basedir"),match.group("mg")
    else:
        usage()

def getCrabdirs(multicrabdir,paths):
    if os.path.abspath(paths[0]) == os.path.abspath(multicrabdir):
        paths = execute("ls %s"%multicrabdir)

    cleanedPaths = []
    for p in paths:
        if p[len(p)-1:] == "/":
            cleanedPaths.append(p[:len(p)-1])
        else:
            cleanedPaths.append(p) 

    crabdirs = []
    cands = execute("ls %s"%multicrabdir)
    for c in cands:
        if c not in cleanedPaths:
            continue
        cp = os.path.join(multicrabdir,c)
        if os.path.isdir(cp) and os.path.exists(os.path.join(cp,"results")):
            crabdirs.append(c)
    return crabdirs

def findRootFiles(path,opts):
    root_re = re.compile("(?P<rootfile>(\S+\.root))")
    files = []
    ls_cmd = "ls"
    if opts.madhatter:
        ls_cmd = GRIDLS
    subpaths = execute("%s %s"%(ls_cmd,path))
    for sp in subpaths:
        if sp == "log":
            continue
        match = root_re.search(sp)
        if match:
            files.append(os.path.join(path,sp))
        else:
            files.extend(findRootFiles(os.path.join(path,sp),opts))
    return files

def retrieve(paths,savedir,opts):
    cdir = os.path.basename(os.path.dirname(savedir))
    for path in paths:
        rootfiles = findRootFiles(path,opts)
        if len(rootfiles) == 0:
            continue

        if opts.list or opts.madhatter:
            fOUT = os.path.join(savedir,"files_%s.json"%cdir)
            if not os.path.exists(fOUT):
                dict = {}
                #root://eoscms.cern.ch
                dict['files'] = list(map(lambda x : x.replace("/eos/cms/store/","root://eoscms.cern.ch//eos/cms/store/"), rootfiles))
                if opts.madhatter:
                    dict['files'] = list(map(lambda x : x.replace("gsiftp://","root://"), rootfiles))

                f = open(fOUT, "wb")
                json.dump(dict, f, sort_keys=True, indent=2)
                f.close()
                sys.stdout.write('    Listed files in %s\n'%os.path.basename(fOUT))
            else:
                sys.stdout.write('    List file already found: %s\n'%os.path.basename(fOUT))
            return

        for f in rootfiles:
            forig = os.path.join(path,f)
            fnew = os.path.join(savedir,f.replace('tree_','events_'))
            if not os.path.exists(fnew):
                cmd = "cp %s %s"%(forig,fnew)
                os.system(cmd)
                print(fnew)
    return

def main(opts, args):

    pIN = []
    if len(sys.argv) == 1:
        pIN.append(os.getcwd())
    else:
        pIN.extend(sys.argv[1:])

    basedir,multicrab = getMulticrabDirName(pIN[0])
    crabdirs  = getCrabdirs(os.path.join(basedir,multicrab),pIN)

    print(multicrab)

    for cdir in crabdirs:
        fIN = open(os.path.join(multicrab,"eosConfig_%s.txt"%cdir))
        eoscfg = json.load(fIN)
        retrievePaths = eoscfg["eospath"]
        if opts.madhatter:
            path_re = re.compile("/eos/cms/store/(?P<path>\S+)")
            newpaths = []
            for p in retrievePaths:
                match = path_re.search(p)
                if match:
                    newpaths.append(os.path.join(STORAGE_ELEMENT_PATH,match.group("path")))
            retrievePaths = newpaths
        retrieve(retrievePaths,os.path.join(basedir,multicrab,cdir,"results"),opts)

        
if __name__ == "__main__":
    parser = OptionParser(usage="Usage: %prog [options]")

    parser.add_option("--list", dest="list", default=False, action="store_true",
                      help="Write a list of root files in text files [defaut: False]")
    parser.add_option("--madhatter", dest="madhatter", default=False, action="store_true",
                      help="Write a list of root files from madhatter [defaut: False]")
    (opts, args) = parser.parse_args()

    main(opts, args)

