#!/usr/bin/env python3

import os,sys,re
import subprocess
import json

import ROOT

from optparse import OptionParser
#from collections import OrderedDict

#from CRABClient.UserUtilities import setConsoleLogLevel
#from CRABClient.UserUtilities import getUsernameFromCRIC
#from CRABClient.ClientUtilities import LOGLEVEL_MUTE
#from CRABClient.UserUtilities import getConsoleLogLevel
from CRABAPI.RawCommand import crabCommand

from lumicalc import isData
from pileup import sumPU
from multicrabGetFromMadhatter import proxy,getMulticrabDirName,getCrabdirs,GetIncludeExcludeDatasets,getSEPath

USER = os.environ['USER']
#STORAGE_ELEMENT_PATH = "https://hip-cms-se.csc.fi:2880/store/user/%s/CRAB3_TransferData"%(USER)
#STORAGE_ELEMENT_PATH = "root://hip-cms-se.csc.fi/store/user/%s/CRAB3_TransferData"%(USER)
#STORAGE_ELEMENT_PATH = "gsiftp://madhatter.csc.fi/pnfs/csc.fi/data/cms//store/user/%s/CRAB3_TransferData/"%(USER)
#STORAGE_ELEMENT_PATH = "gsiftp://madhatter.csc.fi/pnfs/csc.fi/data/cms/store/group/local/Higgs/CRAB3_TransferData/"
PU_JSON_PATH = "../../NanoAODAnalysis/Framework/src/nanoanalysis/data/pileup"

USEARC = True

PILEUPBINS = 100

if USEARC:
    GRIDCOPY  = "arccp"
    GRIDLS    = "arcls"
    GRIDPROXY = "arcproxy --info"
else:
    GRIDCOPY  = "srmcp"
    GRIDLS    = "srmls"
    GRIDPROXY = "grid-proxy-info"

#from multicrab import execute
sumlumi = 0
lumidata = {}

def usage():
    print
    print("### Usage:   ",os.path.basename(sys.argv[0])," <multicrab dir|crab dir>")
    print
    sys.exit()

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    f = p.stdout
    ret=[]
    for line in f:
        line = line.decode('utf-8')
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def crab_report(taskdir):
    if not os.path.exists(os.path.join(taskdir,"results","processedLumis.json")):
        cmd_report = "crab report %s"%taskdir
        os.system(cmd_report)

def lumi(multicrabdir,taskdir):

    if opts.updatelumi:
        multicrab_re = re.compile("multicrab_(?P<analysis>\S+Analysis)_v\S+?_Run(?P<year>\d\d\d\d)")
        match = multicrab_re.search(multicrabdir)
        analysis = ""
        year = ""
        if match:
            analysis = match.group("analysis")
            year = match.group("year")

            datasetpath = os.path.join(os.environ['CMSSW_BASE'],"src/NanoAnalysis/NanoAODSkim/python")
            sys.path.append(datasetpath)

            import importlib
            datasets_module = "datasets_"+analysis #datasets_python.replace(".py","")
            analysisDatasets = importlib.import_module(datasets_module)
            datasets = analysisDatasets.getDatasets(year)
            from multicrab import GetRequestName
            lumimask = ""
            for dataset in datasets:
                if not dataset.isData():
                    continue
                name = dataset.name
                if name == "":
                    name = GetRequestName(dataset)
                if name == taskdir:
                    lumimask = dataset.lumiMask
                    break

            if len(lumimask) > 0:
                cmd_rm = "rm -f %s"%(os.path.join(multicrabdir,taskdir,'inputs','*.json'))
                cmd_cp = "cp %s %s"%(lumimask,os.path.join(multicrabdir,taskdir,'inputs'))
                os.system(cmd_rm)
                os.system(cmd_cp)
                #print(cmd_rm)
                #print(cmd_cp)


    from lumicalc import CallBrilcalc
    if isData(taskdir):
        print("    Calculating integrated luminosity")

        #multicrab_ZbAnalysis_v1255p1_Run2023BC_20230612T1617/
        #multicrab_re = re.compile("(?P<multicrab>multicrab_\S+Analysis_v\S+?)/(?P<taskdir>\S+?/)")
        #match = multicrab_re.search(taskdir)
        #if match:
        #    multicrabdir = match.group("multicrab")
        #    d  = match.group("taskdir")

        lumijson = os.path.join(multicrabdir,"lumi.json")

        if os.path.exists(lumijson):
            fIN = open(lumijson)
            lumidata.update(json.load(fIN))
            fIN.close()
            if taskdir in lumidata.keys():
                print("    - lumi already calculated, skipping")
                return

        inputFile = os.path.join(multicrabdir,taskdir,"results","processedLumis.json")
        labelIN = "ProcessedLumis"
        if not os.path.exists(inputFile):
            cmd = "ls %s"%(os.path.join(multicrabdir,taskdir,'inputs','*.json'))
            inputFile = execute(cmd)[0]
            labelIN = "Lumimask"
        fOUT = os.path.join(multicrabdir,taskdir,"results","brilcalc.log")
        lumidata[taskdir] = CallBrilcalc(inputFile,fOUT)
        global sumlumi
        sumlumi += lumidata[taskdir]
        lumijson = os.path.join(multicrabdir,"lumi.json")
        f = open(lumijson, "w")
        json.dump(lumidata, f, sort_keys=True, indent=2)
        f.close()
        print("    Lumi for",taskdir,lumidata[taskdir],"input=%s"%labelIN)
        if lumidata[taskdir] < 0:
            print("    \033[91m Integrated lumi < 0, problem with file %s\033[0m"%inputFile)
            sys.exit()

def pileup(multicrabdir,taskdir,opts):
    if not isData(taskdir):
        return

    print("    Calculating pileup for",taskdir)

    calcMode       = "true"
    maxPileupBin   = "%s"%PILEUPBINS
    numPileupBins  = "%s"%PILEUPBINS
    pileupHistName = "pileup"
    PileUpJSON_2016 = "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/PileUp/UltraLegacy/pileup_latest.txt"
    PileUpJSON_2017 = "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/PileUp/UltraLegacy/pileup_latest.txt"
    PileUpJSON_2018 = "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PileUp/UltraLegacy/pileup_latest.txt"
    PileUpJSON_2022 = "/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/PileUp/BCDEFG/pileup_JSON.txt"
    PileUpJSON_2023 = "/eos/user/c/cmsdqm/www/CAF/certification/Collisions23/PileUp/BCD/pileup_JSON.txt"
    PileUpJSON_2024 = PU_JSON_PATH + "/pileup_latest_2024.txt"

    NormTagJSON_Run2 = "/cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_PHYSICS.json"
    NormTagJSON_Run3 = "/cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_BRIL.json"

    pupath = os.path.join(os.environ['CMSSW_BASE'],"src/NanoAnalysis/NanoAODSkim/python")

    from pileup import CallPileupCalc,getYear

    fOUT = os.path.join(multicrabdir,taskdir,"results","PileUp.root")
    if os.path.exists(fOUT) and not opts.overwrite:
        print("    - pileup already calculated, skipping")
        return

    inputFile = os.path.join(multicrabdir,taskdir,"results","processedLumis.json")
    # crab report not working for nAOD yet, using processedLumis only if exists
    if not os.path.exists(inputFile):
        cmd = "ls %s"%(os.path.join(multicrabdir,taskdir,'inputs','*Collisions*.*'))
        inputFile = execute(cmd)[0]

#    fOUT_target = os.path.join(multicrabdir,taskdir,"results","PileUp.root")
#    fOUT = fOUT_target
    # Recommended minimum bias xsection
    minBiasXsecNominal = 69200 #from https://twiki.cern.ch/twiki/bin/viewauth/CMS/POGRecipesICHEP2016
    NormTagJSON = NormTagJSON_Run2
    run = "Run2"
    puUncert    = 0.05

    year = getYear(multicrabdir)
    if year == "2016":
        PileUpJSON = PileUpJSON_2016
    if year == "2017":
        PileUpJSON = PileUpJSON_2017
    if year == "2018":
        PileUpJSON = PileUpJSON_2018
    if year == "2022":
        minBiasXsecNominal = 80000
        PileUpJSON = PileUpJSON_2022
        NormTagJSON = NormTagJSON_Run3
        run = "Run3"
    if year == "2023":
        minBiasXsecNominal = 80000
        PileUpJSON = PileUpJSON_2023
        NormTagJSON = NormTagJSON_Run3
        run = "Run3"
    if year == "2024":
        minBiasXsecNominal = 75300
        PileUpJSON = PileUpJSON_2024
        NormTagJSON = NormTagJSON_Run3
        run = "Run3"

    inputLumiJSON = PileUpJSON

    #if len(opts.hltpath) > 0:
    #    pOUT = os.path.join(multicrabdir,taskdir,"results")
    #    hltJSON = CallPileupCalc_HLTpaths(inputFile, opts.hltpath, NormTagJSON, inputLumiJSON, pOUT, run)
    #    inputLumiJSON = hltJSON
    #    fOUT = os.path.join(pOUT,"PileUp_%s.root"%opts.hltpath)

    hName = pileupHistName
    minBiasXsec = minBiasXsecNominal
    CallPileupCalc(inputFile,inputLumiJSON,minBiasXsec,fOUT,hName)

    minBiasXsec_up = minBiasXsec*(1+puUncert)
    fOUT_up        = fOUT.replace(".root","_up.root")
    hName_up       = pileupHistName+"_up"
    CallPileupCalc(inputFile,inputLumiJSON,minBiasXsec_up,fOUT_up,hName_up)

    minBiasXsec_down = minBiasXsec*(1-puUncert)
    fOUT_down        = fOUT.replace(".root","_down.root")
    hName_down       = pileupHistName+"_down"
    CallPileupCalc(inputFile,inputLumiJSON,minBiasXsec_down,fOUT_down,hName_down)

    hadd_cmd = "hadd -v 0 pu.root %s %s %s && mv pu.root %s"%(fOUT,fOUT_up,fOUT_down,fOUT)
    #if len(opts.hltpath) > 0:
    #    hadd_cmd = "hadd -a %s %s %s %s"%(fOUT_target,fOUT,fOUT_up,fOUT_down)
    #print(hadd_cmd)
    os.system(hadd_cmd)

def pileup_mc(multicrabdir,taskdir,opts):
    if isData(taskdir):
        return

    print("    Calculating pileup for",taskdir)
    files = []
    jsonfile = os.path.join(multicrabdir,taskdir,"results","files_%s.json"%taskdir)
    #print("check jsonfile",jsonfile)
    if os.path.exists(jsonfile):
        f = open(jsonfile)
        data = json.loads(f.read())
        files.extend(data['files'])
        f.close()
    else:
        cands = execute("ls %s"%os.path.join(multicrabdir,taskdir,"results"))
        root_re = re.compile("(?P<rootfile>([^/]*events_\d+\.root))")
        for c in cands:
            match = root_re.search(c)
            if match:
                files.append(os.path.join(multicrabdir,taskdir,"results",match.group("rootfile")))
    if len(files) == 0:
        print("Warning, empty dataset",taskdir)
    #print("check files",files)
    #from pileup_mc import Dataset,getDatasets,getRun

    pileup_template = ROOT.TH1F("pileup","",PILEUPBINS,0,PILEUPBINS)

    filepath = os.path.join(multicrabdir,taskdir,"results","PileUp.root")
    if os.path.exists(filepath) and not opts.overwrite:
        print("    - %s: Pileup file already exists"%taskdir)
        return

    tchain = ROOT.TChain("Events")
    tchain.SetCacheSize(10000000)

    for f in files:
        tchain.Add(f)

    if tchain.GetEntries() == 0:
        return

    pileup_template.Reset()
    tchain.Draw("Pileup_nTrueInt>>pileup", ROOT.TCut(""), "goff")
    print("     - Pileup distribution with",pileup_template.GetEntries(),"entries, integral",pileup_template.Integral())

    fOUT = ROOT.TFile.Open(filepath,"RECREATE")
    fOUT.cd()
    pileup_template.Write()
    fOUT.Close()
    #print(filepath)

def create_filelist(multicrabdir,taskdir,opts):

    from multicrabGetFromMadhatter import getSEPath,findRootFiles,restart_line,retrieve

    storagePath = getSEPath(multicrabdir)

    retrievePaths = {}

    if os.path.exists(os.path.join(multicrabdir,taskdir,"results","files_%s.json"%taskdir)):
        print("    - files list already created, skipping")
        return

    sys.stdout.write("    Scanning files at the SE: %s\n"%taskdir)
    gsipath = os.path.join(storagePath,multicrabdir)
    # print(gsipath)

    cands = execute("%s %s"%(GRIDLS,gsipath))
    for ddir in cands:
        cands2 = execute("%s %s"%(GRIDLS,os.path.join(gsipath,ddir)))
        for edir in cands2:
            if "crab_"+taskdir.strip("/") == edir.strip("/"):
                retrievePaths[taskdir] = os.path.join(gsipath,ddir,edir)
    if taskdir in retrievePaths.keys():
         retrieve(retrievePaths[taskdir],os.path.join(multicrabdir,taskdir,"results"),opts)
    else:
        print("\033[35m%s not retrieved\033[0m"%taskdir)

def das_json(multicrab,taskdir,opts):
    crab_cfg = os.path.join(multicrab,"crabConfig_%s.py"%taskdir)
    cmd_grep = "grep inputDataset %s"%(crab_cfg)
    dataset_grep = execute(cmd_grep)[0]
    dataset_re = re.compile("'(?P<dataset>\S+)'")
    dataset_match = dataset_re.search(dataset_grep)
    if dataset_match:
        datasetname = dataset_match.group("dataset")
        cmd = 'dasgoclient --query="dataset=%s" -json'%datasetname
        cand = execute(cmd)
        das_json = ""
        for x in cand:
            if 'nevents' in x:
                das_json = x
        das_json = das_json.rstrip(',')
        das_json = das_json.rstrip(' ')
        #print("check3 >%s<"%das_json)
        with open(os.path.join(multicrab,taskdir,"results","das.json"),"w") as fOUT:
            fOUT.write(das_json)

        name_re = re.compile('"name":"(?P<name>\S+?),"')
        name_match = name_re.search(das_json)
        nevents_re = re.compile('"nevents":(?P<nevents>\d+),')
        nevents_match = nevents_re.search(das_json)
        if name_match and nevents_match:
            name = name_match.group("name")
            nevents = nevents_match.group("nevents")
            print("Dataset",name,", DAS nevents=",nevents)


def main(opts, args):
    if len(sys.argv) < 2:
        usage()

    proxy()

    pIN = []
    if len(sys.argv) == 1:
        pIN.append(os.getcwd())
    else:
        pIN.extend(args)

    opts.includeTasks = opts.includeTasks.split(',')
    opts.excludeTasks = opts.excludeTasks.split(',')
    if 'None' in opts.includeTasks:
        opts.includeTasks = opts.includeTasks.remove('None')
    if 'None' in opts.excludeTasks:
        opts.excludeTasks = opts.excludeTasks.remove('None')

    basedir,multicrab = getMulticrabDirName(pIN[0])
    crabdirs  = getCrabdirs(os.path.join(basedir,multicrab),pIN)
    taskdirs = GetIncludeExcludeDatasets(crabdirs,opts)

    storagePath = getSEPath(os.path.join(basedir,multicrab))

    for taskdir in taskdirs:
        print("\033[34m\nProcessing taskdir %s\033[0m"%taskdir)
        #crab_report(taskdir) # create processedLumis.json

        if opts.lumi or opts.updatelumi:
            lumi(multicrab,taskdir)
        if opts.pileup:
            pileup(multicrab,taskdir,opts)
        if opts.files:
            create_filelist(multicrab,taskdir,opts)
        if opts.pileupmc:
            pileup_mc(multicrab,taskdir,opts)
        if opts.das_json:
            das_json(multicrab,taskdir,opts)

    if opts.pileup:
        sumPU(multicrab)
    if opts.lumi:
        print("Sum lumi",sumlumi)

if __name__=="__main__":
    parser = OptionParser(usage="Usage: %prog [options]")

    parser.add_option("--copy", dest="copy", default=False, action="store_true",
                      help="Copy root files from T2 to local disk [defaut: False]")
    parser.add_option("-i", "--includeTasks", dest="includeTasks", default="None", type="string",
                      help="Only perform action for this dataset(s) [default: \"\"]")
    parser.add_option("-e", "--excludeTasks", dest="excludeTasks", default="None", type="string",
                      help="Exclude this dataset(s) from action [default: \"\"]")
    parser.add_option("--overwrite", dest="overwrite", default=False, action="store_true",
                      help="Overwrite the pileup distributions [default: False")
    parser.add_option("--lumi", dest="lumi", default=False, action="store_true",
                      help="Process lumi only [default: False")
    parser.add_option("--updatelumi", dest="updatelumi", default=False, action="store_true",
                      help="Copy the lumijsons from datasets definition to multicrab and process lumi only [default: False")
    parser.add_option("--pileup", dest="pileup", default=False, action="store_true",
                      help="Process pileup only [default: False")
    parser.add_option("--files", dest="files", default=False, action="store_true",
                      help="Load filelists only [default: False")
    parser.add_option("--pileupmc", dest="pileupmc", default=False, action="store_true",
                      help="Process MC pileup only [default: False")
    parser.add_option("--das", dest="das_json", default=False, action="store_true",
                      help="Store DAS nevents into main counter [default: False")

    (opts, args) = parser.parse_args()
    if not (opts.lumi or opts.updatelumi or opts.pileup or opts.files or opts.pileupmc or opts.das_json):
        opts.lumi     = True
        opts.pileup   = True
        opts.files    = True
        opts.pileupmc = True
        opts.das_json = True

    main(opts, args)
