#!/usr/bin/env python

import sys,os,re
import subprocess

def usage():
    print
    print("### Usage:   ",os.path.basename(sys.argv[0])," <multicrab dir|crab dir>")
    print
    sys.exit()

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    f = p.stdout
    ret=[]
    for line in f:
        line = line.decode('utf-8')
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

#files_Muon1_Run2023D_PromptReco_v1.json

def main():
    files = []
    if os.path.isfile(sys.argv[1]):
        files.append(sys.argv[1])
    else:
        multicrabdirs = sys.argv[1:]
        for multicrabdir in multicrabdirs:
            files.extend(execute("find %s -name 'files_*.json'"%multicrabdir))
    for f in files:
        print(f)
        if not os.path.exists(f+'bak'):
            copy_cmd = "cp %s %s"%(f,f+'bak')
            os.system(copy_cmd)
        cmd = "sed -i 's/root:\/\/madhatter.csc.fi\/pnfs\/csc.fi\/data\/cms\//root:\/\/hip-cms-se.csc.fi:1094/' %s"%f
        os.system(cmd)

main()
