#!/usr/bin/env python
import os
import sys
import math
import PSet
import re

from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import * 

#this takes care of converting the input files from CRAB
from PhysicsTools.NanoAODTools.postprocessing.framework.crabhelper import inputFiles,runsAndLumis

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

import ROOT

from array import array

#ANALYSISNAME = sys.argv[0][9:-7]
ANALYSISNAME = "SingleMuonTriggerAnalysis"

class Counter():
    def __init__(self,name):
        self.name  = name
        self.count = 0
        
    def increment(self):
        self.count += 1
        
    def Print(self):
        self.name += " "
        while len(self.name) < 39:
            self.name += "."
        print(self.name,self.count)

class Skim(Module):
    def __init__(self):
        self.cControl       = Counter("Skim: control")
        self.cControl.increment()
        self.cAllEvents     = Counter("Skim: All events")
        self.cTrigger       = Counter("Skim: Trigger selection")
        self.cPassedEvents  = Counter("Skim: Passed events")

        self.objs = []


    def __del__(self):
        self.cAllEvents.Print()
        self.cTrigger.Print()
        self.cPassedEvents.Print()

    def beginJob(self):

        self.h_pileup = ROOT.TH1F('pileup','',200,0,200)
        self.addObject(self.h_pileup)

        self.h_skimcounter = ROOT.TH1F("SkimCounter","",4,0,4)
        self.addObject(self.h_skimcounter)


    def endJob(self):
        pass

    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.dir = outputFile.mkdir("configInfo")


    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):

        outputFile.cd()

        self.dir.cd()
        self.h_pileup.Write()

        
        self.h_skimcounter.SetBinContent(1,self.cControl.count)
        self.h_skimcounter.GetXaxis().SetBinLabel(1,self.cControl.name)
        self.h_skimcounter.SetBinContent(2,self.cAllEvents.count)
        self.h_skimcounter.GetXaxis().SetBinLabel(2,self.cAllEvents.name)
        self.h_skimcounter.SetBinContent(3,self.cTrigger.count)
        self.h_skimcounter.GetXaxis().SetBinLabel(3,self.cTrigger.name)
        self.h_skimcounter.SetBinContent(4,self.cPassedEvents.count)
        self.h_skimcounter.GetXaxis().SetBinLabel(4,self.cPassedEvents.name)
        self.h_skimcounter.Write()


    def analyze(self, event):

        if event._tree.GetListOfBranches().FindObject("Pileup_nTrueInt"):
            self.h_pileup.Fill(event.Pileup_nTrueInt)
        
        self.cAllEvents.increment()
        
        # selection
        triggerDecision = False
        if hasattr(event._tree, 'HLT_IsoMu24_eta2p1'):
            triggerDecision = triggerDecision or event.HLT_IsoMu24_eta2p1
        if hasattr(event._tree, 'HLT_IsoMu27'):
            triggerDecision = triggerDecision or event.HLT_IsoMu27


        #if not triggerDecision:
            #return False
        self.cTrigger.increment()

        self.cPassedEvents.increment()

        return True

if __name__ == "__main__":
    SkimModule = lambda : Skim()

    files=["root://xrootd-cms.infn.it//store/data/Run2024I/Muon0/NANOAOD/PromptReco-v1/000/386/673/00000/28043d9b-42d7-4d6e-a9ed-d79c23892b83.root"]

    #p=PostProcessor(".",inputFiles(),"",modules=[SkimModule()],provenance=True,fwkJobReport=True,haddFileName="events.root")

    if 'HOSTNAME' in os.environ and "lxplus" in os.environ['HOSTNAME']:
        print("Running local files",files)
        p=PostProcessor(".",files,"",modules=[SkimModule()],provenance=True,fwkJobReport=True,haddFileName="events.root",maxEntries=20000)
    else:
        print("Running crab inputFiles")
        p=PostProcessor(".",inputFiles(),"",modules=[SkimModule()],provenance=True,fwkJobReport=True,haddFileName="events.root")

    p.run()

    os.system("mv *_Skim.root events.root")

    print("DONE")
