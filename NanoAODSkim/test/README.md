################################################
# NanoAOD skimming with trigger bit only
# 10.12.2019/SLehti
# Updated 29.10.2024/akossi
################################################

## CMSSW working area

Skimming can be performed on scram arch `el8` or `el9`, but the post-processing
step cannot be run from a singularity os unless nordugrid arc binaries are mounted
manually. The warning about the production os for CMSSW_14_0_7 being different
from the used arch can be ignored.

`scram p CMSSW CMSSW_14_0_7`\
`cd CMSSW_14_0_7/src`\
`cmsenv`\
`git clone ssh://git@gitlab.cern.ch:7999/${USER}/NanoAnalysis.git`\
`cd NanoAnalysis/NanoAODSkim/test`

Note that skimming is performed using NanoAODTools, which starting from CMSSW_13_3_0
is [maintained within CMSSW](https://github.com/cms-nanoAOD/nanoAOD-tools),
removing the need to set it up manually for the environment.

## How to make your own skim

Copy test/NanoAOD_*AnalysisSkim.py for your analysis name
and change the triggers in the code.

Copy python/datasets_*Analysis.py for your analysis name
and change the datasets in the file.

## How to submit

`../scripts/multicrab.py NanoAOD_*AnalysisSkim.py 2017UL`\
`../scripts/multicrab.py NanoAOD_Hpp2ditauAnalysisSkim.py 2017UL`

## Monitoring

Once the `multicrab.py` script has created your result directory with all the tasks:

```
cd <path/to/multicrab/directory>
path/to/scripts/multicrab.py [--status|--resubmit|--log|--get]
```

## Post-processing

After DATA is 100% complete:

`../scripts/multicrab_postprocess.py <multicrabdir>`
