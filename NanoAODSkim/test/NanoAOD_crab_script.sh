#this is not meant to be run locally
#
echo "START "$0
echo Check if TTY
if [ "`tty`" != "not a tty" ]; then
  echo "YOU SHOULD NOT RUN THIS IN INTERACTIVE, IT DELETES YOUR LOCAL FILES"
else

ls -lR .
echo "ENV..................................."
env
echo "VOMS"
voms-proxy-info -all
echo "CMSSW BASE, python path, pwd"
echo $CMSSW_BASE
echo $PYTHON_PATH
echo $PWD

echo Found Proxy in: $X509_USER_PROXY
python3 NanoAOD_*AnalysisSkim.py $1
ls -lt
fi
