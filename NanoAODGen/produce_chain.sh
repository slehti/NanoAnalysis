#!/bin/sh

# Default generator values

# The gridpack path must be changed, the default value is more of an example
gridpack="/eos/user/a/akossi/work/genproductions/bin/MadGraph5_aMCatNLO/ChargedHiggs_taunu_heavy_NLO_M500_el8_amd64_gcc10_CMSSW_12_4_8_tarball.tar.xz"

# The generator fragment must be provided, an example for H+ -> tau nu is included in this directory
fragment=""

# simulation configuration, the default is for Summer23 Run3_2023 campaign
conditions="130X_mcRun3_2023_realistic_v15"
beamspot="Realistic25ns13p6TeVEarly2023Collision"
pileup="dbs:/Neutrino_E-10_gun/Run3Summer21PrePremix-Summer23_130X_mcRun3_2023_realistic_v13-v1/PREMIX"
era="Run3_2023"

# number of generated events
num_events=500

# CMSSW release information, this must match the version used in the campaign!
sw="CMSSW_13_0_14"
scram_arch="el8_amd64_gcc11"

SEED="123456"

# Parse arguments
while [[ $# -gt 0 ]]; do
  case "$1" in
    -a|--arch)
      scram_arch="$2"
      shift 2
      ;;
    -c|--conditions)
      conditions="$2"
      shift 2
      ;;
    -f|--fragment)
      fragment="$2"
      shift 2
      ;;
    -g|--gridpack)
      gridpack="$2"
      shift 2
      ;;
    -n|--num-events)
      num_events="$2"
      shift 2
      ;;
    -e|--era)
      era="$2"
      shift 2
      ;;
    -s|--sw)
      sw="$2"
      shift 2
      ;;
    -b|--beamspot)
      beamspot="$2"
      shift 2
      ;;
    *)
      echo "Unknown option: $1"
      exit 1
      ;;
  esac
done

fragment_path="Configuration/Genproduction/python"
fragment_name=$(basename "$fragment")

cat <<EndOfTestFile > NanoAODchain.sh
#!/bin/bash

export SCRAM_ARCH="$scram_arch"

mkdir -p "$fragment_path"
cp "$fragment" "$fragment_path"
if [ -r $sw/src ] ; then
  echo release $sw already exists
else
  scram p CMSSW $sw
fi
cd "${sw}/src/"
eval \`scram runtime -sh\`
mv ../../Configuration .
scram b
cd -

# GEN-SIM
if [ -r LHEGS.root ] ; then
    echo "found existing gen-sim file, continuing to pileup premixing"
else
    cmsDriver.py ${fragment_path}/${fragment_name} --python_file LHEGS_cfg.py \\
                 --eventcontent RAWSIM,LHE --customise Configuration/DataProcessing/Utils.addMonitoring \\
                 --datatier GEN-SIM,LHE --fileout file:LHEGS.root --conditions $conditions \\
                 --beamspot $beamspot --customise_commands process.RandomNumberGeneratorService.externalLHEProducer.initialSeed="${SEED}" \\
                 --step LHE,GEN,SIM --geometry DB:Extended --era $era --no_exec --mc -n $num_events || exit $? ;
    cmsRun -e -j LHEGS_report.xml LHEGS_cfg.py
fi

# premix
if [ -r Premix_0.root ] ; then
    echo "found existing premix, continuing to HLT"
else
    cmsDriver.py --python_file Premix_cfg.py \\
                 --eventcontent PREMIXRAW --customise Configuration/DataProcessing/Utils.addMonitoring \\
                 --datatier GEN-SIM-RAW --fileout file:Premix_0.root --pileup_input $pileup --filein "file:LHEGS.root" \\
                 --conditions $conditions --step DIGI,DATAMIX,L1,DIGI2RAW,HLT:2023v12 --procModifiers premix_stage2 \\
                 --beamspot $beamspot --geometry DB:Extended --era $era --datamix PreMix --no_exec --mc -n $num_events || exit $? ;
    cmsRun -e -j Premix_report.xml Premix_cfg.py
fi

# digi, reco
if [ -r Premix.root ] ; then
    echo "found existing reco file, continuing to NanoAOD"
else
    cmsDriver.py  --python_filename Premix_cfg_2.py --eventcontent AODSIM \\
                  --customise Configuration/DataProcessing/Utils.addMonitoring --datatier AODSIM \\
                  --fileout file:Premix.root --conditions $conditions\\
                  --step RAW2DIGI,L1Reco,RECO,RECOSIM --geometry DB:Extended --filein file:Premix_0.root \\
                  --era $era --no_exec --mc -n $num_events || exit $? ;
    cmsRun -e -j Premix_report_2.xml Premix_cfg_2.py
fi

# Nano
cmsDriver.py  --python_filename NanoAOD_cfg.py --eventcontent NANOEDMAODSIM \\
              --customise Configuration/DataProcessing/Utils.addMonitoring --datatier NANOAODSIM \\
              --fileout file:NanoAOD.root --conditions $conditions --step PAT,NANO --scenario pp \\
              --filein file:Premix.root --era $era --no_exec --mc -n $num_events
cmsRun -e -j NanoAOD_report.xml NanoAOD_cfg.py

EndOfTestFile
chmod +x NanoAODchain.sh

export SINGULARITY_CACHEDIR="/tmp/$(whoami)/singularity"
export APPTAINER_BINDPATH='/afs,/cvmfs,/cvmfs/grid.cern.ch/etc/grid-security:/etc/grid-security,/eos,/etc/pki/ca-trust,/run/user,/var/run/user'
arch="${scram_arch:0:3}"

cmssw-$arch -- $(pwd)/NanoAODchain.sh


