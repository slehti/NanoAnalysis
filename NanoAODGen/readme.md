# Private MC sample production with MadGraph5_aMC@NLO

## Disclaimer

This directory contains a collection of scripts/commands possibly useful for MC sample production

No guarantees can be made as of yet for an out-of-the-box experience, as the commands need
to be adapted for different processes, generators, data eras etc.

The scripts are a work in progress, and they can be used to generate Charged Higgs datasets
with Summer23 campaign configuration directly.

## How to generate gridpacks

[The CMS MC generator group](https://twiki.cern.ch/twiki/bin/view/CMS/GeneratorMain#How_to_produce_gridpacks) twiki has resources for different generators

See the [MG5](https://twiki.cern.ch/twiki/bin/view/CMS/QuickGuideMadGraph5aMCatNLO) guide, read carefully which project branch to checkout and the section on [quick gridpack production](https://twiki.cern.ch/twiki/bin/view/CMS/QuickGuideMadGraph5aMCatNLO#Quick_tutorial_on_how_to_produce). After running `gridpack_generation.sh`, your working directory should include the produced gridpack as a tarball. You can test to make sure your gridpack is working by unpacking the tarball and running the contained `runcmsgrid.sh` script. For producing samples, this gridpack must be listed in the generator fragment as the external LHE producer.

## Generator fragments

General information is found in the [twiki page](https://twiki.cern.ch/twiki/bin/view/CMS/GitRepositoryForGenProduction), including instructions on how to get a new fragment included in the repository. Ready fragments are available in the [genproductions repository](https://github.com/cms-sw/genproductions/tree/master/genfragments). The generator fragment is the input in the first step of event production using CMSSW.


## Private production guide and resources

Commands used to generate existing datasets can be seen in [McM](https://cms-pdmv-prod.web.cern.ch/mcm/). For a specific request, a script including all the commands necessary to run the production step is obtained from the "get test" button. For example: [TTto4Q-2Jets Summer23](https://cms-pdmv-prod.web.cern.ch/mcm/public/restapi/requests/get_test/GEN-Run3Summer23wmLHEGS-00277). A simpler interface for finding datasets and their McM requests is provided by [GrASP](https://cms-pdmv-prod.web.cern.ch/grasp/).

The key commands in the test script are cmsDriver.py and cmsRun. cmsDriver creates the configuration file (also called PSet) that cmsRun reads and runs the specified processing steps. For successive steps, the output root file created in the previous runCms command is usually used as the input.

### Difficulties you may run into

The cmssw environment used for the commands is important as it specifies which detector and beam configurations are available. Make sure you are running the production commands on the correct versions of
1. operating system (el7, el8 or el9)
2. scram_arch (for example el8_amd64_gcc11)
3. CMSSW version

For a given production era, you can check the used versions from the test script of an existing and up-to-date dataset from GrASP.

Additionally, you should not produce your gridpacks using an architecture that is newer than the architecture used for cmsRun. Ideally the architectures are the same, but older gridpacks should run without issues on newer architectures.

### The scripts
`produce_chain.sh` runs all steps from LHE generation to NanoAOD sequentially, and all code is executed locally.
`crab_stepN.sh` are scripts for generating larger samples, and accomplish sequentially the same steps from `produce_chain.sh`. The storage site for intermediate steps cannot be cernbox when you intend to use the output dataset as input for the following step, as CMS dataset discovery does not support cernbox storage.

See the source code of the scripts for simple documentation at the top of the files for available arguments, which specify the number of events, the storage site for results, the production campaign, CMSSW versioning etc.

