import FWCore.ParameterSet.Config as cms

# link to card:
# https://github.com/cms-sw/genproductions/blob/52c3fd916eee84e5fa502c4e86ab607fa4f6f910/bin/MadGraph5_aMCatNLO/cards/production/2017/13TeV/ChargedHiggs_taunu

# The args argument must be changed to point to your gridpack. Ideally this is should be stored in cvmfs
externalLHEProducer = cms.EDProducer("ExternalLHEProducer",
    args = cms.vstring('/eos/user/a/akossi/work/genproductions/bin/MadGraph5_aMCatNLO/ChargedHiggs_taunu_heavy_NLO_M500_el8_amd64_gcc10_CMSSW_12_4_8_tarball.tar.xz'),
    nEvents = cms.untracked.uint32(5000),
    numberOfParameters = cms.uint32(1),
    outputFile = cms.string('cmsgrid_final.lhe'),
    scriptName = cms.FileInPath('GeneratorInterface/LHEInterface/data/run_generic_tarball_cvmfs.sh')
)

from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunesRun3ECM13p6TeV.PythiaCP5Settings_cfi import *
from Configuration.Generator.Pythia8aMCatNLOSettings_cfi import *
from Configuration.Generator.PSweightsPythia.PythiaPSweightsSettings_cfi import *

generator = cms.EDFilter("Pythia8ConcurrentHadronizerFilter",
    maxEventsToPrint = cms.untracked.int32(1),
    pythiaPylistVerbosity = cms.untracked.int32(1),
    filterEfficiency = cms.untracked.double(1.0),
    pythiaHepMCVerbosity = cms.untracked.bool(False),
    comEnergy = cms.double(13600.),
    PythiaParameters = cms.PSet(
        pythia8CommonSettingsBlock,
        pythia8CP5SettingsBlock,
        pythia8aMCatNLOSettingsBlock,
        pythia8PSweightsSettingsBlock,
        processParameters = cms.vstring(
            'TimeShower:nPartonsInBorn = 2', #number of coloured particles (before resonance decays) in born matrix element
            'Higgs:useBSM = on', ## enable handling of bsm particles
            'SLHA:minMassSM = 100.', # as above
            '37:mayDecay = on', ## decay of the ch higgs
            '37:onMode = off',
            '37:onIfAny = 15',
            '37:doForceWidth = on' ## make it with a width
        ),
        parameterSets = cms.vstring('pythia8CommonSettings',
                                    'pythia8CP5Settings',
                                    'pythia8aMCatNLOSettings',
                                    'pythia8PSweightsSettings',
                                    'processParameters',
                                    )
    )
)
