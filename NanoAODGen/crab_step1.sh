#!/bin/sh

# Default generator values, the gridpack must match the generator fragment
gridpack="ChargedHiggs_taunu_heavy_NLO_M200_el8_amd64_gcc10_CMSSW_12_4_8_tarball.tar.xz"

# default sim values
conditions="130X_mcRun3_2023_realistic_v15"
beamspot="Realistic25ns13p6TeVEarly2023Collision"
pileup="dbs:/Neutrino_E-10_gun/Run3Summer21PrePremix-Summer23_130X_mcRun3_2023_realistic_v13-v1/PREMIX"
era="Run3_2023"

# number of generated events
num_events=100000
ev_per_job=200

# Primary name for the published dataset
primary_name="Hp2taunu_Run3Summer23"
info="Run3_2023"

# CMSSW release information
sw="CMSSW_13_0_14"
scram_arch="el8_amd64_gcc11"

SEED="123456"
publication="False"
storage="T3_CH_CERNBOX"

# Parse arguments
while [[ $# -gt 0 ]]; do
  case "$1" in
    -p|--publication)
      publication="True"
      shift 1
      ;;
    -a|--arch)
      scram_arch="$2"
      shift 2
      ;;
    -c|--conditions)
      conditions="$2"
      shift 2
      ;;
    -g|--gridpack)
      gridpack="$2"
      shift 2
      ;;
    -i|--info)
      info="$2"
      shift 2
      ;;
    -n|--num-events)
      num_events="$2"
      shift 2
      ;;
    -e|--era)
      era="$2"
      shift 2
      ;;
    --storage)
      storage="$2"
      shift 2
      ;;
    -s|--sw)
      sw="$2"
      shift 2
      ;;
    -b|--beamspot)
      beamspot="$2"
      shift 2
      ;;
    *)
      echo "Unknown option: $1"
      exit 1
      ;;
  esac
done


fragment_path="Configuration/Genproduction/python"
mkdir -p "$fragment_path"
fragment_name=$(basename "$gridpack" .tar.xz)-fragment.py

request_name="${primary_name}GEN_${info}"

# For different gen settings, the fragment needs to be customized.
cat <<EndOfFragmentFile > ${fragment_path}/${fragment_name}
import FWCore.ParameterSet.Config as cms

# link to card:
# https://github.com/cms-sw/genproductions/blob/52c3fd916eee84e5fa502c4e86ab607fa4f6f910/bin/MadGraph5_aMCatNLO/cards/production/2017/13TeV/ChargedHiggs_taunu

externalLHEProducer = cms.EDProducer("ExternalLHEProducer",
    args = cms.vstring('/srv/$(basename "$gridpack")'),
    nEvents = cms.untracked.uint32(5000),
    numberOfParameters = cms.uint32(1),
    outputFile = cms.string('cmsgrid_final.lhe'),
    scriptName = cms.FileInPath('GeneratorInterface/LHEInterface/data/run_generic_tarball_cvmfs.sh')
)

from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunesRun3ECM13p6TeV.PythiaCP5Settings_cfi import *
from Configuration.Generator.Pythia8aMCatNLOSettings_cfi import *
from Configuration.Generator.PSweightsPythia.PythiaPSweightsSettings_cfi import *

generator = cms.EDFilter("Pythia8ConcurrentHadronizerFilter",
    maxEventsToPrint = cms.untracked.int32(1),
    pythiaPylistVerbosity = cms.untracked.int32(1),
    filterEfficiency = cms.untracked.double(1.0),
    pythiaHepMCVerbosity = cms.untracked.bool(False),
    comEnergy = cms.double(13600.),
    PythiaParameters = cms.PSet(
        pythia8CommonSettingsBlock,
        pythia8CP5SettingsBlock,
        pythia8aMCatNLOSettingsBlock,
        pythia8PSweightsSettingsBlock,
        processParameters = cms.vstring(
            'TimeShower:nPartonsInBorn = 2', #number of coloured particles (before resonance decays) in born matrix element
            'Higgs:useBSM = on', ## enable handling of bsm particles
            'SLHA:minMassSM = 100.', # as above
            '37:mayDecay = on', ## decay of the ch higgs
            '37:onMode = off',
            '37:onIfAny = 15',
            '37:doForceWidth = on' ## make it with a width
        ),
        parameterSets = cms.vstring('pythia8CommonSettings',
                                    'pythia8CP5Settings',
                                    'pythia8aMCatNLOSettings',
                                    'pythia8PSweightsSettings',
                                    'processParameters',
                                    )
    )
)

EndOfFragmentFile


cat <<EndOfTestFile > CRAB_GEN.sh
#!/bin/bash

export SCRAM_ARCH="$scram_arch"

if [ -r $sw/src ] ; then
  echo release $sw already exists
else
  scram p CMSSW $sw
fi
cd "${sw}/src/"
eval \`scram runtime -sh\`
rm -r ./Configuration
mv ../../Configuration .
scram b
cd -

# GEN-SIM
cmsDriver.py ${fragment_path}/${fragment_name} --python_file ${primary_name}GEN_${info}_cfg.py \\
         --eventcontent RAWSIM --customise Configuration/DataProcessing/Utils.addMonitoring \\
         --datatier GEN-SIM --fileout file:${primary_name}GEN_${info}.root --conditions $conditions \\
         --beamspot $beamspot --customise_commands process.RandomNumberGeneratorService.externalLHEProducer.initialSeed="${SEED}" \\
         --step LHE,GEN,SIM --geometry DB:Extended --era $era --no_exec --mc -n $num_events || exit $? ;
crab submit -c CRAB_LHE_cfg.py || exit $? ;

# # premix
# if [ -r Premix_0.root ] ; then
#     echo "found existing premix, continuing to HLT"
# else
#     cmsDriver.py --python_file Premix_cfg.py \\
#                  --eventcontent PREMIXRAW --customise Configuration/DataProcessing/Utils.addMonitoring \\
#                  --datatier GEN-SIM-RAW --fileout file:Premix_0.root --pileup_input $pileup --filein "file:LHEGS.root" \\
#                  --conditions $conditions --step DIGI,DATAMIX,L1,DIGI2RAW,HLT:2023v12 --procModifiers premix_stage2 \\
#                  --beamspot $beamspot --geometry DB:Extended --era $era --datamix PreMix --no_exec --mc -n $num_events || exit $? ;
#     cmsRun -e -j Premix_report.xml Premix_cfg.py
# fi
#
# # digi, reco
# if [ -r Premix.root ] ; then
#     echo "found existing reco file, continuing to NanoAOD"
# else
#     cmsDriver.py  --python_filename Premix_cfg_2.py --eventcontent AODSIM \\
#                   --customise Configuration/DataProcessing/Utils.addMonitoring --datatier AODSIM \\
#                   --fileout file:Premix.root --conditions $conditions\\
#                   --step RAW2DIGI,L1Reco,RECO,RECOSIM --geometry DB:Extended --filein file:Premix_0.root \\
#                   --era $era --no_exec --mc -n $num_events || exit $? ;
#     cmsRun -e -j Premix_report_2.xml Premix_cfg_2.py
# fi
#
# # Nano
# cmsDriver.py  --python_filename NanoAOD_cfg.py --eventcontent NANOEDMAODSIM \\
#               --customise Configuration/DataProcessing/Utils.addMonitoring --datatier NANOAODSIM \\
#               --fileout file:NanoAOD.root --conditions $conditions --step PAT,NANO --scenario pp \\
#               --filein file:Premix.root --era $era --no_exec --mc -n $num_events
# cmsRun -e -j NanoAOD_report.xml NanoAOD_cfg.py

EndOfTestFile
chmod +x CRAB_GEN.sh

# Define crab configuration file for LHE step
cat<<EndOfCRABLHECfg > 'CRAB_LHE_cfg.py'
# Import needed modules
from WMCore.Configuration import Configuration

# General settings
config = Configuration()
config.section_("General")
config.General.requestName = '$request_name'
config.General.transferOutputs = True

# JobType settings
config.section_("JobType")
config.JobType.pluginName = 'PrivateMC'
config.JobType.psetName = '${primary_name}GEN_${info}_cfg.py'
config.JobType.outputFiles = ['${primary_name}GEN_${info}.root']
config.JobType.eventsPerLumi = 1000
config.JobType.allowUndistributedCMSSW = True

config.JobType.inputFiles = ['${gridpack}']
config.JobType.maxMemoryMB = 3000

# Data settings
config.section_("Data")
config.Data.splitting = 'EventBased'
config.Data.unitsPerJob = $ev_per_job
config.Data.totalUnits = $num_events
config.Data.outLFNDirBase = '/store/user/${USER}/CRAB3_TransferData'

config.Data.outputPrimaryDataset = '${primary_name}'
config.Data.publication = ${publication}
config.Data.publishDBS = 'https://cmsweb.cern.ch/dbs/prod/phys03/DBSWriter/'
config.Data.outputDatasetTag = config.General.requestName
# config.Data.ignoreLocality = True

config.section_("Site")
#config.Site.storageSite = 'T2_FI_HIP'
#config.Site.storageSite = 'T3_CH_CERNBOX'
config.Site.storageSite = '${storage}'
#config.Site.whitelist = ['T2_CH_*', 'T2_DE_*', 'T2_US_*']
#config.Site.blacklist = ['T2_FI_HIP']
#config.Site.whitelist = ['T2_FI_HIP']

EndOfCRABLHECfg

export SINGULARITY_CACHEDIR="/tmp/$(whoami)/singularity"
export APPTAINER_BINDPATH='/afs,/cvmfs,/cvmfs/grid.cern.ch/etc/grid-security:/etc/grid-security,/eos,/etc/pki/ca-trust,/run/user,/var/run/user'
arch="${scram_arch:0:3}"
if [ -e "/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/${arch}:amd64" ]; then
  CONTAINER_NAME="${arch}:amd64"
elif [ -e "/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/${arch}:x86_64" ]; then
  CONTAINER_NAME="${arch}:x86_64"
fi

cmssw-$arch -- $(pwd)/CRAB_GEN.sh


