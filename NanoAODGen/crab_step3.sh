#!/bin/sh

# input dataset name
inputdataset=""

# default sim values
conditions="130X_mcRun3_2023_realistic_v15"
beamspot="Realistic25ns13p6TeVEarly2023Collision"
era="Run3_2023"

# number of generated events
nfiles=1000
files_per_job=200

# Identify this dataset in DAS
primary_name="Hp2taunu_Run3Summer23"
info="Run3_2023"

# CMSSW release information
sw="CMSSW_13_0_14"
scram_arch="el8_amd64_gcc11"

SEED="123456"
publication="False"
storage="T3_CH_CERNBOX"

# Parse arguments
while [[ $# -gt 0 ]]; do
  case "$1" in
    -p|--publication)
      publication="True"
      shift 1
      ;;
    -i|--info)
      info="$2"
      shift 2
      ;;
    -a|--arch)
      scram_arch="$2"
      shift 2
      ;;
    -c|--conditions)
      conditions="$2"
      shift 2
      ;;
    -d|--inputdataset)
      inputdataset="$2"
      shift 2
      ;;
    -n|--nfiles)
      nfiles="$2"
      shift 2
      ;;
    -f|--files-per-job)
      files_per_job="$2"
      shift 2
      ;;
    -e|--era)
      era="$2"
      shift 2
      ;;
    --storage)
      storage="$2"
      shift 2
      ;;
    -s|--sw)
      sw="$2"
      shift 2
      ;;
    -b|--beamspot)
      beamspot="$2"
      shift 2
      ;;
    *)
      echo "Unknown option: $1"
      exit 1
      ;;
  esac
done

request_name="${primary_name}NanoAOD_${info}"

cat <<EndOfTestFile > CRAB_NANO.sh
#!/bin/bash

export SCRAM_ARCH="$scram_arch"

if [ -r $sw/src ] ; then
  echo release $sw already exists
else
  scram p CMSSW $sw
fi
cd "${sw}/src/"
eval \`scram runtime -sh\`
rm -r ./Configuration
mv ../../Configuration .
scram b
cd -

# Mini, Nano
cmsDriver.py  --python_filename ${primary_name}NanoAOD_${info}_cfg.py --eventcontent NANOAODSIM \\
              --customise Configuration/DataProcessing/Utils.addMonitoring --datatier NANOAODSIM \\
              --fileout file:${primary_name}NanoAOD_${info}.root --conditions $conditions \\
              --step PAT,NANO --scenario pp --filein dbs:$inputdataset --era $era --no_exec --mc -n 1
crab submit -c CRAB_NanoAOD_cfg.py || exit $? ;

EndOfTestFile
chmod +x CRAB_NANO.sh

# Define crab configuration file
cat<<EndOfCRABNanoCfg > 'CRAB_NanoAOD_cfg.py'
# Import needed modules
from WMCore.Configuration import Configuration

# General settings
config = Configuration()
config.section_("General")
config.General.requestName = '$request_name'
config.General.transferOutputs = True

# JobType settings
config.section_("JobType")
config.JobType.pluginName = 'Analysis'
config.JobType.psetName = '${primary_name}NanoAOD_${info}_cfg.py'
config.JobType.outputFiles = ['${primary_name}NanoAOD_${info}.root']
config.JobType.allowUndistributedCMSSW = True
# config.JobType.maxMemoryMB = 5000

# Data settings
config.section_("Data")
config.Data.inputDataset = '${inputdataset}'
config.Data.inputDBS = 'phys03'
config.Data.splitting = 'FileBased'
config.Data.unitsPerJob = $files_per_job
config.Data.totalUnits = $nfiles
config.Data.outLFNDirBase = '/store/user/${USER}/CRAB3_TransferData'

config.Data.publication = ${publication}
config.Data.publishDBS = 'https://cmsweb.cern.ch/dbs/prod/phys03/DBSWriter/'
config.Data.outputDatasetTag = config.General.requestName

config.section_("Site")
#config.Site.storageSite = 'T2_FI_HIP'
#config.Site.storageSite = 'T3_CH_CERNBOX'
config.Site.storageSite = '${storage}'
#config.Site.whitelist = ['T2_CH_*', 'T2_DE_*', 'T2_US_*']
#config.Site.blacklist = ['T2_FI_HIP']
#config.Site.whitelist = ['T2_FI_HIP']

EndOfCRABNanoCfg

export SINGULARITY_CACHEDIR="/tmp/$(whoami)/singularity"
export APPTAINER_BINDPATH='/afs,/cvmfs,/cvmfs/grid.cern.ch/etc/grid-security:/etc/grid-security,/eos,/etc/pki/ca-trust,/run/user,/var/run/user'
arch="${scram_arch:0:3}"
if [ -e "/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/${arch}:amd64" ]; then
  CONTAINER_NAME="${arch}:amd64"
elif [ -e "/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/${arch}:x86_64" ]; then
  CONTAINER_NAME="${arch}:x86_64"
fi

cmssw-$arch -- $(pwd)/CRAB_NANO.sh

